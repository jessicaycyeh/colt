/*!
 * \file    parallel.cc
 *
 * \brief   MPI and OpenMP parallel computing framework.
 *
 * \details This file contains code related to the general parallel computing
 *          framework, including setup for the MPI and OpenMP configurations.
 */

#include "proto.h"
#include <random>

/*! \brief Setup for MPI and OpenMP parallelization.
 *
 *  \param[in] argc Number of arguments passed to the program.
 *  \param[in] argv Full list of arguments passed to the program.
 */
void setup_parallelism(int argc, char** argv) {
  // Start up MPI checking support for thread serialization
  int provided;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  if (provided != MPI_THREAD_MULTIPLE)
    error("MPI implementation must support MPI_THREAD_MULTIPLE");
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);    // Get the process rank
  MPI_Comm_size(MPI_COMM_WORLD, &n_ranks); // Get the number of processes
  root = (rank == ROOT);                   // Only one processor is root

  // Get the number of OpenMP threads
  #pragma omp parallel
  #pragma omp single
  n_threads = omp_get_num_threads();

  // Set unique thread and random seed
  #pragma omp parallel
  {
    thread = omp_get_thread_num(); // Get the thread number once
    std::random_device rd; // Obtains seeds for the random number engine
    seed = rd();           // Set random seeds for parallel processes
  }
}

/* Force MPI to perform a clean exit. */
// void mpi_exit() {
//   MPI_Barrier(MPI_COMM_WORLD);               // Wait for all ranks
//   MPI_Finalize();                            // Shut down MPI
//   exit(0);                                   // Quit the program
// }

/*! \brief Convenience function to MPI_Allreduce vector elements.
 *
 *  \param[in,out] vec Vector to reduce.
 *  \param[in] op MPI reduction operation.
 */
void mpi_allreduce(vector<double>& vec, MPI_Op op) {
  MPI_Allreduce(MPI_IN_PLACE, vec.data(), vec.size(), MPI_DOUBLE, op, MPI_COMM_WORLD);
}

/*! \brief Uniform random number generator for everyday use.
 *
 *  This is the Ranq1 random number generator suggested by Numerical Recipes.
 *  The period is \f$\sim 1.8 \times 10^{19}\f$ sufficient for our applications.
 *  The performance may potentially be sped up with SIMD intrinsics.
 *
 *  \return Random number drawn uniformly in the interval \f$[0,1)\f$.
 */
double ran() {
  seed ^= seed >> 21;
  seed ^= seed << 35;
  seed ^= seed >> 4;
  return 5.42101086242752217E-20 * (seed * 2685821657736338717LL);
}
