/*!
 * \file    image.h
 *
 * \brief   Data structures for images (2D) and data cubes (3D).
 *
 * \details This file contains convenient data structures and the associated
 *          operations needed for camera images (2D) and data cubes (3D). We
 *          note that the underlying layout is consistent with HDF5 and other
 *          tools that assume row-major indexing for flattened arrays.
 */

#ifndef IMAGE_INCLUDED
#define IMAGE_INCLUDED

#include <vector>
#include <stddef.h>

//! \brief Row-major 2D matrix array which takes arguments in (\f$i_x, i_y\f$) order.
template <typename T>
class Image {
public:
  //! \brief Default constructor.
  Image() = default;
  /*! \brief Constructor based on (\f$n_x, n_y\f$) pixel dimensions.
   *
   *  \param[in] nx Number of horizontal pixels (\f$n_x\f$).
   *  \param[in] ny Number of vertical pixels (\f$n_y\f$).
   */
  Image(const size_t nx, const size_t ny) :
        inner(std::vector<T>(nx * ny)),
        nx_impl(nx), ny_impl(ny) {};

  /*! \brief Access the number of horizontal pixels (\f$n_x\f$).
   *
   *  \return Number of horizontal pixels (\f$n_x\f$).
   */
  size_t nx() const {
    return nx_impl;
  }
  /*! \brief Access the number of vertical pixels (\f$n_y\f$).
   *
   *  \return Number of vertical pixels (\f$n_y\f$).
   */
  size_t ny() const {
    return ny_impl;
  }
  /*! \brief Access the total number of pixels (\f$n_x \cdot n_y\f$).
   *
   *  \return Total number of pixels (\f$n_x \cdot n_y\f$).
   */
  size_t size() const {
    return inner.size();
  }

  /*! \brief Access a pointer to the internal data.
   *
   *  \return Pointer to the internal data.
   */
  T* data() {
    return inner.data();
  }
  /*! \brief Access a pointer to the \f$i_x^\text{th}\f$ row of data.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *
   *  \return Pointer to the \f$i_x^\text{th}\f$ row of data.
   */
  T* data(const size_t ix) {
    return inner.data() + ix * ny_impl;
  }
  /*! \brief Read access for the (\f$i_x^\text{th}, i_y^\text{th}\f$) element.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *  \param[in] iy Column index (\f$i_y\f$).
   *
   *  \return Value of the data at (\f$i_x, i_y\f$).
   */
  T operator()(const size_t ix, const size_t iy) const {
    return inner[ix * ny_impl + iy];
  }
  /*! \brief Write access for the (\f$i_x^\text{th}, i_y^\text{th}\f$) element.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *  \param[in] iy Column index (\f$i_y\f$).
   *
   *  \return Reference to the data at (\f$i_x, i_y\f$).
   */
  T& operator()(const size_t ix, const size_t iy) {
    return inner[ix * ny_impl + iy];
  }
  /*! \brief Read access for the \f$i^\text{th}\f$ flat element.
   *
   *  \param[in] i Flat index (\f$i\f$).
   *
   *  \return Value of the flat data at (\f$i\f$).
   */
  T operator[](const size_t i) const {
    return inner[i];
  }
  /*! \brief Write access for the \f$i^\text{th}\f$ flat element.
   *
   *  \param[in] i Flat index (\f$i\f$).
   *
   *  \return Reference to the flat data at (\f$i\f$).
   */
  T& operator[](const size_t i) {
    return inner[i];
  }

  typedef typename std::vector<T>::iterator iterator;
  typedef typename std::vector<T>::const_iterator const_iterator;
  /*! \brief Access an iterator for the beginning of the internal data.
   *
   *  \return Iterator for the beginning of the internal data.
   */
  iterator begin() {
    return inner.begin();
  }
  /*! \brief Access an iterator for the end of the internal data.
   *
   *  \return Iterator for the end of the internal data.
   */
  iterator end() {
    return inner.end();
  }
  /*! \brief Access a const iterator for the beginning of the internal data.
   *
   *  \return Const iterator for the beginning of the internal data.
   */
  const_iterator cbegin() const {
    return inner.cbegin();
  }
  /*! \brief Access a const iterator for the end of the internal data.
   *
   *  \return Const iterator for the end of the internal data.
   */
  const_iterator cend() const {
    return inner.cend();
  }

  /*! \brief Calculate the sum of all data elements using OpenMP parallelization.
   *
   *  \return Sum of all data.
   */
  T omp_sum() const {
    T result = static_cast<T>(0);
    const size_t i_end = inner.size();
    #pragma omp parallel for reduction(+:result)
    for (size_t i = 0; i < i_end; ++i)
      result += inner[i];
    return result;
  }
  /*! \brief Calculate the sum of the \f$i_x^\text{th}\f$ row of data elements.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *
   *  \return Sum of the \f$i_x^\text{th}\f$ row of data.
   */
  T sum(const size_t ix) const {
    T result = static_cast<T>(0);
    const size_t i_start = ix * ny_impl;
    const size_t i_end = i_start + ny_impl;
    for (size_t i = i_start; i < i_end; ++i)
      result += inner[i];
    return result;
  }
  /*! \brief Calculate the average of the \f$i_x^\text{th}\f$ row of data elements.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *
   *  \return Average of the \f$i_x^\text{th}\f$ row of data.
   */
  T mean(const size_t ix) const {
    return sum(ix) / static_cast<T>(ny_impl);
  }
  /*! \brief Calculate the weighted average by \f$w\f$ of the \f$i_x^\text{th}\f$ row of data elements.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *  \param[in] weight Weight field (\f$w\f$).
   *
   *  \return Weighted average by \f$w\f$ of the \f$i_x^\text{th}\f$ row of data.
   */
  T weighted_mean(const size_t ix, const Image<T>& weight) const {
    const T zero = static_cast<T>(0);
    T result = zero;
    T result_norm = zero;
    const size_t i_start = ix * ny_impl;
    const size_t i_end = i_start + ny_impl;
    for (size_t i = i_start; i < i_end; ++i) {
      result += weight[i] * inner[i];
      result_norm += weight[i];
    }
    return (result_norm > zero) ? result / result_norm : zero;
  }

private:
  std::vector<T> inner;                      // Internal data
  size_t nx_impl = 0;                        // Number of horizontal pixels
  size_t ny_impl = 0;                        // Number of vertical pixels
};

//! \brief Row-major 3D cube array which takes arguments in (\f$i_x, i_y, i_z\f$) order.
template <typename T>
class Cube {
public:
  //! \brief Default constructor.
  Cube() = default;
  /*! \brief Constructor based on (\f$n_x, n_y, n_z\f$) voxel dimensions.
   *
   *  \param[in] nx Number of horizontal voxels (\f$n_x\f$).
   *  \param[in] ny Number of vertical voxels (\f$n_y\f$).
   *  \param[in] nz Number of depth voxels (\f$n_z\f$).
   */
  Cube(const size_t nx, const size_t ny, const size_t nz) :
       inner(std::vector<T>(nx * ny * nz)),
       nx_impl(nx), ny_impl(ny), nz_impl(nz) {};

  /*! \brief Access the number of horizontal voxels (\f$n_x\f$).
   *
   *  \return Number of horizontal voxels (\f$n_x\f$).
   */
  size_t nx() const {
    return nx_impl;
  }
  /*! \brief Access the number of vertical voxels (\f$n_y\f$).
   *
   *  \return Number of vertical voxels (\f$n_y\f$).
   */
  size_t ny() const {
    return ny_impl;
  }
  /*! \brief Access the number of depth voxels (\f$n_z\f$).
   *
   *  \return Number of depth voxels (\f$n_z\f$).
   */
  size_t nz() const {
    return nz_impl;
  }
  /*! \brief Access the total number of voxels (\f$n_x \cdot n_y \cdot n_z\f$).
   *
   *  \return Total number of voxels (\f$n_x \cdot n_y \cdot n_z\f$).
   */
  size_t size() const { return inner.size(); }

  /*! \brief Access a pointer to the internal data.
   *
   *  \return Pointer to the internal data.
   */
  T* data() {
    return inner.data();
  }
  /*! \brief Access a pointer to the (\f$i_x^\text{th}, i_y^\text{th}\f$) row and column of data.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *  \param[in] iy Column index (\f$i_y\f$).
   *
   *  \return Pointer to the (\f$i_x^\text{th}, i_y^\text{th}\f$) row and column of data.
   */
  T* data(const size_t ix, const size_t iy) {
    return inner.data() + (ix * ny_impl + iy) * nz_impl;
  }
  /*! \brief Read access for the (\f$i_x^\text{th}, i_y^\text{th}, i_z^\text{th}\f$) element.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *  \param[in] iy Column index (\f$i_y\f$).
   *  \param[in] iz Depth index (\f$i_z\f$).
   *
   *  \return Value of the data at (\f$i_x, i_y, i_z\f$).
   */
  T operator()(const size_t ix, const size_t iy, const size_t iz) const {
    return inner[(ix * ny_impl + iy) * nz_impl + iz];
  }
  /*! \brief Write access for the (\f$i_x^\text{th}, i_y^\text{th}, i_z^\text{th}\f$) element.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *  \param[in] iy Column index (\f$i_y\f$).
   *  \param[in] iz Depth index (\f$i_z\f$).
   *
   *  \return Reference to the data at (\f$i_x, i_y, i_z\f$).
   */
  T& operator()(const size_t ix, const size_t iy, const size_t iz) {
    return inner[(ix * ny_impl + iy) * nz_impl + iz];
  }
  /*! \brief Read access for the \f$i^\text{th}\f$ flat element.
   *
   *  \param[in] i Flat index (\f$i\f$).
   *
   *  \return Value of the flat data at (\f$i\f$).
   */
  T operator[](const size_t i) const {
    return inner[i];
  }
  /*! \brief Write access for the \f$i^\text{th}\f$ flat element.
   *
   *  \param[in] i Flat index (\f$i\f$).
   *
   *  \return Reference to the flat data at (\f$i\f$).
   */
  T& operator[](const size_t i) {
    return inner[i];
  }

  typedef typename std::vector<T>::iterator iterator;
  typedef typename std::vector<T>::const_iterator const_iterator;
  /*! \brief Access an iterator for the beginning of the internal data.
   *
   *  \return Iterator for the beginning of the internal data.
   */
  iterator begin() {
    return inner.begin();
  }
  /*! \brief Access an iterator for the end of the internal data.
   *
   *  \return Iterator for the end of the internal data.
   */
  iterator end() {
    return inner.end();
  }
  /*! \brief Access a const iterator for the beginning of the internal data.
   *
   *  \return Const iterator for the beginning of the internal data.
   */
  const_iterator cbegin() const {
    return inner.cbegin();
  }
  /*! \brief Access a const iterator for the end of the internal data.
   *
   *  \return Const iterator for the end of the internal data.
   */
  const_iterator cend() const {
    return inner.cend();
  }

private:
  std::vector<T> inner;                      // Internal data
  size_t nx_impl = 0;                        // Number of horizontal voxels
  size_t ny_impl = 0;                        // Number of vertical voxels
  size_t nz_impl = 0;                        // Number of depth voxels
};

#endif // IMAGE_INCLUDED
