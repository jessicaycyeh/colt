/**********
 * Vec3.h *
 **********

 * Convenience for handling 3D vectors in (x,y,z) order.

*/

#ifndef VEC3_INCLUDED
#define VEC3_INCLUDED

#include <cmath>
#include <ostream>

template <typename T>
struct GenericVec3 {
  T x = static_cast<T>(0);
  T y = static_cast<T>(0);
  T z = static_cast<T>(0);

  GenericVec3() = default;

  GenericVec3(T x_, T y_, T z_) : x(x_), y(y_), z(z_) {}

  GenericVec3(T x_) : x(x_), y(x_), z(x_) {}

  explicit GenericVec3(T* data) {
    x = data[0];
    y = data[1];
    z = data[2];
  }

  T min() const {
    return (x < y) ? ((x < z) ? x : z) : ((y < z) ? y : z);
  }

  T max() const {
    return (x > y) ? ((x > z) ? x : z) : ((y > z) ? y : z);
  }

  T norm() const {
    return sqrt(x*x + y*y + z*z);
  }

  void normalize() {
    const T inv_norm = static_cast<T>(1.) / sqrt(x*x + y*y + z*z);
    x *= inv_norm;
    y *= inv_norm;
    z *= inv_norm;
  }

  T dot() const {
    return x * x + y * y + z * z;
  }

  T dot(const GenericVec3 other) const {
    return x * other.x + y * other.y + z * other.z;
  }

  // Compute the distance between two vectors: ||A-B||
  T dist(const GenericVec3 other) const {
    const T dx = x - other.x;
    const T dy = y - other.y;
    const T dz = z - other.z;
    return sqrt(dx*dx + dy*dy + dz*dz);
  }

  GenericVec3 cross(const GenericVec3 other) const {
    return {
      y * other.z - z * other.y,
      z * other.x - x * other.z,
      x * other.y - y * other.x,
    };
  }

  bool operator==(const GenericVec3& other) const {
    return (x == other.x) && (y == other.y) && (z == other.z);
  }

  GenericVec3 operator+(const GenericVec3 other) const {
    return {
      x + other.x,
      y + other.y,
      z + other.z,
    };
  }

  GenericVec3 operator+(const T l) const {
    return {
      x + l,
      y + l,
      z + l,
    };
  }

  void operator+=(const GenericVec3 other) {
    x += other.x;
    y += other.y;
    z += other.z;
  }

  GenericVec3 operator-(const GenericVec3 other) const {
    return {
      x - other.x,
      y - other.y,
      z - other.z,
    };
  }

  GenericVec3 operator-(const T l) const {
    return {
      x - l,
      y - l,
      z - l,
    };
  }

  void operator-=(const GenericVec3 other) {
    x -= other.x;
    y -= other.y;
    z -= other.z;
  }

  GenericVec3 operator*(const GenericVec3 other) const {
    return {
      x * other.x,
      y * other.y,
      z * other.z,
    };
  }

  GenericVec3 operator*(const T l) const {
    return {
      x * l,
      y * l,
      z * l,
    };
  }

  GenericVec3 operator/(const GenericVec3 other) const {
    return {
      x / other.x,
      y / other.y,
      z / other.z,
    };
  }

  GenericVec3 operator/(const T l) const {
    return {
      x / l,
      y / l,
      z / l,
    };
  }

  void operator*=(const T l) {
    x *= l;
    y *= l;
    z *= l;
  }

  void operator/=(const T l) {
    x /= l;
    y /= l;
    z /= l;
  }

  T operator[](std::size_t index) const {
    return ((T*)this)[index];
  }

  T& operator[](size_t index) {
    return ((T*)this)[index];
  }
};

template <typename T>
inline std::ostream& operator<<(std::ostream& os, const GenericVec3<T>& v) {
  const T zero = static_cast<T>(0);
  return os << "(" << ((v.x == -zero) ? zero : v.x) << ", "
                   << ((v.y == -zero) ? zero : v.y) << ", "
                   << ((v.z == -zero) ? zero : v.z) << ")";
}

template <typename T>
inline std::string to_string(const GenericVec3<T> v) {
  return "(" + std::to_string(v.x) + ", "
             + std::to_string(v.y) + ", "
             + std::to_string(v.z) + ")";
}

using Vec3 = GenericVec3<double>;
using Vec3F = GenericVec3<float>;

#endif // VEC3_INCLUDED
