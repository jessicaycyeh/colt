/************
 * utils.cc *
 ************

 * General utility function definitions.

*/

#include "proto.h"

bool avoid_cell(const int cell); // Avoid calculations for certain cells

/* Find the minimum and maximum values of a vector. */
tuple<double, double> min_max(vector<double>& vec) {
  double vec_min = positive_infinity, vec_max = negative_infinity;
  for (int i = 0; i < n_cells; ++i) {
    if (avoid_cell(i))
      continue;
    if (vec[i] < vec_min)                    // Found new minimum value
      vec_min = vec[i];
    if (vec[i] > vec_max)                    // Found new maximum value
      vec_max = vec[i];
  }
  return make_tuple(vec_min, vec_max);
}

/* Find the minimum and maximum values of a Vec3 vector. */
tuple<Vec3, Vec3> min_max(vector<Vec3>& vec) {
  Vec3 vec_min{positive_infinity,positive_infinity,positive_infinity};
  Vec3 vec_max{negative_infinity,negative_infinity,negative_infinity};
  for (int i = 0; i < n_cells; ++i) {
    if (avoid_cell(i))
      continue;
    for (int j = 0; j < 3; ++j) {
      if (vec[i][j] < vec_min[j])            // Found new minimum value
        vec_min[j] = vec[i][j];
      if (vec[i][j] > vec_max[j])            // Found new maximum value
        vec_max[j] = vec[i][j];
    }
  }
  return make_tuple(vec_min, vec_max);
}

/* Print the minimum and maximum values of a vector. */
void print_min_max(string name, vector<double> vec, string units, double conv) {
  double vec_min, vec_max;
  tie(vec_min, vec_max) = min_max(vec);      // Find the min/max values
  cout << name << " = [" << vec_min / conv << ", "
                         << vec_max / conv << "] " << units << endl;
}

/* Print the minimum and maximum values of a Vec3 vector. */
void print_min_max(string name, vector<Vec3> vec, string units, double conv) {
  Vec3 vec_min, vec_max;
  tie(vec_min, vec_max) = min_max(vec);      // Find the min/max values
  cout << name << " = [" << vec_min / conv << ",\n" << string(name.length(), ' ')
               << "    " << vec_max / conv << "] " << units << endl;
}
