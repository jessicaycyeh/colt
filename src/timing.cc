/*************
 * timing.cc *
 *************

 * Timing: Wall clock and real time functions.

*/

#include "proto.h"
#include <sstream>
#include <sys/time.h> // gettimeofday
#include "timing.h" // Timing functionality

/* Start the timer. */
void Timer::start() {
  // Minor error handling
  if (timer_is_active)
    error("Cannot start the '" + name + "' timer more than once.");
  gettimeofday(&tv_start, 0); // Current time
  clock_start = clock();      // Current clock
  timer_is_active = true;     // Set active flag
}

/* Stop the timer. */
void Timer::stop() {
  // Minor error handling
  if (!timer_is_active)
    error("Cannot stop the '" + name + "' timer more than once.");
  if (!timer_is_active)
    start();                 // Start the timer if needed
  gettimeofday(&tv_stop, 0); // Current time
  clock_stop = clock();      // Current clock
  elapsed_wall += (clock_stop - clock_start) / double(CLOCKS_PER_SEC);
  elapsed_time += (tv_stop.tv_sec - tv_start.tv_sec) + 1e-6 * (tv_stop.tv_usec - tv_start.tv_usec);
  timer_is_active = false;   // Reset active flag
}

/* Returns the name. */
string Timer::get_name() { return name; }

/* Returns the elapsed real time. */
double Timer::get_time() {
  if (timer_is_active)
    stop(); // Stop the timer if needed
  return elapsed_time;
}

/* Returns the elapsed wall time. */
double Timer::get_wall() {
  if (timer_is_active)
    stop(); // Stop the timer if needed
  return elapsed_wall;
}

/* Sets the elapsed real time. */
void Timer::set_time(double time) {
  elapsed_time = time;
}

/* Sets the elapsed wall time. */
void Timer::set_wall(double wall) {
  elapsed_wall = wall;
}

static const double SECONDS_PER_MINUTE = 60.;
static const double SECONDS_PER_HOUR = 60. * SECONDS_PER_MINUTE;
static const double SECONDS_PER_DAY = 24. * SECONDS_PER_HOUR;

/* Returns a string with the real elapsed time. */
string Timer::str() {
  if (timer_is_active)
    stop(); // Stop the timer if needed
  double seconds = elapsed_time;
  int minutes, hours, days;
  days = int(seconds / SECONDS_PER_DAY);
  seconds -= SECONDS_PER_DAY * double(days);
  hours = int(seconds / SECONDS_PER_HOUR);
  seconds -= SECONDS_PER_HOUR * double(hours);
  minutes = int(seconds / SECONDS_PER_MINUTE);
  seconds -= SECONDS_PER_MINUTE * double(minutes);

  std::stringstream ss;
  if (days > 0)
    ss << days << " days ";
  ss << hours << ":" << std::setfill('0') << std::setw(2) << minutes << ":"
     << std::setw(6) << std::fixed << std::setprecision(3) << seconds << " = ";
  ss.unsetf(std::ios_base::fixed);
  ss << std::setfill(' ') << std::setprecision(6) << elapsed_time << " s";
  return ss.str();
}

/* Returns a string with the start date and time. */
string Timer::date() {
  if (!timer_is_active)
    start();
  return ctime((const time_t*)&tv_start.tv_sec);
}

/* Returns a formatted string of the ratio of two numbers in percent. */
static string efficiency(double t1, double t2) {
  std::stringstream ss;
  ss << std::fixed << std::setw(5) << std::setprecision(2) << 100. * t1 / t2 << "%";
  return ss.str();
}

static string efficiency(Timer& t1, Timer& t2) { return efficiency(t1.get_time(), t2.get_time()); }

/* Returns a string with the wall/real time efficiency. */
string Timer::efficiency() {
  if (timer_is_active)
    stop(); // Stop the timer if needed
  return ::efficiency(elapsed_wall, elapsed_time);
}

/* Returns a string with the overall and internal efficiencies. */
string info(Timer& t1, Timer& t2) {
  std::stringstream ss;
  string str_1 = t1.str();
  ss << std::left << std::setw(28) << str_1;
  if (t2.get_name() == "main")
    ss << " -->  " << efficiency(t1, t2) << "  [w/r: " << t1.efficiency() << "]";
  else
    ss << "     (" << efficiency(t1, t2) << ") [w/r: " << t1.efficiency() << "]";
  return ss.str();
}

template<typename It>
class Range {
  It b, e;
public:
  Range(It b, It e) : b(b), e(e) {}
  It begin() const { return b; }
  It end() const { return e; }
};

template<typename ORange, typename OIt = decltype(std::begin(std::declval<ORange>())), typename It = std::reverse_iterator<OIt>>
Range<It> reverse(ORange && originalRange) {
  return Range<It>(It(std::end(originalRange)), It(std::begin(originalRange)));
}

/* MPI reduction for overall timings to the root. */
template <std::size_t N>
static void reduce_clock_timings(array<Timer *, N>& timer_array) {
  // Collect the elapsed real and wall clock times for all timers
  vector<double> times;
  for(const auto& t : timer_array) {
    times.push_back(t->get_time());
    times.push_back(t->get_wall());
  }

  // Reduce timings to the root
  if (root) {
    MPI_Reduce(MPI_IN_PLACE, times.data(), times.size(), MPI_DOUBLE, MPI_SUM, ROOT, MPI_COMM_WORLD);

    // Populate the root timers with reduced values
    const double factor = 1. / double(n_ranks);
    for(const auto& t : reverse(timer_array)) {
      t->set_wall(factor * times.back());    // Set wall time
      times.pop_back();                      // Remove from list
      t->set_time(factor * times.back());    // Set real time average
      times.pop_back();                      // Remove from list
    }
  } else
    MPI_Reduce(times.data(), times.data(), times.size(), MPI_DOUBLE, MPI_SUM, ROOT, MPI_COMM_WORLD);
}

/* Print module clock timings. */
Timer main_timer = Timer("main", true);      // True starts the main timer
Timer init_timer = Timer("init");            // The other timers start later
Timer read_timer = Timer("read");
Timer final_timer = Timer("final");
Timer write_timer = Timer("write");
static array<Timer *, 5> timers = { &main_timer, &init_timer, &read_timer, &final_timer, &write_timer };

// Monte Carlo radiative transfer
Timer atau_timer = Timer("atau");
Timer mcrt_timer = Timer("mcrt");
Timer mcrt_proj_timer = Timer("mcrt_proj");
Timer reduce_timer = Timer("reduce");
static array<Timer *, 4> mcrt_timers = { &atau_timer, &mcrt_timer, &mcrt_proj_timer, &reduce_timer };
extern bool dynamical_core_skipping;         // Dynamical core-skipping
extern bool output_proj_emission;            // Intrinsic emission without transport
extern bool output_proj_attenuation;         // Attenuated emission without scattering

// Escaping radiative transfer
Timer escape_timer = Timer("escape");
static array<Timer *, 1> escape_timers = { &escape_timer };

// Projection images
Timer proj_timer = Timer("proj");
static array<Timer *, 1> proj_timers = { &proj_timer };

// Ray extraction analysis
Timer rays_timer = Timer("rays_timer");
Timer extract_cylinders_timer = Timer("extract_cylinders");
Timer combine_cylinders_timer = Timer("combine_cylinders");
Timer cylinders_to_rays_timer = Timer("cylinders_to_rays");
Timer combine_rays_timer = Timer("combine_rays");
static array<Timer *, 5> rays_timers = { &rays_timer, &extract_cylinders_timer,
  &combine_cylinders_timer, &cylinders_to_rays_timer, &combine_rays_timer };

// Reionization box analysis
Timer EoR_timer = Timer("EoR_timer");
static array<Timer *, 1> EoR_timers = { &EoR_timer };

/* MPI reduction for overall timings to the root. */
void reduce_clock_timings() {
  // Reduce main and module timings
  reduce_clock_timings(timers);
  if (module == "mcrt")
    reduce_clock_timings(mcrt_timers);
  else if (module == "projections")
    reduce_clock_timings(proj_timers);
  else if (module == "escape")
    reduce_clock_timings(escape_timers);
  else if (module == "rays")
    reduce_clock_timings(rays_timers);
  else if (module == "reionization")
    reduce_clock_timings(EoR_timers);
}

/* Print clock timings. */
void print_clock_timings() {
  const double factor = 1. / double(n_threads);
  for (auto& t : timers)
    t->set_wall(factor * t->get_wall());
  cout << "\nClock timings:  [w/r = wall/real scaled by core count]"
       << "\n"
       << "\n  (*) Initialization = " << info(init_timer, main_timer)
       << "\n        |  Read time = " << info(read_timer, init_timer);
  if (module == "mcrt" || module == "ionization") {
    for (auto& t : mcrt_timers)
      t->set_wall(factor * t->get_wall());
    if (dynamical_core_skipping)
      cout << "\n        |  atau calc = " << info(atau_timer, init_timer);
    cout << "\n"
         << "\n  (*) COLT MCRT calc = " << info(mcrt_timer, main_timer);
    if (output_proj_emission || output_proj_attenuation)
      cout << "\n        | Ray images = " << info(mcrt_proj_timer, mcrt_timer);
    if (n_ranks > 1)
      cout << "\n        | MPI_reduce = " << info(reduce_timer, mcrt_timer);
  } else if (module == "projections") {
    for (auto& t : proj_timers)
      t->set_wall(factor * t->get_wall());
    cout << "\n"
         << "\n  (*) Projections    = " << info(proj_timer, main_timer);
    // if (n_ranks > 1)
    //   cout << "\n        | MPI_reduce = " << info(proj_reduce_timer, proj_timer);
  } else if (module == "escape") {
    for (auto& t : escape_timers)
      t->set_wall(factor * t->get_wall());
    cout << "\n"
         << "\n  (*) Escaping rays  = " << info(escape_timer, main_timer);
    // if (n_ranks > 1)
    //   cout << "\n        | MPI_reduce = " << info(escape_reduce_timer, escape_timer);
  } else if (module == "rays") {
    for (auto& t : rays_timers)
      t->set_wall(factor * t->get_wall());
    cout << "\n"
         << "\n  (*) Ray extraction = " << info(rays_timer, main_timer)
         << "\n        |  Cylinders = " << info(extract_cylinders_timer, rays_timer)
         << "\n        |  Zip files = " << info(combine_cylinders_timer, rays_timer)
         << "\n        | Build rays = " << info(cylinders_to_rays_timer, rays_timer)
         << "\n        |  Rays file = " << info(combine_rays_timer, rays_timer);
  } else if (module == "reionization") {
    for (auto& t : EoR_timers)
      t->set_wall(factor * t->get_wall());
    cout << "\n"
         << "\n  (*) Reionization   = " << info(EoR_timer, main_timer);
    // if (n_ranks > 1)
    //   cout << "\n        | MPI_reduce = " << info(EoR_reduce_timer, EoR_timer);
  }
  cout << "\n"
       << "\n  (*) Finalization   = " << info(final_timer, main_timer)
       << "\n        | Write time = " << info(write_timer, final_timer)
       << "\n  ____________________________________________________________________________"
       << "\n"
       << "\n  Total real clock time = " << main_timer.str()
       << "\n  Wall clock efficiency = " << main_timer.efficiency() << endl;
}
