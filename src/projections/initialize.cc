/*****************************
 * projections/initialize.cc *
 *****************************

 * Initialization: Simulation setup, allocation, opening messages, etc.

*/

#include "../proto.h"
#include "Projections.h"

/* General setup for projections. */
void Simulation::setup_projections() {
  // Setup projection depth
  Vec3 dist_L = camera_center - bbox[0];     // Left bbox distances
  Vec3 dist_R = bbox[1] - camera_center;     // Right bbox distances
  const double min_L = dist_L.min();         // Min left bbox distance
  const double min_R = dist_R.min();         // Min right bbox distance
  const double dist_min = (min_L < min_R) ? min_L : min_R; // Min bbox distance

  // Consistency checks for image width and number of pixels
  if (proj_radius_bbox > 0.)
    proj_radius = dist_min * proj_radius_bbox; // Set based on distance to bbox
  if (proj_radius >= 0.)
    proj_depth = 2. * proj_radius;           // Set depth from radius
  if (proj_depth_cMpc > 0.)
    proj_depth = proj_depth_cMpc * Mpc / (1. + z); // Set depth from comoving Mpc
  if (proj_depth <= 0.)
    proj_depth = image_width;                // Default to image width
  if (proj_depth > 3. * max_bbox_width)
    proj_depth = 3. * max_bbox_width;        // Avoid very large depths

  // Set redundant projection parameters
  proj_radius = 0.5 * proj_depth;            // Set radius based on depth
}

/* Setup everything for projections. */
void Projections::setup() {
  // General setup for cameras
  setup_cameras();

  // General setup for projections
  setup_projections();

  // Allocate projection data
  projections.resize(n_projections);
  #pragma omp parallel for
  for (int i = 0; i < n_projections; ++i)
    projections[i] = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
}
