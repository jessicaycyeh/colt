/*****************************
 * projections/Projections.h *
 *****************************

 * Module declarations.

*/

#ifndef PROJECTIONS_INCLUDED
#define PROJECTIONS_INCLUDED

#include "../Simulation.h" // Base simulation class

/* Ray-based projections module. */
class Projections : public Simulation {
public:
  void run() override;                       // Module calculations

protected:
  void module_config(YAML::Node& file, YAML::Node& node) override;
  void setup() override;                     // Setup simulation data
  void print_info() override;                // Print simulation information
  void write_module(const H5::H5File& f) override; // Module output

private:
  int n_projections = 0;                     // Number of projections
  strings projection_units;                  // List of projection units
  vector<FieldWeightPair> proj_list;         // Projection (field, weight) list
  vector<vector<Image<double>>> projections; // Projection images

  bool check_perp_fields();                  // Test whether there are any perpendicular fields
  double ray_trace_single_field(const double rx, const double ry); // Single ray (single field)
  double pixel_quad_2D(const int ix, const int iy); // Adaptive 2D integrator
  void calculate_projections();              // Single image and direction
  void run_adaptive(const int start_cam, const int end_cam); // Adaptive projections
  void ray_trace_all_fields(vector<double>& results, const double rx, const double ry); // Single ray (all fields)
  void run_standard(const int start_cam, const int end_cam); // Non-adaptive projections
  void ray_trace_perspective_all_fields(vector<double>& results, const double rx, const double ry, const bool perp_flag); // Single ray (all fields)
  void run_perspective(const int start_cam, const int end_cam); // Non-adaptive perspective projections
};

#endif // PROJECTIONS_INCLUDED
