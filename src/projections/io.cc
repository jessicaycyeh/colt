/*********************
 * projections/io.cc *
 *********************

 * All I/O operations related to projections.

*/

#include "../proto.h"
#include "Projections.h"
#include "../io_hdf5.h" // HDF5 read/write functions

/* Function to print additional projection information. */
void Projections::print_info() {
  // Print field list
  cout << "\nProjection information:"
       << "\n  n_fields   = " << n_projections
       << "\n  depth      = " << proj_depth / kpc << " kpc"
       << "\n  radius     = " << proj_radius / kpc << " kpc";
  if (adaptive)
    cout << "\n  pixel_rtol = " << 100. * pixel_rtol << "%  (adaptive convergence)";
  if (perspective)
    cout << "\nNote: Using a perspective camera.";

  cout << "\n\nProjection fields:";
  string field, weight;
  for (auto& fw : proj_list) {
    field = fields[fw.field].name;
    if (fw.weight == SUM)
      weight = "sum";
    else if (fw.weight == AVG)
      weight = "avg";
    else if (fw.weight == MASS)
      weight = "mass";
    else
      weight = fields[fw.weight].name;
    cout << "\n  " << field << " [" << weight << "]";
  }
  cout << endl;

  print_cameras();                           // General camera parameters
}

/* Copy of a string with underscores removed. */
static inline string no_underscores(const string str) {
  string result;
  result.reserve(str.size());                // Avoids buffer reallocations
  for (size_t i = 0; i < str.size(); ++i)
    if (str[i] != '_')
      result += str[i];
  return result;
}

/* Writes projection data and info to the specified hdf5 file. */
void Projections::write_module(const H5File& f) {
  write(f, "n_projections", n_projections);  // Number of projections
  write(f, "proj_depth", proj_depth);        // Projection depth [cm]
  write(f, "proj_radius", proj_radius);      // Projection radius [cm]
  write(f, "adaptive", adaptive);            // Adaptive convergence flag
  if (adaptive)
    write(f, "pixel_rtol", pixel_rtol);      // Relative tolerance per pixel
  write(f, "perspective", perspective);      // Perspective camera flag

  for (int i = 0; i < n_projections; ++i) {
    const int field = proj_list[i].field;
    const int weight = proj_list[i].weight;
    string name = "proj_" + no_underscores(fields[field].name) + "_";
    if (weight == SUM)                     // Conserved integral projection
      name += "sum";
    else if (weight == AVG)                // Volume-weighted average
      name += "avg";
    else if (weight == MASS)               // Mass-weighted average
      name += "mass";
    else                                   // BoxField-weighted average
      name += no_underscores(fields[weight].name);
    write(f, name, projections[i], projection_units[i]); // Projection data
  }
}
