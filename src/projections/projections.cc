/******************************
 * projections/projections.cc *
 ******************************

 * Driver: Assign rays for projections, etc.

*/

#include "../proto.h"
#include "Projections.h"
#include "../timing.h" // Timing functionality

extern Timer proj_timer; // Clock timing
bool avoid_cell(const int cell); // Avoid calculations for certain cells
int find_cell(const Vec3 point, int cell); // Cell index of a point
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell

// Quad tree data structure
struct QuadTreeNode {
  int children;                              // Index of the first child node
  double rtol;                               // Node relative tolerance
  double xL, xR;                             // Node x range [xmin, xmax]
  double yL, yR;                             // Node y range [ymin, ymax]
  double fLL, fLR, fRL, fRR;                 // Pass pre-calculated values to children
  double fc;                                 // Cumulative integral over node region

  QuadTreeNode() = default;
  QuadTreeNode(double rtol, double xL, double xR, double yL, double yR,
               double fLL, double fLR, double fRL, double fRR) :
               children(-1), rtol(rtol), xL(xL), xR(xR), yL(yL), yR(yR),
               fLL(fLL), fLR(fLR), fRL(fRL), fRR(fRR), fc(0.) {};
};

static int start_cell;                       // Starting cell for searches
#pragma omp threadprivate(start_cell)        // Each thread needs start_cell
static Image<double> corners;                // Pixel corner values
static vector<vector<QuadTreeNode>> trees;   // Quad tree arrangement of nodes
static double V_col;                         // Projection column volume [cm^3]
static double inverse_proj_depth;            // Inverse projection depth [1/cm]
static double inverse_proj_radius;           // Inverse projection radius [1/cm]
static vector<double> weighted_field;        // Integration field
static int projection;                       // Projection index
static int camera;                           // Camera index
static Vec3 direction;                       // Camera direction
static Vec3 xaxis;                           // Camera x-axis
static Vec3 yaxis;                           // Camera y-axis

/* Calculate projections for a single line of sight. */
double Projections::ray_trace_single_field(const double rx, const double ry) {
  // Set the ray starting point based on (rx,ry,-proj_radius)
  Vec3 point = {camera_center.x + rx * xaxis.x
                                + ry * yaxis.x
                                - proj_radius * direction.x,
                camera_center.y + rx * xaxis.y
                                + ry * yaxis.y
                                - proj_radius * direction.y,
                camera_center.z + rx * xaxis.z
                                + ry * yaxis.z
                                - proj_radius * direction.z};
  int cell = find_cell(point, start_cell);   // Current cell index
  if (cell != start_cell)
    start_cell = cell;                       // Save the latest starting cell
  int next_cell;                             // Next cell index
  double dl, l_stop = proj_depth;            // Path lengths
  double result = 0.;                        // Integration result

  // Integrate through the fields for the specified distance
  while (true) {                             // Ray trace until escape
    tie(dl, next_cell) = face_distance(point, direction, cell); // Maximum propagation distance
    if (dl > l_stop) {                       // Stop before next cell
      dl = l_stop;                           // Do not go past the end
      next_cell = OUTSIDE;                   // Signal stopping criterion
    }
    result += weighted_field[cell] * dl;     // Volume-weighted integration
#ifdef SPHERICAL
    if (next_cell == INSIDE)                 // Check if the ray is trapped
      break;
#endif
    if (next_cell == OUTSIDE)
      break;                                 // Finished ray tracing
    l_stop -= dl;                            // Remaining distance
    point += direction * dl;                 // Move to the new position
    cell = next_cell;                        // Update the next cell index
  }

  // Normalize the units to return the average
  return result * inverse_proj_depth;        // Divide by the projected depth
}

// Adaptive quadrature 2D integrator
double Projections::pixel_quad_2D(const int ix, const int iy) {
  // Additional quadtree variables
  double rtol, xL, xM, xR, yL, yM, yR;
  double fLL, fLR, fRL, fRR, fLM, fML, fMM, fMR, fRM, f_trap, f_simp;
  auto& tree = trees[thread];                // Reference to the local tree

  // Initialize the base node
  int n_nodes = 1;                           // Initialize the node counter
  tree.resize(0);                            // Clear the tree (retain capacity)
  tree.emplace_back(                         // Start with no children
    pixel_rtol,                              // Base tolerance is target tolerance
    image_edges[ix], image_edges[ix+1],      // Domain: x in (xL,xR)
    image_edges[iy], image_edges[iy+1],      // Domain: y in (yL,yR)
    corners(ix,iy), corners(ix,iy+1),        // LL, LR corners
    corners(ix+1,iy), corners(ix+1,iy+1)     // RL, RR corners, cumulative integral
  );

  // Populate the quadtree
  for (int i = 0; i < n_nodes; ++i) {        // Note: n_nodes changes size
    rtol = tree[i].rtol;                     // Relative tolerance
    xL = tree[i].xL;  xR = tree[i].xR;       // Domain x edges
    yL = tree[i].yL;  yR = tree[i].yR;       // Domain y edges
    fLL = tree[i].fLL;  fLR = tree[i].fLR;   // LL, LR corners
    fRL = tree[i].fRL;  fRR = tree[i].fRR;   // RL, RR corners
    xM = 0.5 * (xL + xR);                    // Local node x midpoint
    yM = 0.5 * (yL + yR);                    // Local node y midpoint
    fLM = ray_trace_single_field(xL, yM);    // Midpoint evaluations
    fML = ray_trace_single_field(xM, yL);
    fMM = ray_trace_single_field(xM, yM);
    fMR = ray_trace_single_field(xM, yR);
    fRM = ray_trace_single_field(xR, yM);

    // Note: Estimates are multiplied by 36/Area for convenience and corrected later
    f_trap = fLL + fLR + fRL + fRR;          // Trapezoid rule
    f_simp = f_trap + 4.*(fLM + fML + fMR + fRM) + 16.*fMM; // Simpson's rule
    // Check whether the node is converged
    if (fabs(9.*f_trap - f_simp) <= rtol * fabs(f_simp)) {
      tree[i].fc = f_simp;                   // Save converged value
      continue;                              // No need to refine this node
    }

    // Refine the current node
    tree[i].children = n_nodes;              // Append children to the end
    n_nodes += 4;                            // Add four children nodes
    const double child_rtol = 2. * rtol;     // Factor of sqrt(4) per refinement level

    //  Layout of children:
    //
    // (yR) fLR-----fMR-----fRR
    //       |       |       |
    //       |   2   |   4   |
    //       |       |       |
    // (yM) fLM-----fMM-----fRM
    //       |       |       |
    //       |   1   |   3   |
    //       |       |       |
    // (yL) fLL-----fML-----fRL
    //
    //      (xL)    (xM)    (xR)

    // Initialize 1st child
    tree.emplace_back(
      child_rtol,                            // No children, child tolerance
      xL, xM, yL, yM,                        // (xL,xR,yL,yR) = (xL,xM,yL,xM)
      fLL, fLM, fML, fMM                     // (LL,LR,RL,RR) = (LL,LM,ML,MM)
    );

    // Initialize 2nd child
    tree.emplace_back(
      child_rtol,                            // No children, child tolerance
      xL, xM, yM, yR,                        // (xL,xR,yL,yR) = (xL,xM,yM,xR)
      fLM, fLR, fMM, fMR                     // (LL,LR,RL,RR) = (LM,LR,MM,MR)
    );

    // Initialize 3rd child
    tree.emplace_back(
      child_rtol,                            // No children, child tolerance
      xM, xR, yL, yM,                        // (xL,xR,yL,yR) = (xM,xR,yL,yM)
      fML, fMM, fRL, fRM                     // (LL,LR,RL,RR) = (ML,MM,RL,RM)
    );

    // Initialize 4th child
    tree.emplace_back(
      child_rtol,                            // No children, child tolerance
      xM, xR, yM, yR,                        // (xL,xR,yL,yR) = (xM,xR,yM,yR)
      fMM, fMR, fRM, fRR                     // (LL,LR,RL,RR) = (MM,MR,RM,RR)
    );
  }

  // Aggregation step for converged sub-domain integrals
  for (int ci, i = n_nodes - 1; i >= 0; --i) {
    ci = tree[i].children;
    if (ci > 0) // The node has children (i.e. not a leaf node)
      tree[i].fc = 0.25 * (tree[ci].fc + tree[ci+1].fc + tree[ci+2].fc + tree[ci+3].fc);
  }

  // Note: Simpson estimate was multiplied by 36/Area for efficiency
  return tree[0].fc / 36.;
}

/* Ray trace for projection calculations. */
void Projections::calculate_projections() {
  // Pre-calculate the pixel corner values
  const int Np1 = n_pixels + 1;              // Number of pixel edges
  const int n_corners = Np1 * Np1;           // Total number of corners
  const int corner_interval = (n_corners < 200) ? 1 : n_corners / 100; // Interval between updates
  int n_finished = 0;                        // Progress for printing
  if (root)
    cout << "\n    Pixel corner calculations:   0%\b\b\b\b" << std::flush;
  #pragma omp parallel for schedule(dynamic)
  for (int i = 0; i < n_corners; ++i) {
    // Ray tracing for each corner
    const int ix = i / Np1;                  // Corner ix,iy index
    const int iy = i % Np1;
    const double rx = image_edges[ix];       // Corner position
    const double ry = image_edges[iy];
    corners(ix,iy) = ray_trace_single_field(rx, ry);

    // Print completed progress
    if (root) {
      int i_finished;                        // Progress counter
      #pragma omp atomic capture
      i_finished = ++n_finished;             // Update finished counter
      if (i_finished % corner_interval == 0)
        cout << std::setw(3) << (100 * size_t(i_finished)) / size_t(n_corners) << "%\b\b\b\b" << std::flush;
    }
  }

  // Perform adaptive convergence for all pixels
  if (root)
    cout << "100%\n    Pixel convergence calculations:   0%\b\b\b\b" << std::flush;
  const int n_pixels2 = n_pixels * n_pixels; // Total number of pixels
  const int pixel_interval = (n_pixels2 < 200) ? 1 : n_pixels2 / 100; // Interval between updates
  n_finished = 0;                            // Reset progress counter
  #pragma omp parallel for schedule(dynamic)
  for (int i = 0; i < n_pixels2; ++i) {
    // Ray tracing for each pixel
    const int ix = i / n_pixels;             // Pixel ix,iy index
    const int iy = i % n_pixels;
    projections[projection][camera](ix,iy) = pixel_quad_2D(ix, iy);

    // Print completed progress
    if (root) {
      int i_finished;                        // Progress counter
      #pragma omp atomic capture
      i_finished = ++n_finished;             // Update finished counter
      if (i_finished % pixel_interval == 0)
        cout << std::setw(3) << (100 * size_t(i_finished)) / size_t(n_pixels2) << "%\b\b\b\b" << std::flush;
    }
  }
  if (root)
    cout << "100%" << endl;
}

/* Update fields that depend on the line of sight. */
static inline void update_LOS_field(vector<double>& field, const int fw_field) {
  switch (fw_field) {
    case VelocitySquaredLOS:                 // Velocity_LOS^2 [cm^2/s^2]
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        if (avoid_cell(i))
          continue;
        field[i] = v[i].dot(direction);      // LOS velocity
        field[i] *= field[i];                // LOS velocity^2
      }
      break;
    case VelocityLOS:                        // Velocity_LOS [cm/s]
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        if (avoid_cell(i))
          continue;
        field[i] = v[i].dot(direction);      // LOS velocity
      }
      break;
    case VelocityX:                          // Velocity_X [cm/s]
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        if (avoid_cell(i))
          continue;
        field[i] = v[i].dot(xaxis);          // X-axis velocity
      }
      break;
    case VelocityY:                          // Velocity_Y [cm/s]
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        if (avoid_cell(i))
          continue;
        field[i] = v[i].dot(yaxis);          // Y-axis velocity
      }
      break;
    case MagneticFieldLOS:                   // Magnetic field LOS [G]
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        if (avoid_cell(i))
          continue;
        field[i] = B[i].dot(direction);      // LOS magnetic field
      }
      break;
    case MagneticFieldX:                     // Magnetic field X [G]
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        if (avoid_cell(i))
          continue;
        field[i] = B[i].dot(xaxis);          // X-axis magnetic field
      }
      break;
    case MagneticFieldY:                     // Magnetic field Y [G]
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        if (avoid_cell(i))
          continue;
        field[i] = B[i].dot(yaxis);          // Y-axis magnetic field
      }
      break;
    default:                                 // No camera direction dependence
      break;
  }
}

/* Test whether a given field is a LOS field. */
static inline bool check_LOS_field(const int field) {
  return (field == VelocitySquaredLOS || field == VelocityLOS
    || field == VelocityX || field == VelocityY || field == MagneticFieldLOS
    || field == MagneticFieldX || field == MagneticFieldY);
}

/* Test whether a given field is a perpendicular field. */
static inline bool check_perp_field(const int field) {
  return (field == VelocityX || field == VelocityY || field == MagneticFieldX || field == MagneticFieldY);
}

/* Test whether there are any perpendicular field. */
inline bool Projections::check_perp_fields() {
  for (auto& fw : proj_list) {               // Field-wieght indices
    if (check_perp_field(fw.field))
      return true;                           // At least one perp field
    if (check_perp_field(fw.weight))
      return true;                           // At least one perp field
  }
  return false;
}

/* Run adaptive projections. */
void Projections::run_adaptive(const int start_cam, const int end_cam) {
  // Allocate pixel corner values and quad trees
  corners = Image<double>(n_pixels+1, n_pixels+1); // Pixel corner values
  trees.resize(n_threads);                   // Each thread has its own tree
  #pragma omp parallel
  trees[thread].reserve(4097);               // Reserve reasonable tree sizes

  // Setup specific projection field and camera
  string field_name, weight_name;            // Field-wieght names
  weighted_field.resize(n_cells);            // Data for weighted fields
  for (projection = 0; projection < n_projections; ++projection) {
    const auto& fw = proj_list[projection];  // Field-wieght indices
    vector<double>& field = fields[fw.field].data;
    field_name = fields[fw.field].name;
    if (fw.weight == SUM)
      weight_name = "sum";
    else if (fw.weight == AVG)
      weight_name = "avg";
    else if (fw.weight == MASS)
      weight_name = "mass";
    else
      weight_name = fields[fw.weight].name;
    if (root)
      cout << "\nProjection: " << field_name << " [" << weight_name << "]";

    const bool LOS_field_flag = check_LOS_field(fw.field);
    const bool LOS_weight_flag = check_LOS_field(fw.weight);
    const bool LOS_flag = (LOS_field_flag || LOS_weight_flag);

    for (camera = start_cam; camera < end_cam; ++camera) {
      direction = camera_directions[camera]; // Camera direction
      xaxis = camera_xaxes[camera];          // Camera x-axis
      yaxis = camera_yaxes[camera];          // Camera y-axis
      if (root) {
        cout << "\n  Camera progress: " << camera+1 << " / " << end_cam;
        if (n_ranks > 1)
          cout << " (root)";
      }

      // Some fields depend on the camera direction
      if (LOS_field_flag)
        update_LOS_field(field, fw.field);

      // Calculations are determined by the weight
      if (LOS_flag || camera == start_cam)
        switch (fw.weight) {
          case SUM:                          // Conserved integral projection
            #pragma omp parallel for
            for (int i = 0; i < n_cells; ++i) {
              if (avoid_cell(i))
                continue;
              weighted_field[i] = field[i] * V_col / V[i]; // Transform to density-like field
            }
            break;
          case AVG:                          // Volume-weighted average
            #pragma omp parallel for
            for (int i = 0; i < n_cells; ++i) {
              if (avoid_cell(i))
                continue;
              weighted_field[i] = field[i];  // Already a density-like field
            }
            break;
          case MASS:                         // Mass-weighted average
            #pragma omp parallel for
            for (int i = 0; i < n_cells; ++i) {
              if (avoid_cell(i))
                continue;
              weighted_field[i] = field[i] * rho[i]; // Include mass-weighting
            }
            break;
          default:                           // Field-weighted average
            vector<double>& weight = fields[fw.weight].data;
            if (LOS_weight_flag)
              update_LOS_field(weight, fw.weight); // Ensure updated LOS weight fields
            #pragma omp parallel for
            for (int i = 0; i < n_cells; ++i) {
              if (avoid_cell(i))
                continue;
              weighted_field[i] = field[i] * weight[i] * V_col / V[i]; // Transform to weighted field
            }
        }

      // Perform the actual projection calculations
      calculate_projections();
    }
  }
  weighted_field = vector<double>();         // Free memory
}

/* Calculate projections for a single line of sight (all fields). */
void Projections::ray_trace_all_fields(vector<double>& results, const double rx, const double ry) {
  // Set the ray starting point based on (rx,ry,-proj_radius)
  Vec3 point = {camera_center.x + rx * xaxis.x
                                + ry * yaxis.x
                                - proj_radius * direction.x,
                camera_center.y + rx * xaxis.y
                                + ry * yaxis.y
                                - proj_radius * direction.y,
                camera_center.z + rx * xaxis.z
                                + ry * yaxis.z
                                - proj_radius * direction.z};
  int cell = find_cell(point, start_cell);   // Current cell index
  if (cell != start_cell)
    start_cell = cell;                       // Save the latest starting cell
  int next_cell;                             // Next cell index
  double dl, l_stop = proj_depth;            // Path lengths
  for (int i = 0; i < n_projections; ++i)
    results[i] = 0.;                         // Integration results
  if (avoid_cell(cell))
    return;                                  // Ignore rays from bad cells

  // Integrate through the fields for the specified distance
  while (true) {                             // Ray trace until escape
    tie(dl, next_cell) = face_distance(point, direction, cell); // Maximum propagation distance
    if (dl > l_stop) {                       // Stop before next cell
      dl = l_stop;                           // Do not go past the end
      next_cell = OUTSIDE;                   // Signal stopping criterion
    }
    for (int i = 0; i < n_projections; ++i) {
      const auto& fw = proj_list[i];         // Field-wieght indices
      vector<double>& field = fields[fw.field].data;
      switch (fw.weight) {
        case SUM:                            // Conserved integral projection
          results[i] += field[cell] * V_col / V[cell] * dl; // Transform to density-like field
          break;
        case AVG:                            // Volume-weighted average
          results[i] += field[cell] * dl;    // Already a density-like field
          break;
        case MASS:                           // Mass-weighted average
          results[i] += field[cell] * rho[cell] * dl; // Include mass-weighting
          break;
        default:                             // Field-weighted average
          vector<double>& weight = fields[fw.weight].data;
          results[i] += field[cell] * weight[cell] * V_col / V[cell] * dl; // Transform to weighted field
      }
    }
#ifdef SPHERICAL
    if (next_cell == INSIDE)                 // Check if the ray is trapped
      break;
#endif
    if (next_cell == OUTSIDE)
      break;                                 // Finished ray tracing
    l_stop -= dl;                            // Remaining distance
    point += direction * dl;                 // Move to the new position
    cell = next_cell;                        // Update the next cell index
  }

  // Normalize the units to return the average
  for (int i = 0; i < n_projections; ++i)
    results[i] *= inverse_proj_depth;        // Divide by the projected depth
}

/* Run non-adaptive projections. */
void Projections::run_standard(const int start_cam, const int end_cam) {
  // Set up minimal LOS set
  std::set<int> LOS_checks;
  for (int i = 0; i < n_projections; ++i) {
    const auto& fw = proj_list[i];           // Field-wieght indices
    if (check_LOS_field(fw.field))
      LOS_checks.insert(fw.field);
    if (check_LOS_field(fw.weight))
      LOS_checks.insert(fw.weight);
  }
  auto image_centers = vector<double>(n_pixels); // Image center positions [cm]
  for (int i = 0; i < n_pixels; ++i)
    image_centers[i] = 0.5 * (image_edges[i] + image_edges[i+1]);
  for (camera = start_cam; camera < end_cam; ++camera) {
    direction = camera_directions[camera];   // Camera direction
    xaxis = camera_xaxes[camera];            // Camera x-axis
    yaxis = camera_yaxes[camera];            // Camera y-axis
    if (root) {
      cout << "\nCamera progress: " << camera+1 << " / " << end_cam;
      if (n_ranks > 1)
        cout << " (root)";
    }

    for (auto LOS_check : LOS_checks)        // Ensure updated LOS fields and weights
      update_LOS_field(fields[LOS_check].data, LOS_check);

    // Perform projection calculations for all pixels
    if (root)
      cout << "\n  Pixel calculations:   0%\b\b\b\b" << std::flush;
    const int n_pixels2 = n_pixels * n_pixels; // Total number of pixels
    const int pixel_interval = (n_pixels2 < 200) ? 1 : n_pixels2 / 100; // Interval between updates
    int n_finished = 0;                      // Progress for printing
    #pragma omp parallel
    {
      auto results = vector<double>(n_projections); // Integration results
      #pragma omp for schedule(dynamic)
      for (int i = 0; i < n_pixels2; ++i) {
        // Ray tracing for each pixel
        const int ix = i / n_pixels;         // Pixel ix,iy index
        const int iy = i % n_pixels;
        const double rx = image_centers[ix]; // Pixel center position
        const double ry = image_centers[iy];
        ray_trace_all_fields(results, rx, ry); // Single ray (all fields)
        for (int i = 0; i < n_projections; ++i)
          projections[i][camera](ix,iy) = results[i]; // Save final results

        // Print completed progress
        if (root) {
          int i_finished;                    // Progress counter
          #pragma omp atomic capture
          i_finished = ++n_finished;         // Update finished counter
          if (i_finished % pixel_interval == 0)
            cout << std::setw(3) << (100 * size_t(i_finished)) / size_t(n_pixels2) << "%\b\b\b\b" << std::flush;
        }
      }
    }
    if (root)
      cout << "100%" << endl;
  }
}

/* Returns the value of a field for a given cell and direction. */
static inline double get_cell_value(const int fw_field, const int cell, const Vec3& k_LOS, const Vec3& k_xa, const Vec3& k_ya) {
  switch (fw_field) {
    case VelocitySquaredLOS:                 // Velocity_LOS^2 [cm^2/s^2]
    {
      double v_LOS = v[cell].dot(k_LOS);     // LOS velocity
      return v_LOS * v_LOS;                  // LOS velocity^2
      break;
    }
    case VelocityLOS:                        // Velocity_LOS [cm/s]
      return v[cell].dot(k_LOS);             // LOS velocity
      break;
    case VelocityX:                          // Velocity_X [cm/s]
      return v[cell].dot(k_xa);              // X-axis velocity
      break;
    case VelocityY:                          // Velocity_Y [cm/s]
      return v[cell].dot(k_ya);              // Y-axis velocity
      break;
    case MagneticFieldLOS:                   // Magnetic field LOS [G]
      return B[cell].dot(k_LOS);             // LOS magnetic field
      break;
    case MagneticFieldX:                     // Magnetic field X [G]
      return B[cell].dot(k_xa);              // X-axis magnetic field
      break;
    case MagneticFieldY:                     // Magnetic field Y [G]
      return B[cell].dot(k_ya);              // Y-axis magnetic field
      break;
    default:                                 // No camera direction dependence
    {
      vector<double>& field = fields[fw_field].data;
      return field[cell];                    // Normal field data access
      break;
    }
  }
}

/* Updates the values of the local perpendicular axes. */
static inline void update_axes(const Vec3& dir, Vec3& x_axis, Vec3& y_axis) {
  if (1. - fabs(dir.z) > 1e-9) {           // Use (0,0,1) to build basis
    // y-axis = z_hat - proj(dir,z_hat) = z_hat - dir[z] * dir
    y_axis.x = -dir.x * dir.z;             // Orthogonal projection (+z)
    y_axis.y = -dir.y * dir.z;
    y_axis.z = 1. - dir.z * dir.z;
  } else if (dir.z > 0.) {                 // Degenerate case (+z)
    // y-axis = y_hat - proj(dir,y_hat) = y_hat - dir[y] * dir
    y_axis.x = -dir.x * dir.y;             // Orthogonal projection (+y)
    y_axis.y = 1. - dir.y * dir.y;
    y_axis.z = -dir.z * dir.y;
  } else {                                 // Degenerate case (-z)
    // y-axis = x_hat - proj(dir,x_hat) = x_hat - dir[x] * dir
    y_axis.x = 1. - dir.x * dir.x;         // Orthogonal projection (+x)
    y_axis.y = -dir.y * dir.x;
    y_axis.z = -dir.z * dir.x;
  }
  y_axis.normalize();                      // Make it a unit vector
  x_axis = y_axis.cross(dir);              // Final orthogonal axis
  x_axis.normalize();                      // Ensure normalization
}

/* Calculate projections for a single line of sight (all fields). */
void Projections::ray_trace_perspective_all_fields(vector<double>& results, const double rx, const double ry, const bool perp_flag) {
  // Set the ray starting point at the camera center
  Vec3 point = camera_center;
  // Set the ray direction based on the image coordinates (-rx,ry,proj_radius)
  Vec3 k_LOS = {-rx * xaxis.x + ry * yaxis.x + proj_radius * direction.x,
                -rx * xaxis.y + ry * yaxis.y + proj_radius * direction.y,
                -rx * xaxis.z + ry * yaxis.z + proj_radius * direction.z};
  k_LOS.normalize();                         // Normalized perspective direction
  Vec3 k_xa, k_ya;                           // Local perpendicular axes
  if (perp_flag)
    update_axes(k_LOS, k_xa, k_ya);          // Update the perpendicular axes
  int cell = start_cell, next_cell;          // Current and next cell indices
  double dl, l_stop = proj_radius;           // Path lengths
  for (int i = 0; i < n_projections; ++i)
    results[i] = 0.;                         // Integration results

  // Integrate through the fields for the specified distance
  while (true) {                             // Ray trace until escape
    tie(dl, next_cell) = face_distance(point, k_LOS, cell); // Maximum propagation distance
    if (dl > l_stop) {                       // Stop before next cell
      dl = l_stop;                           // Do not go past the end
      next_cell = OUTSIDE;                   // Signal stopping criterion
    }
    for (int i = 0; i < n_projections; ++i) {
      const auto& fw = proj_list[i];         // Field-wieght indices
      const double field_cell = get_cell_value(fw.field, cell, k_LOS, k_xa, k_ya);
      switch (fw.weight) {
        case SUM:                            // Conserved integral projection
          results[i] += field_cell * V_col / V[cell] * dl; // Transform to density-like field
          break;
        case AVG:                            // Volume-weighted average
          results[i] += field_cell * dl;     // Already a density-like field
          break;
        case MASS:                           // Mass-weighted average
          results[i] += field_cell * rho[cell] * dl; // Include mass-weighting
          break;
        default:                             // Field-weighted average
        {
          const double weight_cell = get_cell_value(fw.weight, cell, k_LOS, k_xa, k_ya);
          results[i] += field_cell * weight_cell * V_col / V[cell] * dl; // Transform to weighted field
        }
      }
    }
#ifdef SPHERICAL
    if (next_cell == INSIDE)                 // Check if the ray is trapped
      break;
#endif
    if (next_cell == OUTSIDE)
      break;                                 // Finished ray tracing
    l_stop -= dl;                            // Remaining distance
    point += k_LOS * dl;                     // Move to the new position
    cell = next_cell;                        // Update the next cell index
  }

  // Normalize the units to return the average
  for (int i = 0; i < n_projections; ++i)
    results[i] *= inverse_proj_radius;       // Divide by the projected radius
}

/* Run non-adaptive perspective projections. */
void Projections::run_perspective(const int start_cam, const int end_cam) {
  const bool perp_flag = check_perp_fields(); // Perpendicular fields need to be updated
  auto image_centers = vector<double>(n_pixels); // Image center positions [cm]
  for (int i = 0; i < n_pixels; ++i)
    image_centers[i] = 0.5 * (image_edges[i] + image_edges[i+1]);
  for (camera = start_cam; camera < end_cam; ++camera) {
    direction = camera_directions[camera];   // Camera direction
    xaxis = camera_xaxes[camera];            // Camera x-axis
    yaxis = camera_yaxes[camera];            // Camera y-axis
    if (root) {
      cout << "\nCamera progress: " << camera+1 << " / " << end_cam;
      if (n_ranks > 1)
        cout << " (root)";
    }

    // Perform projection calculations for all pixels
    if (root)
      cout << "\n  Pixel calculations:   0%\b\b\b\b" << std::flush;
    const int n_pixels2 = n_pixels * n_pixels; // Total number of pixels
    const int pixel_interval = (n_pixels2 < 200) ? 1 : n_pixels2 / 100; // Interval between updates
    int n_finished = 0;                      // Progress for printing
    #pragma omp parallel
    {
      start_cell = find_cell(camera_center, 0); // Common starting cell index
      if (avoid_cell(start_cell))
        error("Starting from a bad cell!");  // Ignore rays from bad cells
      auto results = vector<double>(n_projections); // Integration results
      #pragma omp for schedule(dynamic)
      for (int i = 0; i < n_pixels2; ++i) {
        // Ray tracing for each pixel
        const int ix = i / n_pixels;         // Pixel ix,iy index
        const int iy = i % n_pixels;
        const double rx = image_centers[ix]; // Pixel center position
        const double ry = image_centers[iy];
        ray_trace_perspective_all_fields(results, rx, ry, perp_flag); // Single ray (all fields)
        for (int i = 0; i < n_projections; ++i)
          projections[i][camera](ix,iy) = results[i]; // Save final results

        // Print completed progress
        if (root) {
          int i_finished;                    // Progress counter
          #pragma omp atomic capture
          i_finished = ++n_finished;         // Update finished counter
          if (i_finished % pixel_interval == 0)
            cout << std::setw(3) << (100 * size_t(i_finished)) / size_t(n_pixels2) << "%\b\b\b\b" << std::flush;
        }
      }
    }
    if (root)
      cout << "100%" << endl;
  }
}

/* Driver for projection calculations. */
void Projections::run() {
  proj_timer.start();

  // Set redundant projection parameters
  const int n_cam_rank = n_cameras / n_ranks; // Equal assignment
  const int remainder = n_cameras - n_cam_rank * n_ranks;
  int start_cam = rank * n_cam_rank;         // Camera start range
  int end_cam = (rank + 1) * n_cam_rank;     // Camera end range
  if (rank < remainder) {
    start_cam += rank;                       // Correct start range
    end_cam += (rank + 1);                   // Correct end range
  } else {
    start_cam += remainder;                  // Correct start range
    end_cam += remainder;                    // Correct end range
  }
  V_col = proj_depth * pixel_area;           // Projection column volume
  inverse_proj_depth = 1. / proj_depth;      // Inverse projection depth
  inverse_proj_radius = 1. / proj_radius;    // Inverse projection radius
  #pragma omp parallel
  start_cell = 0;                            // Reset the starting cell

  if (adaptive) {
    if (perspective) {
      root_error("Adaptive perspective camera projections are not implemented.");
    } else
      run_adaptive(start_cam, end_cam);      // Adaptive projections
  } else {
    if (perspective)
      run_perspective(start_cam, end_cam);   // Non-adaptive perspective projections
    else
      run_standard(start_cam, end_cam);      // Non-adaptive projections
  }

  // Correct the units for weighted projections (divide by weight sum)
  const int n_pixels2 = n_pixels * n_pixels; // Total number of pixels
  for (projection = 0; projection < n_projections; ++projection) {
    const auto& fw = proj_list[projection];  // Field-wieght indices
    if (fw.weight == SUM || fw.weight == AVG)
      continue;                              // Already in the correct units
    int denominator = -1;                    // Find the denominator index
    int den_field = -1, den_weight = -1;     // Denominator field and weight
    if (fw.weight == MASS) {
      den_field = Density;                   // Mass-weighting is defined as:
      den_weight = AVG;                      // <f>_mass = sum(f rho dl) / sum(rho dl)
    } else {
      den_field = fw.weight;                 // Field-weighting is defined as:
      den_weight = SUM;                      // <f>_w = sum(f w dl/V) / sum(w dl/V)
    }
    for (int i = 0; i < n_projections; ++i) {
      const auto& dfw = proj_list[i];        // Denominator field-wieght indices
      if (dfw.field == den_field && dfw.weight == den_weight)
        denominator = i;
    }
    if (denominator == -1)
      error("Missing the projection weight denominator.");
    for (camera = start_cam; camera < end_cam; ++camera) {
      auto& data_n = projections[projection][camera];
      auto& data_d = projections[denominator][camera];
      #pragma omp parallel for
      for (int i = 0; i < n_pixels2; ++i) {
        if (data_d[i] > 0.)
          data_n[i] /= data_d[i];            // Weight normalization
      }
    }
  }

  // Reduce all projections data to root
  if (n_ranks > 1) {
    for (auto& ps : projections) {          // Collect one field at a time
      if (root) {
        for (camera = end_cam; camera < n_cameras; ++camera)
          MPI_Recv(ps[camera].data(), ps[camera].size(), MPI_DOUBLE, MPI_ANY_SOURCE, camera, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      } else {
        for (camera = start_cam; camera < end_cam; ++camera)
          MPI_Send(ps[camera].data(), ps[camera].size(), MPI_DOUBLE, ROOT, camera, MPI_COMM_WORLD);
      }
    }
  }

  proj_timer.stop();
}
