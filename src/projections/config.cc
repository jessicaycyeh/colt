/*************************
 * projections/config.cc *
 *************************

 * Projections module configuration.

*/

#include "../proto.h"
#include "Projections.h"
#include "../config.h" // Configuration functions

static inline int key_of_known_field(const string& field) {
  if (field == "Density" || field == "rho")
    return Density;
  else if (field == "Volume" || field == "V")
    return Volume;
  else if (field == "Temperature" || field == "T")
    return Temperature;
  else if (field == "HydrogenDensity" || field == "n_H")
    return HydrogenDensity;
  else if (field == "LowerDensity" || field == "n_lower")
    return LowerDensity;
  else if (field == "UpperDensity" || field == "n_upper")
    return UpperDensity;
  else if (field == "HI_Fraction" || field == "x_HI")
    return HI_Fraction;
  else if (field == "HII_Fraction" || field == "x_HII")
    return HII_Fraction;
  else if (field == "H2_Fraction" || field == "x_H2")
    return H2_Fraction;
  else if (field == "HeI_Fraction" || field == "x_HeI")
    return HeI_Fraction;
  else if (field == "HeII_Fraction" || field == "x_HeII")
    return HeII_Fraction;
  else if (field == "ElectronAbundance" || field == "x_e")
    return ElectronAbundance;
  else if (field == "StarFormationRate" || field == "SFR")
    return StarFormationRate;
  else if (field == "DustTemperature" || field == "T_dust")
    return DustTemperature;
  else if (field == "VelocitySquared" || field == "v2")
    return VelocitySquared;
  else if (field == "VelocitySquaredLOS" || field == "v2_LOS")
    return VelocitySquaredLOS;
  else if (field == "VelocityLOS" || field == "v_LOS" || field == "vz")
    return VelocityLOS;
  else if (field == "VelocityX" || field == "v_x" || field == "vx")
    return VelocityX;
  else if (field == "VelocityY" || field == "v_y" || field == "vy")
    return VelocityY;
  else if (field == "MagneticFieldSquared" || field == "B2")
    return MagneticFieldSquared;
  else if (field == "MagneticFieldLOS" || field == "B_LOS" || field == "Bz")
    return MagneticFieldLOS;
  else if (field == "MagneticFieldX" || field == "B_x" || field == "Bx")
    return MagneticFieldX;
  else if (field == "MagneticFieldY" || field == "B_y" || field == "By")
    return MagneticFieldY;
  else if (field == "HydrogenFraction" || field == "X")
    return HydrogenFraction;
  else if (field == "Metallicity" || field == "Z")
    return Metallicity;
  else if (field == "DustMetallicity" || field == "D")
    return DustMetallicity;
  else if (field == "DensitySquared" || field == "rho2")
    return DensitySquared;
  else if (field == "IonizationFront" || field == "ion_front")
    return IonizationFront;
  // else if (field == "DustCoefficient" || field == "k_dust")
  //   return DustCoefficient;
  // else if (field == "DustDensity" || field == "rho_dust")
  //   return DustDensity;
  // else if (field == "LineCoefficient" || field == "k_0")
  //   return LineCoefficient;
  // else if (field == "DampingParameter" || field == "a")
  //   return DampingParameter;
  // else if (field == "NonLocalATau0" || field == "atau")
  //   return NonLocalATau0;
  error("Unrecognized field name: " + field);
  return 0;
}

static inline int key_of_known_weight(const string& weight) {
  if (weight == "Sum" || weight == "sum")
    return SUM;
  else if (weight == "Average" || weight == "avg" || weight == "mean")
    return AVG;
  else if (weight == "Mass" || weight == "mass")
    return MASS;
  return key_of_known_field(weight);
}

static string units_of_known_field(const int field) {
  switch (field) {
    case Density:                            // Density [g/cm^3]
      return "g/cm^3";
    case Volume:                             // Volume [cm^3]
      return "cm^3";
    case Temperature:                        // Temperature [K]
      return "K";
    case HydrogenDensity:                    // Hydrogen number density [cm^-3]
      return "cm^-3";
    case LowerDensity:                       // Number density of lower transition [cm^-3]
      return "cm^-3";
    case UpperDensity:                       // Number density of upper transition [cm^-3]
      return "cm^-3";
    case HI_Fraction:                        // x_HI = n_HI / n_H
      return "";
    case HII_Fraction:                       // x_HII = n_HII / n_H
      return "";
    case H2_Fraction:                        // x_H2 = n_H2 / n_H
      return "";
    case HeI_Fraction:                       // x_HeI = n_HeI / n_H
      return "";
    case HeII_Fraction:                      // x_HeII = n_HeII / n_H
      return "";
    case ElectronAbundance:                  // Volume [cm^3]
      return "";
    case StarFormationRate:                  // Star formation rate [Msun/yr]
      return "Msun/yr";
    case DustTemperature:                    // Dust temperature [K]
      return "K";
    case VelocitySquared:                    // Velocity^2 [cm^2/s^2]
      return "cm^2/s^2";
    case VelocitySquaredLOS:                 // Velocity_LOS^2 [cm^2/s^2]
      return "cm^2/s^2";
    case VelocityLOS:                        // Velocity_LOS [cm/s]
      return "cm/s";
    case VelocityX:                          // Velocity_X [cm/s]
      return "cm/s";
    case VelocityY:                          // Velocity_Y [cm/s]
      return "cm/s";
    case MagneticFieldSquared:               // Magnetic field^2 [G^2]
      return "G^2";
    case MagneticFieldLOS:                   // Magnetic field LOS [G]
      return "G";
    case MagneticFieldX:                     // Magnetic field X [G]
      return "G";
    case MagneticFieldY:                     // Magnetic field Y [G]
      return "G";
    case HydrogenFraction:                   // Mass fraction of hydrogen
      return "";
    case Metallicity:                        // Metallicity [mass fraction]
      return "";
    case DustMetallicity:                    // Dust metallicity [mass fraction]
      return "";
    case DensitySquared:                     // Density^2 [g^2/cm^6]
      return "g^2/cm^6";
    case IonizationFront:                    // Ionization front
      return "";
    default:
      error("Field units are not known: " + to_string(field));
  }
  return "";
}

/* General projection configuration. */
void Simulation::projection_config(YAML::Node& file, YAML::Node& node) {
  // Setup the projection depth
  if (file["proj_radius_bbox"]) {
    load("proj_radius_bbox", proj_radius_bbox); // Projection radius [min distance to bbox edge]
    if (proj_radius_bbox < 0.)
      error("proj_radius_bbox must be >= 0"); // Validate proj_radius_bbox range
    if (file["proj_radius"])
      error("Cannot specify both proj_radius and proj_radius_bbox in " + config_file);
    if (file["proj_depth_cMpc"])
      error("Cannot specify both proj_depth_cMpc and proj_radius_bbox in " + config_file);
    if (file["proj_depth"])
      error("Cannot specify both proj_depth and proj_radius_bbox in " + config_file);
  } else if (file["proj_radius"]) {
    load("proj_radius", proj_radius);        // Projection radius [cm] (proj_depth / 2)
    if (proj_radius < 0.)
      error("proj_radius must be >= 0");     // Validate proj_radius range
    if (file["proj_depth_cMpc"])
      error("Cannot specify both proj_depth_cMpc and proj_radius in " + config_file);
    if (file["proj_depth"])
      error("Cannot specify both proj_depth and proj_radius in " + config_file);
  } else if (file["proj_depth_cMpc"]) {
    load("proj_depth_cMpc", proj_depth_cMpc); // Projection depth [cMpc]
    if (proj_depth_cMpc < 0.)
      error("proj_depth_cMpc must be >= 0"); // Validate proj_depth_cMpc range
    if (file["proj_depth_cMpc"])
      error("Cannot specify both proj_depth and proj_depth_cMpc in " + config_file);
  } else {
    load("proj_depth", proj_depth);          // Projection depth [cm] (defaults to image_width)
    if (proj_depth < 0.)
      error("proj_depth must be >= 0");      // Validate proj_depth range
  }

  // Relative tolerence for adaptive convergence
  load("adaptive", adaptive);                // Adaptive convergence projections
  if (adaptive)
    load("pixel_rtol", pixel_rtol);          // Relative tolerance per pixel
  load("perspective", perspective);          // Perspective camera
}

// Some compilers allow duplicate (field, weight) pairs, so we avoid this
static inline void safe_insert(std::set<FieldWeightPair>& proj_set, const FieldWeightPair& fwp) {
  const bool is_in = proj_set.find(fwp) != proj_set.end();
  if (!is_in)
    proj_set.insert(fwp);                    // Only add if not found
}
static inline void safe_insert(std::set<FieldWeightPair>& proj_set, const int field, const int weight) {
  auto fwp = FieldWeightPair(field, weight); // Object to insert
  safe_insert(proj_set, fwp);                // Wrapper
}
static inline void safe_insert(std::set<FieldWeightPair>& proj_set, const string& f_str, const string& w_str) {
  safe_insert(proj_set, key_of_known_field(f_str), key_of_known_weight(w_str)); // Wrapper
}
static inline void safe_insert(std::set<FieldWeightPair>& proj_set, const std::pair<string,string>& fw) {
  safe_insert(proj_set, fw.first, fw.second); // Wrapper
}

/* Projections module configuration. */
void Projections::module_config(YAML::Node& file, YAML::Node& node) {
  // Information about the cameras
  camera_config(file, node);                 // General camera setup
  if (n_cameras <= 0)
    root_error("The projections module requires at least one camera "
               "but none were specified in " + config_file);
  if (n_cameras < n_ranks)
    root_error("Projections require n_cameras >= n_ranks to avoid idle processors.");

  projection_config(file, node);             // General projection setup

  // List of fields
  std::set<FieldWeightPair> proj_set;
  if (file["field_weight_pairs"]) {
    vector<std::pair<string,string>> field_weight_pairs;
    load("field_weight_pairs", field_weight_pairs); // Projection fields and weights
    for (const auto& fw : field_weight_pairs)
      safe_insert(proj_set, fw);             // Save unique pairs
  } else {
    strings projection_fields;               // Projection field names
    if (file["fields"]) {
      load("fields", projection_fields);     // Projection fields
    } else {
      projection_fields = { "Density" };     // Default denisty projection
    }

    strings projection_weights;              // Projection weight names
    if (file["weights"])
      load("weights", projection_weights);   // Projection weights

    const size_t n_proj = projection_fields.size();
    while (projection_weights.size() > n_proj)
      projection_weights.pop_back();         // Remove extra weights
    while (projection_weights.size() < n_proj)
      projection_weights.push_back("avg");   // Default to volume averages

    // Remove duplicate fields
    for (size_t i = 0; i < n_proj; ++i)
      safe_insert(proj_set, projection_fields[i], projection_weights[i]);
  }

  // Add related weight sums
  std::set<int> proj_set_weights;
  bool have_mass_weight = false;             // Check for mass-weights
  for (auto& fw : proj_set) {
    if (fw.weight == MASS)
      have_mass_weight = true;               // Need mass-weight denominator
    else if (fw.weight >= 0)
      proj_set_weights.insert(fw.weight);
  }
  if (have_mass_weight)
    safe_insert(proj_set, Density, AVG);     // Require: sum(rho dl)
  for (auto& weight : proj_set_weights)
    safe_insert(proj_set, weight, SUM);      // Require: sum(w dl/V)

  // Save unique projections list
  n_projections = proj_set.size();           // Number of projections
  proj_list.reserve(n_projections);          // Reserve list size
  vector<int> active_flags(n_fields);        // Set active field flags
  for (auto& fw : proj_set) {
    active_flags[fw.field] = 1;              // Ensure field is active
    if (fw.weight >= 0)                      // Avoid weight alias range
      active_flags[fw.weight] = 1;           // Ensure weight is active
    proj_list.push_back(fw);                 // Copy to projections list
  }
  active_flags[Density] = 1;                 // Density is always active
  active_flags[Volume] = 1;                  // Volume is always active
  for (int field = 0; field < n_fields; ++field)
    if (active_flags[field])
      active_fields.push_back(field);        // Save list of active fields
  n_active_fields = active_fields.size();    // Save number of active fields

  // Nonstandard read flags
  if (active_flags[StarFormationRate])
    read_SFR = true;
  if (active_flags[HII_Fraction])
    read_ionized_fraction = true;
  if (active_flags[H2_Fraction])
    read_molecular_fraction = true;
  if (active_flags[HeI_Fraction] || active_flags[HeII_Fraction])
    read_helium = true;
  if (active_flags[ElectronAbundance])
    read_electron_fraction = true;
  if (active_flags[HydrogenFraction])
    read_hydrogen_fraction = true;
  if (active_flags[DustTemperature])
    read_T_dust = true;
  if (active_flags[VelocitySquared])
    read_v2 = true;
  if (active_flags[VelocitySquaredLOS])
    read_v2_LOS = true;
  if (active_flags[VelocityLOS])
    read_v_LOS = true;
  if (active_flags[VelocityX])
    read_v_x = true;
  if (active_flags[VelocityY])
    read_v_y = true;
  if (active_flags[MagneticFieldSquared])
    read_B2 = true;
  if (active_flags[MagneticFieldLOS])
    read_B_LOS = true;
  if (active_flags[MagneticFieldX])
    read_B_x = true;
  if (active_flags[MagneticFieldY])
    read_B_y = true;
  if (read_B2 || read_B_LOS || read_B_x || read_B_y)
    read_B = true;
  if (active_flags[DensitySquared])
    read_rho2 = true;
  if (active_flags[IonizationFront])
    read_ion_front = true;
  if (file["constant_temperature"]) {
    load("constant_temperature", constant_temperature); // Constant temperature: T [K]
    if (constant_temperature < 0.)
      error("constant_temperature must be positive"); // Validate constant_temperature range
  } else if (file["T_floor"]) {
    load("T_floor", T_floor);                // Apply a temperature floor: T [K]
    if (T_floor < 0.)
      error("T_floor must be positive");     // Validate T_floor range
  }
  if (file["neutral_fraction"]) {
    load("neutral_fraction", neutral_fraction); // Constant neutral fraction: x_HI
    if (neutral_fraction < 0. || neutral_fraction > 1.)
      error("neutral_fraction must be in the range [0,1]"); // Validate neutral_fraction range
  }
  if (file["metallicity"]) {
    load("metallicity", metallicity);        // Constant metallicity [mass fraction]
    if (metallicity < 0. || metallicity > 1.)
      error("metallicity must be in the range [0,1]"); // Validate metallicity range
  }
  if (file["dust_to_metal"]) {
    if (file["dust_to_gas"])
      error("Cannot specify both dust_to_metal and dust_to_gas");
    load("dust_to_metal", dust_to_metal);    // Constant dust-to-metal ratio
    if (dust_to_metal < 0. || dust_to_metal > 1.)
      error("dust_to_metal must be in the range [0,1]"); // Validate dust_to_metal range
  }
  if (file["dust_to_gas"]) {
    load("dust_to_gas", dust_to_gas);        // Constant dust-to-gas ratio
    if (dust_to_gas < 0. || dust_to_gas > 1.)
      error("dust_to_gas must be in the range [0,1]"); // Validate dust_to_gas range
  }
  if (file["dust_boost"]) {
    load("dust_boost", dust_boost);          // Optional dust boost factor
    if (dust_boost <= 0.)
      error("dust_boost must be positive");  // Validate dust_boost range
  }
  load("set_density_from_mass", set_density_from_mass); // Calculate density as mass / volume
  load("read_density_as_mass", read_density_as_mass); // Read density as mass

  // Setup projection units
  projection_units.resize(n_projections);
  for (int i = 0; i < n_projections; ++i)
    projection_units[i] = units_of_known_field(proj_list[i].field);
}
