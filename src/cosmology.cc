/*!
 * \file    cosmology.cc
 *
 * \brief   General cosmology functions.
 *
 * \details This file contains code related to general cosmology calculations.
 */

#include "proto.h"
#include "Simulation.h"

/*! \brief Calculate the luminosity distance [cm].
 *
 *  The luminosity distance is \f$d_\text{L} = (1+z) \int_0^z c\,\text{d}z' / H(z')\f$
 *  and is caluclated by trapezoid integration with 1000 redshift bins.
 *
 *  \param[in] z Cosmological redshift.
 *
 *  \return Luminosity distance [cm].
 */
double Simulation::calculate_d_L(const double z) {
  // Recall: d_L = (1+z) Integrate[c/H(zp), {zp, 0, z}]
  const double OmegaLambda = 1. - Omega0;    // Dark energy density
  const double H0 = 100. * h100 * km / Mpc;  // Hubble constant [1/s]
  const double R_H0 = c / H0;                // Hubble radius [cm]
  const double dz = z / 999.;                // Equal redshift intervals
  double zp = 0., zp1 = 1. + z;
  double sum = -0.5 * (R_H0 + R_H0 / sqrt(OmegaLambda + Omega0 * zp1*zp1*zp1));
  for (int i = 0; i < 1000; ++i, zp += dz) {
    zp1 = 1. + zp;
    sum += R_H0 / sqrt(OmegaLambda + Omega0 * zp1*zp1*zp1);
  }
  return (1. + z) * sum * dz;
}
