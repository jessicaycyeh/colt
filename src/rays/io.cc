/**************
 * rays/io.cc *
 **************

 * All I/O operations related to the rays module.

*/

#include "../proto.h"
#include "Rays.h"
#include "../io_hdf5.h" // Module HDF5 read/write functions
#include "../timing.h" // Timing functionality

extern Timer read_timer;
extern Timer write_timer;

double calculate_d_L(const double z); // Luminosity distance [cm]
void write_sim_info(const H5File& f); // Write general simulation information

/* Function to print additional rays information. */
void Rays::print_info() {
  cout << "\nCamera parameters:"
       << "\n  n_cameras  = " << n_cameras;
  if (n_exp >= 0)
    cout << "\n  n_exp      = " << n_exp << " (Note: Healpix in RING ordering)";
  cout << endl;
  if (verbose)
    print("  directions", camera_directions);
}

/* Read top level information. */
void Rays::read_info() {
  // Setup file naming scheme
  pre_filename = init_dir + (init_dir.empty() ? "" : "/") + "snapdir" +
                 snap_str + "/" + init_base + snap_str + ".";
  const string filename = sub_file(0);       // Read from the first file

  // Open file and read info
  H5File f(filename, H5F_ACC_RDONLY);

  // Initialize cosmology information
  Group g = f.openGroup("Header");           // Info is in the Header group
  read(g, "Redshift", z);                    // Current simulation redshift
  read(g, "HubbleParam", h100);              // Hubble constant [100 km/s/Mpc]
  read(g, "Omega0", Omega0);                 // Matter density [rho_crit_0]
  read(g, "OmegaBaryon", OmegaB);            // Baryon density [rho_crit_0]
  d_L = calculate_d_L(z);                    // Luminosity distance [cm]

  // Check length, mass, and velocity units
  read(g, "UnitLength_in_cm", unit_length);  // Unit length [cm]
  read(g, "UnitMass_in_g", unit_mass);       // Unit mass [g]
  read(g, "UnitVelocity_in_cm_per_s", unit_velocity); // Unit velocity [cm/s]
  if (fabs(1. - unit_length / kpc) > 0.01)
    root_error("UnitLength_in_cm is not kpc: " + to_string(unit_length));
  if (fabs(1. - unit_mass / (1e10*Msun)) > 0.01)
    root_error("UnitMass_in_g is not 10^10 Msun: " + to_string(unit_mass));
  if (fabs(1. - unit_velocity / km) > 0.01)
    root_error("UnitVelocity_in_cm_per_s is not km/s: " + to_string(unit_velocity));

  // Initialize ray information
  read(g, "NumFilesPerSnapshot", n_files_tot); // Number of files per snapshot
  if (n_files_tot < n_ranks)
    root_error("Cannot have more MPI ranks than subfiles: " + to_string(n_files_tot));
  read(g, "BoxSize", box_size);              // Simulation box size [a*length/h]
  box_half = box_size / 2.;                  // Half box size [a*length/h]

  // File assignments
  n_files = n_files_tot / n_ranks;           // Equal assignment
  first_file = rank * n_files;               // File start range
  const int remainder = n_files_tot - n_files * n_ranks;
  if (n_ranks - rank <= remainder) {
    ++n_files;                               // Assign remaining work
    first_file += remainder - n_ranks + rank; // Correct file range
  }
}

/* Read file offsets information. */
void Rays::read_file_offsets_info() {
  // Setup offsets file naming scheme
  const string filename = init_dir + (init_dir.empty() ? "" : "/")
    + "../postprocessing/offsets/offsets" + snap_str + ".hdf5";

  // Open file and read info
  H5File f(filename, H5F_ACC_RDONLY);

  // Initialize halo information
  Group g = f.openGroup("FileOffsets");      // File offsets
  read(g, "Group", FileOffsetsGroup);        // File group offsets
  read(g, "Subhalo", FileOffsetsSubhalo);    // File subhalo offsets
}

/* Read particle offsets information. */
void Rays::read_offsets_info(const int s0) {
  // Setup offsets file naming scheme
  const string filename = init_dir + (init_dir.empty() ? "" : "/")
    + "../postprocessing/offsets/offsets" + snap_str + ".hdf5";

  // Open file and read info
  H5File f(filename, H5F_ACC_RDONLY);

  // Initialize halo information
  Group s = f.openGroup("Subhalo");          // Subhalo
  read(s, "SnapByType", SubhaloOffsetsType, s0); // Subhalo particle offsets by type

  Group g = f.openGroup("FileOffsets");      // File offsets
  read(g, "SnapByType", FileOffsetsType);    // File particle offsets by type
}

/* Read information about the most massive black hole of the subhalo. */
void Rays::read_max_black_hole_info() {
  // Set the black hole offset and count
  read_offsets_info(subhalo_id);
  const long long n_bh = SubhaloLenType[5];
  const long long bh_offset = SubhaloOffsetsType[5];
  const long long bh_last = bh_offset + n_bh - 1;
  if (n_bh <= 0)
    root_error("There are no black holes in the selected subhalo!");

  // Check the min/max file range
  int bh_file;
  for (bh_file = 0; bh_file < n_files_tot; ++bh_file) {
    if (FileOffsetsType[bh_file][5] > bh_offset) {
      --bh_file;                             // Undo last check
      break;                                 // Completed search
    }
  }
  int bh_last_file;
  for (bh_last_file = bh_file; bh_last_file < n_files_tot; ++bh_last_file) {
    if (FileOffsetsType[bh_last_file][5] > bh_last) {
      --bh_last_file;                        // Undo last check
      break;                                 // Completed search
    }
  }
  if (bh_last_file > bh_file)
    error("Subhalo black holes spread over multiple files is not allowed");
  const long long bh_local = bh_offset - FileOffsetsType[bh_file][5];
  FileOffsetsType = vector<array<long long,6>>(); // Free memory

  // Setup fof file naming scheme
  pre_fof_name = init_dir + (init_dir.empty() ? "" : "/") + "snapdir" +
                 snap_str + "/snap" + snap_str + ".";
  const string filename = fof_sub_file(bh_file); // Read from the first file

  // Open file and read info
  H5File f(filename, H5F_ACC_RDONLY);

  // Initialize halo information
  Group p5 = f.openGroup("PartType5");       // Black hole particle data
  float max_BH_Mass = 0.;                    // Most massive black hole mass
  long long i_bh_max = 0;                    // Index of most massive black hole
  for (long long i_bh = 0; i_bh < n_bh; ++i_bh) {
    float BH_Mass;
    read(p5, "BH_Mass", BH_Mass, bh_local+i_bh); // Black hole masses
    if (BH_Mass > max_BH_Mass) {
      i_bh_max = i_bh;
      max_BH_Mass = BH_Mass;                 // Update the mass and index
    }
  }

  // Set the ray origin to the black hole position
  read(p5, "Coordinates", ray_origin, bh_local+i_bh_max); // [a*length/h]
  read(p5, "Velocities", ray_systemic, bh_local+i_bh_max); // [sqrt(a)*velocity]
  ray_systemic /= sqrt(1. + z);              // Convert to km/s
}

/* Read information from the Lyman-alpha subhalo catalog. */
void Rays::read_lyman_alpha_info() {
  // Setup Lya file naming scheme
  const string filename = init_dir + (init_dir.empty() ? "" : "/")
    + "../postprocessing/Lya/Lya" + snap_str + ".hdf5";

  // Open file and read info
  H5File f(filename, H5F_ACC_RDONLY);

  // Set the ray origin to the Lyman-alpha center of luminosity
  Group s = f.openGroup("Subhalo");          // Subhalo
  read(s, "LyaPos", ray_origin, subhalo_id); // [a*length/h]
  read(s, "LyaVel", ray_systemic, subhalo_id); // [km/s]
}

/* Read top level halo information. */
void Rays::read_fof_info() {
  // Setup fof file naming scheme
  pre_fof_name = init_dir + (init_dir.empty() ? "" : "/") + "groups" +
                 snap_str + "/" + fof_base + snap_str + ".";
  // Determine which files to read
  int s_file, g_file;
  read_file_offsets_info();                  // Populate file offsets
  if (select_subhalo) {
    subhalo_id = halo_num;                   // Specified by Subhalo ID
    for (s_file = 0; s_file < n_files_tot; ++s_file) {
      if (int(FileOffsetsSubhalo[s_file]) > subhalo_id) {
        --s_file;                            // Undo last check
        break;                               // Completed search
      }
    }
    const string filename = fof_sub_file(s_file); // Read from the file
    H5File f(filename, H5F_ACC_RDONLY);      // Open file and read info
    Group header = f.openGroup("Header");    // Counts are in the Header group
    long long Nsubgroups_Total;
    read(header, "Nsubgroups_Total", Nsubgroups_Total); // Total number of subhalos
    if (int(Nsubgroups_Total) <= subhalo_id)
      error("Requested an out of range subhalo ID");
    const int s0 = subhalo_id - int(FileOffsetsSubhalo[s_file]); // Subhalo ID (local)
    Group s = f.openGroup("Subhalo");        // FoF subhalo
    read(s, "SubhaloGrNr", group_id, s0);    // Read the Group ID
    for (g_file = 0; g_file < n_files_tot; ++g_file) {
      if (int(FileOffsetsGroup[g_file]) > group_id) {
        --g_file;                            // Undo last check
        break;                               // Completed search
      }
    }
  } else {
    group_id = halo_num;                     // Specified by Group ID
    for (g_file = 0; g_file < n_files_tot; ++g_file) {
      if (int(FileOffsetsGroup[g_file]) > group_id) {
        --g_file;                            // Undo last check
        break;                               // Completed search
      }
    }
    const string filename = fof_sub_file(g_file); // Read from the file
    H5File f(filename, H5F_ACC_RDONLY);      // Open file and read info
    Group header = f.openGroup("Header");    // Counts are in the Header group
    long long Ngroups_Total;
    read(header, "Ngroups_Total", Ngroups_Total); // Total number of groups
    if (int(Ngroups_Total) <= group_id)
      error("Requested an out of range group ID");
    const int g0 = group_id - int(FileOffsetsGroup[g_file]); // Group ID (local)
    Group g = f.openGroup("Group");          // FoF group
    read(g, "GroupFirstSub", subhalo_id, g0); // Read the Subhalo ID
    for (s_file = 0; s_file < n_files_tot; ++s_file) {
      if (int(FileOffsetsSubhalo[s_file]) > subhalo_id) {
        --s_file;                            // Undo last check
        break;                               // Completed search
      }
    }
  }
  const int s0 = subhalo_id - int(FileOffsetsSubhalo[s_file]); // Subhalo ID (local)
  const int g0 = group_id - int(FileOffsetsGroup[g_file]); // Group ID (local)
  FileOffsetsSubhalo = vector<long long>();  // Free memory
  FileOffsetsGroup = vector<long long>();

  // Open group file and read info
  {
    const string filename = fof_sub_file(g_file); // Read from the group file
    H5File f(filename, H5F_ACC_RDONLY);
    Group g = f.openGroup("Group");          // FoF group
    read(g, "Group_R_Crit200", R_Crit200, g0); // Halo virial radius [a*length/h]
  }

  // Open subhalo file and read info
  {
    const string filename = fof_sub_file(s_file); // Read from the subhalo file
    H5File f(filename, H5F_ACC_RDONLY);
    Group s = f.openGroup("Subhalo");        // FoF subhalo
    read(s, "SubhaloMass", M_halo, s0);      // Halo virial mass [mass/h]
    read(s, "SubhaloPos", r_halo, s0);       // Halo position [a*length/h]
    read(s, "SubhaloCM", r_com, s0);         // Halo center of mass [a*length/h]
    read(s, "SubhaloVel", v_halo, s0);       // Halo velocity [km/s]
    read(s, "SubhaloMassInRadType", SubhaloMassInRadType, s0); // Subhalo mass in radius [mass/h]
    read(s, "SubhaloMassType", SubhaloMassType, s0); // Subhalo mass in halo [mass/h]
    read(s, "SubhaloLenType", SubhaloLenType, s0); // Subhalo particle counts by type
  }

  // Determine ray origin based on halo config options
  if (black_hole_origin)
    read_max_black_hole_info();              // Set by most massive black hole
  else if (lyman_alpha_origin)
    read_lyman_alpha_info();                 // Set by Lyman-alpha catalog
  else {
    ray_origin = r_halo;                     // Default to halo center [a*length/h]
    ray_systemic = v_halo;                   // Default to halo velocity [km/s]
  }
}

/* Read subfile dataset sizes. */
static inline int get_file_sizes(const string filename) {
  H5File f(filename, H5F_ACC_RDONLY);        // Open file
  return get_size(f, "PartType0/Density");   // Infer dataset size
}

/* Allocate space for each file. */
void Rays::setup_dataspace() {
  file_sizes.resize(n_files);                // Allocate space for each file
  max_file_size = 0;                         // Largest assigned file size
  for (int i = 0; i < n_files; ++i) {
    file_sizes[i] = get_file_sizes(sub_file(i)); // Infer the dataset sizes
    if (file_sizes[i] > max_file_size)
      max_file_size = file_sizes[i];         // Update the largest file size
  }
}

/* Initialize active fields. Units(a, h, length, mass, velocity, to_cgs) */
void Rays::setup_fields() {
  r.reserve(max_file_size);                  // Allocate max space
  vel.data.reserve(max_file_size);
  for (int field = 0; field < n_copy_fields; ++field)
    fields[field].data.reserve(max_file_size); // Copy fields

  const double dimensionless = 0.;
  const double unit_volume = unit_length * unit_length * unit_length; // L^3
  const double unit_density = unit_mass / unit_volume; // M / L^3
  const double unit_velocity2 = unit_velocity * unit_velocity; // V^2
  const double unit_SFR = unit_mass * unit_velocity / unit_length; // M V / L

  // Ray fields
  fields[Density].name = "Density";          // Gas density [h^2/a^3 * mass/length^3]
  fields[Density].units = Units(-3., 2., -3., 1., 0., unit_density);

  fields[DustMetallicity].name = "GFM_DustMetallicity"; // Dust-to-gas ratio
  fields[DustMetallicity].units = Units(0., 0., 0., 0., 0., dimensionless);

  fields[Metallicity].name = "GFM_Metallicity";  // Gas metallicity
  fields[Metallicity].units = Units(0., 0., 0., 0., 0., dimensionless);

  fields[HI_Fraction].name = "HI_Fraction";  // HI fraction
  fields[HI_Fraction].units = Units(0., 0., 0., 0., 0., dimensionless);

  fields[ElectronAbundance].name = "ElectronAbundance"; // Electron abundance
  fields[ElectronAbundance].units = Units(0., 0., 0., 0., 0., dimensionless);

  fields[InternalEnergy].name = "InternalEnergy"; // Internal energy [velocity^2]
  fields[InternalEnergy].units = Units(0., 0., 0., 0., 2., unit_velocity2);

  fields[StarFormationRate].name = "StarFormationRate"; // Star formation rate [Msun/yr]
  fields[StarFormationRate].units = Units(0., 0., -1., 1., 1., unit_SFR);

  fields[Velocity].name = "Velocity"; // Velocity [velocity]
  fields[Velocity].units = Units(0.5, 0., 0., 0., 1., unit_velocity);

  fields[Masses].name = "Masses";            // Gas mass [mass/h]
  fields[Masses].units = Units(0., -1., 0., 1., 0., unit_mass);
}

static inline void verify_units(DataSet& d, const string& name, const double value) {
  if (H5Aexists(d.getId(), name.c_str()) > 0) {
    double attr;                             // Attribute value
    Attribute a = d.openAttribute(name);     // Access the attribute
    a.read(PredType::NATIVE_DOUBLE, &attr);  // Read attribute from file
    if (attr != value)
      error("Unexpected units for " + name);
  }
}

/* Verify the units of a specified dataset. */
static inline void verify_units(DataSet& d, Units& units) {
  verify_units(d, "a_scaling", units.a_scaling);
  verify_units(d, "h_scaling", units.h_scaling);
  verify_units(d, "length_scaling", units.length_scaling);
  verify_units(d, "mass_scaling", units.mass_scaling);
  verify_units(d, "velocity_scaling", units.velocity_scaling);
}

/* Read a file dataset. */
static inline void read(const string& name, vector<float>& data, Units& units,
                        const hsize_t size, const string& filename) {
  H5File f(filename, H5F_ACC_RDONLY);        // Open file
  verify_field(f, name);                     // Ensure the dataset exists
  DataSet d = f.openDataSet(name);           // Access the dataset
  verify_shape(f, name, d, {size});          // Ensure it has the expected shape
  d.read(data.data(), PredType::NATIVE_FLOAT); // Read dataset from file
  verify_units(d, units);                    // Verify it has the expected units
}

/* Read a file Vec3 dataset. */
template <typename T>
static inline void read(const string& name, vector<GenericVec3<T>>& data, Units& units,
                        const hsize_t size, const string& filename) {
  H5File f(filename, H5F_ACC_RDONLY);        // Open file
  verify_field(f, name);                     // Ensure the dataset exists
  DataSet d = f.openDataSet(name);           // Access the dataset
  verify_shape(f, name, d, {size, 3});       // Ensure it has the expected shape
  d.read(data.data(), H5PT);                 // Read dataset from file
  verify_units(d, units);                    // Verify it has the expected units
}

/* Read full data after initial setup. */
void Rays::read_data(const int file) {
  const int size = file_sizes[file];         // Array sizes
  const string sfile = sub_file(file);       // Subfile name

  // Read particle positions
  r.resize(size);                            // Ensure data space availability
  auto r_units = Units(1., -1., 1., 0., 0., unit_length);
  read("PartType0/Coordinates", r, r_units, size, sfile);

  // Read particle velocities
  vel.data.resize(size);                     // Ensure data space availability
  auto v_units = fields[Velocity].units;
  read("PartType0/Velocities", vel.data, v_units, size, sfile);

  // Read all copy datasets
  for (int field = 0; field < n_read_fields; ++field) {
    RayField& fld = fields[field];           // RayField struct
    fld.data.resize(size);                   // Adjust the data size
    read("PartType0/" + fld.name, fld.data, fld.units, size, sfile);
  }
}

/* Calculate the maximum cell radius. */
double Rays::calculate_max_cell_radius() {
  auto& dens_fld = fields[Density];          // Density field
  auto& mass_fld = fields[Masses];           // Masses field
  auto& dens = dens_fld.data;                // Local references
  auto& mass = mass_fld.data;
  mass.reserve(max_file_size);               // Allocate max space
  float V_max = 0.;
  for (int file = 0; file < n_files; ++file) {
    const int size = file_sizes[file];
    dens.resize(size);                       // Ensure data space availability
    mass.resize(size);
    read("PartType0/" + dens_fld.name, dens, dens_fld.units, size, sub_file(file));
    read("PartType0/" + mass_fld.name, mass, mass_fld.units, size, sub_file(file));
    #pragma omp parallel for reduction(max:V_max)
    for (int i = 0; i < size; ++i) {
      if (mass[i] > V_max * dens[i])
        V_max = mass[i] / dens[i];
    }
  }
  mass = vector<float>();                    // Free memory

  // Distribute the global max
  if (n_ranks > 1)
    MPI_Allreduce(MPI_IN_PLACE, &V_max, 1, MPI_FLOAT, MPI_MAX, MPI_COMM_WORLD);

  return 0.75 * pow(V_max, 1./3.);           // Allow distorted Voronoi cells
  // return pow(3. * V_max / (4. * M_PI), 1./3.); // Sphere: (3 V_max / 4 pi)^1/3 = 0.62 V_max^1/3
}

/* Write dataset unit scalings. */
template <typename T>
static inline void write(T& f, Units& units) {
  write(f, "a_scaling", units.a_scaling);
  write(f, "h_scaling", units.h_scaling);
  write(f, "length_scaling", units.length_scaling);
  write(f, "mass_scaling", units.mass_scaling);
  write(f, "velocity_scaling", units.velocity_scaling);
  write(f, "to_cgs", units.to_cgs);
}

/* Reads rays data and info from the hdf5 files. */
void Rays::read_hdf5() {
  read_timer.start();

  read_info();                               // Read top level info
  setup_dataspace();                         // Allocate space for each file
  setup_fields();                            // Initialize active fields
  const double zp1 = 1. + z;                 // 1/a factor
  const double OmegaLambda = 1. - Omega0;    // Dark energy density (flat cosmology)
  const double Hz = 100. * h100 * sqrt(Omega0 * zp1*zp1*zp1 + OmegaLambda); // Hubble parameter [km/s/Mpc]
  if (r_max_kpc > 0.)
    r_max = h100 * zp1 * r_max_kpc;          // Pre-specified cylinder radius [kpc]
  else
    r_max = calculate_max_cell_radius();     // Cylinder radius [ckpc/h]
  if (fof_origin)
    read_fof_info();                         // Read top level halo info

  // Calculate derived quantities
  r2_max = r_max * r_max;                    // Cylinder radius^2 [ckpc/h]^2
  if (start_fvir > 0.)
    r_start = start_fvir * R_Crit200;        // Specify starting radius from f_vir
  else
    r_start = 1e3 * start_cMpc * h100;       // Specify starting radius from cMpc
  if (length_kms > 0.)
    length_Mpc = length_kms / Hz;
  if (length_Mpc > 0.)
    length_cMpc = zp1 * length_Mpc;          // Ray length [cMpc]
  length_Mpc = length_cMpc / zp1;            // Ray length [Mpc]
  length_kms = Hz * length_Mpc;              // Ray length proxy [km/s]
  r_len = 1e3 * length_cMpc * h100;          // Specify ray lengths from cMpc
  r_end = r_start + r_len;                   // Add start offset
  r_start_buffer = r_start - r_max;          // Add padding
  r_end_buffer = r_end + r_max;
  r2_start_buffer = r_start_buffer * r_start_buffer;
  r2_end_buffer = r_end_buffer * r_end_buffer;
  if (r_start_buffer < 0.)                   // Ensure consistency
    r2_start_buffer *= -1.;
  if (r_end_buffer < 0.)
    r2_end_buffer *= -1.;
  bbox[0] = {-r_max, -r_max, -r_max};        // Bounding box min [a*length/h]
  bbox[1] = {r_max, r_max, r_max + r_len};   // Bounding box max [a*length/h]
  max_bbox_width = 2.*r_max + r_len;         // Max bbox width [a*length/h]

  // Print the initial conditions data
  if (root) {
    const double length_to_kpc = 1. / (h100 * zp1);
    const double length_to_Mpc = 1e-3 * length_to_kpc;
    const double length_to_cMpc = 1e-3 / h100;
    const double length_to_kms = Hz * length_to_Mpc;
    cout << "\nSimulation info: [Rays]"
         << "\n  redshift   = " << z;
    if (verbose)
      cout << "\n  Omega0     = " << Omega0 << " rho_crit_0"
           << "\n  OmegaB     = " << OmegaB << " rho_crit_0"
           << "\n  H_0        = " << 100. * h100 << " km/s/Mpc"
           << "\n  H(z)       = " << Hz << " km/s/Mpc"
           << "\n  d_L        = " << d_L / Gpc << " Gpc"
           << "\n  n_files    = " << n_files_tot;
    cout << "\n  box_size   = " << 1e-3 * box_size << " cMpc/h = "
                                << box_size * length_to_cMpc << " cMpc = "
                                << box_size * length_to_Mpc << " Mpc = "
                                << box_size * length_to_kms << " km/s"
         << "\n  r_length   = " << 1e-3 * r_len << " cMpc/h = "
                                << r_len * length_to_cMpc << " cMpc = "
                                << r_len * length_to_Mpc << " Mpc = "
                                << r_len * length_to_kms << " km/s"
         << "\n  r_cylinder = " << r_max * length_to_kpc << " kpc" << endl;
    if (fof_origin) {
      const double dr_com = r_halo.dist(r_com);
      cout << "\nSelected halo " << subhalo_id << " from group " << group_id
           << "\n  R_Crit200  = " << R_Crit200 << " ckpc/h = " << R_Crit200*length_to_kpc << " kpc"
           << "\n  halo mass  = " << M_halo * unit_mass / (h100 * Msun) << " Msun"
           << "\n  position   = " << r_halo * length_to_Mpc << " Mpc"
           << "\n  centroid   = " << r_com * length_to_Mpc << " Mpc"
           << "\n  offset     = " << dr_com * length_to_kpc << " kpc"
           << "\n  velocity   = " << v_halo << " km/s" << endl;
      print("  mass (rad)", SubhaloMassInRadType, "Msun", Msun*h100/unit_mass);
      print(" mass (halo)", SubhaloMassType, "Msun", Msun*h100/unit_mass);
      print(" n_particles", SubhaloLenType);
      if (black_hole_origin) {
        print("part offsets", SubhaloOffsetsType);
        cout << "  black hole = " << ray_origin * length_to_Mpc << " Mpc" << endl;
      }
      if (lyman_alpha_origin)
        cout << "  Lya center = " << ray_origin * length_to_Mpc << " Mpc" << endl;
      if (black_hole_origin || lyman_alpha_origin) {
        cout << "  v_systemic = " << ray_systemic << " km/s\n"
             << "  | v_sys |  = " << ray_systemic.norm() << " km/s" << endl;
      }
    } else if (multiple_origins) {
      cout << "\nUsing distinct ray origins" << endl;
      if (verbose)
        print("  origins   ", ray_origins, "box size");
    } else {
      cout << "\nUser specified ray origin: " << ray_origin << " box size" << endl;
    }
  }
  read_timer.stop();
}

/* Create cylinder data groups. */
void Rays::create_data_groups(const string& prefix, const int file) {
  // Open file and write header
  H5File f(output_dir + "/" + prefix + snap_str + halo_str + "." +
           to_string(first_file + file) + "." + output_ext, H5F_ACC_TRUNC);

  // Set up the file group structure
  for (int field = 0; field < n_copy_fields; ++field)
    Group g = f.createGroup(fields[field].name); // Field groups
  Group g = f.createGroup("Coordinates");    // Coordinates group
}

/* Writes consolidated cylinder file. */
void Rays::write_cylinder(const int ray) {
  // Open file and write header
  H5File f(output_dir + "/cylinder" + snap_str + halo_str + "_" + to_string(ray) + "." + output_ext, H5F_ACC_TRUNC);

  // Write position and field datasets
  write(f, "Coordinates", r);                // Write positions data
  for (int field = 0; field < n_copy_fields; ++field) {
    auto& fld = fields[field];               // RayField struct
    write(f, fld.name, fld.data);            // Write field data
  }
}

/* Create final rays data file. */
void Rays::create_rays_file(const string& filename) {
  // Open file and write header
  H5File f(filename, H5F_ACC_TRUNC);

  // Write general simulation information
  write(f, "Redshift", z);                   // Current simulation redshift
  write(f, "HubbleParam", h100);             // Hubble constant [100 km/s/Mpc]
  write(f, "Omega0", Omega0);                // Matter density [rho_crit_0]
  write(f, "OmegaBaryon", OmegaB);           // Baryon density [rho_crit_0]
  write(f, "UnitLength_in_cm", unit_length); // Unit length [cm]
  write(f, "UnitMass_in_g", unit_mass);      // Unit mass [g]
  write(f, "UnitVelocity_in_cm_per_s", unit_velocity); // Unit velocity [cm/s]
  write(f, "BoxSize", box_size);             // Simulation box size [a*length/h]

  write(f, "MultipleOrigins", multiple_origins);
  if (fof_origin) {
    // Subhalo information
    write(f, "GroupID", group_id);           // Group ID
    write(f, "SubhaloID", subhalo_id);       // Subhalo ID
    write(f, "R_Crit200", R_Crit200);        // Halo virial radius [a*length/h]
    write(f, "SubhaloMass", M_halo);         // Halo virial mass [mass/h]
    write_attr(f, "SubhaloPos", r_halo);     // Halo position [a*length/h]
    write_attr(f, "SubhaloCM", r_com);       // Halo center of mass [a*length/h]
    write_attr(f, "SubhaloVel", v_halo);     // Halo velocity [km/s]
    write_attr(f, "SubhaloMassInRadType", SubhaloMassInRadType); // Subhalo mass in radius [mass/h]
    write_attr(f, "SubhaloMassType", SubhaloMassType); // Subhalo mass in halo [mass/h]
    write_attr(f, "SubhaloLenType", SubhaloLenType); // Subhalo particle counts by type
    if (black_hole_origin)
      write_attr(f, "SubhaloOffsetsType", SubhaloOffsetsType); // Subhalo particle offsets by type
    write_attr(f, "SystemicVelocity", ray_systemic); // Systemic velocity [km/s]
  }

  // Rays information
  vector<Vec3> origins;                      // Ray origin points [a*length/h]
  vector<Vec3> endings;                      // Ray ending points [a*length/h]
  origins.resize(n_cameras);
  endings.resize(n_cameras);

  if (multiple_origins) {
    #pragma omp parallel for
    for (int ray = 0; ray < n_cameras; ++ray) {
      origins[ray] = ray_origins[ray] + camera_directions[ray] * r_start;
      endings[ray] = ray_origins[ray] + camera_directions[ray] * (r_start + r_len);
    }
  } else {
    #pragma omp parallel for
    for (int ray = 0; ray < n_cameras; ++ray) {
      origins[ray] = ray_origin + camera_directions[ray] * r_start;
      endings[ray] = ray_origin + camera_directions[ray] * (r_start + r_len);
    }
  }
  write(f, "RayImpact", r_max);              // Cylinder radius [a*length/h]
  write(f, "RaySphere", r_start);            // Ray starting radius [a*length/h]
  write(f, "RayLength", r_len);              // Ray length [a*length/h]
  if (fof_origin && start_fvir > 0.)
    write(f, "RaySphere_fvir", start_fvir);  // Ray starting radius [fvir]
  else if (start_cMpc > 0.)
    write(f, "RaySphere_cMpc", start_cMpc);  // Ray starting radius [cMpc]
  if (length_cMpc > 0.)
    write(f, "RayLength_cMpc", length_cMpc); // Ray length [cMpc]
  auto r_units = Units(1., -1., 1., 0., 0., unit_length);
  {
    write(f, "RayOrigins", origins);         // Ray origin points [a*length/h]
    DataSet d = f.openDataSet("RayOrigins"); // Access the dataset
    write(d, r_units);                       // Length units
  }
  {
    write(f, "RayEndings", endings);         // Ray origin points [a*length/h]
    DataSet d = f.openDataSet("RayEndings"); // Access the dataset
    write(d, r_units);                       // Length units
  }
  write(f, "NumRays", n_cameras);            // Number of rays
  if (n_exp >= 0)
    write(f, "n_exp", n_exp);                // Healpix exponent for cameras
  write(f, "RayDirections", camera_directions); // Normalized ray directions

  // Set up the file group structure
  for (int field = 0; field < n_copy_fields; ++field) {
    auto& fld = fields[field];               // RayField struct
    Group g = f.createGroup(fld.name);       // Field groups
    write(g, fld.units);                     // Field units
  }
  Group g = f.createGroup("RaySegments");    // Ray segments group
  write(g, r_units);                         // Length units
}

/* Writes data and info to the specified hdf5 filename. */
void Rays::write_hdf5() {
  write_timer.start();
  // Dummy function for this module
  write_timer.stop();
}
