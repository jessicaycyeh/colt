/******************
 * rays/config.cc *
 ******************

 * Rays module configuration.

*/

#include "../proto.h"
#include "Rays.h"
#include "../config.h" // Configuration functions

/* Rays module configuration. */
void Rays::module_config(YAML::Node& file, YAML::Node& node) {
  if (geometry != voronoi)
    root_error("Rays module requires voronoi geometry");
#ifndef HAVE_CGAL
  root_error("Rays module requires CGAL for mesh connectivity");
#endif
#ifdef CGAL_LINKED_WITH_TBB
  root_error("Rays module should not be run with CGAL_LINKED_WITH_TBB");
#endif

  if (!cosmological)
    cosmological = true;                     // Require cosmological flag

  // Information about the cameras
  camera_config(file, node);                 // General camera setup
  if (n_cameras <= 0)
    root_error("The rays module requires at least one camera "
               "but none were specified in " + config_file);
  if (file["origins_bbox"]) {
    load("origins_bbox", ray_origins);       // List of camera origins
    multiple_origins = true;                 // Flag for camera controls
  } else if (file["origin_bbox"]) {
    load("origin_bbox", ray_origin);         // Camera origin
  } else {
    fof_origin = true;                       // Read origin from fof file
  }
  if (multiple_origins)                      // Check that the sizes match
    if ((int)ray_origins.size() != n_cameras)
      root_error("ray_origins.size() != n_cameras");
  if (!fof_origin && (file["fof_base"] || file["select_subhalo"] || file["start_fvir"]))
    root_error("Not expecting fof_base, select_subhalo, or start_fvir when ray origins are specified.");

  // Information about the rays
  if (fof_origin) {
    load("fof_base", fof_base);              // Friends of friends base name
    if (halo_str.empty())
      halo_str = "0";                        // Select the first group/subhalo
    load("black_hole_origin", black_hole_origin); // Most massive black hole
    load("lyman_alpha_origin", lyman_alpha_origin); // Lyman-alpha catalog
    if (black_hole_origin && lyman_alpha_origin)
      root_error("Cannot use black_hole_origin and lyman_alpha_origin together.");
  }
  if (fof_origin && file["start_fvir"]) {
    load("start_fvir", start_fvir);          // Ray starting radius [f_vir]
    if (start_fvir < 0.)
      root_error("start_fvir must be >= 0"); // Validate start_fvir range
  } else {
    load("start_cMpc", start_cMpc);          // Ray starting radius [cMpc]
    if (start_cMpc < 0.)
      root_error("start_cMpc must be >= 0"); // Validate start_cMpc range
  }
  if (file["length_kms"]) {
    load("length_kms", length_kms);          // Ray length proxy [km/s]
    if (length_kms <= 0.)
      root_error("length_kms must be > 0");  // Validate length_kms range
  } else if (file["length_Mpc"]) {
    load("length_Mpc", length_Mpc);          // Ray length [Mpc]
    if (length_Mpc <= 0.)
      root_error("length_Mpc must be > 0");  // Validate length_Mpc range
  } else {
    load("length_cMpc", length_cMpc);        // Ray length [cMpc]
    if (length_cMpc <= 0.)
      root_error("length_cMpc must be > 0"); // Validate length_cMpc range
  }
  if (file["impact_kpc"]) {
    load("impact_kpc", r_max_kpc);           // Cylinder impact radius [kpc]
    if (r_max_kpc <= 0.)
      root_error("impact_kpc must be > 0");  // Validate impact_kpc range
  }
}
