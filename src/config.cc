/*!
 * \file    config.cc
 *
 * \brief   General configuration for the simulation.
 *
 * \details This file contains code to set up high level configuration options.
 *          Configuration options for individual modules are delegated to the
 *          overloaded member function ``module_config``.
 */

#include "proto.h"
#include <fstream> // File handling
#include <sys/stat.h> // Make directories
#include "config.h" // Configuration functions
#include "Simulation.h" // Base simulation class

//! \brief General configuration for the simulation.
void Simulation::setup_config() {
  // Setup file and validation maps
  YAML::Node file = YAML::LoadFile(config_file); // User-specified
  YAML::Node node;                           // Validation starts empty

  // Setup verbose output
  load("verbose", verbose);                  // Verbose output
  if (verbose && root)
    cout << "\nLoading config options from " << config_file << endl;

  // Simulation parameters
  if (!snap_str.empty()) {                   // Ensure "_000" convention
    int snap_padding = 3;                    // Leading zeros for snap_str
    if (file["snap_padding"])
      load("snap_padding", snap_padding);    // Allow overriding default
    snap_str = "_" + string(snap_padding - snap_str.length(), '0') + snap_str;
  }
  if (!halo_str.empty()) {
    load("select_subhalo", select_subhalo); // Specified a subhalo
    halo_str = (select_subhalo ? "_s" : "_g") + halo_str;
  }
  if (geometry == voronoi) {
    load("save_connections", save_connections); // Save voronoi connections
    load("save_circulators", save_circulators); // Save voronoi circulators
  }
  const bool cgal = (geometry == voronoi) && save_connections;
  if (file["init_base"]) {
    if (file["init_dir"]) {
      load("init_dir", init_dir);
      if (cgal) {
        cgal_dir = init_dir;                 // Set default from init_dir
        load("cgal_dir", cgal_dir);
        // Ensure the cgal directory exists
        if (root) {
          int status = mkdir(cgal_dir.c_str(), S_IRWXU | S_IRGRP | S_IXGRP);
          if (status != 0 && errno != EEXIST)
            error("Could not create the connections directory: " + cgal_dir);
        }
      }
    }
    load("init_base", init_base);
    load("init_ext", init_ext);
    if (!init_dir.empty())
      init_file = init_dir + "/";            // Prepend directory info
    init_file += init_base + snap_str + halo_str;
    if (cgal) {
      cgal_base = init_base;                 // Set default from init_base
      load("cgal_base", cgal_base);
      cgal_ext = init_ext;                   // Set default from init_ext
      load("cgal_ext", cgal_ext);
      if (!cgal_dir.empty())
        cgal_file = cgal_dir + "/";          // Prepend directory info
      cgal_file += cgal_base + snap_str + halo_str + "." + cgal_ext;
    }
  } else {
    require("init_file", init_file);         // Initial conditions file
    if (cgal) {
      cgal_file = init_file;                 // Set default from init_file
      load("cgal_file", cgal_file);
    }
  }
  load("output_dir", output_dir);            // Output directory name
  load("output_base", output_base);          // Output file base name
  load("output_ext", output_ext);            // Output file extension

  // Ensure the output directory exists
  if (root) {
    int status = mkdir(output_dir.c_str(), S_IRWXU | S_IRGRP | S_IXGRP);
    if (status != 0 && errno != EEXIST)
      error("Could not create the output directory: " + output_dir);
  }
  MPI_Barrier(MPI_COMM_WORLD);

  // Cosmological parameters
  load("cosmological", cosmological);        // Flag for cosmological simulations
  if (cosmological) {
    load("Omega0", Omega0);                  // Matter density [rho_crit_0]
    load("OmegaB", OmegaB);                  // Baryon density [rho_crit_0]
    load("h100", h100);                      // Hubble constant [100 km/s/Mpc]
    if (Omega0 < 0. || Omega0 > 1.)
      root_error("Omega0 must be in the range [0,1]"); // Validate Omega0 range
    if (OmegaB < 0. || OmegaB > Omega0)
      root_error("OmegaB must be in the range [0,Omega0]"); // Validate OmegaB range
    if (h100 <= 0.)
      root_error("h100 must be > 0");        // Validate h100 range
  }

  // Module parameter configuration
  module_config(file, node);                 // Module configuration

  // Check for unexpected parameters
  for (const auto& kv : file)
    root_error("Unexpected or repeated field '" + kv.first.as<string>() + "' in " + config_file);

  // Write a full backup config file
  if (root) {
    std::ofstream fout(config_file + "-full");
    fout << "--- !" << module << endl;
    fout << node;
  }
}
