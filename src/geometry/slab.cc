/********************
 * geometry/slab.cc *
 ********************

 * Slab specific functions.

*/

#include "../compile_time.h" // Compile time options

#ifdef SLAB

#include "../proto.h"
#include "../io_hdf5.h" // HDF5 read/write functions

double ran(); // Random number (from global seed)

/* Read slab specific data. */
void read_geometry(H5File& f) {
  vector<double> r_1D;                       // Cell edge position (1D) [cm]
  n_cells = read(f, "r_edges", r_1D, "cm");  // Cell edge position [cm]
  r.resize(n_cells);                         // Allocate space
  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i)
    r[i] = {r_1D[i], 0., 0.};                // Convert to 3D position [cm]
  n_cells--;                                 // More edges than cells

  // Additional geometry info
  bbox[0] = {r_1D[0], 0., 0.};               // Bounding box [cm]
  bbox[1] = {r_1D[n_cells], 0., 0.};         // Min/Max edges
  max_bbox_width = r_1D[n_cells] - r_1D[0];  // Max bbox width [cm]
  w.resize(n_cells);                         // Allocate space for width
  V.resize(n_cells);                         // Allocate space for volume
  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i) {
    if (r_1D[i+1] <= r_1D[i])
      error("r_edges must be in increasing order. [slab]");
    w[i] = r_1D[i+1] - r_1D[i];              // Cell width [cm]
    V[i] = w[i];                             // Volume [cm] (V = w^3)
  }
}

/* Avoid calculations for certain cells. */
bool avoid_cell(const int) {
  return false;                              // Every cell is valid
}

/* Return the center position of the cell. */
Vec3 cell_center(const int cell) {
  return {r[cell].x + 0.5 * w[cell], 0., 0.}; // Left edge plus half width
}

/* Draw position uniformly within the cell volume. */
Vec3 random_point_in_cell(const int cell) {
  return {r[cell].x + w[cell] * ran(), 0., 0.}; // [x,x+w]
}

/* Check whether a point is in the specified cell. */
bool point_is_in_cell(const Vec3 point, const int cell) {
  if (point.x < r[cell].x || point.x >= r[cell+1].x)
    return false;                            // Point is not in the cell
  return true;                               // Point is in the cell
}

/* Find the cell index of the specified point. */
int find_cell(const Vec3 point, int cell) {
  // Invalid cell indices start from the first cell
  if (cell < 0 || cell >= n_cells)
    cell = 0;

  // Check the cells to the left and right
  while (point.x < r[cell].x)
    if (0 == cell--)                         // Move left  ( <-- )
      return OUTSIDE;                        // Point is outside left edge
  while (point.x >= r[cell+1].x)
    if (++cell == n_cells)                   // Move right ( --> )
      return OUTSIDE;                        // Point is outside right edge

  return cell;                               // Found a valid cell
}

/* Find the minimum distance to a face from a given point in the cell. */
double min_face_distance(const Vec3& point, const int cell) {
  const double l_L = point.x - r[cell].x;    // Left face
  const double l_R = r[cell+1].x - point.x;  // Right face
  return (l_L < l_R) ? l_L : l_R;            // Return min
}

/* Compute the maximum distance the photon can travel in the cell. */
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell) {
  // Protect against parallel rays
  if (direction.x == 0.)
    return make_tuple(positive_infinity, cell); // Ray is parallel to the face

  if (direction.x > 0.) {
    const int next = cell + 1;               // Right face values
    return make_tuple((r[next].x - point.x) / direction.x,
                      (next < n_cells) ? next : OUTSIDE);
  } else                                     // Left face values
    return make_tuple((r[cell].x - point.x) / direction.x,
                      (cell > 0) ? cell - 1 : OUTSIDE);
}

#endif // SLAB
