/************************
 * geometry/geometry.cc *
 ************************

 * Geometry: Calculations of distances etc.

*/

#include "../proto.h"

bool point_is_in_cell(const Vec3 point, const int cell); // Cell validation
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell

/* Returns the integer n, which fulfills n^2 <= arg < (n+1)^2. */
static inline int isqrt(const int arg) {
  return sqrt(arg + 0.5);
}

/* Returns healpix directions in RING ordering from a given refinement level. */
vector<Vec3> healpix_vectors(const int order) {
  const int nside = 1 << order;              // nside = 2^order
  const int npface = nside << order;         // npface = nside^2
  const int ncap = (npface - nside) << 1;    // ncap = 2 nside (nside - 1)
  const int npix = 12 * npface;              // npix = 12 nside^2 = 12*4^order
  const double fact2 = 4. / npix;            // Useful constants
  const double fact1 = (nside << 1) * fact2; // 8 nside / npix

  vector<Vec3> vecs;                         // Resulting healpix vectors
  vecs.resize(npix);                         // Allocate space

  #pragma omp parallel for
  for (int pix = 0; pix < npix; ++pix) {
    double z, phi, sth;                      // pix -> (z, phi, sqrt(theta))
    if (pix < ncap) {                        // North polar cap
      int iring = (1 + isqrt(1 + 2*pix)) >> 1; // Counted from north pole
      int iphi = (pix+1) - 2*iring*(iring-1);

      double tmp = (iring * iring) * fact2;
      z = 1. - tmp;
      phi = (iphi - 0.5) * M_PI_2 / iring;
      sth = (z > 0.99) ? sqrt(tmp * (2. - tmp)) : sqrt(1. - z * z);
    } else if (pix < npix - ncap) {          // Equatorial region
      int nl4 = 4 * nside;
      int ip  = pix - ncap;
      int tmp = (order >= 0) ? ip >> (order+2) : ip / nl4;
      int iring = tmp + nside;
      int iphi = ip - nl4*tmp + 1;
      // 1 if iring + nside is odd, 1/2 otherwise
      double fodd = ((iring + nside) & 1) ? 1. : 0.5;

      z = (2*nside - iring) * fact1;
      phi = (iphi - fodd) * M_PI * 0.75 * fact1;
      sth = sqrt(1. - z * z);
    } else {                                 // South polar cap
      int ip = npix - pix;
      int iring = (1 + isqrt(2*ip - 1)) >> 1; // Counted from south pole
      int iphi  = 4*iring + 1 - (ip - 2*iring*(iring-1));

      double tmp = (iring * iring) * fact2;
      z = tmp - 1.;
      phi = (iphi - 0.5) * M_PI_2 / iring;
      sth = (z < -0.99) ? sqrt(tmp * (2. - tmp)) : sqrt(1. - z * z);
    }
    vecs[pix] = {sth * cos(phi), sth * sin(phi), z};
  }
  return vecs;
}

/* Calculate the distance to escape the bounding sphere. */
double spherical_escape_distance(Vec3 point, Vec3 direction) {
  const Vec3 dr = point - escape_center;     // Position relative to the center
  const double k_dot_r = dr.dot(direction);  // Angular cosine
  const double r_dot_r = dr.dot();           // Radius squared
  return sqrt(k_dot_r*k_dot_r - r_dot_r + escape_radius2) - k_dot_r - escape_epsilon;
}

/* Calculate the distance to escape the emission sphere. */
double spherical_emission_distance(Vec3 point, Vec3 direction) {
  const Vec3 dr = point - escape_center;     // Position relative to the center
  const double k_dot_r = dr.dot(direction);  // Angular cosine
  const double r_dot_r = dr.dot();           // Radius squared
  return sqrt(k_dot_r*k_dot_r - r_dot_r + emission_radius2) - k_dot_r;
}

/* Integrate a scalar field along a ray until escape. */
double ray_integral(vector<double>& field, Vec3 point, Vec3 direction, int cell) {
  if (!point_is_in_cell(point, cell))
    error("Ray integration point is not in the starting cell.");
  int next_cell;                             // Next cell index
  double l, l_exit = 0., result = 0.;        // Path lengths and result
  if (spherical_escape) {                    // Initialize escape distance
    if ((point - escape_center).dot() >= escape_radius2)
      error("Attempted ray integration outside the spherical escape region");
    l_exit = spherical_escape_distance(point, direction);
  }
  while (true) {                             // Ray trace until escape
    tie(l, next_cell) = face_distance(point, direction, cell); // Maximum propagation distance
    if (spherical_escape) {                  // Check for spherical escape
      if (l_exit <= l)                       // Exit sphere before cell
        return result + field[cell] * l_exit; // += field integral
      l_exit -= l;                           // Remaining distance to exit
    }
    result += field[cell] * l;               // Path integral of the field
#ifdef SPHERICAL
    if (next_cell == INSIDE)                 // Check if the ray is trapped
      return result;
#endif
    if (next_cell == OUTSIDE)                // Check if the ray escapes
      return result;
    point += direction * l;                  // Move to the new position
    cell = next_cell;                        // Update the next cell index
  }
}
