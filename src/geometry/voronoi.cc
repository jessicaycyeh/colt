/***********************
 * geometry/voronoi.cc *
 ***********************

 * Voronoi tessellation specific functions.

*/

#include "../compile_time.h" // Compile time options

#ifdef VORONOI

#include "../proto.h"
#include "../timing.h" // Timing functionality
#include "../io_hdf5.h" // HDF5 read/write functions

extern bool cell_based_emission;             // Include cell-based sources

double ran(); // Random number (from global seed)

#ifdef HAVE_CGAL
void calculate_connectivity(const bool verbose_cgal); // Calculate Delaunay connections
#endif

/* Read the connection data from the initial conditions file. */
static void read_cell_connectivity() {
  // Open file in read only mode
  H5File f(cgal_file, H5F_ACC_RDONLY);

  // Flat array declarations
  vector<int> edges;                         // List of edge indices
  vector<int> neighbor_indices;              // Cumulative n_neighbors count
  vector<int> neighbor_indptr;               // Neighbor indices
  vector<double> neighbor_areas;             // Neighbor face areas
  vector<int> circulator_indices;            // Cumulative n_circulators count
  vector<int> circulator_indptr;             // Circulator indices

  // Read mesh connectivity data
  n_edges = read(f, "edges", edges);         // Edge cell indices
  n_neighbors_tot = read(f, "neighbor_indptr", neighbor_indptr); // Neighbor indices (flat)
  read(f, "neighbor_indices", neighbor_indices, n_cells+1); // Neighbor indices helper
  read(f, "V", V, n_cells, "cm^3");          // Cell volumes [cm^3]

  if (cell_based_emission && save_circulators) {
    n_circulators_tot = read(f, "circulator_indptr", circulator_indptr); // Circulator indices (flat)
    read(f, "circulator_indices", circulator_indices, n_neighbors_tot+1); // Circulator indices helper
    read(f, "neighbor_areas", neighbor_areas, n_neighbors_tot, "cm^2"); // Face areas [cm^2]
  } else
    n_circulators_tot = 0;                   // Do not initialize face data

  // Convert edge indices to edge flags
  edge_flag.resize(n_cells);                 // Allocate space for edge flags
  for (int i = 0; i < n_cells; ++i)
    edge_flag[i] = false;                    // Set edge flags to false
  for (int i = 0; i < n_edges; ++i)
    edge_flag[edges[i]] = true;              // true => cell is an edge

  // Convert from flat arrays to nested vectors
  if (cell_based_emission && save_circulators) { // Also initialize circulators
    faces.resize(n_cells);                   // Allocate space for faces
    #pragma omp parallel for
    for (int cell = 0; cell < n_cells; ++cell) {
      int i_neib = neighbor_indices[cell];   // Flat neighbor index
      int i_circ = circulator_indices[i_neib]; // Flat circulator index
      const int n_faces = neighbor_indices[cell+1] - i_neib;
      faces[cell].resize(n_faces);
      for (auto& face : faces[cell]) {
        face.neighbor = neighbor_indptr[i_neib];
        face.area = neighbor_areas[i_neib];
        const int n_circulators = circulator_indices[++i_neib] - i_circ;
        face.circulator.resize(n_circulators);
        for (auto& circ : face.circulator)
          circ = circulator_indptr[i_circ++];
      }
    }
  } else {                                   // Only initialize neighbors
    faces.resize(n_cells);                   // Allocate space for faces
    #pragma omp parallel for
    for (int cell = 0; cell < n_cells; ++cell) {
      int i_neib = neighbor_indices[cell];   // Flat neighbor index
      const int n_faces = neighbor_indices[cell+1] - i_neib;
      faces[cell].resize(n_faces);
      for (auto& face : faces[cell])
        face.neighbor = neighbor_indptr[i_neib++];
    }
  }
}

#ifdef HAVE_CGAL
/* Write the new connection data to the initial conditions file. */
static void write_cell_connectivity() {
  // Open file and write new data
  H5File f(cgal_file, (cgal_file == init_file) ? H5F_ACC_RDWR : H5F_ACC_TRUNC);

  // Flat array declarations
  vector<int> edges;                         // List of edge indices
  vector<int> neighbor_indices;              // Cumulative n_neighbors count
  vector<int> neighbor_indptr;               // Neighbor indices
  vector<double> neighbor_areas;             // Neighbor face areas
  vector<int> circulator_indices;            // Cumulative n_circulators count
  vector<int> circulator_indptr;             // Circulator indices

  // Count edge cells
  n_edges = 0;                               // Start count with zero
  #pragma omp parallel for reduction(+:n_edges)
  for (int cell = 0; cell < n_cells; ++cell)
    if (edge_flag[cell])
      n_edges++;

  // Flat array allocations
  edges.resize(n_edges);                     // List of edge indices
  neighbor_indices.resize(n_cells+1);        // Cumulative n_neighbors count
  neighbor_indptr.resize(n_neighbors_tot);   // Neighbor indices
  neighbor_areas.resize(n_neighbors_tot);    // Neighbor face areas
  if (save_circulators) {
    circulator_indices.resize(n_neighbors_tot+1); // Cumulative n_circulators count
    circulator_indptr.resize(n_circulators_tot); // Circulator indices
  }

  // Convert to flat arrays
  if (save_circulators) {
    neighbor_indices[0] = 0;                 // Index offset counters
    circulator_indices[0] = 0;               // Always start at zero
    int i_neib = 0, i_circ = 0, i_edge = 0;  // Flat indices
    for (int cell = 0; cell < n_cells; ++cell) {
      if (edge_flag[cell])                   // Populate edge cell indices
        edges[i_edge++] = cell;
      const int n_faces = faces[cell].size(); // Face count
      neighbor_indices[cell+1] = neighbor_indices[cell] + n_faces; // Cumulative
      for (const auto& face: faces[cell]) {
        neighbor_indptr[i_neib] = face.neighbor; // Neighbor index
        neighbor_areas[i_neib] = face.area;  // Face area [cm^2]
        const int n_circs = face.circulator.size(); // Circulator count
        circulator_indices[i_neib+1] = circulator_indices[i_neib] + n_circs; // Cumulative
        i_neib++;                            // Advance flat neighbor index
        for (const auto& circ : face.circulator)
          circulator_indptr[i_circ++] = circ; // Circulator index
      }
    }
  } else {
    neighbor_indices[0] = 0;                 // Index offset counters
    int i_neib = 0, i_edge = 0;              // Flat indices
    for (int cell = 0; cell < n_cells; ++cell) {
      if (edge_flag[cell])                   // Populate edge cell indices
        edges[i_edge++] = cell;
      const int n_faces = faces[cell].size(); // Face count
      neighbor_indices[cell+1] = neighbor_indices[cell] + n_faces; // Cumulative
      for (const auto& face: faces[cell]) {
        neighbor_indptr[i_neib] = face.neighbor; // Neighbor index
        neighbor_areas[i_neib] = face.area;  // Face area [cm^2]
        i_neib++;                            // Advance flat neighbor index
      }
    }
  }

  // Write mesh connectivity data
  if (cgal_file != init_file)
    write(f, "init_file", init_file);        // Location of the parent ic file
  write(f, "n_edges", n_edges);              // Number of edge cells in the mesh
  write(f, "n_neighbors_tot", n_neighbors_tot); // Total number of neighbors for all cells
  write(f, "n_circulators_tot", n_circulators_tot); // Total number of circulators for all faces
  write(f, "edges", edges);                  // Edge cell indices
  write(f, "neighbor_indices", neighbor_indices); // Neighbor indices helper
  write(f, "neighbor_indptr", neighbor_indptr); // Neighbor indices (flat)
  write(f, "neighbor_areas", neighbor_areas, "cm^2"); // Neighbor areas (flat)
  write(f, "V", V, "cm^3");                  // Cell volumes [cm^3]
  if (save_circulators) {
    write(f, "circulator_indices", circulator_indices); // Circulator indices helper
    write(f, "circulator_indptr", circulator_indptr); // Circulator indices (flat)
  }
}
#endif

static inline bool use_previous_connections() {
  // Open file to check if expected data exists
  if (FILE *file = fopen(cgal_file.c_str(), "r")) {
    fclose(file);                            // First check if the file exists
    H5File f(cgal_file, H5F_ACC_RDONLY);
    return H5Lexists(f.getId(), "neighbor_indptr", H5P_DEFAULT) != 0;
  } else {
    return false;                            // File does not exist
  }
}

/* Read voronoi specific data. */
void read_geometry(H5File& f) {
  n_cells = read(f, "r", r, "cm");           // Mesh generating points [cm]

  // Close the hdf5 file
  f.close();

  // Check whether the mesh connectivity is available
  if (save_connections && use_previous_connections())
    read_cell_connectivity();                // Read from initial conditions file
  else {
#ifdef HAVE_CGAL
    Timer cgal_timer = Timer("cgal", true);  // Start connectivity timer
    calculate_connectivity(root);            // Calculate Delaunay connections
    cgal_timer.stop();

    MPI_Barrier(MPI_COMM_WORLD);             // Avoid read/write race condition

    if (root) {
      // Write cell connectivity
      Timer cgal_io_timer = Timer("cgal_io", true); // Start io timer
      if (save_connections) {
        cout << "\nCGAL: Writing connectivity to file ..." << endl;
        write_cell_connectivity();
      }
      cgal_io_timer.stop();

      // Print connectivity data
      cout << "\n  n_edges    = " << n_edges
           << "\n  n_neib_tot = " << n_neighbors_tot
           << "\n  n_circ_tot = " << n_circulators_tot
           << "\n  sum(V)     = " << omp_sum(V) / (kpc*kpc*kpc) << " kpc^3" << endl;

      // Print exit messages
      cgal_timer.set_wall(cgal_timer.get_wall() / double(n_threads));
      cgal_io_timer.set_wall(cgal_io_timer.get_wall() / double(n_threads));
      cout << "\nCGAL timings:  [w/r = wall/real scaled by thread count]"
           << "\n  Calculation = " << cgal_timer.str() << "  [w/r: " << cgal_timer.efficiency() << "]"
           << "\n  Output file = " << cgal_io_timer.str() << "  [w/r: " << cgal_io_timer.efficiency() << "]" << endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);             // Avoid read/write race condition
#else
    root_error("Voronoi mesh connectivity is missing. [HAVE_CGAL is undefined]");
#endif
  }

  // Reopen the hdf5 file
  f = H5File(init_file, H5F_ACC_RDONLY);

  // Additional geometry info
  double r_box = 0.;                         // Box radius [cm]
  read_if_exists(f, "r_box", r_box);
  if (r_box > 0.) {                          // File contains bbox info
    bbox[0] = {-r_box, -r_box, -r_box};
    bbox[1] = {r_box, r_box, r_box};
  } else {                                   // Infer bbox info from cells
    bbox[0] = {positive_infinity,positive_infinity,positive_infinity}; // Bounding box [cm]
    bbox[1] = {negative_infinity,negative_infinity,negative_infinity}; // Min/Max edges
    for (int i = 0; i < n_cells; ++i) {
      if (edge_flag[i]) {                    // Only check edge cells
        for (int j = 0; j < 3; ++j) {
          if (r[i][j] < bbox[0][j])          // Found new minimum value
            bbox[0][j] = r[i][j];
          if (r[i][j] > bbox[1][j])          // Found new maximum value
            bbox[1][j] = r[i][j];
        }
      }
    }
  }
  max_bbox_width = (bbox[1]-bbox[0]).max();  // Max bbox width [cm]
}

/* Avoid calculations for certain cells. */
bool avoid_cell(const int cell) {
  return edge_flag[cell];                    // Edge cells
}

/* Return the center position of the cell. */
Vec3 cell_center(const int cell) {
  return r[cell];                            // Mesh generating point
}

/* Calculate the determinant of a 3x3 matrix (represented by three vectors). */
static double det(const Vec3& A, const Vec3& B, const Vec3& C) {
  // |Ax Bx Cx|        |By Bz|        |Bx Bz|        |Bx By|
  // |Ay By Cy|  =  Ax |Cy Cz|  -  Ay |Cx Cz|  +  Az |Cx Cy|
  // |Az Bz Cz|
  return A.x * (B.y*C.z - B.z*C.y)
       - A.y * (B.x*C.z - B.z*C.x)
       + A.z * (B.x*C.y - B.y*C.x);
}

/* Calculate the 4x4 determinant determining the orientation of the tetrahedron formed by points A, B, C, and D. */
static double det_tetrahedron(const Vec3& A, const Vec3& B, const Vec3& C, const Vec3& D) {
  // |Ax Ay Az 1|   |Ax Ay Az|   |Ax Ay Az|   |Ax Ay Az|   |Bx By Bz|
  // |Bx By Bz 1| = |Bx By Bz| - |Bx By Bz| + |Cx Cy Cz| - |Cx Cy Cz|
  // |Cx Cy Cz 1|   |Cx Cy Cz|   |Dx Dy Dz|   |Dx Dy Dz|   |Dx Dy Dz|
  // |Dx Dy Dz 1|
  return det(A,B,C) - det(A,B,D) + det(A,C,D) - det(B,C,D);
}

/* Calculate the circumcenter of the tetrahedron formed by points A, B, C, and D. */
static Vec3 circumcenter(const Vec3& A, const Vec3& B, const Vec3& C, const Vec3& D) {
  // Define the following quantities:
  //     |Ax Ay Az 1|      |A^2 Ay Az 1|      |A^2 Az Ax 1|      |A^2 Ax Ay 1|
  // a = |Bx By Bz 1|, x = |B^2 By Bz 1|, y = |B^2 Bz Bx 1|, z = |B^2 Bx By 1|
  //     |Cx Cy Cz 1|      |C^2 Cy Cz 1|      |C^2 Cz Cx 1|      |C^2 Cx Cy 1|
  //     |Dx Dy Dz 1|      |D^2 Dy Dz 1|      |D^2 Dz Dx 1|      |D^2 Dx Dy 1|
  const double det_a = det_tetrahedron(A, B, C, D);

  // Calculate squared norms: A^2 = A * A = Ax*Ax + Ay*Ay + Az*Az
  const double A2 = A.dot(), B2 = B.dot(), C2 = C.dot(), D2 = D.dot();

  // Calculate the unnormalized x circumcenter
  Vec3 vA = {A2, A.y, A.z};                  //     |A^2 Ay Az 1|
  Vec3 vB = {B2, B.y, B.z};                  // x = |B^2 By Bz 1|
  Vec3 vC = {C2, C.y, C.z};                  //     |C^2 Cy Cz 1|
  Vec3 vD = {D2, D.y, D.z};                  //     |D^2 Dy Dz 1|
  const double det_x = det_tetrahedron(vA, vB, vC, vD);

  // Calculate the unnormalized y circumcenter
  vA = {A2, A.z, A.x};                       //     |A^2 Az Ax 1|
  vB = {B2, B.z, B.x};                       // y = |B^2 Bz Bx 1|
  vC = {C2, C.z, C.x};                       //     |C^2 Cz Cx 1|
  vD = {D2, D.z, D.x};                       //     |D^2 Dz Dx 1|
  const double det_y = det_tetrahedron(vA, vB, vC, vD);

  // Calculate the unnormalized z circumcenter
  vA = {A2, A.x, A.y};                       //     |A^2 Ax Ay 1|
  vB = {B2, B.x, B.y};                       // z = |B^2 Bx By 1|
  vC = {C2, C.x, C.y};                       //     |C^2 Cx Cy 1|
  vD = {D2, D.x, D.y};                       //     |D^2 Dx Dy 1|
  const double det_z = det_tetrahedron(vA, vB, vC, vD);

  // From these we can write the circumcenter as (x,y,z) / (2a)
  Vec3 det_r = {det_x, det_y, det_z};
  return det_r / (2. * det_a);
}

/* Draw position uniformly within the cell volume. */
Vec3 random_point_in_cell(const int cell) {
  // Determine which face pyramid region to place the point
  const Vec3 rc = r[cell];                   // Cell mesh point
  Face *face = nullptr;                      // Selected face
  int neib = 0;                              // Neighbor index
  double dist;                               // Distance to neighbor
  double V_cum = 0.;                         // Cumulative volume
  const double V_comp = ran() * V[cell];     // Fractional volume
  const int n_faces = faces[cell].size();    // Face count
  for (int i = 0; i < n_faces; ++i) {
    face = &faces[cell][i];                  // Update face
    neib = face->neighbor;                   // Neighbor cell index
    dist = rc.dist(r[neib]);                 // Neighbor distance: |rn - rc|
    V_cum += face->area * dist / 6.;         // Face pyramid volume
    if (V_cum >= V_comp)
      break;                                 // Found correct face
  }

  // Delaunay tetrahedron circumcenters define the Voronoi face region
  const Vec3 rn = r[neib];                   // Neighbor cell mesh point
  const int n_circs = face->circulator.size(); // Circulator count
  Vec3 r1 = r[face->circulator[n_circs-1]];  // First circulator mesh point
  Vec3 r2 = r[face->circulator[0]];          // Second circulator mesh point
  const Vec3 c0 = circumcenter(rc, rn, r1, r2); // Main circumcenter
  r1 = r[face->circulator[1]];               // Update circulator mesh point
  Vec3 c2, c1 = circumcenter(rc, rn, r1, r2) - c0; // Triangle base points

  // Determine which face tetrahedron to place the point
  double area_cum = 0.;                      // Cumulative face area
  const double area_comp = ran() * face->area; // Fractional face area
  for (int i = 2; i < n_circs; ++i) {
    r2 = r[face->circulator[i]];             // Next circulator mesh point
    c2 = circumcenter(rc, rn, r1, r2) - c0;  // Next triangle base point
    const Vec3 c1xc2 = c1.cross(c2);         // Parallelogram area vector
    area_cum += 0.5 * c1xc2.norm();          // Triangle area is half the norm
    if (area_cum >= area_comp)
      break;                                 // Found correct triangle
    r1 = r2;                                 // Update circulator mesh point
    c1 = c2;                                 // Update triangle base point
  }

  // The face triangle and cell center form a tetrahedron
  const Vec3 c3 = rc - c0;

  // Draw the position using barycentric coordinates (adds to unity)
  double u1 = ran(), u2 = ran(), u3 = ran();
  if (u1 + u2 > 1.) { // Cut and fold the cube into a prism
    u1 = 1. - u1;
    u2 = 1. - u2;
  }
  if (u2 + u3 > 1.) { // Cut and fold the prism into a tetrahedron
    const double tmp = u3;
    u3 = 1. - u1 - u2;
    u2 = 1. - tmp;
  } else if (u1 + u2 + u3 > 1.) {
    const double tmp = u3;
    u3 = u1 + u2 + u3 - 1.;
    u1 = 1. - u2 - tmp;
  }

  // Final random position in the cell
  return c0 + c1*u1 + c2*u2 + c3*u3;
}

/* Check whether a point is in the specified cell. */
bool point_is_in_cell(const Vec3 point, const int cell) {
  Vec3 k, p_0;                               // Face direction, face point
  double dr_face;                            // Face distance

  // Loop through neighbors to check which side of each face the point is on
  for (const auto& face : faces[cell]) {
    // Calculate the normal distance to the adjacent cell
    const auto neib = face.neighbor;         // Neighbor cell index
    k = r[neib] - r[cell];                   // k = r_2 - r_1
    p_0 = (r[neib] + r[cell]) * 0.5;         // p_0 = (r_2 + r_1) / 2
    dr_face = k.dot(p_0 - point);            // l = (k*p - k*r) / (k*k)  (proportional)
    if (dr_face < 0.)                        // Sign of face distance
      return false;                          // Don't check any more
  }
  return true;                               // All face distances are positive
}

/* Find the cell index of the specified point. */
int find_cell(const Vec3 point, int cell) {
  int neib;                                  // Neighbor cell index
  Vec3 k, p_0;                               // Face direction, face point
  double dr_face;                            // Face distance

  // Loop through neighbors to iteratively walk towards the correct cell
  int n_faces = faces[cell].size();          // Face count
  for (int i = 0; i < n_faces; ++i) {
    // Calculate the normal distance to the adjacent cell
    neib = faces[cell][i].neighbor;          // Neighbor cell index
    k = r[neib] - r[cell];                   // k = r_2 - r_1
    p_0 = (r[neib] + r[cell]) * 0.5;         // p_0 = (r_2 + r_1) / 2
    dr_face = k.dot(p_0 - point);            // l = (k*p - k*r) / (k*k)  (proportional)
    if (dr_face < 0.) {                      // Sign of face distance
      cell = neib;                           // Neighbor is "closer" to the point
      i = -1;                                // Restart the loop in the next cell
      n_faces = faces[cell].size();          // Reset face count
    }
  }
  return cell;                               // All face distances are positive
}

/* Find the minimum distance to a face from a given point in the cell. */
double min_face_distance(const Vec3& point, const int cell) {
  Vec3 k, p_0;                               // Face direction, face point
  double dr_face = positive_infinity, dr_comp; // Face distance, comparison

  // Loop through neighbors to find the minimum face distance
  for (const auto& face : faces[cell]) {
    // Calculate the normal distance from the point to the face
    const auto neib = face.neighbor;         // Neighbor cell index
    k = r[neib] - r[cell];                   // k = r_2 - r_1
    k.normalize();                           // Normalize for numerical stablility
    p_0 = (r[neib] + r[cell]) * 0.5;         // p_0 = (r_2 + r_1) / 2
    dr_comp = k.dot(p_0 - point);            // l = k*(p - r) / (k*k)  but |k| = 1
    if (dr_comp < dr_face)                   // Check if the face is closest
      dr_face = dr_comp;                     // Set the new length
  }
  return dr_face;                            // Minimum normal face distance
}

/* Compute the maximum distance the photon can travel in the cell. */
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell) {
  int next_cell = -1;                        // Next cell index
  Vec3 k;                                    // Face direction
  double l = positive_infinity, l_comp;      // Set path length to large value
  const double r2 = r[cell].dot();           // r^2 = x^2 + y^2 + z^2 for the cell
  double kdotn;                              // k * n = (r_2 - r_1) * direction
  double kdotp; // k * p = (r_2 - r_1) * (r_1 + r_2) / 2 = (|r_2|^2 - |r_1|^2) / 2

  // Loop through neighbors to find the closest face distance
  for (const auto& face : faces[cell]) {
    // Calculate the distance from the point to the adjacent face
    const auto neib = face.neighbor;         // Neighbor cell index
    k = r[neib] - r[cell];                   // k = r_2 - r_1
    kdotn = k.dot(direction);                // Check ray orientation with face
    if (kdotn <= 0.)                         // Do not consider incoming rays
      continue;
    kdotp = 0.5 * (r[neib].dot() - r2);      // k * p = (|r_2|^2 - |r_1|^2) / 2
    l_comp = (kdotp - k.dot(point)) / kdotn; // l = (k*p - k*point) / (k*direction)

    // Check candidate cell  =>  minimal distance to face
    if (l_comp < l) {
      l = l_comp;                            // Set the new length
      next_cell = neib;                      // Remember the cell index
    }
  }
  if (l < 0.)                                // Corner case (numerical errors)
    l = 0.;                                  // Do not allow negative distances
  if (edge_flag[next_cell])
    next_cell = OUTSIDE;                     // Do not allow transport into edges
  return make_tuple(l, next_cell);           // Minimum face distance and neighbor
}

#endif // VORONOI
