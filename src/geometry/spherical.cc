/*************************
 * geometry/spherical.cc *
 *************************

 * Spherical geometry specific functions.

*/

#include "../compile_time.h" // Compile time options

#ifdef SPHERICAL

#include "../proto.h"
#include "../io_hdf5.h" // HDF5 read/write functions

static const double _1_3 = 1. / 3.;

double ran(); // Random number (from global seed)
Vec3 isotropic_direction(); // Generate direction from an isotropic distribution

/* Read spherical specific data. */
void read_geometry(H5File& f) {
  vector<double> r_1D;                       // Cell edge position (1D) [cm]
  n_cells = read(f, "r_edges", r_1D, "cm");  // Cell edge position [cm]
  r.resize(n_cells);                         // Allocate space
  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i)
    r[i] = {r_1D[i], 0., 0.};                // Convert to 3D position [cm]
  n_cells--;                                 // More edges than cells

  // Additional geometry info
  const double rp = r_1D[n_cells];           // Largest radius [cm]
  bbox[0] = {-rp, -rp, -rp};                 // Bounding box [cm]
  bbox[1] = {rp, rp, rp};                    // Min/Max edges
  max_bbox_width = r_1D[n_cells] - r_1D[0];  // Max bbox width [cm]
  w.resize(n_cells);                         // Allocate space for width
  V.resize(n_cells);                         // Allocate space for volume
  const double ft_pi = 4. * M_PI / 3.;       // Four-thirds pi
  if (r_1D[0] < 0.)
    error("r_edges[0] must be >= 0. [spherical]");
  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i) {
    if (r_1D[i+1] <= r_1D[i])
      error("r_edges must be in increasing order. [spherical]");
    w[i] = r_1D[i+1] - r_1D[i];              // Cell width [cm]
    const double rp3 = pow(r_1D[i+1], 3);    // Outer radius^3
    const double rm3 = pow(r_1D[i], 3);      // Inner radius^3
    V[i] = ft_pi * (rp3 - rm3);              // Volume [cm] (V = 4/3 pi (rp^3 - rm^3))
  }
}

/* Avoid calculations for certain cells. */
bool avoid_cell(const int) {
  return false;                              // Every cell is valid
}

/* Return the center position of the cell. */
Vec3 cell_center(const int cell) {
  return {r[cell].x + 0.5 * w[cell], 0., 0.}; // Left edge plus half width
}

/* Draw position uniformly within the cell volume. */
Vec3 random_point_in_cell(const int cell) {
  const double rp3 = pow(r[cell+1].x, 3);    // Outer radius^3
  const double rm3 = pow(r[cell].x, 3);      // Inner radius^3
  const double r0 = pow(rm3 + (rp3 - rm3) * ran(), _1_3); // Drawn radius
  return isotropic_direction() * r0;         // Random direction
}

/* Check whether a point is in the specified cell. */
bool point_is_in_cell(const Vec3 point, const int cell) {
  const double radius = point.norm();        // Radius of the point
  if (radius < r[cell].x || radius >= r[cell+1].x)
    return false;                            // Point is not in the cell
  return true;                               // Point is in the cell
}

/* Find the cell index of the specified point. */
int find_cell(const Vec3 point, int cell) {
  // Invalid cell indices start from the first cell
  if (cell < 0 || cell >= n_cells)
    cell = 0;

  // Check the cells to the left and right
  const double radius = point.norm();        // Radius of the point
  while (radius < r[cell].x)
    if (0 == cell--)                         // Move left  ( <-- )
      return INSIDE;                         // Point is outside left edge
  while (radius >= r[cell+1].x)
    if (++cell == n_cells)                   // Move right ( --> )
      return OUTSIDE;                        // Point is outside right edge

  return cell;                               // Found a valid cell
}

/* Find the minimum distance to a face from a given point in the cell. */
double min_face_distance(const Vec3& point, const int cell) {
  const double radius = point.norm();        // Radius of the point
  const double l_L = radius - r[cell].x;     // Left face
  const double l_R = r[cell+1].x - radius;   // Right face
  return (l_L < l_R) ? l_L : l_R;            // Return min
}

/* Compute the maximum distance the photon can travel in the cell. */
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell) {
  const double mu_r = direction.dot(point);  // Unnormalized radial cosine
  const double r2 = point.dot();             // Point radius^2
  const double r_diff = mu_r*mu_r - r2;      // (k*r)^2 - r^2
  const double rm = r[cell].x;               // Inner radius
  const double disc_inner = r_diff + rm*rm;  // Inner discriminant
  // Outward radial propagation: l = -k*r + sqrt(|k*r|^2 - r^2 + r_outer^2)
  // Case: k*r >= 0 or k*r can be < 0 but not cross inner radius
  if (mu_r >= 0. || disc_inner <= 0.) {
    const int next = cell + 1;               // Outer face values
    const double rp = r[next].x;             // Outer radius
    return make_tuple(sqrt(r_diff + rp*rp) - mu_r, (next < n_cells) ? next : OUTSIDE);
  }

  // Inward radial propagation: l = -k*r - sqrt(|k*r|^2 - r^2 + r_inner^2)
  // Case: k*r < 0, cell > 0, and disc_inner > 0
  return make_tuple(-sqrt(disc_inner) - mu_r, (cell > 0) ? cell - 1 : INSIDE);
}

#endif // SPHERICAL
