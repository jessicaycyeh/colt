/*!
 * \file    config.h
 *
 * \brief   General configuration for the simulation.
 *
 * \details This file contains general functionality for reading config files.
 *          Everything here depends on utilities from the yamp-cpp library.
 */

#ifndef CONFIG_INCLUDED
#define CONFIG_INCLUDED

#include "proto.h"
#include "yaml-cpp/yaml.h"

//! \brief Custom extensions for the yaml-cpp library.
namespace YAML {
//! \brief Define yaml to Vec3 data conversion.
template<>
struct convert<Vec3> {
  /*! \brief Encode a Vec3 object into a yaml node.
   *
   *  \param[in] vec Vector of (x,y,z) data.
   *
   *  \return Node within a yaml file representation.
   */
  static Node encode(const Vec3& vec) {
    Node node;
    node.push_back(vec.x);
    node.push_back(vec.y);
    node.push_back(vec.z);
    return node;
  }

  /*! \brief Decode a yaml node as a Vec3 object.
   *
   *  \param[in] node Node within a yaml file representation.
   *  \param[in,out] vec Vector of (x,y,z) data.
   *
   *  \return Flag indicating a successful decode.
   */
  static bool decode(const Node& node, Vec3& vec) {
    if(!node.IsSequence() || node.size() != 3) {
      return false;
    }

    vec.x = node[0].as<double>();
    vec.y = node[1].as<double>();
    vec.z = node[2].as<double>();
    return true;
  }
};

/*! \brief Define yaml output stream operations for Vec3 objects.
 *
 *  \param[in] out Emitter for yaml nodes.
 *  \param[in] vec Vector of (x,y,z) data.
 *
 *  \return Emitter after updating with the Vec3 output stream.
 */
inline Emitter& operator << (Emitter& out, const Vec3& vec) {
  out << Flow;
  out << BeginSeq << vec.x << vec.y << vec.z << EndSeq;
  return out;
}
} // end YAML

//! \brief Convenience load macro keeping defualt values if not specified.
#define load(name, val) { load_info(file, node, name, val); }

/*! \brief Load an optional scalar quantity from a yaml file.
 *
 *  \param[in] file Config file read as a yaml representation.
 *  \param[in] node Mirror config file for full yaml output.
 *  \param[in] name Name of the parameter or data to load.
 *  \param[in,out] val Reference for the decoded scalar value.
 */
template <typename T>
void load_info(YAML::Node& file, YAML::Node& node, const string name, T& val) {
  if (file[name]) {
    val = file[name].as<T>();                // Set value from file
    file.remove(name);                       // Remove from the file
  }
  node[name] = val;                          // Set node from default value
  if (verbose && root)
    cout << name << ": " << val << endl;     // Verbose configuration
}

/*! \brief Load an optional vector quantity from a yaml file.
 *
 *  \param[in] file Config file read as a yaml representation.
 *  \param[in] node Mirror config file for full yaml output.
 *  \param[in] name Name of the parameter or data to load.
 *  \param[in,out] val Reference for the decoded vector of values.
 */
template <typename T>
void load_info(YAML::Node& file, YAML::Node& node, const string name, vector<T>& val) {
  if (file[name]) {
    vector<T> vec = file[name].as<vector<T>>();
    val.resize(0);                           // Overwrite the original vector
    for (auto& vec_i : vec)
      val.push_back(vec_i);                  // Set value from file
    file.remove(name);                       // Remove from the file
  }
  node[name] = val;                          // Set node from default value
  if (verbose && root) {
    cout << name << ": [ ";                  // Verbose configuration
    for (auto& val_i : val)
      cout << val_i << ", ";
    cout << "]" << endl;
  }
}

//! \brief Convenience require macro keeping defualt values if not specified.
#define require(name, val) { require_info(file, node, name, val); }

/*! \brief Load a required scalar and raise an error if unsuccessful.
 *
 *  \param[in] file Config file read as a yaml representation.
 *  \param[in] node Mirror config file for full yaml output.
 *  \param[in] name Name of the parameter or data to load.
 *  \param[in,out] val Reference for the decoded scalar value.
 */
template <typename T>
void require_info(YAML::Node& file, YAML::Node& node, const string name, T& val) {
  if (!file[name])
    root_error("Required parameter '" + name + "' is not in " + config_file);
  load_info(file, node, name, val);          // Otherwise load normally
}

//! \brief Convenience load_if_false macro keeping defualt values if not specified.
#define load_if_false(cond, name, val) { load_if_false_info(file, node, cond, name, val); }

/*! \brief Consider a dependency relation and load normally otherwise.
 *
 *  \param[in] file Config file read as a yaml representation.
 *  \param[in] node Mirror config file for full yaml output.
 *  \param[in] cond Conditional determining whether or not to load.
 *  \param[in] name Name of the parameter or data to load.
 *  \param[in,out] val Reference for the decoded scalar value.
 */
static inline void load_if_false_info(YAML::Node& file, YAML::Node& node, const bool cond, const string name, bool& val) {
  if (file[name]) {
    val = file[name].as<bool>();             // Set value from file
    file.remove(name);                       // Remove from the file
  }
  if (cond)
    val = true;                              // Pass on dependency
  node[name] = val;                          // Set node from value
  if (verbose && root)
    cout << name << ": " << val << endl;     // Verbose configuration
}

#endif // CONFIG_INCLUDED
