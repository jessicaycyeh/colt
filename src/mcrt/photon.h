/************
 * photon.h *
 ************

 * Photon packets.

*/

#ifndef PHOTON_INCLUDED
#define PHOTON_INCLUDED

#include "../Vec3.h"                         // 3D vectors in (x,y,z) order

struct Photon {
  Photon();                                  // Initialization constructor
  void print();                              // Print photon data
  void ray_trace();                          // Perform MCRT calculations

  int source_id;                             // Index of emission source
  int source_type;                           // Type of emission source
  double source_weight;                      // Photon weight at emission
  double f_col;                              // Collisional excitation fraction
  int current_cell;                          // Index of the current cell
  Vec3 position;                             // Photon position [cm]
  Vec3 direction;                            // Photon direction (normalized)
  double frequency;                          // Photon frequency
  double weight;                             // Photon weight (at escape)
  double tau_eff_abs;                        // Optical depth traversed so far
#ifdef SPHERICAL
  double radius;                             // Photon radius [cm]
#endif

private:
  void line_scatter();
  void line_scatter_crd();
  void dust_scatter();
#ifdef ELECTRON_SCATTERING
  void electron_scatter();
#endif

  void calculate_LOS_int();
  void calculate_LOS_int(const int camera);

  void calculate_LOS_ext();
  void calculate_LOS_ext(const int camera);

  void calculate_LOS(const int scatter_type);
  void calculate_LOS(const int scatter_type, const int camera);
};

#endif // PHOTON_INCLUDED
