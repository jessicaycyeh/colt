/**********************
 * mcrt/initialize.cc *
 **********************

 * Initialization: Simulation setup, allocation, opening messages, etc.

*/

#include "proto.h"
#include "MCRT.h"

bool avoid_cell(const int cell); // Avoid calculations for certain cells
Vec3 cell_center(const int cell); // Center of the cell
int find_cell(const Vec3 point, int cell); // Cell index of a point
extern double (*H)(double, double); // Voigt-Hjerting function

void atau_NL_calculation(); // Non-local core-skipping calculation
double H_gaussian(double a, double y); // Gaussian line profile
double H_continued_fractions(double a, double y); // Voigt-Hjerting function

static double q_coef, T_12, T_13, T_14, T_15; // Collisional excitation coefficient and temperatures [K]
static double E_12, E_13, E_14, E_15; // Collisional excitation energies [erg]

/* Set the dust parameters: Opacity, albedo, and scattering anisotropy. */
void set_line_and_dust_parameters_default() {
  // Note: Hydrogen transition energies = E_H * (1/n_1^2 - 1/n_2^2)
  const double e2_h = M_PI * ee * ee / h;    // pi e^2 / h
  const double E_H = 2. * me * e2_h * e2_h;  // me e^4 / (2 hbar^2)
  if (line == "Lyman-alpha" || line == "Balmer-alpha" || "Balmer-beta") {
    m_carrier = mH;                          // Hydrogen lines
    E_12 = 3. * E_H / 4.;                    // Lyα energy [erg] (1,2)
    E_13 = 8. * E_H / 9.;                    // Lyβ energy [erg] (1,3)
    E_14 = 15. * E_H / 16.;                  // Lyγ energy [erg] (1,4)
    E_15 = 24. * E_H / 25.;                  // Lyδ energy [erg] (1,5)
    T_12 = E_12 / kB;                        // Lyα temperature [K] (1,2)
    T_13 = E_13 / kB;                        // Lyβ temperature [K] (1,3)
    T_14 = E_14 / kB;                        // Lyγ temperature [K] (1,4)
    T_15 = E_15 / kB;                        // Lyδ temperature [K] (1,5)
    // Lyman series collisional coefficient = sqrt(2π hbar^4 / kB me^3) / ω_1 where ω_1 = 2
    const double hD2pime = h / (2. * M_PI * me);
    q_coef = 0.5 * sqrt(h * hD2pime * hD2pime * hD2pime / kB);
  }

  if (line == "Lyman-alpha") {               // Hydrogen Lyman-alpha (1,2) [1215Å]
    E0 = 0.75 * E_H;                         // Line energy [erg]
    A21 = 4.6986e8;                          // Einstein A coefficient [Hz]
    f12 = 0.41641;                           // Oscillator strength
    g12 = 0.25;                              // Degeneracy ratio: g_lower / g_upper
    if (dust_model == "SMC") {
      kappa_dust = 7.177e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.3303;                       // Dust scattering albedo
      g_dust = 0.5909;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_model == "MW") {
      kappa_dust = 5.802e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.3251;                       // Dust scattering albedo
      g_dust = 0.6761;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_model == "LAURSEN_SMC") {
      double Z_SMC = pow(10., -0.6) * Zsun;  // SMC metallicity [mass fraction]
      sigma_dust = 3.95e-22 / Z_SMC;         // Dust cross section [cm^2/Z/hydrogen atom]
      dust_to_gas = 0.;                      // Dust-to-gas ratio is captured by sigma_dust
      albedo = 0.32;                         // Dust scattering albedo
      g_dust = 0.73;                         // Dust anisotropy parameter: <cosθ>
      f_ion = 0.01;                          // H_II region survival fraction
    } else if (dust_model != "")             // Catch unknown dust models
      error("Requested dust model is not implemented: Lyman-alpha + " + dust_model);
  } else if (line == "Balmer-alpha") {       // Hydrogen Balmer-alpha (2,3) [6562Å]
    E0 = E_H * 5. / 36.;                     // Line energy [erg]
    A21 = 4.4101e7;                          // Einstein A coefficient [Hz]
    f12 = 0.64108;                           // Oscillator strength
    g12 = 4. / 9.;                           // Degeneracy ratio: g_lower / g_upper
    if (dust_model == "SMC") {
      kappa_dust = 3.268e3;                  // Dust opacity [cm^2/g dust]
      albedo = 0.7288;                       // Dust scattering albedo
      g_dust = 0.5219;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_model == "MW") {
      kappa_dust = 6.627e3;                  // Dust opacity [cm^2/g dust]
      albedo = 0.6741;                       // Dust scattering albedo
      g_dust = 0.4967;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_model != "")             // Catch unknown dust models
      error("Requested dust model is not implemented: Balmer-alpha + " + dust_model);
  } else if (line == "Balmer-beta") {        // Hydrogen Balmer-beta (2,4) [4860Å]
    E0 = 0.1875 * E_H;                       // Line energy [erg]
    A21 = 8.4193e6;                          // Einstein A coefficient [Hz]
    f12 = 0.11938;                           // Oscillator strength
    g12 = 0.25;                              // Degeneracy ratio: g_lower / g_upper
    if (dust_model == "SMC") {
      kappa_dust = 4.449e3;                  // Dust opacity [cm^2/g dust]
      albedo = 0.7587;                       // Dust scattering albedo
      g_dust = 0.5613;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_model == "MW") {
      kappa_dust = 1.022e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.6650;                       // Dust scattering albedo
      g_dust = 0.5561;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_model != "")             // Catch unknown dust models
      error("Requested dust model is not implemented: Balmer-beta + " + dust_model);
  } else                                     // Catch unknown lines
    error("Requesting line is not implemented: " + line);

  if (line != "Lyman-alpha")
    line_scattering = false;                 // Only some lines have resonant scattering
}

/* Set the derived dust parameters. */
void set_line_and_dust_parameters_derived() {
  // Constants for the Henyey-Greenstein dust scattering phase function
  _1_2g    = 0.5 / g_dust;                   // = 1 / (2g)
  _1pg2_2g = _1_2g + 0.5 * g_dust;           // = (1+g^2) / (2g) = 1/(2g) + g/2
  _1mg2_2g = _1_2g - 0.5 * g_dust;           // = (1-g^2) / (2g) = 1/(2g) - g/2
  _1mg_2g  = _1_2g - 0.5;                    // = (1-g) / (2g) = 1/(2g) - 1/2
  _1mg2_2D = _1mg2_2g * 0.5 * sqrt(_1_2g);   // = (1-g^2) / [2 (2g)^(3/2)]

  // Constants for the line properties
  T0 = E0 / kB;                              // Line temperature [K]
  nu0 = E0 / h;                              // Line frequency [Hz]
  lambda0 = c / nu0;                         // Line wavelength [cm -> angstrom]
  B21 = A21 * lambda0 * lambda0 / (2. * E0); // Einstein B21 = A21 c^2 / (2 h ν0^3)
  DnuL = A21 / (2. * M_PI);                  // Natural frequency broadening [Hz]
  // Conversion for special relativity (beta = v/c = avthDc u/a)
  avthDc = DnuL / (2. * nu0);                // a vth/c = DnuL / 2 nu0
  // Thermal velocity: vth_div_sqrtT = vth / sqrt(T)  (vth = sqrt(2*kB*T/mH))
  vth_div_sqrtT = sqrt(2. * kB / m_carrier);
  // Doppler width: DnuD_div_sqrtT = DnuD / sqrt(T)  (ΔνD = ν0*vth/c)
  DnuD_div_sqrtT = nu0 * vth_div_sqrtT / c;
  // "damping parameter": a_sqrtT = a * sqrt(T)  (a = ΔνL/2ΔνD)
  a_sqrtT = DnuL / (2. * DnuD_div_sqrtT);
  // Cross section: sigma0_sqrtT = sigma0 * sqrt(T)
  sigma0_sqrtT = 2. * f12 * ee * ee / (M_2_SQRTPI * me * nu0 * vth_div_sqrtT);
  // Constant for stellar continuum emission
  x_div_aDv = -km / (a_sqrtT * vth_div_sqrtT); // -1/(a vth) = constant
  // Constant for observable frequency conversion (x -> Δv)
  // neg_a_vth_kms_exit = -vth_div_sqrtT * a_sqrtT / km; // Note: 1 km = 10^5 cm
  // Recoil parameter / "damping parameter" = 2 h ν0^2 / (mH c^2 ΔνL)
  gDa = 2. * h / (m_carrier * lambda0 * lambda0 * DnuL);
  lambda0 /= angstrom;                       // Convert wavelength [cm -> angstrom]

  if (two_level_atom)
    H = H_gaussian;                          // Gaussian line profile
  else if (line_scattering)
    H = H_continued_fractions;               // Allow resonant scattering
  else
    H = return_zero;                         // Zero line scattering opacity
}

// /* Lyman-alpha effective recombination coefficient (Case B) - Units: cm^3/s */
// static double alpha_eff_B_Lya(const double T) {
//   // Case B recombination coefficient - Units: cm^3/s
//   // α_B(T) is from Hui & Gnedin (1997) good to 0.7% accuracy from 1 K < T < 10^9 K
//   const double lambda = 315614. / T;         // 2 T_i/T where T_i = 157,807 K
//   const double alpha_B = 2.753e-14 * pow(lambda, 1.5) / pow(1. + pow(lambda / 2.74, 0.407), 2.242);
//   // Probability of conversion to a Lyman-alpha photon - Units: dimensionless
//   // From Cantalupo et al. (2008) good to 0.1% accuracy from 100 K < T < 10^5 K
//   const double T4 = T * 1e-4;                // T4 = T / (10^4 K)
//   const double P_Lya_B = 0.686 - 0.106 * log10(T4) - 0.009 * pow(T4, -0.44);
//   return P_Lya_B * alpha_B;                  // α_eff_B = P_B(T) α_B(T)
// }

// /* Balmer-alpha effective recombination coefficient (Case B) - Units: cm^3/s */
// static double alpha_eff_B_Ha(const double T) {
//   // Hα emission rate from Draine (2011)
//   const double T4 = T * 1e-4;                // T4 = T / (10^4 K)
//   return 1.17e-13 * pow(T4, -0.942 - 0.031 * log(T4)); // Units: cm^3/s
// }

// /* Balmer-beta effective recombination coefficient (Case B) - Units: cm^3/s */
// static double alpha_eff_B_Hb(const double T) {
//   // Hβ emission rate from Draine (2011)
//   const double T4 = T * 1e-4;                // T4 = T / (10^4 K)
//   return 3.03e-14 * pow(T4, -0.874 - 0.058 * log(T4)); // Units: cm^3/s
// }

// Temperature and density grid for Storey & Hummer (1995) recombination tables
// Note: T = {5e2, 1e3, 3e3, 5e3, 7.5e3, 1e4, 1.25e4, 1.5e4, 2e4, 3e4}
// Note: n_e = {1e2, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8}
static const double logT_SH95[10] = {log10(5e2), 3., log10(3e3), log10(5e3), log10(7.5e3), 4.,
                                      log10(1.25e4), log10(1.5e4), log10(2e4), log10(3e4)};

// Case B probability of emitting a Lyman-alpha photon per recombination (T,n_e)
static const double P_Lya_SH95[10][7] = {
  {0.80793637, 0.90215197, 0.98157401, 0.99705340, 0.99933853, 0.99934845, 0.99967896},
  {0.78439303, 0.87743841, 0.97423464, 0.99680114, 0.99960243, 0.99967816, 0.99976710},
  {0.74159266, 0.82809599, 0.95734434, 0.99359891, 0.99892162, 0.99927343, 0.99979471},
  {0.71946081, 0.80133228, 0.94581237, 0.99246182, 0.99848958, 0.99940329, 0.99949246},
  {0.70117195, 0.77884530, 0.93441344, 0.99075058, 0.99831301, 0.99960223, 0.99940949},
  {0.68808416, 0.76220739, 0.92503048, 0.98912680, 0.99828523, 0.99940740, 0.99944339},
  {0.67755810, 0.74868826, 0.91762730, 0.98784252, 0.99835238, 0.99903921, 0.99977064},
  {0.66862468, 0.73785038, 0.91033402, 0.98701405, 0.99817046, 0.99892625, 0.99953078},
  {0.65534642, 0.72080463, 0.89823705, 0.98497224, 0.99798854, 0.99881330, 0.99929092},
  {0.63566376, 0.69600394, 0.87952243, 0.98121463, 0.99716593, 0.99896144, 0.99905958}
};

// Case B probability of emitting a Balmer-alpha photon per recombination (T,n_e)
static const double P_Ha_SH95[10][7] = {
  {0.60176416, 0.59203990, 0.57777268, 0.55757870, 0.53218274, 0.50309773, 0.47538990},
  {0.57146208, 0.56548156, 0.55537185, 0.54126081, 0.52171947, 0.49779442, 0.47325611},
  {0.51602376, 0.51308374, 0.50870615, 0.50163462, 0.49109775, 0.47695493, 0.46057657},
  {0.48842989, 0.48670009, 0.48384243, 0.47916786, 0.47205133, 0.46211067, 0.45011817},
  {0.46673159, 0.46571739, 0.46369825, 0.46057413, 0.45555148, 0.44857577, 0.43958049},
  {0.45180723, 0.45102825, 0.44947568, 0.44724222, 0.44349888, 0.43815604, 0.43129878},
  {0.44059820, 0.43967487, 0.43880459, 0.43692031, 0.43424518, 0.42968461, 0.42434709},
  {0.43139784, 0.43080340, 0.43015528, 0.42868403, 0.42629594, 0.42286913, 0.41840805},
  {0.41795682, 0.41743320, 0.41691031, 0.41586673, 0.41447832, 0.41195330, 0.40867726},
  {0.39957948, 0.39949886, 0.39933772, 0.39872322, 0.39820159, 0.39687074, 0.39529763}
};

// Case B probability of emitting a Balmer-beta photon per recombination (T,n_e)
static const double P_Hb_SH95[10][7] = {
  {0.11197758, 0.11382091, 0.11630471, 0.11882216, 0.12108799, 0.12288014, 0.12720456},
  {0.11559987, 0.11672083, 0.11811675, 0.11985224, 0.12129050, 0.12256368, 0.12615841},
  {0.11908521, 0.11942732, 0.11988399, 0.12040081, 0.12086866, 0.12131817, 0.12352423},
  {0.11897682, 0.11914759, 0.11943147, 0.11964361, 0.11981875, 0.11999444, 0.12172966},
  {0.11803310, 0.11815162, 0.11823874, 0.11840903, 0.11841351, 0.11845008, 0.11983194},
  {0.11688913, 0.11694270, 0.11695517, 0.11706977, 0.11706124, 0.11708462, 0.11828955},
  {0.11571266, 0.11566493, 0.11578498, 0.11586298, 0.11583936, 0.11573996, 0.11684562},
  {0.11460234, 0.11463318, 0.11469477, 0.11472462, 0.11466497, 0.11459548, 0.11563639},
  {0.11271958, 0.11270919, 0.11273303, 0.11274643, 0.11268794, 0.11258888, 0.11350468},
  {0.10960571, 0.10960828, 0.10961341, 0.10961006, 0.10954052, 0.10948666, 0.11028151}
};

/* Interpolated probability for conversion to line photons. */
static double P_B(const double (&Ptab)[10][7], const double T, const double n_e) {
  const double T_rec = (T > T_floor_rec) ? T : T_floor_rec; // Temperature floor
  int i_L_T = 8, i_L_n = 0;                  // Lower interpolation index
  double frac_R_T = 1., frac_R_n = 0.;       // Upper interpolation fraction
  if (T_rec <= 5e2) {                        // Temperature minimum = 500 K
    i_L_T = 0;
    frac_R_T = 0.;
  } else if (T_rec < 3e4) {                  // Temperature maximum = 30,000 K
    const double logT = log10(T_rec);        // Interpolate in log space
    while (logT < logT_SH95[i_L_T])
      i_L_T--;                               // Search temperature indices
    frac_R_T = (logT - logT_SH95[i_L_T]) / (logT_SH95[i_L_T+1] - logT_SH95[i_L_T]);
  }
  if (n_e >= 1e8) {                          // Density maximum = 10^8 cm^-3
    i_L_n = 5;
    frac_R_n = 1.;
  } else if (n_e > 1e2) {                    // Density minimum = 10^2 cm^-3
    const double d_logn = log10(n_e) - 2.;   // Table coordinates (log n_e)
    const double f_logn = floor(d_logn);     // Floor coordinate
    frac_R_n = d_logn - f_logn;              // Interpolation fraction
    i_L_n = int(f_logn);                     // Lower table index
  }

  // Bilinear interpolation (based on left and right fractions)
  const int i_R_T = i_L_T + 1, i_R_n = i_L_n + 1;
  const double frac_L_T = 1. - frac_R_T, frac_L_n = 1. - frac_R_n;
  return frac_L_T * (Ptab[i_L_T][i_L_n]*frac_L_n + Ptab[i_L_T][i_R_n]*frac_R_n)
       + frac_R_T * (Ptab[i_R_T][i_L_n]*frac_L_n + Ptab[i_R_T][i_R_n]*frac_R_n);
}

/* Recombination coefficient (HII Case B) - Units: cm^3/s */
static inline double alpha_B_HII(const double T) {
  // Case B recombination coefficient - Units: cm^3/s
  // α_B(T) is from Hui & Gnedin (1997) good to 0.7% accuracy from 1 K < T < 10^9 K
  const double lambda = 315614. / T;         // 2 T_i/T where T_i = 157,807 K
  return 2.753e-14 * pow(lambda, 1.5) / pow(1. + pow(lambda / 2.74, 0.407), 2.242);
}

/* Lyman-alpha effective recombination coefficient (Case B) - Units: cm^3/s */
static double alpha_eff_B_Lya(const double T, const double n_e) {
  return P_B(P_Lya_SH95, T, n_e) * alpha_B_HII(T); // α_eff_B = P_B α_B
}

/* Balmer-alpha effective recombination coefficient (Case B) - Units: cm^3/s */
static double alpha_eff_B_Ha(const double T, const double n_e) {
  return P_B(P_Ha_SH95, T, n_e) * alpha_B_HII(T); // α_eff_B = P_B α_B
}

/* Balmer-beta effective recombination coefficient (Case B) - Units: cm^3/s */
static double alpha_eff_B_Hb(const double T, const double n_e) {
  return P_B(P_Hb_SH95, T, n_e) * alpha_B_HII(T); // α_eff_B = P_B α_B
}

// static const double // Table 3 from Scholz & Walters (1991)
//   c0L = -1.630155e2,   c0M =  5.279996e2,   c0H = -2.8133632e3,
//   c1L =  8.795711e1,   c1M = -1.939399e2,   c1H =  8.1509685e2,
//   c2L = -2.057117e1,   c2M =  2.718982e1,   c2H = -9.4418414e1,
//   c3L =  2.359573,     c3M = -1.883399,     c3H =  5.4280565,
//   c4L = -1.339059e-1,  c4M =  6.462462e-2,  c4H = -1.546712e-1,
//   c5L =  3.021507e-3,  c5M = -8.811076e-4,  c5H =  1.7439112e-3;
// /* Lyman-alpha collisional excitation rate coefficient - Units: cm^3/s */
// static double q_col_Lya(const double T) {
//   // q_1s2p = Equation 17 from Scholz & Walters (1991) (divided by hν)
//   const double y = log(T);
//   if(T < 6e4)                                // Low temperature range
//     return exp(-T0/T + c0L + y * (c1L + y * (c2L + y * (c3L + y * (c4L + y * c5L)))));
//   else if(T > 6e6)                           // High temperature range
//     return exp(-T0/T + c0H + y * (c1H + y * (c2H + y * (c3H + y * (c4H + y * c5H)))));
//   else                                       // Mid temperature range
//     return exp(-T0/T + c0M + y * (c1M + y * (c2M + y * (c3M + y * (c4M + y * c5M)))));
// }

// /* Balmer-alpha collisional excitation rate coefficient - Units: cm^3/s */
// static double q_col_Ha(const double T) {
//   // Hα emission based on the fitting formula from Aggarwal (1983)
//   // Collisional excitation (1->3) followed by radiative de-excitation (3->2)
//   double Gamma_13; // Maxwellian-averaged effective collision strength
//   if(T < 4e3 || T > 5e5)
//     return 0.;     // Likely an unphysical range for collisional excitation
//   else if (T < 2.5e4)
//     Gamma_13 = 0.35 - T * (2.62e-7 + T * (8.15e-11 - 6.19e-15 * T));
//   else
//     Gamma_13 = 0.276 + T * (4.99e-6 - T * (8.85e-12 - 7.18e-18 * T));
//   return q_coef * Gamma_13 / sqrt(T) * exp(-T_13/T);
// }

/* Neutral hydrogen collisional excitation cooling coefficient - Units: erg cm^3/s */
static double e_col_HI(const double T) {
  // Based on Maxwellian-averaged effective collision strengths (N <= 5)
  if (T >= 3e5) // Physical range for collisional excitation (0.5 - 25 eV)
    return (6.107169036302359e-11 * exp(-T_12/T) + 1.5693136239683913e-11 * exp(-T_13/T)
      + 6.665146531965045e-12 * exp(-T_14/T) + 3.4378000369213142e-12 * exp(-T_15/T)) * q_coef / sqrt(T);
  const double T6 = 1e-6 * T;                // T6 = T / (10^6 K)
  return ( E_12 * (0.616414 + T6 * (16.8152 + T6 * (-32.0571 + 35.5428 * T6))) * exp(-T_12/T)
         + E_13 * (0.217382 + T6 * (3.92604 + T6 * (-10.6349 + 13.7721 * T6))) * exp(-T_13/T)
         + E_14 * (0.0959324 + T6 * (1.89951 + T6 * (-6.96467 + 10.6362 * T6))) * exp(-T_14/T)
         + E_15 * (0.0747075 + T6 * (0.670939 + T6 * (-2.28512 + 3.4796 * T6))) * exp(-T_15/T) )
         * q_coef / sqrt(T);                 // 4.3e-6 sum[E_ij G_ij exp(-T_ij/T)] / sqrt(T)
}

/* Lyman-alpha collisional excitation rate coefficient - Units: cm^3/s */
static double q_col_Lya(const double T) {
  // Based on Maxwellian-averaged effective collision strengths (N <= 5)
  if (T >= 3e5) // Physical range for collisional excitation (0.5 - 25 eV)
    return (3.2642689 * exp(-T_12/T) + 0.23382237 * exp(-T_13/T)
      + 0.13772838 * exp(-T_14/T) + 0.07453572 * exp(-T_15/T)) * q_coef / sqrt(T);
  const double T6 = 1e-6 * T;                // T6 = T / (10^6 K)
  return ( (0.348928 + T6 * (15.2426 + T6 * (-25.6168 + 24.0027 * T6))) * exp(-T_12/T)
         + (0.1233 + T6 * (1.01857 + T6 * (-3.93958 + 5.90791 * T6))) * exp(-T_13/T)
         + (0.0544296 + T6 * (0.762786 + T6 * (-2.99013 + 4.57684 * T6))) * exp(-T_14/T)
         + (0.0424305 + T6 * (0.262379 + T6 * (-0.939078 + 1.40402 * T6))) * exp(-T_15/T) )
         * q_coef / sqrt(T);                 // 4.3e-6 sum[G_ij exp(-T_ij/T)] / sqrt(T)
}

/* Balmer-alpha collisional excitation rate coefficient - Units: cm^3/s */
static double q_col_Ha(const double T) {
  // Based on Maxwellian-averaged effective collision strengths (N <= 5)
  if (T >= 3e5) // Physical range for collisional excitation (0.5 - 25 eV)
    return (0.8098997 * exp(-T_13/T) + 0.09820212 * exp(-T_14/T) + 0.052600338 * exp(-T_15/T)) * q_coef / sqrt(T);
  const double T6 = 1e-6 * T;                // T6 = T / (10^6 K)
  return ( (0.217382 + T6 * (3.92604 + T6 * (-10.6349 + 13.7721 * T6))) * exp(-T_13/T)
         + (0.0372036 + T6 * (0.508162 + T6 * (-1.86495 + 2.82946 * T6))) * exp(-T_14/T)
         + (0.029568 + T6 * (0.165593 + T6 * (-0.52784 + 0.772594 * T6))) * exp(-T_15/T) )
         * q_coef / sqrt(T);                 // 4.3e-6 sum[G_ij exp(-T_ij/T)] / sqrt(T)
}

/* Balmer-beta collisional excitation rate coefficient - Units: cm^3/s */
static double q_col_Hb(const double T) {
  // Based on Maxwellian-averaged effective collision strengths (N <= 5)
  if (T >= 3e5) // Physical range for collisional excitation (0.5 - 25 eV)
    return (0.22794233 * exp(-T_14/T) + 0.016138328 * exp(-T_15/T)) * q_coef / sqrt(T);
  const double T6 = 1e-6 * T;                // T6 = T / (10^6 K)
  return ( (0.0587288 + T6 * (1.39135 + T6 * (-5.09972 + 7.80679 * T6))) * exp(-T_14/T)
         + (0.00844856 + T6 * (0.0682112 + T6 * (-0.277137 + 0.450694 * T6))) * exp(-T_15/T) )
         * q_coef / sqrt(T);                 // 4.3e-6 sum[G_ij exp(-T_ij/T)] / sqrt(T)
}

/* General setup for cameras. */
void Simulation::setup_cameras() {
  Vec3 x_axis, y_axis;                       // Image orientation axes
  for (auto& dir : camera_directions) {
    dir.normalize();                         // Normalize camera directions
    if (1. - fabs(dir.z) > 1e-9) {           // Use (0,0,1) to build basis
      // y-axis = z_hat - proj(dir,z_hat) = z_hat - dir[z] * dir
      y_axis.x = -dir.x * dir.z;             // Orthogonal projection (+z)
      y_axis.y = -dir.y * dir.z;
      y_axis.z = 1. - dir.z * dir.z;
    } else if (dir.z > 0.) {                 // Degenerate case (+z)
      // y-axis = y_hat - proj(dir,y_hat) = y_hat - dir[y] * dir
      y_axis.x = -dir.x * dir.y;             // Orthogonal projection (+y)
      y_axis.y = 1. - dir.y * dir.y;
      y_axis.z = -dir.z * dir.y;
    } else {                                 // Degenerate case (-z)
      // y-axis = x_hat - proj(dir,x_hat) = x_hat - dir[x] * dir
      y_axis.x = 1. - dir.x * dir.x;         // Orthogonal projection (+x)
      y_axis.y = -dir.y * dir.x;
      y_axis.z = -dir.z * dir.x;
    }
    y_axis.normalize();                      // Make it a unit vector
    x_axis = y_axis.cross(dir);              // Final orthogonal axis
    x_axis.normalize();                      // Ensure normalization
    camera_xaxes.push_back(x_axis);          // Populate x-axes
    camera_yaxes.push_back(y_axis);          // Populate y-axes
  }

  // Camera center
  Vec3 dist_L = camera_center - bbox[0];     // Left bbox distances
  Vec3 dist_R = bbox[1] - camera_center;     // Right bbox distances
  const double min_L = dist_L.min();         // Min left bbox distance
  const double min_R = dist_R.min();         // Min right bbox distance
  const double dist_min = (min_L < min_R) ? min_L : min_R; // Min bbox distance
  if (dist_min < 0.)
    error("camera_center " + to_string(camera_center) +
          " is outside the bounding box [" + to_string(bbox[0]) +
          ", " + to_string(bbox[1]) + "]");  // Validate escape_center range

  // Consistency checks for image width and number of pixels
  if (image_radius_bbox > 0.)
    image_radius = dist_min * image_radius_bbox; // Set based on distance to bbox
  if (image_radius >= 0.)
    image_width = 2. * image_radius;         // Set image width from radius
  if (image_width_cMpc > 0.)
    image_width = image_width_cMpc * Mpc / (1. + z); // Image width from comoving Mpc
  if (image_width <= 0.) {
    const double max_L = dist_L.max();       // Max left bbox distance
    const double max_R = dist_R.max();       // Max right bbox distance
    const double dist_max = (max_L > max_R) ? max_L : max_R; // Max bbox distance
    image_width = 2. * dist_max;             // Default to capture the entire domain
  }
  if (image_width > 3. * max_bbox_width)
    image_width = 3. * max_bbox_width;       // Avoid very large images

  // Set the pixel width from pixel_arcsec or n_pixels
  if (pixel_arcsec > 0.)
    pixel_width = pixel_arcsec * d_L / (pow(1. + z, 2) * arcsec); // Pixel width from arcseconds
  if (pixel_width > 0.) {
    n_pixels = int(image_width / pixel_width); // Number of full pixels that can fit
    if (n_pixels <= 0) {
      n_pixels = 1;                          // Ensure at least one pixel
      pixel_width = image_width;             // Update the pixel width
    }
    image_width = double(n_pixels) * pixel_width; // Update the width
  } else {
    pixel_width = image_width / double(n_pixels); // Pixel width
    if (cosmological)
      pixel_arcsec = pixel_width * arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for a pixel
  }

  // Set redundant image parameters
  image_radius = 0.5 * image_width;          // Set radius based on width
  inverse_pixel_width = 1. / pixel_width;    // Inverse pixel width
  pixel_area = pixel_width * pixel_width;    // Pixel area [cm^2]
  if (cosmological)
    pixel_arcsec2 = pixel_arcsec * pixel_arcsec; // Set pixel arcsec^2

  // Calculate image edge coordinates
  image_edges = linspace(-image_radius, image_radius, n_pixels+1);
}

/* General setup for spherical escape. */
void Simulation::setup_spherical_escape() {
  // Escape center
  Vec3 dist_L = escape_center - bbox[0];   // Left bbox distances
  Vec3 dist_R = bbox[1] - escape_center;   // Right bbox distances
  const double min_L = dist_L.min();       // Min left bbox distance
  const double min_R = dist_R.min();       // Min right bbox distance
  const double dist_min = (min_L < min_R) ? min_L : min_R; // Min bbox distance
  if (dist_min < 0.)
    error("escape_center " + to_string(escape_center) +
          " is outside the bounding box [" + to_string(bbox[0]) +
          ", " + to_string(bbox[1]) + "]"); // Validate escape_center range

  // Escape radius
  if (escape_radius_bbox > 0.)
    escape_radius = dist_min * escape_radius_bbox; // Set based on distance to bbox
  if (escape_radius == 0.)
    escape_radius = dist_min;              // Min distance to bbox
  escape_epsilon = 1e-10 * escape_radius;  // Escape uncertainty [cm]
  escape_radius2 = escape_radius * escape_radius; // Escape radius^2 [cm^2]

  // Emission radius
  if (emission_radius_bbox > 0.)
    emission_radius = dist_min * emission_radius_bbox; // Set based on distance to bbox
  if (emission_radius == 0. || emission_radius > escape_radius - escape_epsilon)
    emission_radius = escape_radius - escape_epsilon;
  emission_radius2 = emission_radius * emission_radius; // Emission radius^2 [cm^2]
  if (point_source) {
    const Vec3 dr_point = r_point - escape_center; // Position relative to center
    if (dr_point.dot() >= emission_radius2)
      error("Point source location must be within the spherical emission region");
  }
}

//  Computes the following dimensionless integral:
//   ∞  y^2 dy    ∞  y^2 dy     15   ∞           | x^3   3x^2  6 x    6  |            hν
//   ∫ -------- / ∫ --------  = ---  Σ  exp(-nx) | --- + --- + --- + --- |  where x = --
//   x exp(y)-1   0 exp(y)-1    π^4 n=1          |  n    n^2   n^3   n^4 |            kT
//
/* Compute the fraction of the planck function above a given spectral coordinate. */
static double planck_fraction(const double x) {
  const double x2 = x * x / 2.;              // x^2 / 2
  const double x3 = x * x2 / 3.;             // x^3 / 6
  double y, term, sum = 0.;
  for (int i = 1; i < 10000; ++i) {
    y = 1. / double(i);
    term = exp(-double(i)*x) * y * (x3 + y * (x2 + y * (x + y)));
    sum += term;
    if (term < 1e-14 * sum)
      break;                                 // Converged
  }
  return 90. * sum / pow(M_PI, 4);           // (15/pi^4) * 6 * sum
}

/* Setup everything for line radiative transfer. */
void MCRT::setup() {
  // Point source insertion at r = (x_point, y_point, z_point)
  if (point_source)
    i_point = find_cell(r_point, 0);         // Start with default cell = 0

  // Setup frequency information
  // Make sure n_bins and freq_bin_width are compatible
  if (freq_bin_width > 0.)
    n_bins = int((freq_max - freq_min) / freq_bin_width);
  else
    freq_bin_width = (freq_max - freq_min) / double(n_bins); // Bin width
  inverse_freq_bin_width = double(n_bins) / (freq_max - freq_min); // Bin width^-1
  // Camera quantities are in units of redshifted wavelength [obs. angstrom]
  if (cosmological)                          // Convert from velocity units [km/s]
    observed_bin_width = freq_bin_width * km * lambda0 * (1. + z) / c; // Wavelength

  // Setup RGB frequency ranges
  if (output_rgb_images)
    inverse_rgb_freq_bin_width = 256. / (rgb_freq_max - rgb_freq_min); // Bin width^-1

  // Setup continuum ranges
  if (continuum) {
    Dv_cont_res = Dv_cont_max - Dv_cont_min; // Velocity offset [km/s]
    l_cont_res = Dv_cont_res * km * lambda0 / c; // Wavelength [angstrom]
    R_cont_res = c / (Dv_cont_res * km);     // Spectral resolution
  }

  // Setup shell radius
  if (shell_source && geometry == spherical) {
    if (r_shell < 1.000001 * r[0].x)
      r_shell = 1.000001 * r[0].x;           // Minimum shell radius [cm]
    if (r_shell > 0.999999 * r[n_cells].x)
      r_shell = 0.999999 * r[n_cells].x;     // Maximum shell radius [cm]
    if (shell_blackbody) {
      if (T_shell <= 0.) {
        const Vec3 point = {r_shell, 0., 0.}; // Arbitrary point on the sphere
        const int i_shell = find_cell(point, 0); // Find the host cell
        T_shell = T[i_shell];                // Temperature at the shell [K]
      }
      const double x_shell = T0 / T_shell;   // Spectral coordinate of the line
      const double xmin = x_shell / (Dv_cont_max/c + 1.); // Spectral coordinates
      const double xmax = x_shell / (Dv_cont_min/c + 1.); // of the continuum range
      const double frac = planck_fraction(xmin) - planck_fraction(xmax); // Fraction in range
      const double A_shell = 4. * M_PI * r_shell * r_shell; // Shell surface area [cm^2]
      const double sigmaB = 2. * pow(M_PI,5) * pow(kB,4) / (15. * pow(h,3) * pow(c,2)); // Stephan-Boltzmann constant [erg/cm^2/K^4/s]
      L_shell = frac * A_shell * sigmaB * pow(T_shell, 4); // Stefan-Boltzmann law [erg/s]
    }
  }
  if (shell_source)
    L_tot += L_shell;                        // Add to total luminosity

  // Setup shock radius
  if (shock_source && geometry == spherical) {
    if (r_shock < 1.000001 * r[0].x)
      r_shock = 1.000001 * r[0].x;           // Minimum shock radius [cm]
    if (r_shock > 0.999999 * r[n_cells].x)
      r_shock = 0.999999 * r[n_cells].x;     // Maximum shock radius [cm]
    if (shock_blackbody) {
      if (T_shock <= 0.) {
        const Vec3 point = {r_shock, 0., 0.}; // Arbitrary point on the sphere
        const int i_shock = find_cell(point, 0); // Find the host cell
        T_shock = T[i_shock];                // Temperature at the shock [K]
      }
      const double x_shock = T0 / T_shock;   // Spectral coordinate of the line
      const double xmin = x_shock / (Dv_cont_max/c + 1.); // Spectral coordinates
      const double xmax = x_shock / (Dv_cont_min/c + 1.); // of the continuum range
      const double frac = planck_fraction(xmin) - planck_fraction(xmax); // Fraction in range
      const double A_shock = 4. * M_PI * r_shock * r_shock; // Shock surface area [cm^2]
      const double sigmaB = 2. * pow(M_PI,5) * pow(kB,4) / (15. * pow(h,3) * pow(c,2)); // Stephan-Boltzmann constant [erg/cm^2/K^4/s]
      L_shock = frac * A_shock * sigmaB * pow(T_shock, 4); // Stefan-Boltzmann law [erg/s]
    }
  }
  if (shock_source)
    L_tot += L_shock;                        // Add to total luminosity
  if (shell_source || shock_source)
    M_cont = -2.5 * log10(fnu_cont_fac * (L_shell + L_shock)) - 48.6; // Continuum absolute magnitude

  // Setup spherical escape
  if (spherical_escape)
    setup_spherical_escape();                // General spherical escape setup

  // Setup cameras
  if (have_cameras) {
    setup_cameras();                         // General camera setup

    if (root && output_cubes)
      cout << "\nSpectral Data Cubes: "
           << double(n_cameras) * double(n_pixels) * double(n_pixels) * double(n_bins) * 8. / 1024. / 1024. / 1024.
           << " GB  (" << n_cameras << ", " << n_pixels << ", " << n_pixels << ", " << n_bins << ")" << endl;

    // Reset camera data and initialize with zeros
    if (output_escape_fractions)
      f_escs = vector<double>(n_cameras);    // Escape fractions
    if (output_freq_avgs)
      freq_avgs = vector<double>(n_cameras); // Frequency averages
    if (output_freq_stds)
      freq_stds = vector<double>(n_cameras); // Frequency standard deviations
    if (output_freq_skews)
      freq_skews = vector<double>(n_cameras); // Frequency skewnesses
    if (output_freq_kurts)
      freq_kurts = vector<double>(n_cameras); // Frequency kurtoses
    if (output_fluxes)
      fluxes = vector<vector<double>>(n_cameras, vector<double>(n_bins)); // Fluxes
    if (output_images)
      images = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
    if (output_images2)
      images2 = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
    if (output_freq_images)
      freq_images = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
    if (output_freq2_images)
      freq2_images = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
    if (output_freq3_images)
      freq3_images = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
    if (output_freq4_images)
      freq4_images = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
    if (output_rgb_images)
      rgb_images = vector<Image<Vec3>>(n_cameras, Image<Vec3>(n_pixels, n_pixels));
    if (output_cubes)
      cubes = vector<Cube<double>>(n_cameras, Cube<double>(n_pixels, n_pixels, n_bins));

    // Initialize intrinsic cameras (mcrt)
    if (output_mcrt_emission) {
      if (output_freq_avgs)
        freq_avgs_int = vector<double>(n_cameras); // Frequency averages
      if (output_fluxes)
        fluxes_int = vector<vector<double>>(n_cameras, vector<double>(n_bins)); // Fluxes
      if (output_images)
        images_int = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
      if (output_cubes)
        cubes_int = vector<Cube<double>>(n_cameras, Cube<double>(n_pixels, n_pixels, n_bins));
    }

    // Initialize attenuation cameras (mcrt)
    if (output_mcrt_attenuation) {
      if (output_escape_fractions)
        f_escs_ext = vector<double>(n_cameras); // Escape fractions
      if (output_freq_avgs)
        freq_avgs_ext = vector<double>(n_cameras); // Frequency averages
      if (output_fluxes)
        fluxes_ext = vector<vector<double>>(n_cameras, vector<double>(n_bins)); // Fluxes
      if (output_images)
        images_ext = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
      if (output_cubes)
        cubes_ext = vector<Cube<double>>(n_cameras, Cube<double>(n_pixels, n_pixels, n_bins));
    }

    // Initialize intrinsic cameras (proj)
    if (output_proj_emission)
      proj_images_int = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));

    // Initialize attenuation cameras (proj)
    if (output_proj_attenuation)
      proj_images_ext = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));

    if (output_proj_emission || output_proj_attenuation)
      setup_projections();                   // General projections setup

    // Collisional excitation camera data
    if (output_collisions) {
      // Reset camera data and initialize with zeros
      if (output_escape_fractions)
        f_escs_col = vector<double>(n_cameras); // Escape fractions
      if (output_freq_avgs)
        freq_avgs_col = vector<double>(n_cameras); // Frequency averages
      if (output_freq_stds)
        freq_stds_col = vector<double>(n_cameras); // Frequency standard deviations
      if (output_freq_skews)
        freq_skews_col = vector<double>(n_cameras); // Frequency skewnesses
      if (output_freq_kurts)
        freq_kurts_col = vector<double>(n_cameras); // Frequency kurtoses
      if (output_fluxes)
        fluxes_col = vector<vector<double>>(n_cameras, vector<double>(n_bins)); // Fluxes
      if (output_images)
        images_col = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
      if (output_images2)
        images2_col = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
      if (output_freq_images)
        freq_images_col = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
      if (output_freq2_images)
        freq2_images_col = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
      if (output_freq3_images)
        freq3_images_col = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
      if (output_freq4_images)
        freq4_images_col = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));

      // Initialize intrinsic cameras (mcrt)
      if (output_mcrt_emission) {
        if (output_freq_avgs)
          freq_avgs_int_col = vector<double>(n_cameras); // Frequency averages
        if (output_fluxes)
          fluxes_int_col = vector<vector<double>>(n_cameras, vector<double>(n_bins)); // Fluxes
        if (output_images)
          images_int_col = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
      }

      // Initialize attenuation cameras (mcrt)
      if (output_mcrt_attenuation) {
        if (output_escape_fractions)
          f_escs_ext_col = vector<double>(n_cameras); // Escape fractions
        if (output_freq_avgs)
          freq_avgs_ext_col = vector<double>(n_cameras); // Frequency averages
        if (output_fluxes)
          fluxes_ext_col = vector<vector<double>>(n_cameras, vector<double>(n_bins)); // Fluxes
        if (output_images)
          images_ext_col = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
      }

      // Initialize intrinsic cameras (proj)
      if (output_proj_emission)
        proj_images_int_col = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));

      // Initialize attenuation cameras (proj)
      if (output_proj_attenuation)
        proj_images_ext_col = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
    }
  }

  // Setup cell-based emission sources
  if (cell_based_emission) {
    // Assign a function pointer to the correct lines
    double (*alpha_eff_B)(double,double){ return_zero };
    double (*q_col)(double){ return_zero };
    if (line == "Lyman-alpha") {
      if (recombinations)
        alpha_eff_B = alpha_eff_B_Lya;
      if (collisions)
        q_col = q_col_Lya;
    } else if (line == "Balmer-alpha") {
      if (recombinations)
        alpha_eff_B = alpha_eff_B_Ha;
      if (collisions)
        q_col = q_col_Ha;
    } else if (line == "Balmer-beta") {
      if (recombinations)
        alpha_eff_B = alpha_eff_B_Hb;
      if (collisions)
        q_col = q_col_Hb;
    }

    // Calculate the pdf based on cell luminosities
    const double X_prim = 0.76;              // Primordial mass fraction of hydrogen
    const bool restrict_n = (emission_n_min > 0.) || (emission_n_max < positive_infinity);
    const double hc_A21 = h * c * A21;       // Coefficient for spontaneous emission
    j_cdf.resize(n_cells);                   // Cumulative distribution function
    j_weights.resize(n_cells);               // Cell luminosity boosting weights
    if (save_line_emissivity)
      j_line.resize(n_cells);                // Cell emissivities
    if (output_collisions)
      f_col_cells.resize(n_cells);           // Collisional excitation fractions
    #pragma omp parallel for reduction(+:L_rec) reduction(+:L_col) reduction(+:L_sp)
    for (int i = 0; i < n_cells; ++i) {
      // Avoid cells that are special or outside the emission radius
      if (avoid_cell(i) || (spherical_escape &&
         (cell_center(i) - escape_center).dot() >= emission_radius2)) {
        j_cdf[i] = 0.;                       // No emission
        if (save_line_emissivity)
          j_line[i] = 0.;                    // No emission
        if (output_collisions)
          f_col_cells[i] = 0.;               // No emission
        continue;
      }
      if (restrict_n) {
        const double n_tot = rho[i] * (1. + X_prim * (3. + 4. * x_e[i])) / (4. * mH);
        if (n_tot < emission_n_min || n_tot > emission_n_max) {
          j_cdf[i] = 0.;                     // No emission
          if (save_line_emissivity)
            j_line[i] = 0.;                  // No emission
          if (output_collisions)
            f_col_cells[i] = 0.;             // No emission
          continue;
        }
      }
#ifdef CARTESIAN
      const double V_cell = dV;              // Constant cell volume [cm^3]
#else
      const double V_cell = V[i];            // Cell volume [cm^3]
#endif
      const double n_e = x_e[i] * n_H[i];    // Electron number density [cm^-3]
      const double E0_nH_ne = E0 * n_H[i] * n_e; // Shared constant [erg/cm^6]
      // Recombination emission: L_rec = hν ∫ α_eff_B(T) n_e n_p dV
      const double j_rec_cell = E0_nH_ne * x_HII[i] * alpha_eff_B(T[i], n_e); // [erg/s/cm^3]
      const double L_rec_cell = j_rec_cell * V_cell;
      L_rec += L_rec_cell;                   // Recombination luminosity [erg/s]
      // Collisional excitation: L_col = hν ∫ q_col(T) n_e n_HI dV
      const double q_col_T = q_col(T[i]);    // Save the collisional coefficient
      double j_col_cell = E0_nH_ne * x_HI[i] * q_col_T; // [erg/s/cm^3]
      double L_col_cell = j_col_cell * V_cell;
      if (collisions_limiter > 0.) {         // Limit collisional excitation
        const double e_HI = e_col_HI(T[i]);  // Total collisional excitation [erg/s]
        if (e_HI > 0.) {
          const double f_line = E0 * q_col_T / e_HI; // Fraction in this line
          const double L_col_lim = collisions_limiter * G_ion[i] * f_line;
          if (L_col_cell > L_col_lim) {
            L_col_cell = L_col_lim;          // Limited by photoheating rate
            j_col_cell = L_col_lim / V_cell; // Update emissivity too
          }
        }
      }
      L_col += L_col_cell;                   // Collisional luminosity [erg/s]
      // Spontaneous emission: L_sp = hc A21 ∫ n2 dV
      const double j_sp_cell = spontaneous ? hc_A21 * n_upper[i] : 0.;
      const double L_sp_cell = j_sp_cell * V_cell;
      L_sp += L_sp_cell;                     // Spontaneous emission luminosity [erg/s]
      // Track the total luminosity from each cell
      j_cdf[i] = L_rec_cell + L_col_cell + L_sp_cell;
      if (save_line_emissivity)
        j_line[i] = j_rec_cell + j_col_cell + j_sp_cell;
      if (output_collisions)                 // Collisional excitation fraction
        f_col_cells[i] = (L_col_cell > 0.) ? L_col_cell / j_cdf[i] : 0.;
    }

    L_tot += L_rec + L_col + L_sp;           // Add to the total luminosity

    const double _1_L_tot = 1. / (L_rec + L_col + L_sp);
    const double _1_n_photons = 1. / double(n_photons);
    if (j_exp == 1) {                        // No luminosity boosting
      for (int i = 1; i < n_cells; ++i)
        j_cdf[i] += j_cdf[i - 1];            // Turn the pdf into a cdf
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        j_cdf[i] *= _1_L_tot;                // Normalize cdf
        j_weights[i] = _1_n_photons;         // Equal weights
      }
    } else {                                 // Apply luminosity boosting
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        j_weights[i] = j_cdf[i] * _1_L_tot;  // Copy the normalized pdf
        if (j_weights[i] > 0.)               // Apply power law boost
          j_cdf[i] = pow(j_weights[i], j_exp); // pdf^j_exp (unnormalized)
        else
          j_cdf[i] = 0.;                     // No probability
      }
      const double _1_j_exp_sum = 1. / omp_sum(j_cdf);
      const double dn_photons = double(n_photons);
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        j_cdf[i] *= _1_j_exp_sum;            // pdf^j_exp (normalized)
        if (j_cdf[i] > 0.)
          j_weights[i] /= j_cdf[i] * dn_photons; // weight = pdf / (pdf^j_exp * n_photons)
      }
      for (int i = 1; i < n_cells; ++i)
        j_cdf[i] += j_cdf[i - 1];            // Turn the pdf into a cdf
    }

    // Populate a lookup table to quickly find cdf indices (cells)
    const double half_cdf = 0.5 / double(n_cdf); // Half interval width
    for (int i = 0, cell = 0; i < n_cdf; ++i) {
      const double frac_cdf = double(2*i + 1) * half_cdf; // Probability interval midpoint
      while (j_cdf[cell] <= frac_cdf && cell < n_cells - 1)
        cell++;                              // Move to the right bin ( --> )
      j_map[i] = cell;                       // Lookup table for insertion
    }
  }

  // Setup star-based emission sources
  if (star_based_emission) {
    // Calculate the pdf based on star luminosities
    j_cdf_stars.resize(n_stars);             // Cumulative distribution function
    j_weights_stars.resize(n_stars);         // Star luminosity boosting weights
    #pragma omp parallel for reduction(+:L_cont)
    for (int i = 0; i < n_stars; ++i) {
      // Avoid stars that are in special cells or outside the emission radius
      if (avoid_cell(find_cell(r_star[i], 0)) || (spherical_escape &&
         (r_star[i] - escape_center).dot() >= emission_radius2)) {
        j_cdf_stars[i] = 0.;                 // No emission
        continue;
      }
      L_cont_star[i] *= l_cont_res;          // Band range conversion [erg/s]
      L_cont += L_cont_star[i];              // Line continuum [erg/s]
      j_cdf_stars[i] = L_cont_star[i];       // Copy luminosities to build pdf
    }

    M_cont = -2.5 * log10(fnu_cont_fac * L_cont) - 48.6; // Continuum absolute magnitude
    L_tot += L_cont;                         // Add to the total luminosity

    const double _1_L_tot = 1. / L_cont;
    const double _1_n_photons = 1. / double(n_photons);
    if (j_exp == 1) {                        // No luminosity boosting
      for (int i = 1; i < n_stars; ++i)
        j_cdf_stars[i] += j_cdf_stars[i - 1]; // Turn the pdf into a cdf
      #pragma omp parallel for
      for (int i = 0; i < n_stars; ++i) {
        j_cdf_stars[i] *= _1_L_tot;          // Normalize cdf
        j_weights_stars[i] = _1_n_photons;   // Equal weights
      }
    } else {                                 // Apply luminosity boosting
      #pragma omp parallel for
      for (int i = 0; i < n_stars; ++i) {
        j_weights_stars[i] = j_cdf_stars[i] * _1_L_tot;  // Copy the normalized pdf
        if (j_weights_stars[i] > 0.)         // Apply power law boost
          j_cdf_stars[i] = pow(j_weights_stars[i], j_exp); // pdf^j_exp (unnormalized)
        else
          j_cdf_stars[i] = 0.;               // No probability
      }
      const double _1_j_exp_sum = 1. / omp_sum(j_cdf_stars);
      const double dn_photons = double(n_photons);
      #pragma omp parallel for
      for (int i = 0; i < n_stars; ++i) {
        j_cdf_stars[i] *= _1_j_exp_sum;      // pdf^j_exp (normalized)
        if (j_cdf_stars[i] > 0.)
          j_weights_stars[i] /= j_cdf_stars[i] * dn_photons; // weight = pdf / (pdf^j_exp * n_photons)
      }
      for (int i = 1; i < n_stars; ++i)
        j_cdf_stars[i] += j_cdf_stars[i - 1]; // Turn the pdf into a cdf
    }

    // Populate a lookup table to quickly find cdf indices (stars)
    const double half_cdf = 0.5 / double(n_cdf_stars); // Half interval width
    for (int i = 0, star = 0; i < n_cdf_stars; ++i) {
      const double frac_cdf = double(2*i + 1) * half_cdf; // Probability interval midpoint
      while (j_cdf_stars[star] <= frac_cdf && star < n_stars - 1)
        star++;                              // Move to the right bin ( --> )
      j_map_stars[i] = star;                 // Lookup table for insertion
    }
  }

  // Calculate the mixed source pdf based on luminosities
  {
    // Setup mixed source properties (names, flags, and probabilities)
    const array<string, MAX_SOURCE_TYPES> source_names = {"point", "cells", "stars", "shell", "shock"};
    const array<bool, MAX_SOURCE_TYPES> source_flags = {point_source, cell_based_emission, star_based_emission, shell_source, shock_source};
    mixed_cdf = mixed_pdf = {L_point, L_rec + L_col + L_sp, L_cont, L_shell, L_shock}; // Unnormalized pdf
    n_source_types = 0;
    for (int i = 0; i < MAX_SOURCE_TYPES; ++i)
      if (source_flags[i])
        n_source_types++;
    if (n_source_types == 0)
      error("No source types have been specified.");

    // Same procedure as above
    const double _1_L_tot = 1. / omp_sum(mixed_pdf);
    if (j_exp == 1) {                        // No luminosity boosting
      for (int i = 1; i < MAX_SOURCE_TYPES; ++i)
        mixed_cdf[i] += mixed_cdf[i - 1];    // Turn the pdf into a cdf
      for (int i = 0; i < MAX_SOURCE_TYPES; ++i) {
        mixed_pdf[i] *= _1_L_tot;            // Normalize pdf
        mixed_cdf[i] *= _1_L_tot;            // Normalize cdf
        mixed_weights[i] = 1.;               // Equal weights
      }
    } else {                                 // Apply luminosity boosting
      for (int i = 0; i < MAX_SOURCE_TYPES; ++i) {
        mixed_weights[i] = mixed_pdf[i] *= _1_L_tot; // Copy the normalized pdf
        if (mixed_pdf[i] > 0.)               // Apply power law boost
          mixed_cdf[i] = pow(mixed_pdf[i], j_exp); // pdf^j_exp (unnormalized)
        else
          mixed_cdf[i] = 0.;                 // No probability
      }
      const double _1_j_exp_sum = 1. / omp_sum(mixed_cdf);
      for (int i = 0; i < MAX_SOURCE_TYPES; ++i) {
        mixed_cdf[i] *= _1_j_exp_sum;        // pdf^j_exp (normalized)
        if (mixed_cdf[i] > 0.)
          mixed_weights[i] /= mixed_cdf[i];  // weight = pdf / pdf^j_exp
      }
      for (int i = 1; i < MAX_SOURCE_TYPES; ++i)
        mixed_cdf[i] += mixed_cdf[i - 1];    // Turn the pdf into a cdf
    }

    // Setup masked versions
    for (int i = 0; i < MAX_SOURCE_TYPES; ++i)
      if (source_flags[i]) {
        source_types_mask.push_back(i);
        source_names_mask.push_back(source_names[i]);
        mixed_pdf_mask.push_back(mixed_pdf[i]);
        mixed_cdf_mask.push_back(mixed_cdf[i]);
        mixed_weights_mask.push_back(mixed_weights[i]);
      }
  }

  // Reserve photon output buffers
  if (output_photons) {
    source_ids.resize(n_photons);            // Emission cell or star index
    if (n_source_types > 1)
      source_types.resize(n_photons);        // Emission type specifier
    source_weights.resize(n_photons);        // Photon weight at emission
    if (output_collisions)
      f_cols.resize(n_photons);              // Collisional excitation fraction
    freqs.resize(n_photons);                 // Frequency at escape [freq_units]
    weights.resize(n_photons);               // Photon weight at escape
    positions.resize(n_photons);             // Position at escape [cm]
    directions.resize(n_photons);            // Direction at escape
  }

  // Calculate the emission properties (without continuum)
  Ndot_tot = (L_tot - L_cont - L_shell - L_shock) / E0; // erg/s -> photons/s

  /* Calculate exit factors */
  const double sqrtT_exit = sqrt(T_exit);
  a_exit = a_sqrtT / sqrtT_exit;
  DnuD_exit = DnuD_div_sqrtT * sqrtT_exit;
  vth_div_c_exit = vth_div_sqrtT * sqrtT_exit / c;
  neg_vth_kms_exit = -vth_div_sqrtT * sqrtT_exit / km;
  if (exit_wrt_com) { // Allow photons to exit w.r.t. the center of mass frame
    double m_tot = 0.;                       // Total mass [g]
    double vx = 0., vy = 0., vz = 0.;        // Local center of mass velocities
    #pragma omp parallel for reduction(+:m_tot) reduction(+:vx) reduction(+:vy) reduction(+:vz)
    for (int i = 0; i < n_cells; ++i) {
      if (avoid_cell(i))
        continue;
#ifdef CARTESIAN
      const double m_i = rho[i] * dV;        // m = rho V
#else
      const double m_i = rho[i] * V[i];      // m = rho V
#endif
      m_tot += m_i;                          // sum(m)
      vx += v[i].x * m_i;                    // sum(m * vx)
      vy += v[i].y * m_i;                    // sum(m * vy)
      vz += v[i].z * m_i;                    // sum(m * vz)
    }
    v_exit = Vec3(vx,vy,vz) / m_tot;         // sum(m * v) / sum(m)
  }

  // Convert v_exit to code units: u = v / vth
  v_exit /= vth_div_sqrtT * sqrtT_exit;

  const double gamma = 5. / 3.;              // Adiabatic index
  const double turb_coef = km * km / (gamma * kB); // Density scaling [K cm^3/g]

  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i) {
    if (avoid_cell(i))
        continue;

    // Convert from cm/s to unitless velocity: u = v / vth = v / sqrt(2*kB*T/m_carrier)
    double T_turb_local = T_turb;            // Minimum microturbulence value
    if (scaled_microturb && turb_coef * rho[i] > T_turb)
      T_turb_local = turb_coef * rho[i];     // Update T_turb based on scaling
    const double _1_vth = 1. / (vth_div_sqrtT * sqrt(T[i] + T_turb_local));
    v[i] *= _1_vth;
  }

  if (dynamical_core_skipping)
    atau_NL_calculation();                   // Note: Needs v in units of vth
  else
    x_crit_2 = x_crit * x_crit;              // Constant core-skipping: x_crit^2
}
