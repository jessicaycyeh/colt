/******************
 * mcrt/config.cc *
 ******************

 * Module configuration for the simulation.

*/

#include "proto.h"
#include "MCRT.h"
#include "../config.h" // Configuration functions
#include "../io_hdf5.h" // HDF5 read/write functions

vector<Vec3> healpix_vectors(int n_side); // Healpix directions
void set_line_and_dust_parameters_default(); // Line and dust properties (default)
void set_line_and_dust_parameters_derived(); // Line and dust properties (derived)

/* General camera configuration. */
void Simulation::camera_config(YAML::Node& file, YAML::Node& node) {
  if (file["n_exp"]) {
    load("n_exp", n_exp);                    // Healpix exponent for cameras
    if (n_exp < 0)
      error("n_exp must be >= 0");           // Validate n_exp range
    camera_directions = healpix_vectors(n_exp); // Populate directions
    have_cameras = true;                     // Camera-based output
  }
  if (file["n_rot"]) {                       // Rotate around an axis
    load("n_rot", n_rot);                    // Number of rotation vectors
    if (n_rot <= 0)
      error("n_rot must be > 0");            // Validate n_rot range
    // load("rotation_axis", rotation_axis);    // Rotation axis, e.g. (0,0,1)
    // rotation_axis.normalize();               // Ensure normalization
    double phi = 0.;                         // Azimuthal angle
    const double phi_factor = 2. * M_PI / double(n_rot); // 2 pi / n_rot
    for (int i = 0; i < n_rot; ++i) {
      phi = double(i) * phi_factor;
      camera_directions.push_back(Vec3(cos(phi), sin(phi), 0.));
    }
    have_cameras = true;                     // Camera-based output
  }
  if (file["cameras"]) {
    load("cameras", camera_directions);      // List of camera directions
    have_cameras = true;                     // Camera-based output
  }
  if (file["camera"]) {
    Vec3 camera;
    load("camera", camera);                  // Add camera direction manually
    n_cameras++;                             // Track the number of cameras
    camera_directions.push_back(camera);     // Add camera direction to list
    have_cameras = true;                     // Camera-based output
  }
  if (have_cameras) {
    n_cameras = camera_directions.size();    // Update the number of cameras
    load("camera_center", camera_center);    // Camera target position [cm]
    if (file["image_radius_bbox"]) {
      load("image_radius_bbox", image_radius_bbox); // Image radius [min distance to bbox edge]
      if (image_radius_bbox < 0.)
        error("image_radius_bbox must be >= 0"); // Validate image_radius_bbox range
      if (file["image_radius"])
        error("Cannot specify both image_radius and image_radius_bbox in " + config_file);
      if (file["image_width_cMpc"])
        error("Cannot specify both image_width_cMpc and image_radius_bbox in " + config_file);
      if (file["image_width"])
        error("Cannot specify both image_width and image_radius_bbox in " + config_file);
    } else if (file["image_radius"]) {
      load("image_radius", image_radius);    // Image radius [cm] (image_width / 2)
      if (image_radius < 0.)
        error("image_radius must be >= 0");  // Validate image_radius range
      if (file["image_width_cMpc"])
        error("Cannot specify both image_width_cMpc and image_radius in " + config_file);
      if (file["image_width"])
        error("Cannot specify both image_width and image_radius in " + config_file);
    } else if (file["image_width_cMpc"]) {
      load("image_width_cMpc", image_width_cMpc); // Image width [cMpc]
      if (image_width_cMpc < 0.)
        error("image_width_cMpc must be >= 0"); // Validate image_width_cMpc range
      if (file["image_width"])
        error("Cannot specify both image_width_cMpc and image_width in " + config_file);
    } else {
      load("image_width", image_width);      // Image width [cm] (defaults to entire domain)
      if (image_width < 0.)
        error("image_width must be >= 0");   // Validate image_width range
    }
    if (file["pixel_arcsec2"]) {
      load("pixel_arcsec2", pixel_arcsec2);  // Pixel area [arcsec^2] (optional)
      if (pixel_arcsec2 <= 0.)
        error("pixel_arcsec2 must be > 0");  // Validate pixel_arcsec2 range
      pixel_arcsec = sqrt(pixel_arcsec2);    // Set pixel arcsec from arcsec^2
      if (file["pixel_arcsec"])
        error("Cannot specify both pixel_arcsec and pixel_arcsec2 in " + config_file);
      if (file["pixel_width"])
        error("Cannot specify both pixel_width and pixel_arcsec2 in " + config_file);
      if (file["n_pixels"])
        error("Cannot specify both n_pixels and pixel_arcsec2 in " + config_file);
      if (!cosmological)
        error("Cannot accept pixel_arcsec2 for non-cosmological simulations in " + config_file);
    } else if (file["pixel_arcsec"]) {
      load("pixel_arcsec", pixel_arcsec);    // Pixel width [arcsec] (optional)
      if (pixel_arcsec <= 0.)
        error("pixel_arcsec must be > 0");   // Validate pixel_arcsec range
      if (file["pixel_width"])
        error("Cannot specify both pixel_width and pixel_arcsec in " + config_file);
      if (file["n_pixels"])
        error("Cannot specify both n_pixels and pixel_arcsec in " + config_file);
      if (!cosmological)
        error("Cannot accept pixel_arcsec for non-cosmological simulations in " + config_file);
    } else if (file["pixel_width"]) {
      load("pixel_width", pixel_width);      // Pixel width [cm] (optional)
      if (pixel_width <= 0.)
        error("pixel_width must be > 0");    // Validate pixel_width range
      if (file["n_pixels"])
        error("Cannot specify both n_pixels and pixel_width in " + config_file);
    } else {                                 // Set n_pixels directly
      load("n_pixels", n_pixels);            // Number of x,y image pixels
      if (n_pixels <= 0)
        error("n_pixels must be > 0");       // Validate n_pixels range
    }
  }
}

/* General spherical escape configuration. */
void Simulation::spherical_escape_config(YAML::Node& file, YAML::Node& node) {
  if (geometry != spherical)
    load("spherical_escape", spherical_escape); // Photons escape from a sphere
  if (spherical_escape) {
    load("escape_center", escape_center);    // Center of the escape region [cm]
    if (file["escape_radius_bbox"]) {
      load("escape_radius_bbox", escape_radius_bbox); // Radius for spherical escape [min distance to bbox edge]
      if (escape_radius_bbox < 0.)
        error("escape_radius_bbox must be >= 0"); // Validate escape_radius_bbox range
    } else {
      load("escape_radius", escape_radius);  // Radius for spherical escape [cm]
      if (escape_radius < 0.)
        error("escape_radius must be >= 0"); // Validate escape_radius range
    }
    if (file["emission_radius_bbox"]) {
      load("emission_radius_bbox", emission_radius_bbox); // Radius for spherical emission [min distance to bbox edge]
      if (emission_radius_bbox < 0.)
        error("emission_radius_bbox must be >= 0"); // Validate emission_radius_bbox range
    } else {
      load("emission_radius", emission_radius);  // Radius for spherical emission [cm]
      if (emission_radius < 0.)
        error("emission_radius must be >= 0"); // Validate emission_radius range
    }
  }
}

/* Module configuration for the simulation. */
void MCRT::module_config(YAML::Node& file, YAML::Node& node) {
  // Parallelization strategy
  if (file["load_balancing"])
    load("load_balancing", load_balancing);  // Use MPI load balancing algorithms

  // Set the line and dust properties: Opacity, albedo, scattering anisotropy
  load("line", line);                        // Name of the line (default "Lyman-alpha")
  if (file["dust_model"])
    load("dust_model", dust_model);          // Dust model: SMC, MW, etc.
  set_line_and_dust_parameters_default();    // Set default line/dust properties
  load("two_level_atom", two_level_atom);    // Two level atom line transfer
  if (two_level_atom) {
    line_scattering = true;                  // Enforce line scattering
    load("spontaneous", spontaneous);        // Include spontaneous emission
  }
  if (line_scattering)
    load("line_scattering", line_scattering); // Turn line scattering on/off

  // Information about the dust model (allow overriding default model)
  if (file["kappa_dust"]) {
    if (file["sigma_dust"])
      error("Cannot specify both kappa_dust and sigma_dust");
    if (dust_model == "LAURSEN_SMC")
      error("Cannot specify kappa_dust and dust_model = LAURSEN_SMC");
  }
  if (file["sigma_dust"] && dust_model == "LAURSEN_SMC")
    error("Cannot specify sigma_dust and dust_model = LAURSEN_SMC");
  load("kappa_dust", kappa_dust);            // Dust opacity [cm^2/g dust]
  load("sigma_dust", sigma_dust);            // Dust cross section [cm^2/Z/hydrogen atom]
  load("albedo", albedo);                    // Dust albedo for scattering vs. absorption
  load("g_dust", g_dust);                    // Anisotropy parameter: <μ> for dust scattering
  load("f_ion", f_ion);                      // H_II region survival fraction
  load("T_sputter", T_sputter);              // Thermal sputtering cutoff [K]
  if (kappa_dust < 0.)
    error("kappa_dust must be >= 0");        // Validate kappa_dust range
  if (sigma_dust < 0.)
    error("sigma_dust must be >= 0");        // Validate sigma_dust range
  if (albedo < 0. || albedo > 1.)
    error("albedo must be in the range [0,1]"); // Validate albedo range
  if (g_dust < -1. || g_dust > 1.)
    error("g_dust must be in the range [-1,1]"); // Validate g_dust range
  if (fabs(g_dust) < 1e-6)
    g_dust = (g_dust < 0.) ? -1e-6 : 1e-6;   // Avoid division by zero
  if (f_ion < 0. || f_ion > 1.)
    error("f_ion must be in the range [0,1]"); // Validate f_ion range
  if (file["metallicity"]) {
    load("metallicity", metallicity);        // Constant metallicity [mass fraction]
    if (metallicity < 0. || metallicity > 1.)
      error("metallicity must be in the range [0,1]"); // Validate metallicity range
  }
  if (file["dust_to_metal"]) {
    if (file["dust_to_gas"])
      error("Cannot specify both dust_to_metal and dust_to_gas");
    load("dust_to_metal", dust_to_metal);    // Constant dust-to-metal ratio
    if (dust_to_metal < 0. || dust_to_metal > 1.)
      error("dust_to_metal must be in the range [0,1]"); // Validate dust_to_metal range
  }
  if (file["dust_to_gas"]) {
    load("dust_to_gas", dust_to_gas);        // Constant dust-to-gas ratio
    if (dust_to_gas < 0. || dust_to_gas > 1.)
      error("dust_to_gas must be in the range [0,1]"); // Validate dust_to_gas range
  }
  if (file["dust_boost"]) {
    load("dust_boost", dust_boost);          // Optional dust boost factor
    if (dust_boost <= 0.)
      error("dust_boost must be positive");  // Validate dust_boost range
  }
  set_line_and_dust_parameters_derived();    // Set derived properties after override

  // Microturbulence
  if (file["T_turb"]) {
    load("T_turb", T_turb);                  // Microturbulent temperature [K]
    if (file["v_turb"] || file["v_turb_kms"])
      error("Cannot specify both v_turb[_kms] and T_turb");
    if (T_turb <= 0.)
      error("T_turb must be > 0");           // Validate T_turb range
    v_turb = vth_div_sqrtT * sqrt(T_turb);   // Set turbulent velocity [cm/s]
  } else if (file["v_turb_kms"]) {
    double v_turb_kms = 0.;                  // Microturbulent velocity [km/s]
    load("v_turb_kms", v_turb_kms);
    if (file["v_turb"])
      error("Cannot specify both v_turb and v_turb_kms");
    if (v_turb_kms < 0.)
      error("v_turb_kms must be >= 0");      // Validate v_turb_kms range
    v_turb = v_turb_kms * km;                // Convert to [cm/s]
    const double sqrt_T_turb = v_turb / vth_div_sqrtT;
    T_turb = sqrt_T_turb * sqrt_T_turb;      // Set turbulent temperature [K]
  } else {
    load("v_turb", v_turb);                  // Microturbulent velocity [cm/s]
    if (v_turb < 0.)
      error("v_turb must be >= 0");          // Validate v_turb range
    const double sqrt_T_turb = v_turb / vth_div_sqrtT;
    T_turb = sqrt_T_turb * sqrt_T_turb;      // Set turbulent temperature [K]
  }
  load("scaled_microturb", scaled_microturb); // Density scaling flag

  // Ionization parameters
  if (file["constant_temperature"]) {
    load("constant_temperature", constant_temperature); // Constant temperature: T [K]
    if (constant_temperature < 0.)
      error("constant_temperature must be positive"); // Validate constant_temperature range
  } else if (file["T_floor"]) {
    load("T_floor", T_floor);                // Apply a temperature floor: T [K]
    if (T_floor < 0.)
      error("T_floor must be positive");     // Validate T_floor range
  }
  if (file["neutral_fraction"]) {
    load("neutral_fraction", neutral_fraction); // Constant neutral fraction: x_HI
    if (neutral_fraction < 0. || neutral_fraction > 1.)
      error("neutral_fraction must be in the range [0,1]"); // Validate neutral_fraction range
  }
  load("set_density_from_mass", set_density_from_mass); // Calculate density as mass / volume
  load("read_density_as_mass", read_density_as_mass); // Read density as mass
  if (file["read_hydrogen_fraction"])
    load("read_hydrogen_fraction", read_hydrogen_fraction); // Read mass fraction of hydrogen
  load("read_ionized_fraction", read_ionized_fraction); // Read x_HII from the input file
  load("read_electron_fraction", read_electron_fraction); // Read x_e from the input file

  // Configurable line transfer flags
  load("recombinations", recombinations);    // Include recombination emission
  if (file["T_floor_rec"]) {
    load("T_floor_rec", T_floor_rec);        // Apply a recombination temperature floor: T [K]
    if (T_floor_rec < 0.)
      error("T_floor_rec must be positive"); // Validate T_floor_rec range
  }
  load("collisions", collisions);            // Include collisional excitation
  load("output_collisions", output_collisions); // Output collisional excitation data
  if (output_collisions && !collisions)
    root_error("Requested output_collisions but collisions is false!");
  if (collisions && file["collisions_limiter"]) {
    load("collisions_limiter", collisions_limiter); // Limited by photoheating rate
    read_photoheating = true;                // Read photoheating rates from file
    if (collisions_limiter < 1.)
      error("collisions_limiter must be >= 1"); // Validate collisions_limiter range
  }
  load("continuum", continuum);              // Include continuum emission
  if (line_scattering) {
    load("dynamical_core_skipping", dynamical_core_skipping); // Dynamical core-skipping
    if (dynamical_core_skipping) {
      load("n_exp_atau", n_exp_atau);        // Healpix exponent for a*tau0
      if (n_exp_atau < 0 || n_exp_atau > 5)
        error("n_exp_atau must be an integer in the range [0,5]"); // Validate n_exp_atau range
    } else {
      load("x_crit", x_crit);                // Critical frequency for core-skipping
      if (x_crit < 0.)
        error("x_crit must be >= 0");        // Validate x_crit range
    }
  }
  load("doppler_frequency", doppler_frequency); // Output frequency in Doppler widths
  if (doppler_frequency && cosmological)
    error("Doppler frequency units should not be used for cosmological simulations.");
  if (line_scattering)
    load("recoil", recoil);                  // Include recoil with scattering

  // Internal simulation flags
  if (recombinations || collisions || spontaneous)
    cell_based_emission = true;              // Include cell-based sources
  if (continuum)
    star_based_emission = true;              // Include star-based sources
  if (cell_based_emission || star_based_emission) {
    load("j_exp", j_exp);                    // Luminosity boosting exponent
    if (j_exp <= 0. || j_exp > 1.)
      error("j_exp must be in the range (0,1]"); // Validate j_exp range
  }
  if (cell_based_emission) {
    if (file["emission_n_min"])
      load("emission_n_min", emission_n_min); // Minimum number density of emitting cells [cm^-3]
    if (file["emission_n_max"])
      load("emission_n_max", emission_n_max); // Maximum number density of emitting cells [cm^-3]
    if (emission_n_min < 0.)
      error("emission_n_min cannot be negative"); // Validate ranges
    if (emission_n_min >= emission_n_max)
      error("emission_n_min must be smaller than emission_n_max")
  }

  // Point source variables
  if (file["x_point"]) {
    load("x_point", x_point);                // Point source x position [cm]
    r_point.x = x_point;                     // (x, y, z) [cm]
    if (!point_source)
      point_source = true;                   // Update point source flag
  }
  if (file["y_point"]) {
    load("y_point", y_point);                // Point source y position [cm]
    r_point.y = y_point;                     // (x, y, z) [cm]
    if (!point_source)
      point_source = true;                   // Update point source flag
  }
  if (file["z_point"]) {
    load("z_point", z_point);                // Point source z position [cm]
    r_point.z = z_point;                     // (x, y, z) [cm]
    if (!point_source)
      point_source = true;                   // Update point source flag
  }
  if (file["point"]) {
    load("point", r_point);                  // Specified as sequence
    x_point = r_point.x;                     // Update (x,y,z) for consistency
    y_point = r_point.y;
    z_point = r_point.z;
    if (!point_source)
      point_source = true;                   // Update point source flag
  }
  if (point_source) {
    L_point = 1.;                            // Change the default from zero
    load("L_point", L_point);                // Point source luminosity [erg/s]
    L_tot += L_point;                        // Add to total luminosity
    if (L_point <= 0.)
      error("L_point must be > 0");          // Validate L_point range
  }

  // Shell source variables
  if (file["r_shell"]) {
    load("r_shell", r_shell);                // Shell radius [cm]
    shell_source = true;                     // Update shell source flag
    L_shell = 1.;                            // Change the default from zero
    load("shell_blackbody", shell_blackbody); // Set the luminosity based on a black body
    if (shell_blackbody) {
      load("T_shell", T_shell);              // Shell black body temperature [K]
    } else {
      load("L_shell", L_shell);              // Shell source luminosity [erg/s]
      if (L_shell <= 0.)
        error("L_shell must be > 0");        // Validate L_shell range
    }
  }

  // Shock source variables
  if (file["r_shock"]) {
    load("r_shock", r_shock);                // Shock radius [cm]
    shock_source = true;                     // Update shock source flag
    L_shock = 1.;                            // Change the default from zero
    load("shock_blackbody", shock_blackbody); // Set the luminosity based on a black body
    if (shock_blackbody) {
      load("T_shock", T_shock);              // Shock black body temperature [K]
    } else {
      load("L_shock", L_shock);              // Shock source luminosity [erg/s]
      if (L_shock <= 0.)
        error("L_shock must be > 0");        // Validate L_shock range
    }
  }

  if (shell_source || shock_source) {
    if (continuum)                           // Avoid strange configurations
      error("Stellar continuum sources should not be used with shell/shock sources.")
    continuum = true;                        // Continuum emission source
  }

  // Escape variables
  spherical_escape_config(file, node);       // General spherical escape setup
  load("output_photons", output_photons);    // Output escaped photon packets
  if (output_photons)
    load("photon_file", photon_file);        // Output a separate photon file
  // if (output_photons)
  //   load("escaped_frequency_moments", escaped_frequency_moments); // Calculate escaped frequency moments
  if (geometry != spherical) {
    load("exit_wrt_com", exit_wrt_com);      // Exit with respect to the center of mass frame
    if (!exit_wrt_com)
      load("v_exit", v_exit);                // Exit velocity [cm/s]
  }
  load("T_exit", T_exit);                    // Exit temperature [K]
  if (T_exit <= 0.)
    error("T_exit must be > 0");             // Validate T_exit range

  // Information about the simulation
  load("n_photons", n_photons);              // Number of photon packets
  if (n_photons <= 0)
    error("n_photons must be > 0");          // Validate n_photons range

  // Information about the cameras
  camera_config(file, node);                 // General camera setup
  if (have_cameras) {
    load("output_freq_kurts", output_freq_kurts); // Output kurtosis values
    // Output skewness values (Note: kurt requires skew)
    load_if_false(output_freq_kurts, "output_freq_skews", output_freq_skews);
    // Output standard deviation values (Note: skew requires std)
    load_if_false(output_freq_skews, "output_freq_stds", output_freq_stds);
    // Output average frequency values (Note: std requires avg)
    load_if_false(output_freq_stds, "output_freq_avgs", output_freq_avgs);
    // Output camera escape fractions (Note: freq_avg requires f_esc)
    load_if_false(output_freq_avgs, "output_escape_fractions", output_escape_fractions);
    load("output_fluxes", output_fluxes);    // Output spectral fluxes
    load("output_images2", output_images2);  // Output statistical moment images
    load("output_rgb_images", output_rgb_images); // Output flux-weighted frequency RGB images
    load("output_freq4_images", output_freq4_images); // Output frequency^4 images
    // Output frequency^3 images (Note: freq4 requires freq3)
    load_if_false(output_freq4_images, "output_freq3_images", output_freq3_images);
    // Output frequency^2 images (Note: freq3 requires freq2)
    load_if_false(output_freq3_images, "output_freq2_images", output_freq2_images);
    // Output average frequency images (Note: freq2 requires freq)
    load_if_false(output_freq2_images, "output_freq_images", output_freq_images);
    // Output surface brightness images (Note: images2, rgb, and freq require images)
    load_if_false(output_images2 || output_rgb_images || output_freq_images, "output_images", output_images);
    load("output_cubes", output_cubes);      // Output spectral data cubes
    load("output_mcrt_emission", output_mcrt_emission); // Output intrinsic emission without transport (mcrt)
    load("output_mcrt_attenuation", output_mcrt_attenuation); // Output attenuated emission without scattering (mcrt)
    load("output_proj_emission", output_proj_emission); // Output intrinsic emission without transport (proj)
    load("output_proj_attenuation", output_proj_attenuation); // Output attenuated emission without scattering (proj)
    if (output_proj_emission || output_proj_attenuation) {
      projection_config(file, node);         // General projection setup
      save_line_emissivity = true;           // Save emissivity data for projections
    }
    // Check camera output compatibility
    if (have_cameras && !(output_escape_fractions || output_fluxes || output_images || output_cubes))
      root_error("Cameras were requested without any camera output types.");
    load("n_bins", n_bins);                  // Number of frequency bins
    if (n_bins <= 0)
      error("n_bins must be > 0");           // Validate n_bins range

    if (file["freq_range"]) {
      vector<double> freq_lims = {freq_min, freq_max};
      load("freq_range", freq_lims);         // Generic frequency extrema [freq units]
      freq_min = freq_lims[0];
      freq_max = freq_lims[1];
    } else {
      load("freq_min", freq_min);            // Generic frequency extrema [freq units]
      load("freq_max", freq_max);
    }
    if (freq_min >= freq_max)
      error("freq_min must be < freq_max");  // Validate freq_min range

    if (output_rgb_images) {
      if (file["rgb_freq_range"]) {
        vector<double> rgb_freq_lims = {rgb_freq_min, rgb_freq_max};
        load("rgb_freq_range", rgb_freq_lims); // Generic RGB frequency extrema [freq units]
        rgb_freq_min = rgb_freq_lims[0];
        rgb_freq_max = rgb_freq_lims[1];
      } else {
        load("rgb_freq_min", rgb_freq_min);  // Generic RGB frequency extrema [freq units]
        load("rgb_freq_max", rgb_freq_max);
      }
      if (rgb_freq_min >= rgb_freq_max)
        error("rgb_freq_min must be < rgb_freq_max"); // Validate rgb_freq_min range
    }

    if (continuum) {
      vector<double> c_lims = {Dv_cont_min, Dv_cont_max};
      load("continuum_range", c_lims);       // Continuum velocity offset range [km/s]
      Dv_cont_min = c_lims[0];               // Continuum velocity offset lower limit [km/s]
      Dv_cont_max = c_lims[1];               // Continuum velocity offset upper limit [km/s]
      if (Dv_cont_min >= Dv_cont_max)
        error("Dv_cont_min must be < Dv_cont_max"); // Validate Dv_cont_min range
    }

    // Camera frequency offsets
    if (file["adjust_camera_frequency"])     // Apply camera frequency offsets
      load("adjust_camera_frequency", adjust_camera_frequency);
    if (adjust_camera_frequency) {
      string freq_offset_file = "";          // Frequency offset file
      if (node["init_base"]) {
        if (node["init_dir"]) {
          string freq_offset_dir = init_dir; // Set default from init_dir
          load("freq_offset_dir", freq_offset_dir);
          freq_offset_file = freq_offset_dir + "/"; // Prepend directory info
        }
        string freq_offset_base = init_base; // Set default from init_base
        load("freq_offset_base", freq_offset_base);
        string freq_offset_ext = init_ext;   // Set default from init_ext
        load("freq_offset_ext", freq_offset_ext);
        freq_offset_file += freq_offset_base + snap_str + halo_str + "." + freq_offset_ext;
      } else {
        freq_offset_file = init_file;        // Set default from init_file
        load("freq_offset_file", freq_offset_file);
      }
      // Read the frequency offset data from the hdf5 file
      H5File f(freq_offset_file, H5F_ACC_RDONLY);
      const string freq_units = (doppler_frequency) ? "Doppler" : "km/s";
      const int n_cams = read(f, "freq_avgs", freq_offsets, freq_units);
      if (n_cams != n_cameras)
        error("Requested " + to_string(n_cameras) + " cameras but read " +
              to_string(n_cams) + " frequency offsets. [adjust_camera_frequency]");
    }
  } else {
    output_escape_fractions = output_fluxes = output_images = false; // Reset defaults
  }
}
