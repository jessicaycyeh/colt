/**************
 * mcrt/io.cc *
 **************

 * All I/O operations related to line radiative transfer.

*/

#include "proto.h"
#include "MCRT.h"
#include "../io_hdf5.h" // HDF5 read/write functions

bool avoid_cell(const int cell); // Avoid calculations for certain cells
Vec3 cell_center(const int cell); // Center of the cell
tuple<double, double> min_max(vector<double>& vec); // min/max of a vector
void print_min_max(string name, vector<double> vec, string units = "", double conv = 1.);
void print_min_max(string name, vector<Vec3> vec, string units = "", double conv = 1.);
double ray_integral(vector<double>& field, Vec3 point, Vec3 direction, int cell); // Integrate a scalar field
void write_sim_info(const H5File& f); // Write general simulation information

/* Print general camera parameters. */
void Simulation::print_cameras() {
  cout << "\nCamera parameters:"
       << "\n  n_cameras  = " << n_cameras
       << "\n  n_pixels   = " << n_pixels;
  if (n_exp >= 0)
    cout << "\n  n_exp      = " << n_exp << " (Note: Healpix in RING ordering)";
  cout << "\n  center     = " << camera_center / kpc << " kpc" << endl;
  if (verbose) {
    print("  directions", camera_directions);
    print("  x-axes    ", camera_xaxes);
    print("  y-axes    ", camera_yaxes);
  }
  cout << "\nImage information:"
       << "\n  width      = " << image_width / kpc << " kpc"
       << "\n  radius     = " << image_radius / kpc << " kpc"
       << "\n  pixel dx   = " << pixel_width / pc << " pc" << endl;
  if (cosmological)
    cout << "  pixel dl   = " << pixel_arcsec << " arcsec\n"
         << "  pixel dA   = " << pixel_arcsec2 << " arcsec^2" << endl;
}

/* Print additional line radiative transfer information. */
void MCRT::print_info() {
  // Print line constants
  cout << "\nLine properties: " << line << endl;
  if (verbose) {
    cout << "  E0         = " << E0 << " erg"
         << "\n  A21        = " << A21 << " Hz"
         << "\n  f12        = " << f12
         << "\n  g12        = " << g12
         << "\n  T0         = " << T0 << " K"
         << "\n  nu0        = " << nu0 << " Hz"
         << "\n  lambda0    = " << lambda0 << " angstrom"
         << "\n  B21        = " << B21 << " cgs"
         << "\n  DnuL       = " << DnuL << " Hz";
    if (v_turb > 0.) {
      cout << "\n  v_turb     = " << v_turb / km << " km/s"
           << "\n  T_turb     = " << T_turb << " K";
      if (scaled_microturb)
        cout << "  (scales with density)";
    }
    if (line_scattering)
      cout << "\n  gDa        = " << gDa;
    cout << "\n  m_carrier  = " << m_carrier << " g" << endl;
  }

  // Calculate the frequency resolution
  double x_res, nu_res, l_res, Dv_res, R_res;
  const double sqrtT_point = sqrt(T[i_point] + T_turb);
  const double vth_0 = vth_div_sqrtT * sqrtT_point;
  const double DnuD_0 = DnuD_div_sqrtT * sqrtT_point;
  if (doppler_frequency) { // Doppler frequency: x = (nu - nu0) / Delta_nu_D
    x_res = freq_bin_width;
    nu_res = x_res * DnuD_0;
    l_res = x_res * lambda0 * vth_0 / c;
    Dv_res = x_res * vth_0 / km;
    R_res = c / (x_res * vth_0);
  } else { // Velocity offset (km/s)
    Dv_res = freq_bin_width;
    x_res = Dv_res * km / vth_0;
    nu_res = Dv_res * km * nu0 / c;
    l_res = Dv_res * km * lambda0 / c;
    R_res = c / (Dv_res * km);
  }

  // Print photon parameters
  string freq_units = ((doppler_frequency) ? "" : "km/s");
  cout << "\nPhoton parameters:"
       << "\n  n_photons  = " << n_photons
       << "\n  T_exit     = " << T_exit << " K"
       << "\n  v_exit     = (" << -neg_vth_kms_exit * v_exit[0] << ", "
       << -neg_vth_kms_exit * v_exit[1] << ", " << -neg_vth_kms_exit * v_exit[2] << ") km/s  "
       << (exit_wrt_com ? "(center of mass)" : "(user specified)");
  if (spherical_escape)
    cout << "\n  r_escape   = " << escape_radius / kpc << " kpc"
         << "\n  r_emission = " << emission_radius / kpc << " kpc"
         << "\n  esc center = " << escape_center / kpc << " kpc";
  cout << endl;

  cout << "\nDust properties:";
  if (dust_model != "")
    cout << " " << dust_model << " dust model";
  if (sigma_dust > 0.)
    cout << "\n  sigma_dust = " << sigma_dust << " cm^2/H (Z)";
  else
    cout << "\n  kappa_dust = " << kappa_dust << " cm^2/g (D)";
  if (kappa_dust > 0. || sigma_dust > 0.) {
    cout << "\n  albedo     = " << albedo
         << "\n  g_dust     = " << g_dust
         << "\n  f_ion      = " << f_ion
         << "\n  T_sputter  = " << T_sputter << " K";
  }
  cout << endl;

  if (point_source) {
    cout << "\nEmission includes a point source:"
         << "\n  r_point    = " << r_point / pc << " pc" << endl;
    if (verbose)
      cout << "  L_point    = " << L_point << " erg/s"
         << "\n  i_point    = " << i_point
         << "\nConsistency check:"
         << "\n  r[i_point] = " << cell_center(i_point) / pc << " pc"
         << "\n  v[i_point] = " << v[i_point] * (vth_div_sqrtT * sqrtT_point / km) << " km/s" << endl;
  }

  if (shell_source) {
    cout << "\nEmission includes a shell source:"
         << "\n  r_shell    = " << r_shell / pc << " pc";
    if (shell_blackbody)
      cout << "\n  T_shell    = " << T_shell << " K";
    cout << "\n  L_shell    = " << L_shell << " erg/s" << endl;
  }

  if (shock_source) {
    cout << "\nEmission includes a shock source:"
         << "\n  r_shock    = " << r_shock / pc << " pc";
    if (shock_blackbody)
      cout << "\n  T_shock    = " << T_shock << " K";
    cout << "\n  L_shock    = " << L_shock << " erg/s" << endl;
  }

  const double L_tot_cont = L_cont + L_shell + L_shock; // Continuum luminosity [erg/s]
  const double L_tot_no_cont = L_tot - L_tot_cont; // Non-continuum luminosity [erg/s]

  if (cell_based_emission) {
    if (recombinations)
      cout << "\nEmission includes recombination sources:  (L_rec/L_tot = "
           << L_rec / L_tot_no_cont << ")";
    if (collisions)
      cout << "\nEmission includes collisional excitation: (L_col/L_tot = "
           << L_col / L_tot_no_cont << ")";
    if (collisions_limiter > 0.)
      cout << "\nNote: Collisional excitation is limited to " << collisions_limiter
           << " times the photoheating rate";
    if (spontaneous)
      cout << "\nEmission includes spontaneous emission:  (L_sp/L_tot = "
           << L_sp / L_tot_no_cont << ")";
    cout << endl;
    if (verbose) {
      print("  j_cdf     ", j_cdf);
      print("  j_weights ", j_weights);
      if (output_collisions)
        print("  f_col     ", f_col_cells);
    }
  }

  if (continuum) {
    cout << "\nEmission includes stellar continuum sources:"
         << "\n  L_cont     = " << L_tot_cont << " erg/s"
         << "\n  L_cont/ang = " << L_tot_cont / l_cont_res << " erg/s/angstrom"
         << "\n  M_cont     = " << M_cont
         << "\n  Dv (range) = [" << Dv_cont_min << ", "
                                 << Dv_cont_max << "] km/s"
         << "\n  lambda res = " << l_cont_res << " angstoms" << endl;
    if (verbose && star_based_emission) {
      print("  j_cdf     ", j_cdf_stars);
      print("  j_weights ", j_weights_stars);
    }
  }

  if (n_source_types > 1)
    cout << "\nMixed source calculation uses: " << to_string(source_names_mask)
         << "\n  mixed pdf  = " << to_string(mixed_pdf_mask)
         << "\n  boost cdf  = " << to_string(mixed_cdf_mask)
         << "\n   weights   = " << to_string(mixed_weights_mask) << endl;

  cout << "\nStrength of the source:" << (continuum ? " (without continuum)" : "")
       << "\n  Luminosity = " << L_tot_no_cont << " erg/s"
       << "\n             = " << L_tot_no_cont / Lsun << " Lsun"
       << "\n  Prod. Rate = " << Ndot_tot << " photons/s" << endl;
  if (cosmological) {
    double flux_tot = L_tot_no_cont / (4. * M_PI * d_L * d_L * observed_bin_width);
    cout << "  Flux (Int) = " << flux_tot << " erg/s/cm^2/angstrom" << endl;
  }

  // Print camera parameters
  if (have_cameras) {
    string freq_units = ((doppler_frequency) ? "" : "km/s");
    cout << "\nFrequency resolution parameters:"
         << "\n  n_bins     = " << n_bins
         << "\n  Note: Frequencies are given in the rest frame.";
    if (adjust_camera_frequency)
      cout << "\n  Note: Applying camera frequency offsets.";
    cout << "\n  freq_type  = " << ((doppler_frequency) ? "Doppler Frequency" : "Velocity Offset")
         << "\n  freq_lims  = (" << freq_min << ", " << freq_max << ") " << freq_units
         << "\n  resolution = " << freq_bin_width << " " << freq_units;
    if (doppler_frequency)
      cout << "\n    |  x_res = " << x_res;
    cout << "\n    | nu_res = " << nu_res << " Hz"
         << "\n    |  l_res = " << l_res << " angstrom"
         << "\n    | Dv_res = " << Dv_res << " km/s"
         << "\n    |  R_res = " << R_res << endl;

    print_cameras();                         // General camera parameters

    if (output_proj_emission || output_proj_attenuation)
      cout << "\nProjection information: (" << (output_proj_emission ?
              (output_proj_attenuation ? "int, ext)" : "int)") : "ext)")
           << "\n  depth      = " << proj_depth / kpc << " kpc"
           << "\n  radius     = " << proj_radius / kpc << " kpc"
           << "\n  pixel_rtol = " << 100. * pixel_rtol << "%" << endl;
  }

  // Core-skipping parameters
  if (dynamical_core_skipping) {
    cout << "\nUsing a dynamical core-skipping acceleration scheme:"
         << "\n  [ atau0 (ratio) < " << atau0_tolerance
         << " ]  [ Delta_v < " << dv_tolerance << " vth ]";
    // Calculate how many cells have atau > 0 to estimate the efficiency
    int count = 0, n_avoid = 0;
    #pragma omp parallel for reduction(+:count) reduction(+:n_avoid)
    for (int i = 0; i < n_cells; ++i) {
      if (avoid_cell(i))
        n_avoid++;
      else if (atau[i] > 1.)
        count++;
    }
    cout << "\nEfficiency of non-local pre-calculation: (atau > 1) = "
         << 100. * double(count) / double(n_cells - n_avoid) << "%" << endl;
  } else if (x_crit > 0.)
    cout << "\nUsing constant core-skipping with x_crit = " << x_crit << endl;

  // Line data
  if (verbose) {
    cout << "\nLine data:" << endl;
    print("  k_0       ", k_0, "1/cm");
    print("  k_dust    ", k_dust, "1/cm");
    if (module == "ionization")
      print("  rho_dust  ", rho_dust, "g/cm^3");
    print("  a         ", a);
    if (dynamical_core_skipping)
      print("  atau      ", atau);

    double T_min, T_max;
    tie(T_min, T_max) = min_max(T); // Find the min/max values
    const double sqrtT_min = sqrt(T_min + T_turb);
    const double sqrtT_max = sqrt(T_max + T_turb);

    cout << "\nmin/max line data:" << endl;
    print_min_max("  (vx,vy,xz)", v, "vth");
    print_min_max("  k_0       ", k_0, "1/cm");
    print_min_max("  k_dust    ", k_dust, "1/cm");
    if (module == "ionization")
      print_min_max("  rho_dust  ", rho_dust, "g/cm^3");
    cout << "  T          = [" << T_min << ", " << T_max << "] K"
         << "\n  vth        = sqrt(2*kB*T/m_carrier + v_turb^2)"
         << "\n             = [" << vth_div_sqrtT * sqrtT_min / km << ", "
                                 << vth_div_sqrtT * sqrtT_max / km << "] km/s"
         << "\n  DnuD       = vth*nu0/c"
         << "\n             = [" << DnuD_div_sqrtT * sqrtT_min << ", "
                                 << DnuD_div_sqrtT * sqrtT_max << "] Hz"
         << "\n  a          = 0.5*DnuL/DnuD"
         << "\n             = [" << a_sqrtT / sqrtT_max << ", "
                                 << a_sqrtT / sqrtT_min << "]"
         << "\n  sigma0     = f12*sqrt(pi)*ee^2/(me*c*DnuD)"
         << "\n             = [" << sigma0_sqrtT / sqrtT_max << ", "
                                 << sigma0_sqrtT / sqrtT_min << "] cm^2" << endl;
    if (dynamical_core_skipping)
      print_min_max("  atau_NL   ", atau);
    if (point_source && have_cameras) { // Check ray_integral behavior
      double tau0_min = positive_infinity, tau0_max = negative_infinity;
      double tau_dust_min = positive_infinity, tau_dust_max = negative_infinity;
      double comp;  // Temporary comparison
      for (const auto& direction : camera_directions) {
        comp = ray_integral(k_0, r_point, direction, i_point); // τ_0 = Σ kH_0 dl
        if (comp < tau0_min)
          tau0_min = comp;
        if (comp > tau0_max)
          tau0_max = comp;
        comp = ray_integral(k_dust, r_point, direction, i_point); // τ_dust = Σ k_dust dl
        if (comp < tau_dust_min)
          tau_dust_min = comp;
        if (comp > tau_dust_max)
          tau_dust_max = comp;
      }
      cout <<   "  tau0 (LOS) = [" << tau0_min << ", " << tau0_max << "]"
           << "\n  tau_dust   = [" << tau_dust_min << ", " << tau_dust_max << "]" << endl;
    }
  }
}

/* Function to print additional data. */
void print_observables() {
  const string freq_units = (doppler_frequency) ? "Doppler" : "km/s";
  cout << "\nGlobal statistics:"
       << "\n   Escape fraction = " << f_esc
       << "\n Average frequency = " << freq_avg << " " << freq_units
       << "\nStandard deviation = " << freq_std << " " << freq_units
       << "\n Spectral skewness = " << freq_skew
       << "\n Spectral kurtosis = " << freq_kurt << endl;

  if (output_escape_fractions || output_proj_attenuation)
    cout << "\nCamera escape fractions:" << endl;
  if (output_escape_fractions) {
    print(" f_esc (LOS)", f_escs);
    if (output_mcrt_attenuation)
      print(" f_esc (ext)", f_escs_ext);
  }
  if (output_proj_attenuation)
    print(" f_esc (ray)", proj_f_escs_ext);

  if (output_freq_avgs) {
    cout << "\nCamera frequency statistics:" << endl;
    if (output_mcrt_emission)
      print("  avgs (int)", freq_avgs_int, freq_units);
    if (output_mcrt_attenuation)
      print("  avgs (ext)", freq_avgs_ext, freq_units);
    print("  avgs (LOS)", freq_avgs, freq_units);
    if (output_freq_stds) {
      print("  stds (LOS)", freq_stds, freq_units);
      if (output_freq_skews) {
        print(" skews (LOS)", freq_skews);
        if (output_freq_kurts)
          print(" kurts (LOS)", freq_kurts);
      }
    }
  }

  // Print additional collisional excitation data
  if (output_collisions) {
    cout << "\nCollisional excitation global statistics:"
         << "\n   Escape fraction = " << f_esc_col
         << "\n Average frequency = " << freq_avg_col << " " << freq_units
         << "\nStandard deviation = " << freq_std_col << " " << freq_units
         << "\n Spectral skewness = " << freq_skew_col
         << "\n Spectral kurtosis = " << freq_kurt_col << endl;

    if (output_escape_fractions || output_proj_attenuation)
      cout << "\nCollisional excitation camera escape fractions:" << endl;
    if (output_escape_fractions) {
      print(" f_esc (LOS)", f_escs_col);
      if (output_mcrt_attenuation)
        print(" f_esc (ext)", f_escs_ext_col);
    }
    if (output_proj_attenuation)
      print(" f_esc (ray)", proj_f_escs_ext_col);

    if (output_freq_avgs) {
      cout << "\nCollisional excitation camera frequency statistics:" << endl;
      if (output_mcrt_emission)
        print("  avgs (int)", freq_avgs_int_col, freq_units);
      if (output_mcrt_attenuation)
        print("  avgs (ext)", freq_avgs_ext_col, freq_units);
      print("  avgs (LOS)", freq_avgs_col, freq_units);
      if (output_freq_stds) {
        print("  stds (LOS)", freq_stds_col, freq_units);
        if (output_freq_skews) {
          print(" skews (LOS)", freq_skews_col);
          if (output_freq_kurts)
            print(" kurts (LOS)", freq_kurts_col);
        }
      }
    }
  }
}

/* Write general camera parameters. */
void Simulation::write_camera_info(const H5File& f) {
  write(f, "n_cameras", n_cameras);          // Number of cameras
  write(f, "n_pixels", n_pixels);            // Number of x,y image pixels
  if (n_exp >= 0)
    write(f, "n_exp", n_exp);                // Healpix exponent for cameras
  if (n_rot > 0) {
    write(f, "n_rot", n_rot);                // Number of rotation directions
    // write(f, "rotation_axis", rotation_axis); // Rotation axis, e.g. (0,0,1)
  }
  write(f, "camera_directions", camera_directions); // Normalized camera directions
  write(f, "camera_xaxes", camera_xaxes);    // Camera "x-axis" direction vectors (x,y,z)
  write(f, "camera_yaxes", camera_yaxes);    // Camera "y-axis" direction vectors (x,y,z)
  write(f, "camera_center", camera_center, "cm"); // Camera target position [cm]
  write(f, "image_width", image_width);      // Image width [cm] (defaults to entire domain)
  write(f, "image_radius", image_radius);    // Image radius [cm] (image_width / 2)
  write(f, "pixel_width", pixel_width);      // Pixel width [cm]
  write(f, "pixel_area", pixel_area);        // Pixel area [cm^2]
  if (cosmological) {
    write(f, "observed_bin_width", observed_bin_width); // Observed wavelength resolution [angstrom]
    write(f, "pixel_arcsec", pixel_arcsec);  // Pixel width [arcsec] (optional)
    write(f, "pixel_arcsec2", pixel_arcsec2); // Pixel area [arcsec^2] (optional)
  }
}

/* Write general simulation information. */
static void write_mcrt_info(const H5File& f) {
  // Line data
  {
    Group g = f.createGroup("/line");
    write(g, "name", line);                  // Line name, e.g. Lyman-alpha
    write(g, "E0", E0);                      // Line energy [erg]
    write(g, "A21", A21);                    // Einstein A coefficient [Hz]
    write(g, "f12", f12);                    // Oscillator strength
    write(g, "g12", g12);                    // Degeneracy ratio: g_lower / g_upper
    write(g, "T0", T0);                      // Line temperature [K]
    write(g, "nu0", nu0);                    // Line frequency [Hz]
    write(g, "lambda0", lambda0);            // Line wavelength [angstrom]
    write(g, "B21", B21);                    // Einstein B21 = A21 c^2 / (2 h ν0^3)
    write(g, "DnuL", DnuL);                  // Natural frequency broadening [Hz]
    write(g, "m_carrier", m_carrier);        // Mass of carrier [g]
    if (v_turb > 0.) {
      write(g, "v_turb", v_turb);            // Microturbulent velocity [cm/s]
      write(g, "T_turb", T_turb);            // Microturbulent temperature [K]
      if (scaled_microturb)
        write(g, "scaled_microturb", scaled_microturb); // Density scaling flag
    }
    write(g, "vth_div_sqrtT", vth_div_sqrtT); // Thermal velocity: vth = sqrt(2 kB T / m_carrier)
    write(g, "DnuD_div_sqrtT", DnuD_div_sqrtT); // Doppler width: ΔνD = ν0 vth / c
    write(g, "a_sqrtT", a_sqrtT);            // "damping parameter": a = ΔνL / 2 ΔνD
    write(g, "sigma0_sqrtT", sigma0_sqrtT);  // Cross section: sigma0_sqrtT = sigma0 * sqrt(T)
    write(g, "gDa", gDa);                    // Recoil parameter / "damping parameter" = 2 h ν0^2 / (mH c^2 ΔνL)
  }

  // Exit variables
  {
    Group g = f.createGroup("/exit");
    write(g, "v_exit", v_exit * vth_div_sqrtT * sqrt(T_exit) / km, "km/s"); // Exit velocity [km/s]
    write(g, "T_exit", T_exit);              // Exit temperature [K]
    write(g, "a_exit", a_exit);              // "Damping parameter" [T_exit]
    write(g, "DnuD_exit", DnuD_exit);        // Doppler width at T_exit
  }

  // Information about emission sources
  {
    Group g = f.createGroup("/sources");
    write(g, "n_cells", n_cells);            // Number of cells
    if (star_based_emission)
      write(g, "n_stars", n_stars);          // Number of stars
    if (point_source)
      write(g, "r_point", r_point, "cm");    // Point source position [cm]
    if (shell_source)
      write(g, "r_shell", r_shell, "cm");    // Shell source radius [cm]
    if (shell_blackbody)
      write(g, "T_shell", T_shell, "K");     // Shell source temperature [K]
    if (shock_source)
      write(g, "r_shock", r_shock, "cm");    // Shock source radius [cm]
    if (shock_blackbody)
      write(g, "T_shock", T_shock, "K");     // Shock source temperature [K]

    if (point_source)
      write(g, "L_point", L_point);          // Point source luminosity [erg/s]
    if (shell_source)
      write(g, "L_shell", L_shell);          // Shell source luminosity [erg/s]
    if (shock_source)
      write(g, "L_shock", L_shock);          // Shock source luminosity [erg/s]
    if (recombinations)
      write(g, "L_rec", L_rec);              // Recombination luminosity [erg/s]
    if (collisions)
      write(g, "L_col", L_col);              // Collisional excitation luminosity [erg/s]
    if (collisions_limiter > 0.)
      write(g, "collisions_limiter", collisions_limiter); // Limited by photoheating rate
    if (spontaneous)
      write(g, "L_sp", L_sp);                // Spontaneous emission luminosity [erg/s]

    if (continuum) {
      const double L_tot_cont = L_cont + L_shell + L_shock;
      write(g, "L_cont", L_tot_cont);        // Total L_cont [erg/s]
      write(g, "M_cont", M_cont);            // Total M_cont [absolute magnitude]
      write(g, "Dv_cont_min", Dv_cont_min);  // Continuum velocity offset lower limit [km/s]
      write(g, "Dv_cont_max", Dv_cont_max);  // Continuum velocity offset upper limit [km/s]
      write(g, "Dv_cont_res", Dv_cont_res);  // Continuum velocity offset resolution [km/s]
      write(g, "l_cont_res", l_cont_res);    // Continuum wavelength resolution [angstrom]
      write(g, "R_cont_res", R_cont_res);    // Spectral resolution: R = lambda / Delta_lambda
    }

    write(g, "n_source_types", n_source_types); // Number of source types used
    write_attr(g, "source_types", source_types_mask); // Source type mask indices
    write_attr(g, "source_names", source_names_mask); // Source type names (masked)
    write_attr(g, "mixed_pdf", mixed_pdf_mask); // Mixed source pdf (masked)
    write_attr(g, "mixed_cdf", mixed_cdf_mask); // Mixed source cdf (masked)
    write_attr(g, "mixed_weights", mixed_weights_mask); // Mixed source weights (masked)

    write(g, "spherical_escape", spherical_escape); // Photons escape from a sphere
    if (spherical_escape) {
      write(g, "escape_radius", escape_radius); // Radius for spherical escape [cm]
      write(g, "emission_radius", emission_radius); // Radius for spherical emission [cm]
      write(g, "escape_center", escape_center, "cm"); // Center of the escape region [cm]
    }
  }

  // Simulation information
  write(f, "n_photons", n_photons);          // Actual number of photon packets used
  write(f, "Ndot_tot", Ndot_tot);            // Total emission rate [photons/s]
  write(f, "L_tot", L_tot);                  // Total luminosity [erg/s]
  write(f, "f_esc", f_esc);                  // Global escape fraction
  write(f, "freq_avg", freq_avg);            // Global average frequency [freq units]
  write(f, "freq_std", freq_std);            // Global standard deviation [freq units]
  write(f, "freq_skew", freq_skew);          // Global frequency skewness [normalized]
  write(f, "freq_kurt", freq_kurt);          // Global frequency kurtosis [normalized]
  if (output_collisions) {
    write(f, "f_esc_col", f_esc_col);        // Global escape fraction
    write(f, "freq_avg_col", freq_avg_col);  // Global average frequency [freq units]
    write(f, "freq_std_col", freq_std_col);  // Global standard deviation [freq units]
    write(f, "freq_skew_col", freq_skew_col); // Global frequency skewness [normalized]
    write(f, "freq_kurt_col", freq_kurt_col); // Global frequency kurtosis [normalized]
  }
  write(f, "doppler_frequency", doppler_frequency); // Output frequency in Doppler widths
  write(f, "recoil", recoil);                // Include recoil with scattering
  if (dynamical_core_skipping)
    write(f, "n_exp_atau", n_exp_atau);      // Healpix exponent for a*tau0
  else
    write(f, "x_crit", x_crit);              // Critical frequency for core-skipping

  {
    Group g = f.createGroup("/dust");
    write(g, "dust_model", dust_model);      // Dust model: SMC, MW, etc.
    if (metallicity >= 0.)
      write(g, "metallicity", metallicity);  // Constant metallicity
    if (dust_to_metal >= 0.)
      write(g, "dust_to_metal", dust_to_metal); // Constant dust-to-metal ratio
    if (dust_to_gas >= 0.)
      write(g, "dust_to_gas", dust_to_gas);  // Constant dust-to-gas ratio
    if (dust_boost > 0.)
      write(g, "dust_boost", dust_boost);    // Optional dust boost factor
    write(g, "kappa_dust", kappa_dust);      // Dust opacity [cm^2/g dust]
    write(g, "sigma_dust", sigma_dust);      // Dust cross section [cm^2/Z/hydrogen atom]
    write(g, "albedo", albedo);              // Dust albedo for scattering vs. absorption
    write(g, "g_dust", g_dust);              // Anisotropy parameter: <μ> for dust scattering
    write(g, "f_ion", f_ion);                // H_II region survival fraction
    write(g, "T_sputter", T_sputter);        // Thermal sputtering cutoff [K]
  }
}

/* Write photon data to a specified group or file. */
template <typename File_or_Group>
static void write_photons(const File_or_Group& f) {
  const string freq_units = (doppler_frequency) ? "Doppler" : "km/s"; // Code frequency units
  // const string freq_units2 = "(" + freq_units + ")^2"; // Code frequency units^2
  write(f, "n_escaped", n_escaped);          // Number of escaped photons
  write(f, "n_absorbed", n_absorbed);        // Number of photons absorbed
  write(f, "source_id", source_ids);         // Emission cell or star index
  if (n_source_types > 1)
    write(f, "source_type", source_types);   // Emission type specifier
  write(f, "source_weight", source_weights); // Photon weight at emission
  if (output_collisions)
    write(f, "f_col", f_cols);               // Collisional excitation fraction
  write(f, "frequency", freqs, freq_units);  // Frequency at escape [freq_units]
  write(f, "weight", weights);               // Photon weight at escape
  write(f, "position", positions, "cm");     // Position at escape [cm]
  write(f, "direction", directions);         // Direction at escape
}

/* Writes MCRT data and info to the specified hdf5 filename */
void MCRT::write_module(const H5File& f) {
  // Write general MCRT info
  write_mcrt_info(f);

  // Additional camera info
  if (have_cameras) {
    write(f, "n_bins", n_bins);              // Number of frequency bins
    write(f, "freq_min", freq_min);          // Generic frequency extrema [freq units]
    write(f, "freq_max", freq_max);          // Calculated when freq is initialized
    write(f, "freq_bin_width", freq_bin_width); // Frequency bin width [freq units]
    string flux_units, image_units, cube_units;
    const string freq_units = (doppler_frequency) ? "Doppler" : "km/s";
    if (cosmological) {
      flux_units = "erg/s/cm^2/angstrom";
      image_units = "erg/s/cm^2/arcsec^2";
      cube_units = "erg/s/cm^2/arcsec^2/angstrom";
    } else { // Intrinsic units (independent of distance)
      const string per_freq_units = (doppler_frequency) ? "/Doppler" : "/(km/s)";
      flux_units = "erg/s" + per_freq_units;
      image_units = "erg/s/cm^2";
      cube_units = "erg/s/cm^2" + per_freq_units;
    }
    if (adjust_camera_frequency) {
      write(f, "adjust_camera_frequency", adjust_camera_frequency);
      write(f, "freq_offsets", freq_offsets, freq_units); // Frequency offsets [km/s]
    }
    if (output_escape_fractions)
      write(f, "f_escs", f_escs);            // Escape fractions [fraction]
    if (output_freq_avgs)
      write(f, "freq_avgs", freq_avgs, freq_units); // Frequency averages [freq units]
    if (output_freq_stds)
      write(f, "freq_stds", freq_stds, freq_units); // Standard deviations [freq units]
    if (output_freq_skews)
      write(f, "freq_skews", freq_skews);    // Frequency skewnesses [normalized]
    if (output_freq_kurts)
      write(f, "freq_kurts", freq_kurts);    // Frequency kurtoses [normalized]
    if (output_fluxes)
      write(f, "fluxes", fluxes, flux_units); // Spectral fluxes [erg/s/cm^2/angstrom]
    if (output_images)
      write(f, "images", images, image_units); // Surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_images2)
      write(f, "images2", images2, "("+image_units+")^2"); // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
    if (output_freq_images)
      write(f, "freq_images", freq_images, freq_units); // Average frequency images [freq units]
    if (output_freq2_images)
      write(f, "freq2_images", freq2_images, "("+freq_units+")^2"); // Frequency^2 images [freq^2 units]
    if (output_freq3_images)
      write(f, "freq3_images", freq3_images, "("+freq_units+")^3"); // Frequency^3 images [freq^3 units]
    if (output_freq4_images)
      write(f, "freq4_images", freq4_images, "("+freq_units+")^4"); // Frequency^4 images [freq^4 units]
    if (output_rgb_images)
      write(f, "rgb_images", rgb_images);    // Flux-weighted frequency RGB images
    if (output_cubes)
      write(f, "cubes", cubes, cube_units);  // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

    // Intrinsic emission (mcrt)
    if (output_mcrt_emission) {
      if (output_freq_avgs)
        write(f, "freq_avgs_int", freq_avgs_int, freq_units); // Frequency averages [freq units]
      if (output_fluxes)
        write(f, "fluxes_int", fluxes_int, flux_units); // Spectral fluxes [erg/s/cm^2/angstrom]
      if (output_images)
        write(f, "images_int", images_int, image_units); // Surface brightness images [erg/s/cm^2/arcsec^2]
      if (output_cubes)
        write(f, "cubes_int", cubes_int, cube_units);  // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    }

    // Attenuated emission  (mcrt)
    if (output_mcrt_attenuation) {
      if (output_escape_fractions)
        write(f, "f_escs_ext", f_escs_ext);            // Escape fractions [fraction]
      if (output_freq_avgs)
        write(f, "freq_avgs_ext", freq_avgs_ext, freq_units); // Frequency averages [freq units]
      if (output_fluxes)
        write(f, "fluxes_ext", fluxes_ext, flux_units); // Spectral fluxes [erg/s/cm^2/angstrom]
      if (output_images)
        write(f, "images_ext", images_ext, image_units); // Surface brightness images [erg/s/cm^2/arcsec^2]
      if (output_cubes)
        write(f, "cubes_ext", cubes_ext, cube_units);  // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    }

    // Intrinsic emission (proj)
    if (output_proj_emission)
      write(f, "proj_images_int", proj_images_int, "erg/s/cm^2"); // Surface brightness images [erg/s/cm^2]

    // Attenuated emission (proj)
    if (output_proj_attenuation) {
      write(f, "proj_f_escs_ext", proj_f_escs_ext);    // Escape fractions [fraction]
      write(f, "proj_images_ext", proj_images_ext, "erg/s/cm^2"); // Surface brightness images [erg/s/cm^2]
    }

    if (output_proj_emission || output_proj_attenuation) {
      write(f, "proj_depth", proj_depth);    // Projection depth [cm]
      write(f, "proj_radius", proj_radius);  // Projection radius [cm]
      write(f, "pixel_rtol", pixel_rtol);    // Relative tolerance per pixel
    }

    // Collisional excitation camera data
    if (output_collisions) {
      if (output_escape_fractions)
        write(f, "f_escs_col", f_escs_col);  // Escape fractions [fraction]
      if (output_freq_avgs)
        write(f, "freq_avgs_col", freq_avgs_col, freq_units); // Frequency averages [freq units]
      if (output_freq_stds)
        write(f, "freq_stds_col", freq_stds_col, freq_units); // Standard deviations [freq units]
      if (output_freq_skews)
        write(f, "freq_skews_col", freq_skews_col); // Frequency skewnesses [normalized]
      if (output_freq_kurts)
        write(f, "freq_kurts_col", freq_kurts_col); // Frequency kurtoses [normalized]
      if (output_fluxes)
        write(f, "fluxes_col", fluxes_col, flux_units); // Spectral fluxes [erg/s/cm^2/angstrom]
      if (output_images)
        write(f, "images_col", images_col, image_units); // Surface brightness images [erg/s/cm^2/arcsec^2]
      if (output_images2)
        write(f, "images2_col", images2_col, "("+image_units+")^2"); // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
      if (output_freq_images)
        write(f, "freq_images_col", freq_images_col, freq_units); // Average frequency images [freq units]
      if (output_freq2_images)
        write(f, "freq2_images_col", freq2_images_col, "("+freq_units+")^2"); // Frequency^2 images [freq^2 units]
      if (output_freq3_images)
        write(f, "freq3_images_col", freq3_images_col, "("+freq_units+")^3"); // Frequency^3 images [freq^3 units]
      if (output_freq4_images)
        write(f, "freq4_images_col", freq4_images_col, "("+freq_units+")^4"); // Frequency^4 images [freq^4 units]

      // Intrinsic emission (mcrt)
      if (output_mcrt_emission) {
        if (output_freq_avgs)
          write(f, "freq_avgs_int_col", freq_avgs_int_col, freq_units); // Frequency averages [freq units]
        if (output_fluxes)
          write(f, "fluxes_int_col", fluxes_int_col, flux_units); // Spectral fluxes [erg/s/cm^2/angstrom]
        if (output_images)
          write(f, "images_int_col", images_int_col, image_units); // Surface brightness images [erg/s/cm^2/arcsec^2]
      }

      // Attenuated emission  (mcrt)
      if (output_mcrt_attenuation) {
        if (output_escape_fractions)
          write(f, "f_escs_ext_col", f_escs_ext_col); // Escape fractions [fraction]
        if (output_freq_avgs)
          write(f, "freq_avgs_ext_col", freq_avgs_ext_col, freq_units); // Frequency averages [freq units]
        if (output_fluxes)
          write(f, "fluxes_ext_col", fluxes_ext_col, flux_units); // Spectral fluxes [erg/s/cm^2/angstrom]
        if (output_images)
          write(f, "images_ext_col", images_ext_col, image_units); // Surface brightness images [erg/s/cm^2/arcsec^2]
      }

      // Intrinsic emission (proj)
      if (output_proj_emission)
        write(f, "proj_images_int_col", proj_images_int_col, "erg/s/cm^2"); // Surface brightness images [erg/s/cm^2]

      // Attenuated emission (proj)
      if (output_proj_attenuation) {
        write(f, "proj_f_escs_ext_col", proj_f_escs_ext_col); // Escape fractions [fraction]
        write(f, "proj_images_ext_col", proj_images_ext_col, "erg/s/cm^2"); // Surface brightness images [erg/s/cm^2]
      }
    }
  }

  if (output_photons) {
    if (photon_file.empty()) {               // Output photons to the same file
      Group g = f.createGroup("/photons");
      write_photons(g);                      // Write to the photons group
    } else {                                 // Output a separate photon file
      H5File f_photons(output_dir + "/" + output_base + "_" + photon_file +
                       snap_str + halo_str + "." + output_ext, H5F_ACC_TRUNC);
      write_sim_info(f_photons);             // Write general simulation info
      write_mcrt_info(f_photons);            // Write general MCRT info
      write_photons(f_photons);              // Write to the main group
    }
  }
}
