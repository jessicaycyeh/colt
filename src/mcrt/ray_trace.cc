/*********************
 * mcrt/ray_trace.cc *
 *********************

 * Ray tracer: Follow individual photon packets.

*/

#include "proto.h"
#include "photon.h" // Photon packets

extern double (*H)(double, double); // Voigt-Hjerting function

double ran(); // Random number (from global seed)
Vec3 isotropic_direction(); // Generate direction from an isotropic distribution
Vec3 anisotropic_direction(const int scatter_type, const Vec3 k); // Anisotropic distributions
double spherical_escape_distance(Vec3 point, Vec3 direction); // Distance to escape the bounding sphere
double spherical_emission_distance(Vec3 point, Vec3 direction); // Distance to escape the emission sphere
bool avoid_cell(const int cell); // Avoid calculations for certain cells
Vec3 cell_center(const int cell); // Center of the cell
Vec3 random_point_in_cell(const int cell); // Uniform position in cell volume
int find_cell(const Vec3 point, int cell); // Cell index of a point
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell

static const int ISOTROPIC = 1, OUTWARD_SQRT = 5;
static const double _2pi = 2. * M_PI;

Photon::Photon() {
  // Draw the source that the photon comes from (point, cells, stars, shell, shock)
  const double mixed_comp = ran();
  if (mixed_comp < mixed_cdf[POINT]) {       // Point source emission
    source_id = i_point;                     // Cell index (only one point id)
    source_type = POINT;                     // Point source
    source_weight = mixed_weights[POINT] / double(n_photons); // Equal weight photons
    current_cell = i_point;                  // Known cell index
    position = r_point;                      // Known position
  } else if (mixed_comp < mixed_cdf[CELLS]) { // Cell-based emission (recombinations/collisions/spontaneous)
    const double ran_comp = ran();           // Draw the cell that the photon comes from
    int i = j_map[int(ran_comp * double(n_cdf))]; // Start search from lookup table
    while (j_cdf[i] > ran_comp && i > 0)
      --i;                                   // Move "inward"  ( <-- )
    while (j_cdf[i] <= ran_comp && i < n_cells - 1)
      ++i;                                   // Move "outward" ( --> )
    source_id = i;                           // Cell index
    source_type = CELLS;                     // Cell source
    source_weight = mixed_weights[CELLS] * j_weights[i]; // Cell-based luminosity boosting
    current_cell = i;                        // Cell index assigned directly
    position = random_point_in_cell(current_cell); // Uniform distribution within the cell volume
    if (spherical_escape) {                  // Ensure emission is within spherical region
      const Vec3 dr = position - escape_center; // Position relative to center
      if (dr.dot() > emission_radius2) {     // Point is outside emission sphere
        const Vec3 point = cell_center(i);   // Cell center position
        Vec3 sphere_direction = position - point; // Outward facing
        sphere_direction.normalize();        // Normalize the direction
        const double dl = spherical_emission_distance(point, sphere_direction);
        position = point + sphere_direction * dl; // Move photon to a valid position on the sphere
      }
    }
  } else if (mixed_comp < mixed_cdf[STARS]) { // Star-based emission (continuum)
    const double ran_comp = ran();           // Draw the star that the photon comes from
    int i = j_map_stars[int(ran_comp * double(n_cdf_stars))]; // Start search from lookup table
    while (j_cdf_stars[i] > ran_comp && i > 0)
      --i;                                   // Move "inward"  ( <-- )
    while (j_cdf_stars[i] <= ran_comp && i < n_stars - 1)
      ++i;                                   // Move "outward" ( --> )
    source_id = i;                           // Star index
    source_type = STARS;                     // Star source
    source_weight = mixed_weights[STARS] * j_weights_stars[i]; // Star-based luminosity boosting
    current_cell = find_cell(r_star[i], 0);  // Cell index hosting the star (cell = 0)
    position = r_star[i];                    // Set the photon position to the star position
  } else if (mixed_comp < mixed_cdf[SHELL]) { // Shell source emission
    position = isotropic_direction() * r_shell; // Known radius
    current_cell = find_cell(position, 0);   // Cell index hosting the sampled photon
    source_id = current_cell;                // Cell index
    source_type = SHELL;                     // Shell source
    source_weight = mixed_weights[SHELL] / double(n_photons); // Equal weight photons
  } else if (mixed_comp < mixed_cdf[SHOCK]) { // Shock source emission
    position = isotropic_direction() * r_shock; // Known radius
    current_cell = find_cell(position, 0);   // Cell index hosting the sampled photon
    source_id = current_cell;                // Cell index
    source_type = SHOCK;                     // Shock source
    source_weight = mixed_weights[SHOCK] / double(n_photons); // Equal weight photons
  } else
    error("Unexpected source probability behavior");

  if (avoid_cell(current_cell))
    error("Attempting to insert photon in an invalid cell");

  direction = isotropic_direction();         // Initial direction (isotropic)
  tau_eff_abs = -log(source_weight);         // Effective abs. optical depth
  weight = 0.;                               // Placeholder until escape
#ifdef SPHERICAL
  radius = position.norm();                  // Current radius
#endif
  if (output_collisions)                     // Collisional excitation fraction
    f_col = (source_type == CELLS) ? f_col_cells[current_cell] : 0.;

  // Note: calculate_LOS requires an initialized photon so should be done last
  if (source_type == STARS || source_type == SHOCK) {
    frequency = Dv_cont_res * ran() + Dv_cont_min; // Note: Dv_res = Dv_max - Dv_min
    frequency = x_div_aDv * a[current_cell] * frequency; // Convert: Dv -> x

    // LOS Flux calculation for optically thin cases
    calculate_LOS(ISOTROPIC);                // Frequency doesn't matter here
    if (output_mcrt_emission)
      calculate_LOS_int();                   // Intrinsic emission cameras
    if (output_mcrt_attenuation)
      calculate_LOS_ext();                   // Attenuated emission cameras
  } else if (source_type == SHELL) {
    frequency = Dv_cont_res * ran() + Dv_cont_min; // Note: Dv_res = Dv_max - Dv_min
    frequency = x_div_aDv * a[current_cell] * frequency; // Convert: Dv -> x
    direction = anisotropic_direction(OUTWARD_SQRT, position / r_shell);

    // LOS Flux calculation for optically thin cases
    calculate_LOS(OUTWARD_SQRT);             // Frequency doesn't matter here
    if (output_mcrt_emission)
      calculate_LOS_int();                   // Intrinsic emission cameras
    if (output_mcrt_attenuation)
      calculate_LOS_ext();                   // Attenuated emission cameras
  } else { // emission == {point, recombinations, collisions, spontaneous}
    // Initialize velocity to draw from Maxwellian where u = v_atom / v_th
    Vec3 u_atom {
      sqrt(-log(ran())) * cos(_2pi * ran()),
      sqrt(-log(ran())) * cos(_2pi * ran()),
      sqrt(-log(ran())) * cos(_2pi * ran())
    };

    // Don't assume emission from line center, i.e. don't set x = 0
    const double x_natural = a[current_cell] / tan(M_PI * ran());

    // LOS Flux calculation for optically thin cases
    for (int camera = 0; camera < n_cameras; ++camera) {
      frequency = u_atom.dot(camera_directions[camera]) - x_natural;
      calculate_LOS(ISOTROPIC, camera);
      if (output_mcrt_emission)
        calculate_LOS_int(camera);           // Intrinsic emission cameras
      if (output_mcrt_attenuation)
        calculate_LOS_ext(camera);           // Attenuated emission cameras
    }

    // Draw the frequency from the natural Lorentzian and then doppler shift
    frequency = u_atom.dot(direction) - x_natural;
  }
}

/* Print photon data. */
void Photon::print() {
  cout << "Photon {"
       << "\n  source_id     = " << source_id
       << "\n  source_type   = " << source_type
       << "\n  source_weight = " << source_weight
       << "\n  f_col         = " << f_col
       << "\n  current_cell  = " << current_cell
       << "\n  position      = " << position / kpc << " kpc"
#ifdef SPHERICAL
       << "\n  radius        = " << radius / kpc << " kpc"
#endif
       << "\n  direction     = " << direction
       << "\n  frequency     = " << frequency
       << "\n  tau_eff_abs   = " << tau_eff_abs
       << "\n  weight        = " << exp(-tau_eff_abs)
       << "\n}" << endl;
}

#define update_position(distance) {\
  position += direction * (distance);        /* Move photon */\
}

#define update_tau_eff_abs(distance) {\
  tau_eff_abs += k_dust_abs * (distance);    /* Cumulative absorption optical depth */\
  if (tau_eff_abs > TAU_DISCARD)             /* Consider the photon packet absorbed */\
    break;                                   /* Stop ray-tracing procedure */\
}

//! \brief Spherical geometry modifies the frequency during propagation.
#ifdef SPHERICAL
#define calculate_mu(radius) {(radius > 0.) ? direction.dot(position) / (radius) : 0.}
#define move_photon(distance) {\
  mu_old = calculate_mu(radius);             /* Old directional cosine */\
  update_position(distance);                 /* Update photon position */\
  radius = position.norm();                  /* Update current radius */\
  mu = calculate_mu(radius);                 /* Update directional cosine */\
  frequency += v[current_cell].x * (mu_old - mu); /* Apply Doppler shift */\
  update_tau_eff_abs(distance);              /* Update photon weight */\
}
#else // Cartesian-like geometry
#define move_photon(distance) {\
  update_position(distance);                 /* Update photon position */\
  update_tau_eff_abs(distance);              /* Update photon weight */\
}
#endif

/* Ray tracer (MCRT) */
void Photon::ray_trace() {
  int neib_cell;                             // Neighbor cell index
  double dl_neib, dl_exit = 0.;              // Neighbor and exit distances
  double dtau_scat = -log(ran());            // Optical depth to next scatter
  if (spherical_escape)                      // Initialize escape distance
    dl_exit = spherical_escape_distance(position, direction);
#ifdef SPHERICAL
  double mu_old, mu;                         // Needed for spherical geometry
#ifdef SPECIAL_RELATIVITY
  double beta_r;                             // Needed for aberration effects
#endif
#endif

  while (true) {
    // Compute the maximum distance the photon can travel in the cell
    tie(dl_neib, neib_cell) = face_distance(position, direction, current_cell);

    // Check for spherical escape
    if (spherical_escape && dl_exit <= dl_neib) { // Exit sphere before cell
      dl_neib = dl_exit;                     // Set neighbor distance to sphere
      neib_cell = OUTSIDE;                   // Flag for escape condition
    }

    // Check whether the photon is scattered
    const double k_dust_scat = albedo * k_dust[current_cell];
    const double k_dust_abs = k_dust[current_cell] - k_dust_scat;
    const double k_scat = k_0[current_cell] * H(a[current_cell], frequency)
#ifdef ELECTRON_SCATTERING
                        + k_e[current_cell]
#endif
                        + k_dust_scat;
    const double dtau_neib = k_scat * dl_neib; // Optical depth to neighboring cell

    if (dtau_scat <= dtau_neib) {            // A scattering event takes place
      // Calculate the new position from the remaining optical depth
      const double dl_scat = dtau_scat / k_scat; // Actual length traveled
      move_photon(dl_scat);                  // Update position and weight

      // Scatter: Update direction, frequency, cameras, etc.
      const double k_ran = ran() * k_scat;   // Save drawn interaction
      if (k_ran <= k_dust_scat)              // Interaction is with dust
        dust_scatter();
#ifdef ELECTRON_SCATTERING
      else if (k_ran <= k_dust_scat + k_e[current_cell])
        electron_scatter();
#endif
      else if (two_level_atom)               // Complete redistribution
        line_scatter_crd();
      else                                   // Interaction is with line carrier
        line_scatter();

      // Reset the escape distance (new direction)
      if (spherical_escape)
        dl_exit = spherical_escape_distance(position, direction);

      // Generate a new random optical depth
      dtau_scat = -log(ran());
      continue;
    }

#ifdef SPHERICAL
    if (neib_cell == INSIDE)                 // Check if the photon is trapped
      break;                                 // Stop ray-tracing procedure
#endif

    // No scattering event
    dtau_scat -= dtau_neib;                  // Subtract traversed optical depth
    if (spherical_escape)
      dl_exit -= dl_neib;                    // Subtract traversed distance

    // Calculate the new position of the photon
    move_photon(dl_neib);                    // Update position and weight

    // Doppler-shift from current cell comoving frame to grid frame
#ifdef SPHERICAL
#ifdef SPECIAL_RELATIVITY
    beta_r = (radius > 0.) ? avthDc * v[current_cell].x / (a[current_cell] * radius) : 0.;
    direction += position * beta_r;          // Aberration: beta = v/c = avthDc u/a
    direction.normalize();                   // Maintain lab frame normalization
#endif
    frequency += mu * v[current_cell].x;     // Use the spherical cosine
#else // Cartesian-like geometry
    frequency += direction.dot(v[current_cell]);
#endif

    if (neib_cell == OUTSIDE) {              // Check if the photon escapes
      // Transformation of sqrt(T/T') because a ∝ T^(-1/2)
      frequency *= a_exit / a[current_cell];
      // Doppler-shift from grid frame to exit comoving frame
#ifndef SPHERICAL
      frequency -= direction.dot(v_exit);
#endif
      current_cell = OUTSIDE;                // Update the cell index
      weight = exp(-tau_eff_abs);            // Optical depth -> weight
      break;
    } else {                                 // Continue ray tracing in next cell
      // Transformation of sqrt(T/T') because a ∝ T^(-1/2)
      frequency *= a[neib_cell] / a[current_cell];
      // Doppler-shift from grid frame to next cell comoving frame
#ifdef SPHERICAL
#ifdef SPECIAL_RELATIVITY
      mu = calculate_mu(radius);             // Update lab frame directional cosine
      beta_r = (radius > 0.) ? avthDc * v[neib_cell].x / (a[neib_cell] * radius) : 0.;
      direction -= position * beta_r;        // Aberration: beta = v/c = avthDc u/a
      direction.normalize();                 // Maintain vector normalization
#endif
      frequency -= mu * v[neib_cell].x;      // Use the spherical cosine
#else // Cartesian-like geometry
      frequency -= direction.dot(v[neib_cell]);
#endif
      current_cell = neib_cell;              // Update the cell index
    }
  }

  // Convert to frequency output type
  if (!doppler_frequency)
    frequency *= neg_vth_kms_exit;           // Convert: x -> Delta_v

  // Calculate global statistics
  if (weight > 0.) {
    const double weighted_freq = weight * frequency;
    const double weighted_freq2 = weighted_freq * frequency;
    const double weighted_freq3 = weighted_freq2 * frequency;
    #pragma omp atomic
    f_esc += weight;                         // Global escape fraction
    #pragma omp atomic
    freq_avg += weighted_freq;               // Global average frequency
    #pragma omp atomic
    freq_std += weighted_freq2;              // Global standard deviation
    #pragma omp atomic
    freq_skew += weighted_freq3;             // Global frequency skewness
    #pragma omp atomic
    freq_kurt += weighted_freq3 * frequency; // Global frequency kurtosis

    // Collisional excitation global statistics
    if (output_collisions && f_col > 0.) {
      const double weight_col = weight * f_col;
      const double weighted_freq_col = weight_col * frequency;
      const double weighted_freq2_col = weighted_freq_col * frequency;
      const double weighted_freq3_col = weighted_freq2_col * frequency;
      #pragma omp atomic
      f_esc_col += weight_col;               // Global escape fraction
      #pragma omp atomic
      freq_avg_col += weighted_freq_col;     // Global average frequency
      #pragma omp atomic
      freq_std_col += weighted_freq2_col;    // Global standard deviation
      #pragma omp atomic
      freq_skew_col += weighted_freq3_col;   // Global frequency skewness
      #pragma omp atomic
      freq_kurt_col += weighted_freq3_col * frequency; // Global frequency kurtosis
    }
  }
}
