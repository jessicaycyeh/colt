/****************
 * mcrt/proto.h *
 ****************

 * Module function declarations.

*/

#ifndef MCRT_PROTO_INCLUDED
#define MCRT_PROTO_INCLUDED

// Inherited headers
#include "../proto.h"

// Continuum constants
constexpr double lambda_cont = 1500. * angstrom; // Continuum wavelength [cm]
constexpr double R_10pc = 10. * pc;          // Reference distance for continuum [cm]
constexpr double fnu_cont_fac = lambda_cont * lambda_cont / (4. * M_PI * c * R_10pc * R_10pc * angstrom);

// Dynamical core-skipping constants
constexpr double atau0_tolerance = 4.;       // Maximum relative density change for atau criteria
constexpr double dv_tolerance = 10.;         // Maximum velocity change (vth) for atau criteria

// Photon weighting constants, e.g. for LOS next-event estimation
constexpr double TAU_DISCARD = 32.;          // exp(-23) ~ 1e-10; exp(-32) ~ 1e-14

// Line parameters
extern string line;                          // Name of the line
extern double E0;                            // Line energy [erg]
extern double A21;                           // Einstein A coefficient [Hz]
extern double f12;                           // Oscillator strength
extern double g12;                           // Degeneracy ratio: g_lower / g_upper
extern double T0;                            // Line temperature [K]
extern double nu0;                           // Line frequency [Hz]
extern double lambda0;                       // Line wavelength [angstrom]
extern double B21;                           // Einstein B21 = A21 c^2 / (2 h ν0^3)
extern double DnuL;                          // Natural frequency broadening [Hz]
extern double avthDc;                        // Conversion for special relativity
extern double m_carrier;                     // Mass of carrier [g]
extern bool scaled_microturb;                // Scaled microturbulence model
extern double v_turb;                        // Microturbulent velocity [cm/s]
extern double T_turb;                        // Microturbulent temperature [K]
extern double vth_div_sqrtT;                 // Thermal velocity: vth = sqrt(2 kB T / m_carrier)
extern double DnuD_div_sqrtT;                // Doppler width: ΔνD = ν0 vth / c
extern double a_sqrtT;                       // "damping parameter": a = ΔνL / 2 ΔνD
extern double sigma0_sqrtT;                  // Cross section: sigma0_sqrtT = sigma0 * sqrt(T)
extern double x_div_aDv;                     // Constant for stellar continuum emission
extern double gDa;                           // Recoil parameter / "damping parameter" = 2 h ν0^2 / (mH c^2 ΔνL)

// Information about the dust model
extern string dust_model;                    // Dust model: SMC, MW, etc.
extern double kappa_dust;                    // Dust opacity [cm^2/g dust]
extern double sigma_dust;                    // Dust cross section [cm^2/Z/hydrogen atom]
extern double albedo;                        // Dust albedo for scattering vs. absorption
extern double g_dust;                        // Anisotropy parameter: <μ> for dust scattering
extern double f_ion;                         // H_II region survival fraction
extern double T_sputter;                     // Thermal sputtering cutoff [K]
extern double _1_2g, _1pg2_2g, _1mg2_2D, _1mg2_2g, _1mg_2g; // Constants for Henyey-Greenstein dust scattering

// Configurable line transfer flags
extern bool recombinations;                  // Include recombination emission
extern double T_floor_rec;                   // Apply a recombination temperature floor: T [K]
extern bool collisions;                      // Include collisional excitation
extern bool output_collisions;               // Output collisional excitation data
extern double collisions_limiter;            // Limited by photoheating rate
extern bool spontaneous;                     // Include spontaneous emission
extern bool continuum;                       // Include stellar continuum emission
extern bool two_level_atom;                  // Two level atom line transfer
extern bool line_scattering;                 // Turn line scattering on/off
extern bool dynamical_core_skipping;         // Dynamical core-skipping
extern double x_crit, x_crit_2;              // Constant core-skipping frequency
extern bool doppler_frequency;               // Output frequency in Doppler widths
extern bool recoil;                          // Include recoil with scattering

// Information about the source
extern int n_photons;                        // Actual number of photon packets used
extern int n_absorbed;                       // Number of absorbed photons
extern int n_escaped;                        // Number of escaped photons
extern int n_finished;                       // Track finished progress
extern double Ndot_tot;                      // Total emission rate [photons/s]
extern double L_tot;                         // Total luminosity [erg/s]
extern double L_point;                       // Point source luminosity [erg/s]
extern double L_shell;                       // Shell source luminosity [erg/s]
extern double L_shock;                       // Shock source luminosity [erg/s]
extern double L_rec;                         // Recombination luminosity [erg/s]
extern double L_col;                         // Collisional excitation luminosity [erg/s]
extern double L_sp;                          // Spontaneous emission luminosity [erg/s]
extern double L_cont;                        // Continuum emission luminosity [erg/s]
extern double M_cont;                        // Total M_cont [absolute magnitude]

// Point source variables
extern bool point_source;                    // Insert photons at a specified point (default)
extern int i_point;                          // Cell index to insert photons
extern double x_point;                       // Point source position [cm]
extern double y_point;
extern double z_point;
extern Vec3 r_point;                         // (x, y, z) [cm]

// Shell source variables
extern bool shell_source;                    // Insert photons from a shell
extern bool shell_blackbody;                 // Luminosity based on a black body
extern double r_shell;                       // Shell source radius [cm]
extern double T_shell;                       // Shell source temperature [K]

// Shock source variables
extern bool shock_source;                    // Insert photons from a shock
extern bool shock_blackbody;                 // Luminosity based on a black body
extern double r_shock;                       // Shock source radius [cm]
extern double T_shock;                       // Shock source temperature [K]

// Spatial emission from cells and stars
extern bool cell_based_emission;             // Include cell-based sources
extern double j_exp;                         // Luminosity boosting exponent (j_exp < 1 favors lower emissivity cells)
constexpr int n_cdf = 10000;                 // Number of entrees in the cdf lookup table (cells)
extern array<int, n_cdf> j_map;              // Lookup table for photon insertion (cells)
extern vector<double> j_cdf;                 // Cumulative distribution function for emission
extern vector<double> j_weights;             // Initial photon weights for each cell
extern vector<double> f_col_cells;           // Collisional excitation fractions for each cell
constexpr int n_cdf_stars = 10000;           // Number of entrees in the cdf lookup table (stars)
extern array<int, n_cdf_stars> j_map_stars;  // Lookup table for photon insertion (stars)
extern vector<double> j_cdf_stars;           // Cumulative distribution function for emission
extern vector<double> j_weights_stars;       // Initial photon weights for each star
extern double emission_n_min;                // Minimum number density of emitting cells [cm^-3]
extern double emission_n_max;                // Maximum number density of emitting cells [cm^-3]

// Distinguish between source types
enum SourceTypes {
  POINT = 0,                                 // Point source emission
  CELLS = 1,                                 // Cell-based emission (recombinations/collisions/spontaneous)
  STARS = 2,                                 // Star-based emission (continuum)
  SHELL = 3,                                 // Shell source emission
  SHOCK = 4,                                 // Shock source emission
  MAX_SOURCE_TYPES = 5                       // Max number of source types
};
extern int n_source_types;                   // Number of source types used
extern array<double, MAX_SOURCE_TYPES> mixed_pdf; // Mixed source probability distribution function
extern array<double, MAX_SOURCE_TYPES> mixed_cdf; // Mixed source cumulative distribution function
extern array<double, MAX_SOURCE_TYPES> mixed_weights; // Mixed source weights (point, cells, stars)
extern vector<int> source_types_mask;        // Source type mask indices
extern strings source_names_mask;            // Source type names (masked)
extern vector<double> mixed_pdf_mask;        // Mixed source pdf (masked)
extern vector<double> mixed_cdf_mask;        // Mixed source cdf (masked)
extern vector<double> mixed_weights_mask;    // Mixed source weights (masked)

// Exit variables
extern bool exit_wrt_com;                    // Exit with respect to the center of mass frame
extern Vec3 v_exit;                          // Exit velocity [cm/s]
extern double T_exit;                        // Exit temperature [K]
extern double a_exit;                        // "Damping parameter" [T_exit]
extern double DnuD_exit;                     // Doppler width at T_exit
extern double vth_div_c_exit;                // Thermal velocity (vth/c) [T_exit]
extern double neg_vth_kms_exit;              // Thermal velocity (-km/s) [T_exit]
// extern double neg_a_vth_kms_exit;            // Constant for observable frequency conversion (x -> Δv)
extern bool adjust_camera_frequency;         // Adjust camera frequency based on offsets
extern vector<double> freq_offsets;          // Frequency offsets for each camera

// Information about escaped photons
extern double f_esc;                         // Global escape fraction
extern double freq_avg;                      // Global average frequency [freq units]
extern double freq_std;                      // Global standard deviation [freq units]
extern double freq_skew;                     // Global frequency skewness [normalized]
extern double freq_kurt;                     // Global frequency kurtosis [normalized]
extern bool output_photons;                  // Output photon packets (esc/abs)
extern string photon_file;                   // Output a separate photon file
// extern bool escaped_frequency_moments;       // Calculate escaped frequency moments
extern vector<int> source_ids;               // Emission cell or star index
extern vector<int> source_types;             // Emission type specifier
extern vector<double> source_weights;        // Photon weight at emission
extern vector<double> f_cols;                // Collisional excitation fraction
extern vector<double> freqs;                 // Frequency at escape [code_freq]
extern vector<double> weights;               // Photon weight at escape
extern vector<Vec3> positions;               // Position at escape [cm]
extern vector<Vec3> directions;              // Direction at escape

// Information about secondary cameras (mcrt)
extern bool output_mcrt_emission;            // Output intrinsic emission without transport
extern bool output_mcrt_attenuation;         // Output attenuated emission without scattering
extern vector<double> f_escs_ext;            // Attenuated escape fractions [fraction]
extern vector<double> freq_avgs_int;         // Intrinsic frequency averages [freq units]
extern vector<double> freq_avgs_ext;         // Attenuated frequency averages [freq units]
extern vector<vector<double>> fluxes_int;    // Intrinsic spectral fluxes [erg/s/cm^2/angstrom]
extern vector<vector<double>> fluxes_ext;    // Attenuated spectral fluxes [erg/s/cm^2/angstrom]
extern vector<Image<double>> images_int;     // Intrinsic surface brightness images [erg/s/cm^2/arcsec^2]
extern vector<Image<double>> images_ext;     // Attenuated surface brightness images [erg/s/cm^2/arcsec^2]
extern vector<Cube<double>> cubes_int;       // Intrinsic spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
extern vector<Cube<double>> cubes_ext;       // Attenuated spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

// Information about secondary cameras (proj)
extern bool output_proj_emission;            // Output intrinsic emission without transport
extern bool output_proj_attenuation;         // Output attenuated emission without scattering
extern vector<double> proj_f_escs_ext;       // Attenuated escape fractions [fraction]
extern vector<Image<double>> proj_images_int; // Intrinsic surface brightness images [erg/s/cm^2]
extern vector<Image<double>> proj_images_ext; // Attenuated surface brightness images [erg/s/cm^2]

// Collisional excitation camera data
extern double f_esc_col;                     // Global escape fraction
extern double freq_avg_col;                  // Global average frequency [freq units]
extern double freq_std_col;                  // Global standard deviation [freq units]
extern double freq_skew_col;                 // Global frequency skewness [normalized]
extern double freq_kurt_col;                 // Global frequency kurtosis [normalized]
extern vector<double> f_escs_col;            // Escape fractions [fraction]
extern vector<double> freq_avgs_col;         // Frequency averages [freq units]
extern vector<double> freq_stds_col;         // Standard deviations [freq units]
extern vector<double> freq_skews_col;        // Frequency skewnesses [normalized]
extern vector<double> freq_kurts_col;        // Frequency kurtoses [normalized]
extern vector<vector<double>> fluxes_col;    // Spectral fluxes [erg/s/cm^2/angstrom]
extern vector<Image<double>> images_col;     // Surface brightness images [erg/s/cm^2/arcsec^2]
extern vector<Image<double>> images2_col;    // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
extern vector<Image<double>> freq_images_col; // Average frequency images [freq units]
extern vector<Image<double>> freq2_images_col; // Frequency^2 images [freq^2 units]
extern vector<Image<double>> freq3_images_col; // Frequency^3 images [freq^3 units]
extern vector<Image<double>> freq4_images_col; // Frequency^4 images [freq^4 units]
extern vector<double> f_escs_ext_col;        // Attenuated escape fractions [fraction]
extern vector<double> freq_avgs_int_col;     // Intrinsic frequency averages [freq units]
extern vector<double> freq_avgs_ext_col;     // Attenuated frequency averages [freq units]
extern vector<vector<double>> fluxes_int_col; // Intrinsic spectral fluxes [erg/s/cm^2/angstrom]
extern vector<vector<double>> fluxes_ext_col; // Attenuated spectral fluxes [erg/s/cm^2/angstrom]
extern vector<Image<double>> images_int_col; // Intrinsic surface brightness images [erg/s/cm^2/arcsec^2]
extern vector<Image<double>> images_ext_col; // Attenuated surface brightness images [erg/s/cm^2/arcsec^2]
extern vector<double> proj_f_escs_ext_col;   // Attenuated escape fractions [fraction]
extern vector<Image<double>> proj_images_int_col; // Intrinsic surface brightness images [erg/s/cm^2]
extern vector<Image<double>> proj_images_ext_col; // Attenuated surface brightness images [erg/s/cm^2]

#endif // MCRT_PROTO_INCLUDED
