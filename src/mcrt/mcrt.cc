/****************
 * mcrt/mcrt.cc *
 ****************

 * Driver: Assign photon packets for radiative transfer, etc.

*/

#include "proto.h"
#include "MCRT.h"
#include "../timing.h" // Timing functionality
#include "photon.h" // Photon packets
#include <chrono>

extern Timer mcrt_timer; // Clock timing
extern Timer reduce_timer; // MPI timing

void print_observables(); // Post-calculation printing

static inline void print_top() {
  cout << "\n+----------+------------+------------+------------+"
       << "\n| Progress |  Elapsed   |  Estimate  |  Remaining |"
       << "\n+----------+------------+------------+------------+";
}

static inline void print_bottom() {
  cout << "\n+----------+------------+------------+------------+" << endl;
}

/* Print the simulation progress and remaining time estimate. */
static inline void print_progress(const std::chrono::system_clock::time_point start, const int finished, const int total) {
  auto time_elapsed = std::chrono::system_clock::now() - start;
  auto time_remaining = time_elapsed * double(total - finished) / double(finished);
  long long seconds_elapsed = std::chrono::duration_cast<std::chrono::seconds>(time_elapsed).count();
  long long seconds_remaining = std::chrono::duration_cast<std::chrono::seconds>(time_remaining).count();
  long long seconds_estimate = seconds_elapsed + seconds_remaining;
  printf("\n|  %5.1f%%  |  %02lld:%02lld:%02lld  |  %02lld:%02lld:%02lld  |  %02lld:%02lld:%02lld  |",
         100. * double(finished) / double(total),
         seconds_elapsed / 3600, (seconds_elapsed / 60) % 60, seconds_elapsed % 60,
         seconds_estimate / 3600, (seconds_estimate / 60) % 60, seconds_estimate % 60,
         seconds_remaining / 3600, (seconds_remaining / 60) % 60, seconds_remaining % 60);
}

/* Move vector elements within a vector over disjoint ranges. */
template <typename T>
static inline void disjoint_move(vector<T>& vec, const int last, const int move) {
  #pragma omp parallel for
  for (int i = n_escaped; i < last; ++i)
    vec[i+move] = vec[i];                    // Move non-overlapping elements
}

/* Move absorbed photons in the info vectors. */
static inline void move_absorbed_photons(const int dest) {
  const int offset = dest - n_escaped;       // Memory offset for absorbed photons
  if (offset == 0 || n_absorbed == 0)
    return;                                  // No need to move absorbed photons
  if (offset < 0 || n_absorbed < 0 || dest + n_absorbed > n_photons)
    error("Attemping to move absorbed photons out of expected bounds: n_absorbed = " +
          to_string(n_absorbed) + ", dest = " + to_string(dest) + ", n_escaped = " +
          to_string(n_escaped) + ", n_photons = " + to_string(n_photons));
  // Move range: [n_escaped, n_escaped + n_absorbed) -> [dest, dest + n_absorbed)
  // These overlap if n_absorbed > offset, so only move non-overlapping elements
  const int last = (n_absorbed > offset) ? dest : n_escaped + n_absorbed;
  const int move = (n_absorbed > offset) ? n_absorbed : offset;
  disjoint_move(source_ids, last, move);
  if (n_source_types > 1)
    disjoint_move(source_types, last, move);
  disjoint_move(source_weights, last, move);
  if (output_collisions)
    disjoint_move(f_cols, last, move);
  disjoint_move(freqs, last, move);
  disjoint_move(weights, last, move);
  disjoint_move(positions, last, move);
  disjoint_move(directions, last, move);
}

/* Swap photons in the info vectors. */
static inline void swap_photons(const int i, const int j) {
  std::swap(source_ids[i], source_ids[j]);
  if (n_source_types > 1)
    std::swap(source_types[i], source_types[j]);
  std::swap(source_weights[i], source_weights[j]);
  if (output_collisions)
    std::swap(f_cols[i], f_cols[j]);
  std::swap(freqs[i], freqs[j]);
  std::swap(weights[i], weights[j]);
  std::swap(positions[i], positions[j]);
  std::swap(directions[i], directions[j]);
}

/* Add photon information to the output buffers. */
static inline void add_to_output_buffers(const struct Photon& p, const int index) {
  source_ids[index] = p.source_id;
  if (n_source_types > 1)
    source_types[index] = p.source_type;
  source_weights[index] = p.source_weight;
  if (output_collisions)
    f_cols[index] = p.f_col;
  freqs[index] = p.frequency;
  weights[index] = p.weight;
  positions[index] = p.position;
  directions[index] = p.direction;
}

/* Sort escaped photons to the beginning and absorbed photons to the end. */
static void sort_photons() {
  // Count the number of escaped and absorbed photons
  n_escaped = n_absorbed = 0;
  #pragma omp parallel for reduction(+:n_escaped) reduction(+:n_absorbed)
  for (int i = 0; i < n_finished; ++i) {
    if (weights[i] > 0.)
      ++n_escaped;                           // Escaped photon
    else
      ++n_absorbed;                          // Absorbed photon
  }

  // Sort the escaped and absorbed photons
  for (int i_esc = 0, i_abs = 0; i_esc < n_escaped; ++i_esc) {
    if (weights[i_esc] > 0.)
      continue;                              // Valid escaped photon
    do {
      ++i_abs;                               // Search from end of list
    } while (weights[n_finished-i_abs] <= 0.); // Find next escaped photon
    swap_photons(i_esc, n_finished-i_abs);   // Swap esc/abs photons
  }
}

/* All tasks have a static assignment of equal work. */
static void equal_workers() {
  if (root)
    print_top();                             // Progress top
  const auto start = std::chrono::system_clock::now(); // Reference time
  int work = n_photons / n_ranks;            // Work assignment
  if (rank < n_photons - work * n_ranks)
    ++work;                                  // Assign remaining work
  const int checks = pow(double(work+1), 0.25); // Number of time checks
  const int interval = work / checks;        // Interval between updates

  #pragma omp parallel for schedule(dynamic)
  for (int i = 0; i < work; ++i) {
    // Create photon packet and perform ray tracing
    auto p = Photon();
    p.ray_trace();

    // Print completed progress and remaining time estimate
    int index;                               // Photon array index
    #pragma omp atomic capture
    index = n_finished++;                    // Update finished counter
    if (output_photons)
      add_to_output_buffers(p, index);       // Populate output buffers
    if (root && ++index % interval == 0)     // Index -> print counter
      print_progress(start, index, work);
  }
  if (root)
    print_bottom();                          // Progress bottom
}

/* The root task/thread assigns all of the work. */
static void master() {
  print_top();                               // Progress top
  const auto start = std::chrono::system_clock::now(); // Reference time
  const int n_slaves = n_ranks * n_threads - 1; // Number of slaves
  const int f_slaves = 2 * n_slaves;         // Slaves work factor
  int work, n_assigned;                      // Fine-grained load balancing
  if (n_slaves < n_photons) {
    work = n_photons / f_slaves + 1;         // Keep assigning more photons
    n_assigned = work * n_slaves;            // Slaves start with photons
  } else {
    work = 0;                                // Send stop signal immediately
    n_assigned = n_photons;                  // Do not assign beyond n_photons
  }
  const int checks = pow(double(n_photons+1), 0.25); // Number of time checks
  const int interval = n_photons / checks;   // Interval between updates
  int next_check = interval;                 // Next checkpoint for printing
  int finished = 0;                          // Track finished progress
  int message[2];                            // Slave work and thread numbers
  MPI_Status status;                         // MPI status for slave rank

  do {
    // Receive load balancing message from any source
    MPI_Recv(message, 2, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
    if (work > 0) {
      work = (n_photons - n_assigned) / f_slaves + 1;
      n_assigned += work;
      if (n_assigned > n_photons)
        work = 0;                            // Send done messages now
    }

    // Send work flag back to sender
    MPI_Send(&work, 1, MPI_INT, status.MPI_SOURCE, message[1], MPI_COMM_WORLD);

    // Print completed progress and remaining time estimate
    finished += message[0];                  // Work finished by thread
    if (finished >= next_check) {
      next_check += interval;                // Update print checkpoint
      print_progress(start, finished, n_photons);
    }
  } while (finished < n_photons);            // Finish all work
  print_bottom();                            // Progress bottom
}

/* All other tasks/threads request work. */
static void slave() {
  const int f_slaves = 2 * (n_ranks * n_threads - 1); // Slaves work factor
  const int mpi_thread = rank * n_threads + thread; // Global thread number
  if (mpi_thread > n_photons)
    return;                                  // More slaves than photons
  int work = n_photons / f_slaves + 1;       // Initial amount of work
  int message[2] = {work, mpi_thread};       // Load balancing message

  do {
    for (int i = 0; i < work; ++i) {
      // Create photon packet and perform ray tracing
      auto p = Photon();
      p.ray_trace();

      // Add to output buffers (optional)
      if (output_photons) {
        int index;                           // Photon array index
        #pragma omp atomic capture
        index = n_finished++;                // Update finished counter
        add_to_output_buffers(p, index);     // Populate output buffers
      }
    }

    // Send load balancing message to root
    message[0] = work;                       // Update work message
    MPI_Send(message, 2, MPI_INT, ROOT, 0, MPI_COMM_WORLD);

    // Receive work flag back from root
    MPI_Recv(&work, 1, MPI_INT, ROOT, mpi_thread, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  } while (work > 0);                        // New work was assigned
}

/* Convenience function to MPI_Reduce vectors to root. */
template <class T>
static inline void mpi_reduce(vector<T>& vs, MPI_Op op = MPI_SUM) {
  for (auto& v : vs) {
    if (root)
      MPI_Reduce(MPI_IN_PLACE, v.data(), v.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
    else
      MPI_Reduce(v.data(), v.data(), v.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
  }
}

/* Convenience function to MPI_Reduce rgb_images to root. */
static inline void mpi_reduce(vector<Image<Vec3>>& vs, MPI_Op op = MPI_SUM) {
  for (auto& v : vs) {
    if (root)
      MPI_Reduce(MPI_IN_PLACE, v.data(), 3 * v.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
    else
      MPI_Reduce(v.data(), v.data(), 3 * v.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
  }
}

/* Convenience function to MPI_Reduce vector to root. */
static inline void mpi_reduce(vector<double>& v, MPI_Op op = MPI_SUM) {
  if (root)
    MPI_Reduce(MPI_IN_PLACE, v.data(), v.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
  else
    MPI_Reduce(v.data(), v.data(), v.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
}

/* Convenience function to MPI_Reduce scalar to root. */
static inline void mpi_reduce(double *val_ptr, MPI_Op op = MPI_SUM) {
  if (root)
    MPI_Reduce(MPI_IN_PLACE, val_ptr, 1, MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
  else
    MPI_Reduce(val_ptr, val_ptr, 1, MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
}

/* Convenience function to MPI_Reduce disjoint elements to root. */
template <class T>
static inline void mpi_reduce_disjoint(vector<T>& vs) {
  const int size = vs.size();
  const int size_rank = size / n_ranks;      // Equal assignment
  const int remainder = size - size_rank * n_ranks;
  int i_start = rank * size_rank;            // Start range
  int i_end = (rank + 1) * size_rank;        // End range
  if (rank < remainder) {
    i_start += rank;                         // Correct start range
    i_end += (rank + 1);                     // Correct end range
  } else {
    i_start += remainder;                    // Correct start range
    i_end += remainder;                      // Correct end range
  }
  if (root) {
    for (int i = i_end; i < size; ++i)
      MPI_Recv(vs[i].data(), vs[i].size(), MPI_DOUBLE, MPI_ANY_SOURCE, i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  } else {
    for (int i = i_start; i < i_end; ++i)
      MPI_Send(vs[i].data(), vs[i].size(), MPI_DOUBLE, ROOT, i, MPI_COMM_WORLD);
  }
}

// MPI collection helper struct
struct RecvInfo {
  void *buffer;                              // Address of receive buffer
  int count;                                 // Number of elements in buffer
  MPI_Datatype datatype;                     // Datatype of buffer elements
  int source;                                // Rank of the source
  int tag;                                   // Message tag
};

/* Convenience function to MPI_Recv data to root. */
static inline void mpi_recv(struct RecvInfo& recv_info) {
  MPI_Recv(recv_info.buffer, recv_info.count, recv_info.datatype,
           recv_info.source, recv_info.tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

/* Ensure unique MPI message tags for safety. */
enum {
  TAG_ESI, TAG_ASI,                          // Emission cell or star index
  TAG_EST, TAG_AST,                          // Emission type specifier
  TAG_ESW, TAG_ASW,                          // Photon weight at emission
  TAG_EC,  TAG_AC,                           // Collisional excitation fraction
  TAG_EF,  TAG_AF,                           // Frequency at escape [code_freq]
  TAG_EW,  TAG_AW,                           // Photon weight at escape
  TAG_EP,  TAG_AP,                           // Position at escape [cm]
  TAG_ED,  TAG_AD                            // Direction at escape
};

/* Convenience function to MPI_Send photon info to root. */
static void mpi_collect_photons() {
  // Gather the number of escaped and absorbed photons to root
  vector<int> rank_esc, rank_abs;            // Number of esc/abs photons
  if (root) {
    rank_esc.resize(n_ranks);                // Allocate space for esc counts
    rank_abs.resize(n_ranks);                // Allocate space for abs counts
  }
  MPI_Gather(&n_escaped, 1, MPI_INT, rank_esc.data(), 1, MPI_INT, ROOT, MPI_COMM_WORLD);
  MPI_Gather(&n_absorbed, 1, MPI_INT, rank_abs.data(), 1, MPI_INT, ROOT, MPI_COMM_WORLD);

  if (root) {
    vector<int> ind_esc, ind_abs;            // Index ranges for rank collection
    ind_esc.resize(n_ranks+1);               // Allocate space for esc indices
    ind_abs.resize(n_ranks+1);               // Allocate space for abs indices
    ind_esc[0] = 0;                          // Start root index at zero
    for (int i = 0; i < n_ranks; ++i)
      ind_esc[i+1] = ind_esc[i] + rank_esc[i];
    ind_abs[0] = ind_esc[n_ranks];           // Start root index at end of esc
    for (int i = 0; i < n_ranks; ++i)
      ind_abs[i+1] = ind_abs[i] + rank_abs[i];

    // Move root absorbed photons to correct global position
    move_absorbed_photons(ind_abs[0]);

    // Build a master list of buffers and receive counts
    vector<struct RecvInfo> buffers;
    for (int i = 1; i < n_ranks; ++i) {
      // Receive escaped photon info
      const int i_esc = ind_esc[i];
      const int n_esc = ind_esc[i+1] - ind_esc[i];
      if (n_esc > 0) {
        buffers.push_back({&source_ids[i_esc], n_esc, MPI_INT, i, TAG_ESI});
        if (n_source_types > 1)
          buffers.push_back({&source_types[i_esc], n_esc, MPI_INT, i, TAG_EST});
        buffers.push_back({&source_weights[i_esc], n_esc, MPI_DOUBLE, i, TAG_ESW});
        if (output_collisions)
          buffers.push_back({&f_cols[i_esc], n_esc, MPI_DOUBLE, i, TAG_EC});
        buffers.push_back({&freqs[i_esc], n_esc, MPI_DOUBLE, i, TAG_EF});
        buffers.push_back({&weights[i_esc], n_esc, MPI_DOUBLE, i, TAG_EW});
        buffers.push_back({&positions[i_esc], 3*n_esc, MPI_DOUBLE, i, TAG_EP});
        buffers.push_back({&directions[i_esc], 3*n_esc, MPI_DOUBLE, i, TAG_ED});
      }

      // Receive absorbed photon info
      const int i_abs = ind_abs[i];
      const int n_abs = ind_abs[i+1] - ind_abs[i];
      if (n_abs > 0) {
        buffers.push_back({&source_ids[i_abs], n_abs, MPI_INT, i, TAG_ASI});
        if (n_source_types > 1)
          buffers.push_back({&source_types[i_abs], n_abs, MPI_INT, i, TAG_AST});
        buffers.push_back({&source_weights[i_abs], n_abs, MPI_DOUBLE, i, TAG_ASW});
        if (output_collisions)
          buffers.push_back({&f_cols[i_abs], n_abs, MPI_DOUBLE, i, TAG_AC});
        buffers.push_back({&freqs[i_abs], n_abs, MPI_DOUBLE, i, TAG_AF});
        buffers.push_back({&weights[i_abs], n_abs, MPI_DOUBLE, i, TAG_AW});
        buffers.push_back({&positions[i_abs], 3*n_abs, MPI_DOUBLE, i, TAG_AP});
        buffers.push_back({&directions[i_abs], 3*n_abs, MPI_DOUBLE, i, TAG_AD});
      }
    }

    // Receive data from all buffers
    const int n_buffers = buffers.size();    // Number of buffers
    #pragma omp parallel for
    for (int i = 0; i < n_buffers; ++i)
      mpi_recv(buffers[i]);                  // Helper to receive data

    // Update global photon counts
    n_escaped = ind_esc[n_ranks] - ind_esc[0];
    n_absorbed = ind_abs[n_ranks] - ind_abs[0];
  } else {
    // Send escaped photon info
    if (n_escaped > 0) {
      MPI_Send(source_ids.data(), n_escaped, MPI_INT, ROOT, TAG_ESI, MPI_COMM_WORLD);
      if (n_source_types > 1)
        MPI_Send(source_types.data(), n_escaped, MPI_INT, ROOT, TAG_EST, MPI_COMM_WORLD);
      MPI_Send(source_weights.data(), n_escaped, MPI_DOUBLE, ROOT, TAG_ESW, MPI_COMM_WORLD);
      if (output_collisions)
        MPI_Send(f_cols.data(), n_escaped, MPI_DOUBLE, ROOT, TAG_EC, MPI_COMM_WORLD);
      MPI_Send(freqs.data(), n_escaped, MPI_DOUBLE, ROOT, TAG_EF, MPI_COMM_WORLD);
      MPI_Send(weights.data(), n_escaped, MPI_DOUBLE, ROOT, TAG_EW, MPI_COMM_WORLD);
      MPI_Send(positions.data(), 3*n_escaped, MPI_DOUBLE, ROOT, TAG_EP, MPI_COMM_WORLD);
      MPI_Send(directions.data(), 3*n_escaped, MPI_DOUBLE, ROOT, TAG_ED, MPI_COMM_WORLD);
    }

    // Send absorbed photon info
    if (n_absorbed > 0) {
      MPI_Send(&source_ids[n_escaped], n_absorbed, MPI_INT, ROOT, TAG_ASI, MPI_COMM_WORLD);
      if (n_source_types > 1)
        MPI_Send(&source_types[n_escaped], n_absorbed, MPI_INT, ROOT, TAG_AST, MPI_COMM_WORLD);
      MPI_Send(&source_weights[n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_ASW, MPI_COMM_WORLD);
      if (output_collisions)
        MPI_Send(&f_cols[n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_AC, MPI_COMM_WORLD);
      MPI_Send(&freqs[n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_AF, MPI_COMM_WORLD);
      MPI_Send(&weights[n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_AW, MPI_COMM_WORLD);
      MPI_Send(&positions[n_escaped], 3*n_absorbed, MPI_DOUBLE, ROOT, TAG_AP, MPI_COMM_WORLD);
      MPI_Send(&directions[n_escaped], 3*n_absorbed, MPI_DOUBLE, ROOT, TAG_AD, MPI_COMM_WORLD);
    }
  }
}

/* MPI reductions of all output data. */
static void reduce_mcrt() {
  if (root)
    cout << "\nMCRT: MPI Reductions ..." << endl;

  // If we do not synchronize all ranks, the reductions may time out
  MPI_Barrier(MPI_COMM_WORLD);

  reduce_timer.start();

  // Global statistics
  mpi_reduce(&f_esc);                        // Global escape fraction
  mpi_reduce(&freq_avg);                     // Global average frequency
  mpi_reduce(&freq_std);                     // Global standard deviation
  mpi_reduce(&freq_skew);                    // Global frequency skewness
  mpi_reduce(&freq_kurt);                    // Global frequency kurtosis

  // Escape fractions [fraction]
  if (output_escape_fractions)
    mpi_reduce(f_escs);                      // Apply f_esc reductions

  // Frequency averages [freq units]
  if (output_freq_avgs)
    mpi_reduce(freq_avgs);                   // Apply frequency reductions

  // Frequency standard deviations [freq units]
  if (output_freq_stds)
    mpi_reduce(freq_stds);                   // Apply deviation reductions

  // Frequency skewnesses [normalized]
  if (output_freq_skews)
    mpi_reduce(freq_skews);                  // Apply skewness reductions

  // Frequency kurtoses [normalized]
  if (output_freq_kurts)
    mpi_reduce(freq_kurts);                  // Apply kurtosis reductions

  // Spectral fluxes [erg/s/cm^2/angstrom]
  if (output_fluxes)
    mpi_reduce(fluxes);                      // Apply flux reductions

  // Surface brightness images [erg/s/cm^2/arcsec^2]
  if (output_images)
    mpi_reduce(images);                      // Apply image reductions

  // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
  if (output_images2)
    mpi_reduce(images2);                     // Apply image2 reductions

  // Average frequency images [freq units]
  if (output_freq_images)
    mpi_reduce(freq_images);                 // Apply freq_images reductions

  // Frequency^2 images [freq^2 units]
  if (output_freq2_images)
    mpi_reduce(freq2_images);                // Apply freq2_images reductions

  // Frequency^3 images [freq^3 units]
  if (output_freq3_images)
    mpi_reduce(freq3_images);                // Apply freq3_images reductions

  // Frequency^4 images [freq^4 units]
  if (output_freq4_images)
    mpi_reduce(freq4_images);                // Apply freq4_images reductions

  // Flux-weighted frequency RGB images
  if (output_rgb_images)
    mpi_reduce(rgb_images);                  // Apply rgb_image reductions

  // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
  if (output_cubes)
    mpi_reduce(cubes);                       // Apply cube reductions

  // Intrinsic cameras (mcrt)
  if (output_mcrt_emission) {
    // Frequency averages [freq units]
    if (output_freq_avgs)
      mpi_reduce(freq_avgs_int);             // Apply frequency reductions

    // Spectral fluxes [erg/s/cm^2/angstrom]
    if (output_fluxes)
      mpi_reduce(fluxes_int);                // Apply flux reductions

    // Surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_images)
      mpi_reduce(images_int);                // Apply image reductions

    // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    if (output_cubes)
      mpi_reduce(cubes_int);                 // Apply cube reductions
  }

  // Attenuation cameras (mcrt)
  if (output_mcrt_attenuation) {
    // Escape fractions [fraction]
    if (output_escape_fractions)
      mpi_reduce(f_escs_ext);                // Apply f_esc reductions

    // Frequency averages [freq units]
    if (output_freq_avgs)
      mpi_reduce(freq_avgs_ext);             // Apply frequency reductions

    // Spectral fluxes [erg/s/cm^2/angstrom]
    if (output_fluxes)
      mpi_reduce(fluxes_ext);                // Apply flux reductions

    // Surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_images)
      mpi_reduce(images_ext);                // Apply image reductions

    // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    if (output_cubes)
      mpi_reduce(cubes_ext);                 // Apply cube reductions
  }

  // Intrinsic cameras (proj)
  if (output_proj_emission)
    // Surface brightness images [erg/s/cm^2]
    mpi_reduce_disjoint(proj_images_int);    // Apply image reductions

  // Attenuation cameras (proj)
  if (output_proj_attenuation)
    // Surface brightness images [erg/s/cm^2]
    mpi_reduce_disjoint(proj_images_ext);    // Apply image reductions

  // Collisional excitation camera data
  if (output_collisions) {
    // Global statistics
    mpi_reduce(&f_esc_col);                  // Global escape fraction
    mpi_reduce(&freq_avg_col);               // Global average frequency
    mpi_reduce(&freq_std_col);               // Global standard deviation
    mpi_reduce(&freq_skew_col);              // Global frequency skewness
    mpi_reduce(&freq_kurt_col);              // Global frequency kurtosis

    // Escape fractions [fraction]
    if (output_escape_fractions)
      mpi_reduce(f_escs_col);                // Apply f_esc reductions

    // Frequency averages [freq units]
    if (output_freq_avgs)
      mpi_reduce(freq_avgs_col);             // Apply frequency reductions

    // Frequency standard deviations [freq units]
    if (output_freq_stds)
      mpi_reduce(freq_stds_col);             // Apply deviation reductions

    // Frequency skewnesses [normalized]
    if (output_freq_skews)
      mpi_reduce(freq_skews_col);            // Apply skewness reductions

    // Frequency kurtoses [normalized]
    if (output_freq_kurts)
      mpi_reduce(freq_kurts_col);            // Apply kurtosis reductions

    // Spectral fluxes [erg/s/cm^2/angstrom]
    if (output_fluxes)
      mpi_reduce(fluxes_col);                // Apply flux reductions

    // Surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_images)
      mpi_reduce(images_col);                // Apply image reductions

    // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
    if (output_images2)
      mpi_reduce(images2_col);               // Apply image2 reductions

    // Average frequency images [freq units]
    if (output_freq_images)
      mpi_reduce(freq_images_col);           // Apply freq_images reductions

    // Frequency^2 images [freq^2 units]
    if (output_freq2_images)
      mpi_reduce(freq2_images_col);          // Apply freq2_images reductions

    // Frequency^3 images [freq^3 units]
    if (output_freq3_images)
      mpi_reduce(freq3_images_col);          // Apply freq3_images reductions

    // Frequency^4 images [freq^4 units]
    if (output_freq4_images)
      mpi_reduce(freq4_images_col);          // Apply freq4_images reductions

    // Intrinsic cameras (mcrt)
    if (output_mcrt_emission) {
      // Frequency averages [freq units]
      if (output_freq_avgs)
        mpi_reduce(freq_avgs_int_col);       // Apply frequency reductions

      // Spectral fluxes [erg/s/cm^2/angstrom]
      if (output_fluxes)
        mpi_reduce(fluxes_int_col);          // Apply flux reductions

      // Surface brightness images [erg/s/cm^2/arcsec^2]
      if (output_images)
        mpi_reduce(images_int_col);          // Apply image reductions
    }

    // Attenuation cameras (mcrt)
    if (output_mcrt_attenuation) {
      // Escape fractions [fraction]
      if (output_escape_fractions)
        mpi_reduce(f_escs_ext_col);          // Apply f_esc reductions

      // Frequency averages [freq units]
      if (output_freq_avgs)
        mpi_reduce(freq_avgs_ext_col);       // Apply frequency reductions

      // Spectral fluxes [erg/s/cm^2/angstrom]
      if (output_fluxes)
        mpi_reduce(fluxes_ext_col);          // Apply flux reductions

      // Surface brightness images [erg/s/cm^2/arcsec^2]
      if (output_images)
        mpi_reduce(images_ext_col);          // Apply image reductions
    }

    // Intrinsic cameras (proj)
    if (output_proj_emission)
      // Surface brightness images [erg/s/cm^2]
      mpi_reduce_disjoint(proj_images_int_col); // Apply image reductions

    // Attenuation cameras (proj)
    if (output_proj_attenuation)
      // Surface brightness images [erg/s/cm^2]
      mpi_reduce_disjoint(proj_images_ext_col); // Apply image reductions
  }

  // Absorbed and escaped photon info
  if (output_photons)
    mpi_collect_photons();                   // Collect photons to root

  reduce_timer.stop();
}

/* Convert observables to the correct units. */
void MCRT::correct_units() {
  cout << "\nMCRT: Converting to physical units ..." << endl;

  double to_flux_units, to_image_units, to_cube_units;
  if (cosmological) {
    double dang = pixel_width * arcsec * pow(1. + z, 2); // Pixel angle * d_L [cm arcsec]
    to_flux_units = L_tot / (2. * M_PI * d_L * d_L * observed_bin_width); // L / (2π d_L^2 Δλ_obs)
    to_image_units = L_tot / (2. * M_PI * dang * dang); // L / (2π dθ^2)
    to_cube_units = to_image_units / observed_bin_width; // L / (2π dθ^2 Δλ_obs)
  } else {
    to_flux_units = 2. * L_tot / freq_bin_width; // 2 L / Δv
    to_image_units = 2. * L_tot / pixel_area; // 2 L / dA
    to_cube_units = to_image_units / freq_bin_width; // 2 L / (dA Δv)
  }

  // Convert global frequency moments to statistics
  if (f_esc > 0.) {                          // Avoid division by zero
    // Frequency average [freq units]
    const double f_avg = freq_avg / f_esc;   // Save for later
    freq_avg = f_avg;                        // Average

    // Frequency standard deviation [freq units]
    const double f_E2 = freq_std / f_esc;    // Normalized 2nd moment
    const double f_avg2 = f_avg * f_avg;     // Average^2
    const double f_std2 = f_E2 - f_avg2;     // Variance
    const double f_std = sqrt(f_std2);       // Standard deviation
    freq_std = f_std;

    // Frequency skewness [normalized]
    const double f_E3 = freq_skew / f_esc;   // Normalized 3rd moment
    const double f_avg3 = f_avg2 * f_avg;    // Average^3
    const double f_std3 = f_std2 * f_std;    // Deviation^3
    const double f_skew_std3 = f_E3 - 3.*f_std2*f_avg - f_avg3;
    freq_skew = f_skew_std3 / f_std3;        // Skewness

    // Frequency kurtosis [normalized]
    const double f_E4 = freq_kurt / f_esc;   // Normalized 4th moment
    const double f_avg4 = f_avg2 * f_avg2;   // Average^4
    const double f_std4 = f_std2 * f_std2;   // Deviation^4
    const double f_kurt_std4 = f_E4 - 4.*f_skew_std3*f_avg - 6.*f_std2*f_avg2 - f_avg4;
    freq_kurt = f_kurt_std4 / f_std4;        // Kurtosis
  }

  // Convert frequency moments to statistics // Note: Before f_esc correction
  if (output_freq_avgs)
    #pragma omp parallel for
    for (int i = 0; i < n_cameras; ++i) {
      if (f_escs[i] <= 0.)
        continue;                            // Avoid division by zero

      // Frequency averages [freq units]
      const double f_avg = freq_avgs[i] / f_escs[i]; // Save for later
      freq_avgs[i] = f_avg;                  // Average

      // Frequency standard deviations [freq units]
      if (output_freq_stds) {
        const double f_E2 = freq_stds[i] / f_escs[i]; // Normalized 2nd moment
        const double f_avg2 = f_avg * f_avg; // Average^2
        const double f_std2 = f_E2 - f_avg2; // Variance
        const double f_std = sqrt(f_std2);   // Standard deviation
        freq_stds[i] = f_std;

        // Frequency skewnesses [normalized]
        if (output_freq_skews) {
          const double f_E3 = freq_skews[i] / f_escs[i]; // Normalized 3rd moment
          const double f_avg3 = f_avg2 * f_avg; // Average^3
          const double f_std3 = f_std2 * f_std; // Deviation^3
          const double f_skew_std3 = f_E3 - 3.*f_std2*f_avg - f_avg3;
          freq_skews[i] = f_skew_std3 / f_std3; // Skewness

          // Frequency kurtoses [normalized]
          if (output_freq_kurts) {
            const double f_E4 = freq_kurts[i] / f_escs[i]; // Normalized 4th moment
            const double f_avg4 = f_avg2 * f_avg2; // Average^4
            const double f_std4 = f_std2 * f_std2; // Deviation^4
            const double f_kurt_std4 = f_E4 - 4.*f_skew_std3*f_avg - 6.*f_std2*f_avg2 - f_avg4;
            freq_kurts[i] = f_kurt_std4 / f_std4; // Kurtosis
          }
        }
      }
    }

  // Escape fractions [fraction]
  if (output_escape_fractions)
    rescale(f_escs, 2.);                     // Apply phase function correction

  // Spectral fluxes [erg/s/cm^2/angstrom]
  if (output_fluxes)
    rescale(fluxes, to_flux_units);          // Apply flux correction

  // Average frequency images [freq units]   // Note: Before image correction
  if (output_freq_images)
    divide(freq_images, images);             // Apply freq_image correction

  // Frequency^2 images [freq^2 units]
  if (output_freq2_images)
    divide(freq2_images, images);            // Apply freq2_image correction

  // Frequency^3 images [freq^3 units]
  if (output_freq3_images)
    divide(freq3_images, images);            // Apply freq3_image correction

  // Frequency^4 images [freq^4 units]
  if (output_freq4_images)
    divide(freq4_images, images);            // Apply freq4_image correction

  // Flux-weighted frequency RGB images      // Note: Before image correction
  if (output_rgb_images)
    divide(rgb_images, images);              // Apply rgb_image correction

  // Surface brightness images [erg/s/cm^2/arcsec^2]
  if (output_images)
    rescale(images, to_image_units);         // Apply image correction

  // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
  if (output_images2)
    rescale(images2, to_image_units * to_image_units); // Apply image2 correction

  // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
  if (output_cubes)
    rescale(cubes, to_cube_units);           // Apply cube correction

  // Intrinsic cameras (mcrt)
  if (output_mcrt_emission) {
    // Note: No correction needed for freq_avgs_int

    // Spectral fluxes [erg/s/cm^2/angstrom]
    if (output_fluxes)
      rescale(fluxes_int, to_flux_units);    // Apply flux correction

    // Surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_images)
      rescale(images_int, to_image_units);   // Apply image correction

    // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    if (output_cubes)
      rescale(cubes_int, to_cube_units);     // Apply cube correction
  }

  // Attenuation cameras (mcrt)
  if (output_mcrt_attenuation) {
    // Frequency averages [freq units]       // Note: Before f_esc_ext correction
    if (output_freq_avgs)
      divide(freq_avgs_ext, f_escs_ext);     // Apply frequency correction

    // Escape fractions [fraction]
    if (output_escape_fractions)
      rescale(f_escs_ext, 2.);               // Apply phase function correction

    // Spectral fluxes [erg/s/cm^2/angstrom]
    if (output_fluxes)
      rescale(fluxes_ext, to_flux_units);    // Apply flux correction

    // Surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_images)
      rescale(images_ext, to_image_units);   // Apply image correction

    // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    if (output_cubes)
      rescale(cubes_ext, to_cube_units);     // Apply cube correction
  }

  // Attenuation cameras (proj)
  if (output_proj_attenuation) {
    proj_f_escs_ext = vector<double>(n_cameras); // Escape fractions
    for (int camera = 0; camera < n_cameras; ++camera) {
      const double ext_tot = proj_images_ext[camera].omp_sum();
      proj_f_escs_ext[camera] = ext_tot * pixel_area / L_tot; // Escape fractions
    }
  }

  // Collisional excitation camera data
  if (output_collisions) {
    // Convert global frequency moments to statistics
    if (f_esc_col > 0.) {                    // Avoid division by zero
      // Frequency average [freq units]
      const double f_avg = freq_avg_col / f_esc_col; // Save for later
      freq_avg_col = f_avg;                  // Average

      // Frequency standard deviation [freq units]
      const double f_E2 = freq_std_col / f_esc_col; // Normalized 2nd moment
      const double f_avg2 = f_avg * f_avg;   // Average^2
      const double f_std2 = f_E2 - f_avg2;   // Variance
      const double f_std = sqrt(f_std2);     // Standard deviation
      freq_std_col = f_std;

      // Frequency skewness [normalized]
      const double f_E3 = freq_skew_col / f_esc_col; // Normalized 3rd moment
      const double f_avg3 = f_avg2 * f_avg;  // Average^3
      const double f_std3 = f_std2 * f_std;  // Deviation^3
      const double f_skew_std3 = f_E3 - 3.*f_std2*f_avg - f_avg3;
      freq_skew_col = f_skew_std3 / f_std3;  // Skewness

      // Frequency kurtosis [normalized]
      const double f_E4 = freq_kurt_col / f_esc_col; // Normalized 4th moment
      const double f_avg4 = f_avg2 * f_avg2; // Average^4
      const double f_std4 = f_std2 * f_std2; // Deviation^4
      const double f_kurt_std4 = f_E4 - 4.*f_skew_std3*f_avg - 6.*f_std2*f_avg2 - f_avg4;
      freq_kurt_col = f_kurt_std4 / f_std4;  // Kurtosis
    }

    // Convert frequency moments to statistics // Note: Before f_esc_col correction
    if (output_freq_avgs)
      #pragma omp parallel for
      for (int i = 0; i < n_cameras; ++i) {
        if (f_escs_col[i] <= 0.)
          continue;                          // Avoid division by zero

        // Frequency averages [freq units]
        const double f_avg = freq_avgs_col[i] / f_escs_col[i]; // Save for later
        freq_avgs_col[i] = f_avg;            // Average

        // Frequency standard deviations [freq units]
        if (output_freq_stds) {
          const double f_E2 = freq_stds_col[i] / f_escs_col[i]; // Normalized 2nd moment
          const double f_avg2 = f_avg * f_avg; // Average^2
          const double f_std2 = f_E2 - f_avg2; // Variance
          const double f_std = sqrt(f_std2); // Standard deviation
          freq_stds_col[i] = f_std;

          // Frequency skewnesses [normalized]
          if (output_freq_skews) {
            const double f_E3 = freq_skews_col[i] / f_escs_col[i]; // Normalized 3rd moment
            const double f_avg3 = f_avg2 * f_avg; // Average^3
            const double f_std3 = f_std2 * f_std; // Deviation^3
            const double f_skew_std3 = f_E3 - 3.*f_std2*f_avg - f_avg3;
            freq_skews_col[i] = f_skew_std3 / f_std3; // Skewness

            // Frequency kurtoses [normalized]
            if (output_freq_kurts) {
              const double f_E4 = freq_kurts_col[i] / f_escs_col[i]; // Normalized 4th moment
              const double f_avg4 = f_avg2 * f_avg2; // Average^4
              const double f_std4 = f_std2 * f_std2; // Deviation^4
              const double f_kurt_std4 = f_E4 - 4.*f_skew_std3*f_avg - 6.*f_std2*f_avg2 - f_avg4;
              freq_kurts_col[i] = f_kurt_std4 / f_std4; // Kurtosis
            }
          }
        }
      }

    // Escape fractions [fraction]
    f_esc_col *= L_tot / L_col;              // Escape fraction relative to L_col
    if (output_escape_fractions)
      rescale(f_escs_col, 2. * L_tot / L_col); // Apply phase function correction

    // Spectral fluxes [erg/s/cm^2/angstrom]
    if (output_fluxes)
      rescale(fluxes_col, to_flux_units);    // Apply flux correction

    // Average frequency images [freq units] // Note: Before image_col correction
    if (output_freq_images)
      divide(freq_images_col, images_col);   // Apply freq_image_col correction

    // Frequency^2 images [freq^2 units]
    if (output_freq2_images)
      divide(freq2_images_col, images_col);  // Apply freq2_image_col correction

    // Frequency^3 images [freq^3 units]
    if (output_freq3_images)
      divide(freq3_images_col, images_col);  // Apply freq3_image_col correction

    // Frequency^4 images [freq^4 units]
    if (output_freq4_images)
      divide(freq4_images_col, images_col);  // Apply freq4_image_col correction

    // Surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_images)
      rescale(images_col, to_image_units);   // Apply image_col correction

    // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
    if (output_images2)
      rescale(images2_col, to_image_units * to_image_units); // Apply image2_col correction

    // Intrinsic cameras (mcrt)
    if (output_mcrt_emission) {
      // Frequency averages [freq units]
      if (output_freq_avgs)
        rescale(freq_avgs_int_col, L_tot / L_col); // Relative to L_col

      // Spectral fluxes [erg/s/cm^2/angstrom]
      if (output_fluxes)
        rescale(fluxes_int_col, to_flux_units); // Apply flux correction

      // Surface brightness images [erg/s/cm^2/arcsec^2]
      if (output_images)
        rescale(images_int_col, to_image_units); // Apply image correction
    }

    // Attenuation cameras (mcrt)
    if (output_mcrt_attenuation) {
      // Frequency averages [freq units]     // Note: Before f_esc_ext_col correction
      if (output_freq_avgs)
        divide(freq_avgs_ext_col, f_escs_ext_col); // Apply frequency correction

      // Escape fractions [fraction]
      if (output_escape_fractions)
        rescale(f_escs_ext_col, 2. * L_tot / L_col); // Apply phase function correction

      // Spectral fluxes [erg/s/cm^2/angstrom]
      if (output_fluxes)
        rescale(fluxes_ext_col, to_flux_units); // Apply flux correction

      // Surface brightness images [erg/s/cm^2/arcsec^2]
      if (output_images)
        rescale(images_ext_col, to_image_units); // Apply image correction
    }

    // Attenuation cameras (proj)
    if (output_proj_attenuation) {
      proj_f_escs_ext_col = vector<double>(n_cameras); // Escape fractions
      for (int camera = 0; camera < n_cameras; ++camera) {
        const double ext_tot = proj_images_ext_col[camera].omp_sum();
        proj_f_escs_ext_col[camera] = ext_tot * pixel_area / L_col; // Escape fractions
      }
    }
  }
}

/* Driver for MCRT calculations. */
void MCRT::run() {
  mcrt_timer.start();
  // Perform the actual MCRT calculations
  n_finished = 0;                            // Reset finished progress
  if (load_balancing)                        // MPI task/thread work assignment
    #pragma omp parallel
    {
      // One of the root process's threads runs the master
      if (root && (thread == 0))
        master();
      else
        slave();
    }
  else
    equal_workers();                         // Assign equal work across ranks

  // Sort escaped and absorbed photons
  if (output_photons)
    sort_photons();

  // Projected emission and attenuation images
  if (output_proj_emission || output_proj_attenuation)
    run_projections();

  // Add all independent photons from different processors
  if (n_ranks > 1)
    reduce_mcrt();

  if (root) {
    // Convert observables to the correct units
    correct_units();
    print_observables();
  }
  mcrt_timer.stop();
}
