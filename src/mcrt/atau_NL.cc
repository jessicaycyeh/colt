/*******************
 * mcrt/atau_NL.cc *
 *******************

 * Non-local core-skipping calculation.

*/

#include "proto.h"
#include "../timing.h" // Timing functionality

extern Timer atau_timer; // Clock timing
bool avoid_cell(const int cell); // Avoid calculations for certain cells
Vec3 cell_center(const int cell); // Center of the cell
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell

vector<Vec3> healpix_vectors(int n_side); // Healpix directions
double spherical_escape_distance(Vec3 point, Vec3 direction); // Distance to escape the bounding sphere
void mpi_allreduce(vector<double>& v, MPI_Op op = MPI_SUM); // MPI_Allreduce alias

/* Integrate a*tau0 along rays for a non-local estimate for core-skipping. */
// Note: The cut parameter is a threshold past which we immediately return.
// This provides a ~4x speedup for the entire atau_NL calculation.
static double atau_NL(Vec3 direction, int cell, const double cut = positive_infinity) {
  const double ak0_0 = a[cell] * k_0[cell];  // Keep track of the original a*k_0
  const double v_0 = direction.dot(v[cell]); // Also track the parallel velocity
  Vec3 point = cell_center(cell);            // Ray tracing coordinates
  int next_cell;                             // Next cell index
  double l, l_exit = 0.;                     // Path lengths
  double ak0 = 0., result = 0.;              // Path coefficient and result
  // Move to the next cell without adding to a*tau0
  tie(l, next_cell) = face_distance(point, direction, cell);
  // Check if the density/velocity threshold is reached
#ifdef SPHERICAL
  if (next_cell == INSIDE)                   // Check if the ray is trapped
    return 0.;
#endif
  if (next_cell == OUTSIDE ||                // Check if the ray escapes
      ((ak0_0 > (ak0 = a[next_cell] * k_0[next_cell])) // Initialize a*k_0
        ? (ak0_0 > atau0_tolerance * ak0)    // Check a*k_0 density gradients
        : (ak0 > atau0_tolerance * ak0_0)) || // to ensure relative smoothness
      fabs(v_0 - direction.dot(v[next_cell])) > dv_tolerance) // Velocity gradients
    return 0.;                               // No non-local contribution
  if (spherical_escape) {
    l_exit = spherical_escape_distance(point, direction) - l;
    if (l_exit <= 0.)                        // Check for spherical escape
      return 0.;                             // No non-local contribution
  }
  while (true) {                             // Ray trace until escape
    point += direction * l;                  // Move to the new position
    cell = next_cell;                        // Update the next cell index
    tie(l, next_cell) = face_distance(point, direction, cell); // Maximum propagation distance
    if (spherical_escape) {                  // Check for spherical escape
      if (l_exit <= l)                       // Exit sphere before cell
        return result + ak0 * l_exit;        // += atau0
      l_exit -= l;                           // Remaining distance to exit
    }
    result += ak0 * l;                       // Path integral of a*tau0
#ifdef SPHERICAL
    if (next_cell == INSIDE)                 // Check if the ray is trapped
      return result;
#endif
    if (result > cut ||                      // Early termination
        next_cell == OUTSIDE ||              // Check if the ray escapes
        ((ak0_0 > (ak0 = a[next_cell] * k_0[next_cell])) // Update a*k_0
          ? (ak0_0 > atau0_tolerance * ak0)  // Check a*k_0 density gradients
          : (ak0 > atau0_tolerance * ak0_0)) || // to ensure relative smoothness
        fabs(v_0 - direction.dot(v[next_cell])) > dv_tolerance) // Velocity gradients
      return result;                         // Reached stopping criteria
  }
}

/* Find the minimum a*tau0 (non-local) estimate for a set of directions. */
static double min_atau_NL(vector<Vec3>& directions, const int cell) {
  // Avoid cells that are special or outside the escape radius
  if (avoid_cell(cell) || (spherical_escape &&
     (cell_center(cell) - escape_center).dot() >= escape_radius2))
    return 0.;

  double comp, result = positive_infinity;   // Start with a large value
  for (const auto& direction : directions) {
    comp = atau_NL(direction, cell, result); // Comparison value
    if (comp < result)                       // Found new minimum value
      result = comp;
  }
  return result;                             // Minimum
}

/* Find the minimum a*tau0 (non-local) estimate of all cells using healpix directions. */
static void min_atau_NL_healpix() {
  vector<Vec3> directions;
  if (geometry == slab || geometry == spherical)
    directions = {{-1., 0., 0.}, {1., 0., 0.}};
  else
    directions = healpix_vectors(n_exp_atau);

  // Note: Load balancing is approximately achieved by using regular strides
  // for MPI assignments and then OpenMP's dynamic scheduling for the threads
  #pragma omp parallel for schedule(dynamic)
  for (int cell = rank; cell < n_cells; cell += n_ranks)
    atau[cell] = min_atau_NL(directions, cell);

  mpi_allreduce(atau);                       // MPI_Allreduce vector elements
}

/* Pre-calculate sum(a*dtau) for the healpix directed lines of sight */
/* Used as a non-local estimate in the u_perp acceleration scheme */
void atau_NL_calculation() {
  atau_timer.start();
  atau = vector<double>(n_cells);            // Initialize atau to zeros
  if (root && geometry != slab && geometry != spherical)
    cout << "\nCalculating a*tau along different sightlines for each cell..."
         << "\n  n_exp_atau = " << n_exp_atau
         << "\n  n_pix_atau = " << 12 * pow(4, n_exp_atau) << endl;
  min_atau_NL_healpix();                     // Non-local atau calculation
  atau_timer.stop();
}
