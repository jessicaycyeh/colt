/*******************
 * mcrt/scatter.cc *
 *******************

 * Scattering Event: Scattering angle, atom's velocity, optical depth, etc.

*/

#include "proto.h"
#include "photon.h" // Photon packets
#include "colormaps.h" // RGB colormaps

extern double (*H)(double, double); // Voigt-Hjerting function

double ran(); // Random number (from global seed)
Vec3 isotropic_direction(); // Generate direction from an isotropic distribution
double spherical_escape_distance(Vec3 point, Vec3 direction); // Distance to escape the bounding sphere
double min_face_distance(const Vec3& point, const int cell); // Largest sphere around a point in the cell
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell

/* Constants needed to calculate the core-wing transition */
static const double X0 = 6.9184721, X1 = 81.766279, X2 = 14.651253;

static const int CORE_2P12 = 1, CORE_2P32 = 2, WING_SCAT = 3, DUST_SCAT = 4, OUTWARD_SQRT = 5;

static const double _2pi = 2. * M_PI;
static const double _343_27 = 343. / 27.;
static const double _1_3 = 1. / 3.;
static const double _2_3 = 2. / 3.;
static const double _7_3 = 7. / 3.;

static inline Vec3 get_rgb_color(const double freq) {
  const double rgb_freq_cam = inverse_rgb_freq_bin_width * (freq - rgb_freq_min);
  const int rgb_bin = floor(rgb_freq_cam);   // Lookup table index
  if (rgb_bin < 0)
    return BlueRed[0];                       // Saturated lower limit
  if (rgb_bin >= 255)
    return BlueRed[255];                     // Saturated upper limit
  const double rgb_fac = rgb_freq_cam - double(rgb_bin); // Interpolation factor
  return BlueRed[rgb_bin] * (1. - rgb_fac) + BlueRed[rgb_bin+1] * rgb_fac;
}

/* Line-of-sight flux emission calculation. */
void Photon::calculate_LOS_int() {
  for (int camera = 0; camera < n_cameras; ++camera)
    calculate_LOS_int(camera);
}

// Note: Use this version when the camera directional Doppler-shift matters.
void Photon::calculate_LOS_int(int camera) {
  double freq = frequency;                   // Photon frequency
  Vec3 k_cam = camera_directions[camera];    // Camera direction

  // Doppler-shift from original cell comoving frame to grid frame
#ifdef SPHERICAL
  const double mu = (radius > 0.) ? k_cam.dot(position) / (radius) : 0.;
  freq += mu * v[current_cell].x;            // Use the spherical cosine
#else // Cartesian-like geometry
  freq += k_cam.dot(v[current_cell]);
#endif
  // Transformation of sqrt(T/T') because a ∝ T^(-1/2)
  freq *= a_exit / a[current_cell];
  // Doppler-shift from grid frame to exit comoving frame
#ifndef SPHERICAL
  freq -= k_cam.dot(v_exit);
#endif
  // Convert to the frequency output type
  if (!doppler_frequency)
    freq *= neg_vth_kms_exit;                // Convert: x -> Delta_v
  if (adjust_camera_frequency)
    freq -= freq_offsets[camera];            // Adjust camera frequency

  // Surface brightness LOS weight: sum( W(mu) * exp(-tau_esc) )
  const double weight_int = 0.5 * source_weight;

  // Bin the photon's frequency - Only add to bins in the specified range
  const double freq_cam = inverse_freq_bin_width * (freq - freq_min);
  const bool in_freq_range = (freq_cam >= 0. && freq_cam < double(n_bins));

  // Bin the photon's position - Only add to bins in the specified range
  const Vec3 r_cam = position - camera_center; // Position relative to camera center
  const double x_cam = inverse_pixel_width * (r_cam.dot(camera_xaxes[camera]) + image_radius);
  const double y_cam = inverse_pixel_width * (r_cam.dot(camera_yaxes[camera]) + image_radius);
  const bool in_image_range = (x_cam >= 0. && x_cam < double(n_pixels) &&
                               y_cam >= 0. && y_cam < double(n_pixels));

  if (output_freq_avgs)
    #pragma omp atomic
    freq_avgs_int[camera] += freq * weight_int;

  if (output_fluxes && in_freq_range) {
    const int ibin = floor(freq_cam);
    #pragma omp atomic
    fluxes_int[camera][ibin] += weight_int;
  }
  if (in_image_range) {
    const int ix = floor(x_cam);
    const int iy = floor(y_cam);
    if (output_images)
      #pragma omp atomic
      images_int[camera](ix, iy) += weight_int;

    if (output_cubes && in_freq_range) {
      const int ibin = floor(freq_cam);
      #pragma omp atomic
      cubes_int[camera](ix, iy, ibin) += weight_int;
    }
  }

  // Collisional excitation camera data
  if (output_collisions) {
    const double weight_col = weight_int * f_col;

    if (output_freq_avgs)
      #pragma omp atomic
      freq_avgs_int_col[camera] += freq * weight_col;

    if (output_fluxes && in_freq_range) {
      const int ibin = floor(freq_cam);
      #pragma omp atomic
      fluxes_int_col[camera][ibin] += weight_col;
    }
    if (output_images && in_image_range) {
      const int ix = floor(x_cam);
      const int iy = floor(y_cam);
      #pragma omp atomic
      images_int_col[camera](ix, iy) += weight_col;
    }
  }
}

/* Line-of-sight flux attenuation calculation. */
void Photon::calculate_LOS_ext() {
  for (int camera = 0; camera < n_cameras; ++camera)
    calculate_LOS_ext(camera);
}

// Note: Use this version when the camera directional Doppler-shift matters.
void Photon::calculate_LOS_ext(int camera) {
  int cell = current_cell;                   // Current cell index
  int next_cell;                             // Next cell index
  double freq = frequency;                   // Photon frequency
  double tau_esc = tau_eff_abs;              // Optical depth to escape
  double l, l_exit = 0.;                     // Path lengths
  Vec3 point = position;                     // Photon position
  Vec3 k_cam = camera_directions[camera];    // Camera direction
  if (spherical_escape)                      // Initialize escape distance
    l_exit = spherical_escape_distance(point, k_cam);

  while (true) {                             // Ray trace until escape
    // Compute the maximum distance the photon can travel in the cell
    tie(l, next_cell) = face_distance(point, k_cam, cell);

#ifdef SPHERICAL
    if (next_cell == INSIDE)                 // Check if the ray is trapped
      return;                                // No point to continue
#endif

    // Check for spherical escape
    if (spherical_escape) {
      if (l_exit <= l) {                     // Exit sphere before cell
        tau_esc += k_dust[cell] * l_exit;    // Cumulative LOS optical depth
        if (tau_esc > TAU_DISCARD)           // Early exit condition
          return;                            // No point to continue
        break;                               // Valid tau_esc completion
      }
      l_exit -= l;                           // Remaining distance to exit
    }

    // Cumulative LOS optical depth
    tau_esc += k_dust[cell] * l;

    // Discard photons from the calculation if tau_esc > TAU_DISCARD
    if (tau_esc > TAU_DISCARD)
      return;                                // No point to continue

    // Calculate the new position of the photon
    point += k_cam * l;

    if (next_cell == OUTSIDE)                // Check if the photon escapes
      break;
    cell = next_cell;                        // Update the cell index
  }

  // Doppler-shift from original cell comoving frame to grid frame
#ifdef SPHERICAL
  const double mu = (radius > 0.) ? k_cam.dot(position) / (radius) : 0.;
  freq += mu * v[current_cell].x;            // Use the spherical cosine
#else // Cartesian-like geometry
  freq += k_cam.dot(v[current_cell]);
#endif
  // Transformation of sqrt(T/T') because a ∝ T^(-1/2)
  freq *= a_exit / a[current_cell];
  // Doppler-shift from grid frame to exit comoving frame
#ifndef SPHERICAL
  freq -= k_cam.dot(v_exit);
#endif
  // Convert to the frequency output type
  if (!doppler_frequency)
    freq *= neg_vth_kms_exit;                // Convert: x -> Delta_v
  if (adjust_camera_frequency)
    freq -= freq_offsets[camera];            // Adjust camera frequency

  // Surface brightness LOS weight: sum( W(mu) * exp(-tau_esc) )
  const double weight_ext = 0.5 * exp(-tau_esc);

  // Calculate the escape fraction for each camera
  if (output_escape_fractions)
    #pragma omp atomic
    f_escs_ext[camera] += weight_ext;

  // Bin the photon's frequency - Only add to bins in the specified range
  const double freq_cam = inverse_freq_bin_width * (freq - freq_min);
  const bool in_freq_range = (freq_cam >= 0. && freq_cam < double(n_bins));

  // Bin the photon's position - Only add to bins in the specified range
  const Vec3 r_cam = position - camera_center; // Position relative to camera center
  const double x_cam = inverse_pixel_width * (r_cam.dot(camera_xaxes[camera]) + image_radius);
  const double y_cam = inverse_pixel_width * (r_cam.dot(camera_yaxes[camera]) + image_radius);
  const bool in_image_range = (x_cam >= 0. && x_cam < double(n_pixels) &&
                               y_cam >= 0. && y_cam < double(n_pixels));

  if (output_freq_avgs)
    #pragma omp atomic
    freq_avgs_ext[camera] += freq * weight_ext;

  if (output_fluxes && in_freq_range) {
    const int ibin = floor(freq_cam);
    #pragma omp atomic
    fluxes_ext[camera][ibin] += weight_ext;
  }
  if (in_image_range) {
    const int ix = floor(x_cam);
    const int iy = floor(y_cam);
    if (output_images)
      #pragma omp atomic
      images_ext[camera](ix, iy) += weight_ext;

    if (output_cubes && in_freq_range) {
      const int ibin = floor(freq_cam);
      #pragma omp atomic
      cubes_ext[camera](ix, iy, ibin) += weight_ext;
    }
  }

  // Collisional excitation cameras
  if (output_collisions) {
    const double weight_col = weight_ext * f_col;

    // Calculate the escape fraction for each camera
    if (output_escape_fractions)
      #pragma omp atomic
      f_escs_ext_col[camera] += weight_col;

    if (output_freq_avgs)
      #pragma omp atomic
      freq_avgs_ext_col[camera] += freq * weight_col;

    if (output_fluxes && in_freq_range) {
      const int ibin = floor(freq_cam);
      #pragma omp atomic
      fluxes_ext_col[camera][ibin] += weight_col;
    }
    if (output_images && in_image_range) {
      const int ix = floor(x_cam);
      const int iy = floor(y_cam);
      #pragma omp atomic
      images_ext_col[camera](ix, iy) += weight_col;
    }
  }
}

/* Line-of-sight flux calculation via next event estimation. */
void Photon::calculate_LOS(int scatter_type) {
  for (int camera = 0; camera < n_cameras; ++camera)
    calculate_LOS(scatter_type, camera);
}

// Note: Use this version when the camera directional Doppler-shift matters.
void Photon::calculate_LOS(int scatter_type, int camera) {
  int cell = current_cell;                   // Current cell index
  int next_cell;                             // Next cell index
  double freq = frequency;                   // Photon frequency
  double tau_esc = tau_eff_abs;              // Optical depth to escape
  double l, l_exit = 0.;                     // Path lengths
  Vec3 point = position;                     // Photon position
  Vec3 k_cam = camera_directions[camera];    // Camera direction
  if (spherical_escape)                      // Initialize escape distance
    l_exit = spherical_escape_distance(point, k_cam);
#ifdef SPHERICAL
  double radius_LOS = radius;                // Copy the photon radius
  double mu_old, mu_new;                     // Needed for spherical geometry
#ifdef SPECIAL_RELATIVITY
  double beta_r;                             // Needed for aberration effects
#endif
#endif

  while (true) {                             // Ray trace until escape
    // Compute the maximum distance the photon can travel in the cell
    tie(l, next_cell) = face_distance(point, k_cam, cell);

#ifdef SPHERICAL
    if (next_cell == INSIDE)                 // Check if the ray is trapped
      return;                                // No point to continue
#endif

    // Check for spherical escape
    if (spherical_escape) {
      if (l_exit <= l) {                     // Exit sphere before cell
        l = l_exit;                          // Set neighbor distance to sphere
        next_cell = OUTSIDE;                 // Flag for escape condition
      }
      l_exit -= l;                           // Remaining distance to exit
    }

    // Cumulative LOS optical depth
    tau_esc += (k_0[cell] * H(a[cell], freq) // Line opacity
#ifdef ELECTRON_SCATTERING
      + k_e[cell]                            // Electron scattering
#endif
      + k_dust[cell]) * l;                   // Dust absorption

    // Discard photons from the calculation if tau_esc > TAU_DISCARD
    if (tau_esc > TAU_DISCARD)
      return;                                // No point to continue

    // Calculate the new position of the photon
#ifdef SPHERICAL // Spherical geometry modifies frequency during propagation
#define calculate_mu(radius) {(radius > 0.) ? k_cam.dot(point) / (radius) : 0.}
    mu_old = calculate_mu(radius_LOS);       // Old directional cosine
#endif
    point += k_cam * l;                      // Update photon position
#ifdef SPHERICAL
    radius_LOS = point.norm();               // Update current radius
    mu_new = calculate_mu(radius_LOS);       // Update directional cosine
    freq += v[cell].x * (mu_old - mu_new);   // Apply Doppler shift
#endif

    // Doppler-shift from current cell comoving frame to grid frame
#ifdef SPHERICAL
#ifdef SPECIAL_RELATIVITY
    beta_r = (radius_LOS > 0.) ? avthDc * v[cell].x / (a[cell] * radius_LOS) : 0.;
    k_cam += point * beta_r;                 // Aberration: beta = v/c = avthDc u/a
    k_cam.normalize();                       // Maintain lab frame normalization
#endif
    freq += mu_new * v[cell].x;              // Use the spherical cosine
#else // Cartesian-like geometry
    freq += k_cam.dot(v[cell]);
#endif

    if (next_cell == OUTSIDE) {              // Check if the photon escapes
      // Transformation of sqrt(T/T') because a ∝ T^(-1/2)
      freq *= a_exit / a[cell];
      // Doppler-shift from grid frame to exit comoving frame
#ifndef SPHERICAL
      freq -= k_cam.dot(v_exit);
#endif
      break;
    } else {                                 // Continue ray tracing in next cell
      // Transformation of sqrt(T/T') because a ∝ T^(-1/2)
      freq *= a[next_cell] / a[cell];
      // Doppler-shift from grid frame to next cell comoving frame
#ifdef SPHERICAL
#ifdef SPECIAL_RELATIVITY
      mu_new = calculate_mu(radius_LOS);     // Update lab frame directional cosine
      beta_r = (radius_LOS > 0.) ? avthDc * v[next_cell].x / (a[next_cell] * radius_LOS) : 0.;
      k_cam -= point * beta_r;               // Aberration: beta = v/c = avthDc u/a
      k_cam.normalize();                     // Maintain vector normalization
#endif
      freq -= mu_new * v[next_cell].x;       // Use the spherical cosine
#else // Cartesian-like geometry
      freq -= k_cam.dot(v[next_cell]);
#endif
    }
    cell = next_cell;                        // Update the cell index
  }

  // Convert to the frequency output type
  if (!doppler_frequency)
    freq *= neg_vth_kms_exit;                // Convert: x -> Delta_v
  if (adjust_camera_frequency)
    freq -= freq_offsets[camera];            // Adjust camera frequency

  // Surface brightness LOS weight: sum( W(mu) * exp(-tau_esc) )
  double mu, weight_cam;
  switch (scatter_type) {                    // Determine the scattering weight
    case CORE_2P12:                          // Isotropic scattering
      weight_cam = 0.5;
      break;
    case CORE_2P32:                          // Here W = 7/16 * (1 + 3/7 * mu^2)
      mu = direction.dot(k_cam);
      weight_cam = 0.4375 + 0.1875 * mu * mu;
      break;
    case WING_SCAT:                          // Here W = 3/8 * (1 + mu^2)
      mu = direction.dot(k_cam);
      weight_cam = 0.375 * (1. + mu * mu);
      break;
    case DUST_SCAT:                          // Here W = 1/2 (1 - g^2) / (1 + g^2 - 2gμ)^(3/2)
      mu = direction.dot(k_cam);
      weight_cam = _1mg2_2D * pow(_1pg2_2g - mu, -1.5);
      break;
    case OUTWARD_SQRT:
      mu = position.dot(k_cam) / position.norm();
      weight_cam = (mu > 0.) ? 2. * mu : 0.; // Radially outward phase function
      break;
    default:
      weight_cam = 0.;
      error("Unrecognized scatter type in camera calculation.");
  }

  // Combine the phase and optical depth weights
  weight_cam *= exp(-tau_esc);

  // Calculate the escape fraction for each camera
  if (output_escape_fractions)
    #pragma omp atomic
    f_escs[camera] += weight_cam;

  // Note: Convert the position and frequency to image/spectrum coordinates
  // *before* casting to an int to avoid overflow and entering UB territory

  // Bin the photon's frequency - Only add to bins in the specified range
  const double freq_cam = inverse_freq_bin_width * (freq - freq_min);
  const bool in_freq_range = (freq_cam >= 0. && freq_cam < double(n_bins));

  // Bin the photon's position - Only add to bins in the specified range
  const Vec3 r_cam = position - camera_center; // Position relative to camera center
  const double x_cam = inverse_pixel_width * (r_cam.dot(camera_xaxes[camera]) + image_radius);
  const double y_cam = inverse_pixel_width * (r_cam.dot(camera_yaxes[camera]) + image_radius);
  const bool in_image_range = (x_cam >= 0. && x_cam < double(n_pixels) &&
                               y_cam >= 0. && y_cam < double(n_pixels));

  if (output_freq_avgs) {
    const double weighted_freq = weight_cam * freq;
    #pragma omp atomic
    freq_avgs[camera] += weighted_freq;

    if (output_freq_stds) {
      const double weighted_freq2 = weighted_freq * freq;
      #pragma omp atomic
      freq_stds[camera] += weighted_freq2;

      if (output_freq_skews) {
        const double weighted_freq3 = weighted_freq2 * freq;
        #pragma omp atomic
        freq_skews[camera] += weighted_freq3;

        if (output_freq_kurts)
          #pragma omp atomic
          freq_kurts[camera] += weighted_freq3 * freq;
      }
    }
  }

  if (output_fluxes && in_freq_range) {
    const int ibin = floor(freq_cam);
    #pragma omp atomic
    fluxes[camera][ibin] += weight_cam;
  }
  if (in_image_range) {
    const int ix = floor(x_cam);
    const int iy = floor(y_cam);
    if (output_images)
      #pragma omp atomic
      images[camera](ix, iy) += weight_cam;

    if (output_images2)
      #pragma omp atomic
      images2[camera](ix, iy) += weight_cam * weight_cam;

    if (output_freq_images) {
      const double weighted_freq = weight_cam * freq;
      #pragma omp atomic
      freq_images[camera](ix, iy) += weighted_freq;

      if (output_freq2_images) {
        const double weighted_freq2 = weighted_freq * freq;
        #pragma omp atomic
        freq2_images[camera](ix, iy) += weighted_freq2;

        if (output_freq3_images) {
          const double weighted_freq3 = weighted_freq2 * freq;
          #pragma omp atomic
          freq3_images[camera](ix, iy) += weighted_freq3;

          if (output_freq4_images)
            #pragma omp atomic
            freq4_images[camera](ix, iy) += weighted_freq3 * freq;
        }
      }
    }

    if (output_cubes && in_freq_range) {
      const int ibin = floor(freq_cam);
      #pragma omp atomic
      cubes[camera](ix, iy, ibin) += weight_cam;
    }

    if (output_rgb_images) {
      const Vec3 weighted_color = get_rgb_color(freq) * weight_cam;
      Vec3& rgb_pixel = rgb_images[camera](ix, iy);
      #pragma omp atomic
      rgb_pixel.x += weighted_color.x;
      #pragma omp atomic
      rgb_pixel.y += weighted_color.y;
      #pragma omp atomic
      rgb_pixel.z += weighted_color.z;
    }
  }

  // Collisional excitation camera data
  if (output_collisions) {
    const double weight_col = weight_cam * f_col;

    // Calculate the escape fraction for each camera
    if (output_escape_fractions)
      #pragma omp atomic
      f_escs_col[camera] += weight_col;

    if (output_freq_avgs) {
      const double weighted_freq = weight_col * freq;
      #pragma omp atomic
      freq_avgs_col[camera] += weighted_freq;

      if (output_freq_stds) {
        const double weighted_freq2 = weighted_freq * freq;
        #pragma omp atomic
        freq_stds_col[camera] += weighted_freq2;

        if (output_freq_skews) {
          const double weighted_freq3 = weighted_freq2 * freq;
          #pragma omp atomic
          freq_skews_col[camera] += weighted_freq3;

          if (output_freq_kurts)
            #pragma omp atomic
            freq_kurts_col[camera] += weighted_freq3 * freq;
        }
      }
    }

    if (output_fluxes && in_freq_range) {
      const int ibin = floor(freq_cam);
      #pragma omp atomic
      fluxes_col[camera][ibin] += weight_col;
    }
    if (in_image_range) {
      const int ix = floor(x_cam);
      const int iy = floor(y_cam);
      if (output_images)
        #pragma omp atomic
        images_col[camera](ix, iy) += weight_col;

      if (output_images2)
        #pragma omp atomic
        images2_col[camera](ix, iy) += weight_col * weight_col;

      if (output_freq_images) {
        const double weighted_freq = weight_col * freq;
        #pragma omp atomic
        freq_images_col[camera](ix, iy) += weighted_freq;

        if (output_freq2_images) {
          const double weighted_freq2 = weighted_freq * freq;
          #pragma omp atomic
          freq2_images_col[camera](ix, iy) += weighted_freq2;

          if (output_freq3_images) {
            const double weighted_freq3 = weighted_freq2 * freq;
            #pragma omp atomic
            freq3_images_col[camera](ix, iy) += weighted_freq3;

            if (output_freq4_images)
              #pragma omp atomic
              freq4_images_col[camera](ix, iy) += weighted_freq3 * freq;
          }
        }
      }
    }
  }
}

/* Generate a random direction from an isotropic distribution. */
Vec3 isotropic_direction() {
  const double theta = acos(2. * ran() - 1.);
  const double sin_theta = sin(theta);
  const double phi = _2pi * ran();

  // Outgoing direction is a unit vector described by (theta, phi)
  Vec3 k_out {
    sin_theta * cos(phi),
    sin_theta * sin(phi),
    cos(theta),
  };
  k_out.normalize();                         // Normalize to avoid error
  return k_out;
}

/* Generate random angles from an anisotropic distribution. */
Vec3 anisotropic_direction(const int scatter_type, const Vec3 k) {
  if (scatter_type == CORE_2P12)
    return isotropic_direction();
  double muS = 0.;                           // Directional cosine
  if (scatter_type == CORE_2P32) {
    // Scatter: W(theta) propto 1 + R/Q cos(theta)^2  (with R/Q = 3/7)
    // Inverse CDF method gives cubic polynomial solution
    const double rv = 8. * ran() - 4.;       // Random variable
    const double dv = pow(sqrt(_343_27 + rv * rv) - rv, _1_3);
    // = ( sqrt( 343/27 + rv^2 ) - rv )^(1/3) = cubic discriminant
    muS = _7_3 / dv - dv;                    // mu = (7/3) / dv - dv
  } else if (scatter_type == WING_SCAT) {
    // Dipole approximation gives a different cubic polynomial solution
    const double rv = 4. * ran() - 2.;       // Random variable
    const double dv = pow(sqrt(1. + rv * rv) - rv, _1_3);
    // = ( sqrt( 1 + rv^2 ) - rv )^(1/3) = cubic discriminant
    muS = 1. / dv - dv;                      // mu = 1 / dv - dv
  } else if (scatter_type == OUTWARD_SQRT) {
    muS = sqrt(ran());                       // Outward limb darkening cosine
  } else
    error("Invalid scattering mode!");

  // Precompute sin and cos multiplied by sqrt(1-mu^2)
  const double phi = _2pi * ran();           // Arbitrary azimuthal angle
  const double sqrt_1muS2 = sqrt(1. - muS * muS);
  const double sin_phi = sqrt_1muS2 * sin(phi);
  const double cos_phi = sqrt_1muS2 * cos(phi);
  const double zS = sqrt(1. - k.z * k.z);    // sqrt(kx^2+ky^2) = sqrt(1-kz^2)

  // Outgoing direction in terms of the angles mu and phi
  Vec3 k_out {
    muS * k.x + (k.y * cos_phi + k.x * k.z * sin_phi) / zS,
    muS * k.y - (k.x * cos_phi - k.y * k.z * sin_phi) / zS,
    muS * k.z - sin_phi * zS,
  };
  k_out.normalize();                         // Normalize to avoid error
  return k_out;
}

/* Generate random angles from an anisotropic distribution (dust). */
static Vec3 anisotropic_direction_HG(Vec3 k) {
  // Henyey-Greenstein phase function for <μ>: ζ = random number in [0,1]
  const double xiS = _1mg2_2g / (_1mg_2g + ran()); // ξ = (1-g^2) / (1-g+2gζ)
  const double muS = _1pg2_2g - _1_2g * xiS * xiS; // μ = (1+g^2-ξ^2) / (2g)

  // Precompute sin and cos multiplied by sqrt(1-mu^2)
  const double phi = _2pi * ran();           // Arbitrary azimuthal angle
  const double sqrt_1muS2 = sqrt(1. - muS * muS);
  const double sin_phi = sqrt_1muS2 * sin(phi);
  const double cos_phi = sqrt_1muS2 * cos(phi);
  const double zS = sqrt(1. - k.z * k.z);    // sqrt(kx^2+ky^2) = sqrt(1-kz^2)

  // Outgoing direction in terms of the angles mu and phi
  Vec3 k_out {
    muS * k.x + (k.y * cos_phi + k.x * k.z * sin_phi) / zS,
    muS * k.y - (k.x * cos_phi - k.y * k.z * sin_phi) / zS,
    muS * k.z - sin_phi * zS,
  };
  k_out.normalize();                         // Normalize to avoid error
  return k_out;
}

/* Calculate atau0 from non-local (precalculated) and local cell values. */
static double get_atau0(Vec3 point, int cell) {
  // Find the largest sphere centered around the point contained in the cell
  double dl_face = min_face_distance(point, cell);

  // Check whether the photon is closer to an exit condition
  if (spherical_escape) {
    const double dl_sphere = escape_radius - (point - escape_center).norm();
    if (dl_sphere < dl_face)
      dl_face = dl_sphere;                   // Update the face distance
  }

  return atau[cell] + a[cell] * k_0[cell] * dl_face;
}

/* Isolate everything that happens during the scattering event. */
void Photon::line_scatter() {
  const double x = frequency;                // Frequency before scattering
  const double a0 = a[current_cell];         // Save the damping parameter
  const double absx = fabs(x);               // Assume x > 0 and correct sign later
  const double xcw = X0 + X1 / (log(a0) - X2); // Transition from the core to the wing

  int scatter_type; // Either Core 2P_1/2, Core 2P_3/2, or Wing scattering
  if (absx < xcw) { // Core
    // Give 1/3 probability of an isotropic scatter - 2P_1/2 state
    if (ran() < _1_3) // Isotropic direction and LOS weight
      scatter_type = CORE_2P12;
    else // Or 2/3 probability of anisotropic scatter - 2P_3/2 state
      scatter_type = CORE_2P32;
  } else
    scatter_type = WING_SCAT;

  // Direction: Anisotropic with W(theta) propto 1 + R/Q cos(theta)^2
  const Vec3 k_out = anisotropic_direction(scatter_type, direction);

  // Atom's velocity is generated from the parallel component distribution
  double u_par; // Accept-Reject from exp(-upar^2) / (a^2 + (upar-x)^2)
  if (absx < 1.) {
    do {
      u_par = x + a0 * tan(M_PI * (ran() - 0.5));
    } while (ran() > exp(-u_par * u_par));
  } else if (absx > 9.) { // Draw from a Gaussian for large frequencies
    u_par = sqrt(-log(ran())) * cos(_2pi * ran()) + 1. / x;
  } else { // Acceleration scheme: compare to exp(u0^2 - upar^2) / (a^2 + (upar-nu)^2)
    double u0, comp;
    if (absx < xcw)
      u0 = absx - 1. / (absx + exp(1. - absx * absx) / a0);
    else
      u0 = xcw - 1. / xcw + 0.15 * (absx - xcw);

    const double t0 = atan((u0 - absx) / a0);
    const double exp_u2 = exp(-u0 * u0);
    const double p0 = (t0 + M_PI_2) / ((1. - exp_u2) * t0 + (1. + exp_u2) * M_PI_2);

    do {
      if (ran() <= p0)                       // Tan argument from [-pi/2, theta0]
        u_par = a0 * tan(ran() * (t0 + M_PI_2) - M_PI_2) + absx;
      else                                   // Otherwise from [theta0, pi/2]
        u_par = a0 * tan(ran() * (t0 - M_PI_2) + M_PI_2) + absx;
      if (u_par <= u0)                       // Compare to g1
        comp = exp(-u_par * u_par);
      else                                   // Compare to g2
        comp = exp(u0 * u0 - u_par * u_par);
    } while (ran() > comp);                  // Only accept proper values
    if (x < 0.)
      u_par *= -1.;                          // Asymmetric distribution
  }

  // Perpendicular velocity is Maxwellian modified by core-skipping
  double u_perp;                             // Perpendicular velocity
  if (absx < xcw) {                          // Must be a core photon
    if (dynamical_core_skipping) {
      double atau0 = get_atau0(position, current_cell);
      if (atau0 > 1.)                        // Optically thick => atau0 > 1
        u_perp = sqrt(0.04 * pow(atau0, _2_3) - log(ran())); // Volcano gaussian
      else                                   // Optically thin => Don't skip
        u_perp = sqrt(-log(ran()));          // Bypass core-skipping => gaussian
    } else                                   // Constant core-skipping
      u_perp = sqrt(x_crit_2 - log(ran()));  // Volcano gaussian
  } else                                     // Don't accelerate wing photons
    u_perp = sqrt(-log(ran()));              // Bypass core-skipping => gaussian

  // Assemble the complete vector u_atom = v_atom / v_th
  // u = sum of the vectors u_par, u_perp_cos, and u_perp_sin
  // Idea: find two orthonormal vectors to u_par = {kx, ky, kz}
  // un-normalized u_perp vectors: {-ky, kx, 0} and {-kx kz, -ky kz, kx^2 + ky^2}
  const double phi = _2pi * ran();           // Azimuthal angle
  const double u_perp_cos = u_perp * cos(phi); // Magnitudes of perpendicular vectors
  const double u_perp_sin = u_perp * sin(phi);
  const double zS = sqrt(1. - direction.z * direction.z); // sqrt(kx^2+ky^2) = sqrt(1-kz^2)

  Vec3 u_atom {
    u_par * direction.x - (direction.y * u_perp_cos + direction.x * direction.z * u_perp_sin) / zS,
    u_par * direction.y + (direction.x * u_perp_cos - direction.y * direction.z * u_perp_sin) / zS,
    u_par * direction.z + u_perp_sin * zS,
  };

  // Camera calculations using the updated frequency (but previous direction)
  for (int camera = 0; camera < n_cameras; ++camera) {
    frequency = x + camera_directions[camera].dot(u_atom) - u_par;
    if (recoil)
      frequency += gDa * a0 * (direction.dot(camera_directions[camera]) - 1.);
    calculate_LOS(scatter_type, camera);     // Next event estimation
  }

  // Account for recoil and Doppler-shifting from the scattering atom
  // g = h*DnuD/2*kB*T  =>  gDa = g/a = 2*h*nu0^2/mH*c^2*DnuL = 0.54
  frequency = x + u_atom.dot(k_out) - u_par;
  if (recoil)
    frequency += gDa * a0 * (direction.dot(k_out) - 1.);

  // Update the photon direction (after camera calculations)
  direction = k_out;
}

/* Isolate everything that happens during the scattering event (complete redistribution). */
void Photon::line_scatter_crd() {
  // Direction: Anisotropic with W(theta) propto 1 + R/Q cos(theta)^2
  // const Vec3 k_out = anisotropic_direction(WING_SCAT, direction);
  const Vec3 k_out = isotropic_direction();

  // Atom's velocity is generated from a Maxwellian distribution
  // Assemble the complete vector u_atom = v_atom / v_th
  // u = sum of the vectors u_par, u_perp_cos, and u_perp_sin
  // Idea: find two orthonormal vectors to u_par = {kx, ky, kz}
  // un-normalized u_perp vectors: {-ky, kx, 0} and {-kx kz, -ky kz, kx^2 + ky^2}
  const double u_par = sqrt(-log(ran())) * cos(_2pi * ran()); // Parallel velocity
  const double phi = _2pi * ran();           // Azimuthal angle
  const double u_perp = sqrt(-log(ran()));   // Perpendicular velocity
  const double u_perp_cos = u_perp * cos(phi); // Magnitudes of perpendicular vectors
  const double u_perp_sin = u_perp * sin(phi);
  const double zS = sqrt(1. - direction.z * direction.z); // sqrt(kx^2+ky^2) = sqrt(1-kz^2)

  Vec3 u_atom {
    u_par * direction.x - (direction.y * u_perp_cos + direction.x * direction.z * u_perp_sin) / zS,
    u_par * direction.y + (direction.x * u_perp_cos - direction.y * direction.z * u_perp_sin) / zS,
    u_par * direction.z + u_perp_sin * zS,
  };

  // Camera calculations using the updated frequency (but previous direction)
  const double x = frequency;                // Frequency before scattering
  for (int camera = 0; camera < n_cameras; ++camera) {
    frequency = x + camera_directions[camera].dot(u_atom) - u_par;
    calculate_LOS(WING_SCAT, camera);        // Next event estimation
  }

  // Account for recoil and Doppler-shifting from the scattering atom
  // g = h*DnuD/2*kB*T  =>  gDa = g/a = 2*h*nu0^2/mH*c^2*DnuL = 0.54
  frequency = x + u_atom.dot(k_out) - u_par;

  // Update the photon direction (after camera calculations)
  direction = k_out;
}

/* Isolate everything that happens during the dust scattering event. */
void Photon::dust_scatter() {
  // Camera calculations using the dust phase function
  calculate_LOS(DUST_SCAT);

  // Outgoing direction from the anisotropic Henyey-Greenstein phase function
  direction = anisotropic_direction_HG(direction);
}

#ifdef ELECTRON_SCATTERING
static const double sqrt_mH_me = sqrt(mH / me); // Thermal velocity conversion
/* Isolate everything that happens during the scattering event. */
void Photon::electron_scatter() {
  // Direction: Anisotropic with W(theta) propto 1 + R/Q cos(theta)^2
  const Vec3 k_out = anisotropic_direction(WING_SCAT, direction);

  // Electron's velocity is generated from a Maxwellian distribution
  // Assemble the complete vector u_electron = v_electron / v_th
  // u = sum of the vectors u_par, u_perp_cos, and u_perp_sin
  // Idea: find two orthonormal vectors to u_par = {kx, ky, kz}
  // un-normalized u_perp vectors: {-ky, kx, 0} and {-kx kz, -ky kz, kx^2 + ky^2}
  const double u_par = sqrt_mH_me * sqrt(-log(ran())) * cos(_2pi * ran()); // Parallel velocity
  const double phi = _2pi * ran();           // Azimuthal angle
  const double u_perp = sqrt_mH_me * sqrt(-log(ran())); // Perpendicular velocity
  const double u_perp_cos = u_perp * cos(phi); // Magnitudes of perpendicular vectors
  const double u_perp_sin = u_perp * sin(phi);
  const double zS = sqrt(1. - direction.z * direction.z); // sqrt(kx^2+ky^2) = sqrt(1-kz^2)

  Vec3 u_electron {
    u_par * direction.x - (direction.y * u_perp_cos + direction.x * direction.z * u_perp_sin) / zS,
    u_par * direction.y + (direction.x * u_perp_cos - direction.y * direction.z * u_perp_sin) / zS,
    u_par * direction.z + u_perp_sin * zS,
  };

  // Camera calculations using the updated frequency (but previous direction)
  const double x = frequency;                // Frequency before scattering
  for (int camera = 0; camera < n_cameras; ++camera) {
    frequency = x + camera_directions[camera].dot(u_electron) - u_par;
    calculate_LOS(WING_SCAT, camera);        // Next event estimation
  }

  // Account for recoil and Doppler-shifting from the scattering electron
  frequency = x + u_electron.dot(k_out) - u_par;
  // TODO: Include electron recoil
  // g = h*DnuD/2*kB*T  =>  gDa = g/a = 2*h*nu0^2/mH*c^2*DnuL = 0.54
  // frequency += gDa * a0 * (direction.dot(k_out) - 1.);

  // Update the photon direction (after camera calculations)
  direction = k_out;
}
#endif
