/****************
 * mcrt/vars.cc *
 ****************

 * Module global variable declarations (defaults).

*/

#include "proto.h"

// Line parameters
string line = "Lyman-alpha";                 // Name of the line
double E0;                                   // Line energy [erg]
double A21;                                  // Einstein A coefficient [Hz]
double f12;                                  // Oscillator strength
double g12;                                  // Degeneracy ratio: g_lower / g_upper
double T0;                                   // Line temperature [K]
double nu0;                                  // Line frequency [Hz]
double lambda0;                              // Line wavelength [angstrom]
double B21;                                  // Einstein B21 = A21 c^2 / (2 h ν0^3)
double DnuL;                                 // Natural frequency broadening [Hz]
double avthDc;                               // Conversion for special relativity
double m_carrier;                            // Mass of carrier [g]
bool scaled_microturb = false;               // Scaled microturbulence model
double v_turb = 0.;                          // Microturbulent velocity [cm/s]
double T_turb;                               // Microturbulent temperature [K]
double vth_div_sqrtT;                        // Thermal velocity: vth = sqrt(2 kB T / m_carrier)
double DnuD_div_sqrtT;                       // Doppler width: ΔνD = ν0 vth / c
double a_sqrtT;                              // "damping parameter": a = ΔνL / 2 ΔνD
double sigma0_sqrtT;                         // Cross section: sigma0_sqrtT = sigma0 * sqrt(T)
double x_div_aDv;                            // Constant for stellar continuum emission
double gDa;                                  // Recoil parameter / "damping parameter" = 2 h ν0^2 / (mH c^2 ΔνL)

// Information about the dust model
string dust_model = "";                      // Dust model: SMC, MW, etc.
double kappa_dust = 0.;                      // Dust opacity [cm^2/g dust]
double sigma_dust = 0.;                      // Dust cross section [cm^2/Z/hydrogen atom]
double albedo = 0.;                          // Dust albedo for scattering vs. absorption
double g_dust = 0.;                          // Anisotropy parameter: <μ> for dust scattering
double f_ion = 1.;                           // H_II region survival fraction
double T_sputter = 1e6;                      // Thermal sputtering cutoff [K]
double _1_2g, _1pg2_2g, _1mg2_2D, _1mg2_2g, _1mg_2g; // Constants for Henyey-Greenstein dust scattering

// Configurable line transfer flags
bool recombinations = false;                 // Include recombination emission
double T_floor_rec = 0.;                     // Apply a recombination temperature floor: T [K]
bool collisions = false;                     // Include collisional excitation
bool output_collisions = false;              // Output collisional excitation data
double collisions_limiter = 0.;              // Limited by photoheating rate
bool spontaneous = false;                    // Include spontaneous emission
bool continuum = false;                      // Include stellar continuum emission
bool two_level_atom = false;                 // Two level atom line transfer
bool line_scattering = true;                 // Turn line scattering on/off
bool dynamical_core_skipping = false;        // Dynamical core-skipping
double x_crit = 0., x_crit_2;                // Constant core-skipping frequency
bool doppler_frequency = false;              // Output frequency in Doppler widths
bool recoil = true;                          // Include recoil with scattering

// Information about the source
int n_photons = 1;                           // Actual number of photon packets used
int n_absorbed;                              // Number of absorbed photons
int n_escaped;                               // Number of escaped photons
int n_finished;                              // Track finished progress
double Ndot_tot = 0.;                        // Total emission rate [photons/s]
double L_tot = 0.;                           // Total luminosity [erg/s]
double L_point = 0.;                         // Point source luminosity [erg/s]
double L_shell = 0.;                         // Shell source luminosity [erg/s]
double L_shock = 0.;                         // Shock source luminosity [erg/s]
double L_rec = 0.;                           // Recombination luminosity [erg/s]
double L_col = 0.;                           // Collisional excitation luminosity [erg/s]
double L_sp = 0.;                            // Spontaneous emission luminosity [erg/s]
double L_cont = 0.;                          // Continuum emission luminosity [erg/s]
double M_cont = 0.;                          // Total M_cont [absolute magnitude]

// Point source variables
bool point_source = false;                   // Insert photons at a specified point (default)
int i_point = 0;                             // Cell index to insert photons
double x_point = 1e-12;                      // Point source position [cm]
double y_point = 1e-12;
double z_point = 1e-12;
Vec3 r_point = {x_point, y_point, z_point};  // (x, y, z) [cm]

// Shell source variables
bool shell_source = false;                   // Insert photons from a shell
bool shell_blackbody = false;                // Luminosity based on a black body
double r_shell = 1e-12;                      // Shell source radius [cm]
double T_shell = 0.;                         // Shell source temperature [K]

// Shock source variables
bool shock_source = false;                   // Insert photons from a shock
bool shock_blackbody = false;                // Luminosity based on a black body
double r_shock = 1e-12;                      // Shock source radius [cm]
double T_shock = 0.;                         // Shock source temperature [K]

// Spatial emission from cells and stars
bool cell_based_emission = false;            // Include cell-based sources
double j_exp = 1;                            // Luminosity boosting exponent (j_exp < 1 favors lower emissivity cells)
array<int, n_cdf> j_map;                     // Lookup table for photon insertion (cells)
vector<double> j_cdf;                        // Cumulative distribution function for emission
vector<double> j_weights;                    // Initial photon weights for each cell
vector<double> f_col_cells;                  // Collisional excitation fractions for each cell
array<int, n_cdf_stars> j_map_stars;         // Lookup table for photon insertion (stars)
vector<double> j_cdf_stars;                  // Cumulative distribution function for emission
vector<double> j_weights_stars;              // Initial photon weights for each star
double emission_n_min = 0.;                  // Minimum number density of emitting cells [cm^-3]
double emission_n_max = positive_infinity;   // Maximum number density of emitting cells [cm^-3]

// Distinguish between source types
int n_source_types;                          // Number of source types used
array<double, MAX_SOURCE_TYPES> mixed_pdf;   // Mixed source probability distribution function
array<double, MAX_SOURCE_TYPES> mixed_cdf;   // Mixed source cumulative distribution function
array<double, MAX_SOURCE_TYPES> mixed_weights; // Mixed source weights (point, cells, stars)
vector<int> source_types_mask;               // Source type mask indices
strings source_names_mask;                   // Source type names (masked)
vector<double> mixed_pdf_mask;               // Mixed source pdf (masked)
vector<double> mixed_cdf_mask;               // Mixed source cdf (masked)
vector<double> mixed_weights_mask;           // Mixed source weights (masked)

// Exit variables
bool exit_wrt_com = false;                   // Exit with respect to the center of mass frame
Vec3 v_exit = {0.,0.,0.};                    // Exit velocity [cm/s]
double T_exit = 1e4;                         // Exit temperature [K]
double a_exit;                               // "Damping parameter" [T_exit]
double DnuD_exit;                            // Doppler width at T_exit
double vth_div_c_exit;                       // Thermal velocity (vth/c) [T_exit]
double neg_vth_kms_exit;                     // Thermal velocity (-km/s) [T_exit]
// double neg_a_vth_kms_exit;                   // Constant for observable frequency conversion (x -> Δv)
bool adjust_camera_frequency = false;        // Adjust camera frequency based on offsets
vector<double> freq_offsets;                 // Frequency offsets for each camera

// Information about escaped photons
double f_esc = 0.;                           // Global escape fraction
double freq_avg = 0.;                        // Global average frequency [freq units]
double freq_std = 0.;                        // Global standard deviation [freq units]
double freq_skew = 0.;                       // Global frequency skewness [normalized]
double freq_kurt = 0.;                       // Global frequency kurtosis [normalized]
bool output_photons = true;                  // Output photon packets (esc/abs)
string photon_file = "";                     // Output a separate photon file
// bool escaped_frequency_moments = false;      // Calculate escaped frequency moments
vector<int> source_ids;                      // Emission cell or star index
vector<int> source_types;                    // Emission type specifier
vector<double> source_weights;               // Photon weight at emission
vector<double> f_cols;                       // Collisional excitation fraction
vector<double> freqs;                        // Frequency at escape [code_freq]
vector<double> weights;                      // Photon weight at escape
vector<Vec3> positions;                      // Position at escape [cm]
vector<Vec3> directions;                     // Direction at escape

// Information about secondary cameras (mcrt)
bool output_mcrt_emission = false;           // Output intrinsic emission without transport
bool output_mcrt_attenuation = false;        // Output attenuated emission without scattering
vector<double> f_escs_ext;                   // Attenuated escape fractions [fraction]
vector<double> freq_avgs_int;                // Intrinsic frequency averages [freq units]
vector<double> freq_avgs_ext;                // Attenuated frequency averages [freq units]
vector<vector<double>> fluxes_int;           // Intrinsic spectral fluxes [erg/s/cm^2/angstrom]
vector<vector<double>> fluxes_ext;           // Attenuated spectral fluxes [erg/s/cm^2/angstrom]
vector<Image<double>> images_int;            // Intrinsic surface brightness images [erg/s/cm^2/arcsec^2]
vector<Image<double>> images_ext;            // Attenuated surface brightness images [erg/s/cm^2/arcsec^2]
vector<Cube<double>> cubes_int;              // Intrinsic spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
vector<Cube<double>> cubes_ext;              // Attenuated spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

// Information about secondary cameras (proj)
bool output_proj_emission = false;           // Output intrinsic emission without transport
bool output_proj_attenuation = false;        // Output attenuated emission without scattering
vector<double> proj_f_escs_ext;              // Attenuated escape fractions [fraction]
vector<Image<double>> proj_images_int;       // Intrinsic surface brightness images [erg/s/cm^2]
vector<Image<double>> proj_images_ext;       // Attenuated surface brightness images [erg/s/cm^2]

// Collisional excitation camera data
double f_esc_col = 0.;                       // Global escape fraction
double freq_avg_col = 0.;                    // Global average frequency [freq units]
double freq_std_col = 0.;                    // Global standard deviation [freq units]
double freq_skew_col = 0.;                   // Global frequency skewness [normalized]
double freq_kurt_col = 0.;                   // Global frequency kurtosis [normalized]
vector<double> f_escs_col;                   // Escape fractions [fraction]
vector<double> freq_avgs_col;                // Frequency averages [freq units]
vector<double> freq_stds_col;                // Standard deviations [freq units]
vector<double> freq_skews_col;               // Frequency skewnesses [normalized]
vector<double> freq_kurts_col;               // Frequency kurtoses [normalized]
vector<vector<double>> fluxes_col;           // Spectral fluxes [erg/s/cm^2/angstrom]
vector<Image<double>> images_col;            // Surface brightness images [erg/s/cm^2/arcsec^2]
vector<Image<double>> images2_col;           // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
vector<Image<double>> freq_images_col;       // Average frequency images [freq units]
vector<Image<double>> freq2_images_col;      // Frequency^2 images [freq^2 units]
vector<Image<double>> freq3_images_col;      // Frequency^3 images [freq^3 units]
vector<Image<double>> freq4_images_col;      // Frequency^4 images [freq^4 units]
vector<double> f_escs_ext_col;               // Attenuated escape fractions [fraction]
vector<double> freq_avgs_int_col;            // Intrinsic frequency averages [freq units]
vector<double> freq_avgs_ext_col;            // Attenuated frequency averages [freq units]
vector<vector<double>> fluxes_int_col;       // Intrinsic spectral fluxes [erg/s/cm^2/angstrom]
vector<vector<double>> fluxes_ext_col;       // Attenuated spectral fluxes [erg/s/cm^2/angstrom]
vector<Image<double>> images_int_col;        // Intrinsic surface brightness images [erg/s/cm^2/arcsec^2]
vector<Image<double>> images_ext_col;        // Attenuated surface brightness images [erg/s/cm^2/arcsec^2]
vector<double> proj_f_escs_ext_col;          // Attenuated escape fractions [fraction]
vector<Image<double>> proj_images_int_col;   // Intrinsic surface brightness images [erg/s/cm^2]
vector<Image<double>> proj_images_ext_col;   // Attenuated surface brightness images [erg/s/cm^2]
