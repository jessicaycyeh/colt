/*!
 * \file    io.cc
 *
 * \brief   General input and output functionality.
 *
 * \details This file contains general input and output functionality for the
 *          flow of the program, mostly print/read/write operations.
 */

#include "proto.h"
#include "Simulation.h"
#include "timing.h" // Timing functionality
#include "io_hdf5.h" // HDF5 read/write functions

extern Timer main_timer; // Clock timing
extern Timer read_timer;
extern Timer write_timer;

// Module specific parameters
extern bool star_based_emission;             // Include star-based sources
extern bool continuum;                       // Include stellar continuum emission
extern bool two_level_atom;                  // Two level atom line transfer
extern double g12;                           // Degeneracy ratio: g_lower / g_upper
extern bool scaled_microturb;                // Scaled microturbulence model
extern double T_turb;                        // Microturbulent temperature [K]
extern double a_sqrtT;                       // "damping parameter": a = ΔνL / 2 ΔνD
extern double sigma0_sqrtT;                  // Cross section: sigma0_sqrtT = sigma0 * sqrt(T)
extern double f_ion;                         // H_II region survival fraction
extern double T_sputter;                     // Thermal sputtering cutoff [K]
extern double kappa_dust;                    // Dust opacity [cm^2/g dust]
extern double sigma_dust;                    // Dust cross section [cm^2/Z/hydrogen atom]

void print_min_max(string name, vector<double> vec, string units = "", double conv = 1.);
void print_min_max(string name, vector<Vec3> vec, string units = "", double conv = 1.);
double calculate_d_L(const double z); // Luminosity distance [cm]
void read_geometry(H5File& f); // Read geometry specific data
bool avoid_cell(const int cell); // Avoid calculations for certain cells

// Utility function to print singular or plural words.
static inline string plural(const int size, const string str) {
  return to_string(size) + " " + str + (size > 1 ? "s" : "");
}

//! \brief Print a header for the code.
void print_header() {
  cout << main_timer.date()
       << "\n                  _"
       << "\n                 | |  _"
       << "\n         ___ ___ | |_| |_"
       << "\n        / __) _ \\| (_   _)"
       << "\n       ( (_| |_| | | | |_"
       << "\n        \\___)___/ \\_) \\__)"
       << "\n"
       << "\n Cosmic Lyman-alpha Transfer code"
       << "\n"
       << "\n"
       << "\nWorking with " << plural(n_ranks * n_threads, "core") << " ("
       << plural(n_ranks, "task") << ", " << plural(n_threads, "thread") << ")"
       << "\n"
       << "\nRun as " << executable << " " << config_file
       << ((snap_num >= 0) ? " " + to_string(snap_num) : "")
       << ((halo_num >= 0) ? " " + to_string(halo_num) : "")
       << "\n"
       << "\nMain simulation parameters:"
       << "\n  Initial conditions file = " << init_file
       << "\n  Data output directory   = " << output_dir
       << "\n  Output file base name   = " << output_base << endl;
}

//! \brief Print a footer for code setup.
void print_footer() {
  cout << "\n+-------------------------------------------------+"
       << "\n| Initialization complete. Starting calculations. |"
       << "\n+-------------------------------------------------+" << endl;
}

//! \brief Print a success banner.
void print_success() {
  cout << "\n+-------------------------------------------------+"
       << "\n| Completed successfully!  Finalizing simulation. |"
       << "\n+-------------------------------------------------+" << endl;
}

// Set a given vector to a constant value of size n_cells.
static inline void fill_constant(vector<double>& vec, const double val) {
  vec.resize(n_cells);                       // Allocate space
  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i)
    vec[i] = val;                            // Set to constant value
}

// Set a given vector to the product of a constant and a vector of size n_cells.
static inline void fill_product(vector<double>& vec, const vector<double>& ref, const double val = 1.) {
  vec.resize(n_cells);                       // Allocate space
  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i)
    vec[i] = ref[i] * val;                   // Set to product value
}

// Set a given vector to the difference of a constant and a vector of size n_cells.
static inline void fill_remaining(vector<double>& vec, const vector<double>& ref, const double tot = 1.) {
  vec.resize(n_cells);                       // Allocate space
  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i)
    vec[i] = tot - ref[i];                   // Set to remaining value
}

//! \brief Read the initial conditions from the specified hdf5 filename.
void Simulation::read_hdf5() {
  read_timer.start();

  if (!init_base.empty())
    init_file += "." + init_ext;             // Add file extension

  // Open file and read all data
  H5File f(init_file, H5F_ACC_RDONLY);

  // Initialize cosmology information
  if (cosmological) {
    read(f, "redshift", z);                  // Current simulation redshift
    read_if_exists(f, "Omega0", Omega0);     // Matter density [rho_crit_0]
    read_if_exists(f, "OmegaB", OmegaB);     // Baryon density [rho_crit_0]
    read_if_exists(f, "h100", h100);         // Hubble constant [100 km/s/Mpc]
    d_L = calculate_d_L(z);                  // Luminosity distance [cm]
  } else
    read_if_exists(f, "time", sim_time);     // Simulation time [s]

  // Read data fields
  read_geometry(f);                          // Read geometry specific data
  if (set_density_from_mass)
    read(f, "m", rho, n_cells, "g");         // Mass [g] (converted below)
  else
    read(f, "rho", rho, n_cells, "g/cm^3");  // Density [g/cm^3]
  if (read_density_as_mass || set_density_from_mass)
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (avoid_cell(i))
        rho[i] = 0.;                         // Ignore certain cells
      else
        rho[i] /= V[i];                      // Convert mass to density
    }
  read(f, "v", v, n_cells, "cm/s");          // Bulk velocity [cm/s]
  if (constant_temperature >= 0.)
    fill_constant(T, constant_temperature);  // User defined temperature
  else
    read(f, "T", T, n_cells, "K");           // Temperature [K]
  if (T_floor >= 0.) {
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i)
      if (T[i] < T_floor)
        T[i] = T_floor;                      // Apply temperature floor [K]
  }
  if (neutral_fraction >= 0.)
    fill_constant(x_HI, neutral_fraction);   // User defined neutral fraction
  else
    read(f, "x_HI", x_HI, n_cells);          // x_HI = n_HI / n_H
  if (read_ionized_fraction)
    read(f, "x_HII", x_HII, n_cells);        // x_HII = n_HII / n_H
  else
    fill_remaining(x_HII, x_HI);             // Use approx: x_HII = 1 - x_HI
  if (read_molecular_fraction)
    read(f, "x_H2", x_H2, n_cells);          // Mass fraction of molecular hydrogen
  if (read_electron_fraction)
    read(f, "x_e", x_e, n_cells);            // Electron fraction: x_e = n_e / n_H
  else
    fill_product(x_e, x_HII, 1.157894737);   // Use approx: x_e = 1.16 x_HII
  if (read_helium) {
    const double X_prim = 0.76;              // Primordial mass fraction of hydrogen
    const double yHe = (1. - X_prim) / (4. * X_prim); // Conversion to helium (Note: Ignores metals)
    read_if_exists(f, "x_HeI", x_HeI, yHe, n_cells);  // x_HeI = n_HeI / n_H
    read_if_exists(f, "x_HeII", x_HeII, 0., n_cells); // x_HeII = n_HeII / n_H
  }
  if (read_photoheating)
    read(f, "G_ion", G_ion, n_cells, "erg/s"); // Photoheating rate [erg/s]
  if (two_level_atom) {
    read(f, "n_lower", n_lower, n_cells, "cm^-3"); // Number density of lower transition
    read(f, "n_upper", n_upper, n_cells, "cm^-3"); // Number density of upper transition
  }
  if (read_hydrogen_fraction)
    read(f, "X", X, n_cells);                // Mass fraction of hydrogen
  if (metallicity >= 0.)
    fill_constant(Z, metallicity);           // User defined metallicity
  else
    read(f, "Z", Z, n_cells);                // Metallicity [mass fraction]
  if (dust_to_gas >= 0.)
    fill_constant(D, dust_to_gas);           // User defined dust-to-gas ratio
  else if (dust_to_metal >= 0.)
    fill_product(D, Z, dust_to_metal);       // User defined dust-to-metal ratio
  else
    read(f, "D", D, n_cells);                // Dust-to-gas ratio [mass fraction]
  if (dust_boost > 0.)
    rescale_omp(D, dust_boost);              // Optional dust boost factor
  if (read_SFR)
    read(f, "SFR", SFR, n_cells, "Msun/yr"); // Star formation rate [Msun/yr]
  if (read_T_dust)
    read(f, "T_dust", T_dust, n_cells, "K"); // Dust temperature [K]
  if (read_v2) {
    v2.resize(n_cells);                      // Velocity^2 [cm^2/s^2]
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i)
      v2[i] = v[i].dot();                    // Velocity^2 [cm^2/s^2]
  }
  if (read_v2_LOS)
    v2_LOS.resize(n_cells);                  // Velocity_LOS^2 [cm^2/s^2]
  if (read_v_LOS)
    v_LOS.resize(n_cells);                   // Velocity_LOS [cm/s]
  if (read_v_x)
    v_x.resize(n_cells);                     // Velocity_X [cm/s]
  if (read_v_y)
    v_y.resize(n_cells);                     // Velocity_Y [cm/s]
  if (read_B)
    read(f, "B", B, n_cells, "G");           // Magnetic field [G]
  if (read_B2) {
    B2.resize(n_cells);                      // Magnetic field^2 [G^2]
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i)
      B2[i] = B[i].dot();                    // Magnetic field^2 [G^2]
  }
  if (read_B_LOS)
    B_LOS.resize(n_cells);                   // Magnetic field LOS [G]
  if (read_B_x)
    B_x.resize(n_cells);                     // Magnetic field X [G]
  if (read_B_y)
    B_y.resize(n_cells);                     // Magnetic field Y [G]
  if(read_rho2) {
    rho2.resize(n_cells);
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i)
      rho2[i] = rho[i] * rho[i];             // Density^2 [g^2/cm^6]
  }
  if (read_ion_front) {
    ion_front.resize(n_cells);
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i)
      ion_front[i] = exp(-1000.0 * pow(x_HI[i]-0.01,2)); // Ionization front
  }

  // Read star data
  if (star_based_emission) {
    n_stars = read(f, "r_star", r_star, "cm"); // Star positions [cm]
    if (continuum)
      read(f, strings{"L_cont", "L_UV"}, L_cont_star, n_stars, "erg/s/angstrom"); // Stellar continuum [erg/s/angstrom]
    if (read_m_massive_star)
      read(f, "m_massive_star", m_massive_star, n_stars, "Msun"); // Massive star mass [Msun]
    if (read_m_init_star)
      read(f, strings{"m_init_star", "m_star"}, m_init_star, n_stars, "Msun"); // Initial star mass [Msun]
    if (read_Z_star)
      read(f, "Z_star", Z_star, n_stars); // Star metallicity [mass fraction]
    if (read_age_star)
      read(f, "age_star", age_star, n_stars, "Gyr"); // Star age [Gyr]
  }

  // Calculate derived quantities
  n_H.resize(n_cells);                       // Hydrogen number density [cm^-3]
  k_0.resize(n_cells);                       // Absorption coefficient of neutral hydrogen [1/cm]
  k_dust.resize(n_cells);                    // Absorption coefficient of dust [1/cm]
  a.resize(n_cells);                         // "Damping parameter"

  const double X_prim = 0.76;                // Primordial mass fraction of hydrogen
  const double gamma = 5. / 3.;              // Adiabatic index
  const double turb_coef = km * km / (gamma * kB); // Density scaling [K cm^3/g]

  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i) {
    if (avoid_cell(i))
      continue;
    const double f_gas = 1. - Z[i];          // Gas fraction not in metals
    if (f_gas < 0. || Z[i] < 0.)
      error("Metal mass fraction is out of range [0,1]: Z = " + to_string(Z[i]) + ", D = " + to_string(D[i]));
    if (read_hydrogen_fraction) {
      if (X[i] < 0. || X[i] > 1.)
        error("Hydrogen mass fraction is out of range [0,1]: " + to_string(X[i]));
      n_H[i] = X[i] * rho[i] * f_gas / mH;   // n_H = X rho (1-Z) / mH
    } else
      n_H[i] = X_prim * rho[i] * f_gas / mH; // n_H = X rho (1-Z) / mH

    double T_turb_local = T_turb;            // Minimum microturbulence value
    if (scaled_microturb && turb_coef * rho[i] > T_turb)
      T_turb_local = turb_coef * rho[i];     // Update T_turb based on scaling

    // Calculate absorption coefficient: k_0 = n_HI * sigma0
    const double _1_sqrt_T = 1. / sqrt(T[i] + T_turb_local);
    if (two_level_atom)
      k_0[i] = (n_lower[i] - g12 * n_upper[i]) * sigma0_sqrtT * _1_sqrt_T;
    else
      k_0[i] = x_HI[i] * n_H[i] * sigma0_sqrtT * _1_sqrt_T;

    // Dust absorption coefficient using:
    //  - constant albedo (accounted for in ray tracing)
    //  - constant dust-to-gas ratio (or metallicity scaling)
    //  - frequency-independent dust opacity (or cross section)
    //  - constant f_ion survival in H_II regions
    //  - thermal sputtering destroys dust above T_sputter
    const double f_dust = (T[i] > T_sputter) ? 0. : 1. + (f_ion - 1.) * x_HII[i];
    if (sigma_dust > 0.)
      k_dust[i] = f_dust * n_H[i] * sigma_dust * (Z[i] + D[i]); // Cross-section and metallicity
    else
      k_dust[i] = f_dust * kappa_dust * rho[i] * D[i]; // Opacity and dust-to-gas ratio

    // Calculate the "damping parameter" once: a = 0.5 * DnuL / DnuD
    a[i] = a_sqrtT * _1_sqrt_T;
  }

  if (module == "ionization") {
    rho_dust.resize(n_cells);                // Dust density [g/cm^3]
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (avoid_cell(i))
        continue;
      const double f_dust = (T[i] > T_sputter) ? 0. : 1. + (f_ion - 1.) * x_HII[i];
      rho_dust[i] = f_dust * rho[i] * D[i];  // Based on a dust-to-gas ratio
    }
  }

  if (electron_scattering) {
    const double r_0 = ee * ee / (me * c * c); // Electron radius = e^2 / m c^2
    const double sigma_T = 8. * M_PI * r_0 * r_0 / 3.; // Thomson cross-section [cm^2]
    k_e.resize(n_cells);                     // Absorption coefficient of electrons [1/cm]
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (avoid_cell(i))
        continue;
      k_e[i] = x_e[i] * n_H[i] * sigma_T;    // Optical depth: τ_e = ∫ n_e σ_T dl
    }
  }

  // Print the initial conditions data
  if (root) {
    string geometry_name;
    if (geometry == slab)
      geometry_name = "slab";
    else if (geometry == spherical)
      geometry_name = "spherical";
    else if (geometry == cartesian)
      geometry_name = "cartesian";
    else if (geometry == octree)
      geometry_name = "octree";
    else if (geometry == voronoi)
      geometry_name = "voronoi";
    cout << "\nSimulation info: [" << geometry_name << " geometry]";
    if (cosmological) {
      cout << "\n  redshift   = " << z;
      if (verbose)
        cout << "\n  Omega0     = " << Omega0 << " rho_crit_0"
             << "\n  OmegaB     = " << OmegaB << " rho_crit_0"
             << "\n  H_0        = " << 100. * h100 << " km/s/Mpc"
             << "\n  d_L        = " << d_L / Gpc << " Gpc";
    } else if (sim_time >= 0.)
      cout << "\n  time       = " << sim_time / Myr << " Myr";
    if (geometry == cartesian)
      cout << "\n  (nx,ny,nz) = (" << nx << ", " << ny << ", " << nz << ")";
    else if (geometry == octree)
      cout << "\n  n_leafs    = " << n_leafs;
    cout << "\n  n_cells    = " << n_cells << endl;
    if (verbose) {
      cout << "\nGrid data:" << endl;
      if (geometry == cartesian)
        cout << "  (dx,dy,dz) = " << Vec3(wx,wy,wz) / pc << " pc" << endl;
      else {
        print("  r         ", r, "kpc", kpc);
        print("  V         ", V, "pc^3", pc * pc * pc);
      }
      if (geometry == slab || geometry == spherical || geometry == octree)
        print("  w         ", w, "pc", pc);
      print("  n_H       ", n_H, "cm^-3");
      if (two_level_atom) {
        print("  n_lower   ", n_lower, "cm^-3");
        print("  n_upper   ", n_upper, "cm^-3");
      }
      print("  rho       ", rho, "g/cm^3");
      print("  T         ", T, "K");
      print("  v         ", v, "km/s", km);
      if (read_hydrogen_fraction)
        print("  X         ", X);
      print("  Z         ", Z);
      print("  D         ", D);
      print("  x_HI      ", x_HI);
      print("  x_HII     ", x_HII);
      if (read_molecular_fraction)
        print("  x_H2      ", x_H2);
      if (read_helium) {
        print("  x_HeI     ", x_HeI);
        print("  x_HeII    ", x_HeII);
      }
      print("  x_e       ", x_e);
      if (electron_scattering)
        print("  k_e       ", k_e, "cm^-1");

      cout << "\nmin/max cell data:"
           << "\n  (x,y,z)    = [" << bbox[0] / kpc << ","
           << "\n                " << bbox[1] / kpc << "] kpc" << endl;
      if (geometry == slab || geometry == spherical || geometry == octree)
        print_min_max("  w         ", w, "pc", pc);
      print_min_max("  (vx,vy,xz)", v, "km/s", km);
      if (geometry != cartesian)
        print_min_max("  V         ", V, "pc^3", pc*pc*pc);
      print_min_max("  n_H       ", n_H, "cm^-3");
      if (two_level_atom) {
        print_min_max("  n_lower   ", n_lower, "cm^-3");
        print_min_max("  n_upper   ", n_upper, "cm^-3");
      }
      print_min_max("  rho       ", rho, "g/cm^3");
      print_min_max("  T         ", T, "K");
      if (read_hydrogen_fraction)
        print_min_max("  X         ", X);
      print_min_max("  Z         ", Z);
      print_min_max("  D         ", D);
      print_min_max("  x_HI      ", x_HI);
      print_min_max("  x_HII     ", x_HII);
      if (read_molecular_fraction)
        print_min_max("  x_H2      ", x_H2);
      if (read_helium) {
        print_min_max("  x_HeI     ", x_HeI);
        print_min_max("  x_HeII    ", x_HeII);
      }
      print_min_max("  x_e       ", x_e);
      if (electron_scattering)
        print_min_max("  k_e       ", k_e, "1/cm");
    }

    if (star_based_emission) {
      cout << "\nStar data:"
           << "\n  n_stars    = " << n_stars << endl;
      if (verbose) {
        print("  r_star    ", r_star, "kpc", kpc);
        if (continuum)
          print("  L_cont    ", L_cont_star, "erg/s/angstrom");
        if (read_m_massive_star)
          print("  m_star(OB)", m_massive_star, "Msun");
        if (read_m_init_star)
          print("  m_star(0) ", m_init_star, "Msun");
        if (read_Z_star)
          print("  Z_star    ", Z_star);
        if (read_age_star)
          print("  age_star  ", age_star, "Gyr");
      }
    }
  }
  read_timer.stop();
}

/*! \brief Write general simulation information.
 *
 *  \param[in] f HDF5 File object.
 */
void Simulation::write_sim_info(const H5File& f) {
  // Information about the cosmology
  write(f, "cosmological", cosmological);    // Flag for cosmological simulations
  if (cosmological) {
    write(f, "Omega0", Omega0);              // Matter density [rho_crit_0]
    write(f, "OmegaB", OmegaB);              // Baryon density [rho_crit_0]
    write(f, "h100", h100);                  // Hubble constant [100 km/s/Mpc]
    write(f, "z", z);                        // Simulation redshift
    write(f, "d_L", d_L);                    // Luminosity distance [cm]
  } else if (sim_time >= 0.)
    write(f, "time", sim_time);              // Simulation time [s]
}

//! \brief Writes data and info to the specified hdf5 filename.
void Simulation::write_hdf5() {
  write_timer.start();
  // Open file and write all data
  H5File f(output_dir + "/" + output_base + snap_str + halo_str + "." + output_ext, H5F_ACC_TRUNC);

  // Write general simulation information
  write_sim_info(f);

  // Write general camera parameters
  if (have_cameras)
    write_camera_info(f);

  // Write module specific data
  write_module(f);

  write_timer.stop();
}
