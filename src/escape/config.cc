/********************
 * escape/config.cc *
 ********************

 * Post-processing escape module configuration.

*/

#include "../proto.h"
#include "Escape.h"
#include "../config.h" // Configuration functions

void setup_bc03_imf_solar_table(const string& dust_model); // Tabulated BC03 spectral properties

/* Escape module configuration. */
void Escape::module_config(YAML::Node& file, YAML::Node& node) {
  if (n_ranks > 1)
    error("Escape with n_ranks > 1 is not implemented yet.");

  // Information about the cameras
  camera_config(file, node);                 // General camera setup
  if (!have_cameras)
    root_error("The escape module requires at least one camera "
               "but none were specified in " + config_file);
  load("output_escape_fractions", output_escape_fractions); // Output camera escape fractions
  load("output_bin_escape_fractions", output_bin_escape_fractions); // Output bin escape fractions
  load("output_images", output_images);      // Output surface brightness images
  load("output_bin_images", output_bin_images); // Output bin SB images
  load("output_emission", output_emission);  // Output intrinsic emission without transport
  load("output_gas_columns_int", output_gas_columns_int); // Output camera gas column densities (intrinsic)
  load("output_gas_columns_esc", output_gas_columns_esc); // Output camera gas column densities (escaped)
  if (!(output_escape_fractions || output_bin_escape_fractions || output_images ||
        output_bin_images || output_gas_columns_int || output_gas_columns_esc))
    root_error("The escape module requires at least one camera output type.");

  // Setup the escape parameters
  spherical_escape_config(file, node);       // General spherical escape setup
  load("source_model", source_model);        // Model for the sources
  if (source_model == "GMC") {
    star_based_emission = true;              // Star-based emission
    read_m_massive_star = true;              // Requires (massive) stellar masses
    read_helium = true;                      // Requires helium states too
    n_bins = 3;                              // Ionizing photons [HI,HeI,HeII]
    global_sigma_HI = {2.99466e-18, 5.68179e-19, 7.97294e-20}; // HI ionizing cross-sections
    global_sigma_HeI = {0., 3.71495e-18, 5.70978e-19}; // HeI ionizing cross-sections
    global_sigma_HeII = {0., 0., 1.06525e-18}; // HeII ionizing cross-sections
    global_mean_energy = {18.8567*eV, 35.0815*eV, 64.9768*eV}; // Mean energy of each bin [erg]
  } else if (source_model == "MRT") {
    star_based_emission = true;              // Star-based emission
    read_m_init_star = true;                 // Requires initial stellar masses
    read_Z_star = true;                      // Requires stellar metallicities
    read_age_star = true;                    // Requires stellar ages
    read_helium = true;                      // Requires helium states too
    n_bins = 3;                              // Ionizing photons [HI,HeI,HeII]
    global_sigma_HI = {3.3718e-18, 7.89957e-19, 1.09584e-19}; // HI ionizing cross-sections
    global_sigma_HeI = {0., 5.10279e-18, 7.76916e-19}; // HeI ionizing cross-sections
    global_sigma_HeII = {0., 0., 1.42413e-18}; // HeII ionizing cross-sections
    global_mean_energy = {18.0058*eV, 29.8868*eV, 56.8456*eV}; // Mean energy of each bin [erg]
  } else if (source_model == "BC03-IMF-SOLAR") {
    star_based_emission = true;              // Star-based emission
    read_m_init_star = true;                 // Requires initial stellar masses
    read_age_star = true;                    // Requires stellar ages
    read_helium = true;                      // Requires helium states too
    n_bins = 3;                              // Ionizing photons [HI,HeI,HeII]
    if (source_model == "BC03-IMF-SOLAR")
      setup_bc03_imf_solar_table("MW");      // Tabulated BC03 spectral properties
  } else
    error("Unrecognized value for source_model: " + source_model);
}
