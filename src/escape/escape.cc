/********************
 * escape/escape.cc *
 ********************

 * Driver: Assign rays for escape, etc.

*/

#include "../proto.h"
#include "Escape.h"
#include "../timing.h" // Timing functionality

static vector<vector<double>> all_L_int;     // Intrinsic bin luminosities
static vector<vector<double>> all_sigma_HI;  // Bin cross-sections for HI
static vector<vector<double>> all_sigma_HeI; // Bin cross-sections for HeI
static vector<vector<double>> all_sigma_HeII; // Bin cross-sections for HeII
static vector<vector<double>> all_k_abs;     // Bin absorption coefficients
static vector<vector<double>> all_tau_escs;  // Optical depth to escape
static void (*setup_spectra)(const int);     // Set the bin spectral properties

extern Timer escape_timer; // Clock timing
double spherical_escape_distance(Vec3 point, Vec3 direction); // Distance to escape the bounding sphere
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell

/* Set the spectral properties of each bin based on the GMC model. */
static void setup_spectra_GMC(const int star) {
  auto& L_int = all_L_int[thread];           // Reference to the local L_int
  const double logM = log10(m_massive_star[star]);
  L_int[0] = pow(10., 29.1169841759 + 8.09061*logM - 1.82165*logM*logM); // HI [erg/s]
  L_int[1] = pow(10., 28.3419526386 + 9.45*logM - 2.14563*logM*logM);    // HeI [erg/s]
  L_int[2] = pow(10., 24.9894768034 + 13.0864*logM - 3.00213*logM*logM); // HeII [erg/s]
}

/* Set the spectral properties of each bin based on the BC03-IMF-SOLAR model. */
static void setup_spectra_imf_solar(const int star) {
  auto& L_int = all_L_int[thread];           // Reference to the local L_int
  auto& sigma_HI = all_sigma_HI[thread];     // Reference to the local sigma_HI
  auto& sigma_HeI = all_sigma_HeI[thread];   // Reference to the local sigma_HeI
  auto& sigma_HeII = all_sigma_HeII[thread]; // Reference to the local sigma_HeII
  const double mass = m_init_star[star];     // Initial mass of the star [Msun]
  const double age = age_star[star];         // Age of the star [Gyr]
  int edge = -1;
  if (age <= 1.00000001e-3)                  // Age minimum = 1 Myr
    edge = 0;
  else if (age >= 99.999999)                 // Age maximum = 100 Gyr
    edge = 50;
  else {
    const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
    const double f_age = floor(d_age);       // Floor coordinate
    const double frac_R = d_age - f_age;     // Interpolation fraction (right)
    const double frac_L = 1. - frac_R;       // Interpolation fraction (left)
    const int i_L = f_age;                   // Table age index (left)
    const int i_R = i_L + 1;                 // Table age index (right)
    const double mass_Ndot[3] = {            // Interpolation normalization
      1. / (ion_age.Ndot_HI[i_L]*frac_L + ion_age.Ndot_HI[i_R]*frac_R),
      1. / (ion_age.Ndot_HeI[i_L]*frac_L + ion_age.Ndot_HeI[i_R]*frac_R),
      1. / (ion_age.Ndot_HeII[i_L]*frac_L + ion_age.Ndot_HeII[i_R]*frac_R)};
    L_int[0] = mass * (ion_age.L_HI[i_L]*frac_L + ion_age.L_HI[i_R]*frac_R); // HI [erg/s]
    L_int[1] = mass * (ion_age.L_HeI[i_L]*frac_L + ion_age.L_HeI[i_R]*frac_R); // HeI [erg/s]
    L_int[2] = mass * (ion_age.L_HeII[i_L]*frac_L + ion_age.L_HeII[i_R]*frac_R); // HeII [erg/s]
    sigma_HI[0] = mass_Ndot[0] * (ion_age.sigma_HI_1[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.sigma_HI_1[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Cross-sections [cm^2]
    sigma_HI[1] = mass_Ndot[1] * (ion_age.sigma_HI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.sigma_HI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
    sigma_HI[2] = mass_Ndot[2] * (ion_age.sigma_HI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.sigma_HI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
    sigma_HeI[1] = mass_Ndot[1] * (ion_age.sigma_HeI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.sigma_HeI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
    sigma_HeI[2] = mass_Ndot[2] * (ion_age.sigma_HeI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.sigma_HeI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
    sigma_HeII[2] = mass_Ndot[2] * (ion_age.sigma_HeII_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.sigma_HeII_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
    return;                                  // Finished setup
  }

  // Age was out of range so use the bounding value
  L_int[0] = mass * ion_age.L_HI[edge];      // HI [erg/s]
  L_int[1] = mass * ion_age.L_HeI[edge];     // HeI [erg/s]
  L_int[2] = mass * ion_age.L_HeII[edge];    // HeII [erg/s]
  sigma_HI[0] = ion_age.sigma_HI_1[edge];    // Cross-sections [cm^2]
  sigma_HI[1] = ion_age.sigma_HI_2[edge];
  sigma_HI[2] = ion_age.sigma_HI_3[edge];
  sigma_HeI[1] = ion_age.sigma_HeI_2[edge];
  sigma_HeI[2] = ion_age.sigma_HeI_3[edge];
  sigma_HeII[2] = ion_age.sigma_HeII_3[edge];
}

/* Calculate escape for a single line of sight. */
void Escape::ray_trace(const int star, const int camera) {
  auto& tau_escs = all_tau_escs[thread];     // Reference to the local tau_escs
  auto& k_abs = all_k_abs[thread];           // Reference to the local k_abs
  const auto& sigma_HI = all_sigma_HI[thread]; // Reference to the local sigma_HI
  const auto& sigma_HeI = all_sigma_HeI[thread]; // Reference to the local sigma_HeI
  const auto& sigma_HeII = all_sigma_HeII[thread]; // Reference to the local sigma_HeII
  const auto& L_int = all_L_int[thread];     // Reference to the local L_int
  int cell = cell_of_star[star];             // Cell index
  int next_cell;                             // Next cell index
  double l, l_exit = 0.;                     // Path lengths
  double n_HI, n_HeI, n_HeII;                // Cell number densities
  Vec3 point = r_star[star];                 // Emission position
  Vec3 k_cam = camera_directions[camera];    // Camera direction
  if (spherical_escape)                      // Initialize escape distance
    l_exit = spherical_escape_distance(point, k_cam);
  for (int bin = 0; bin < n_bins; ++bin)
    tau_escs[bin] = 0.;                      // Reset optical depth to escape
  double rho_dl = 0.;                        // Gas column density [g/cm^2]
  setup_spectra(star);                       // Set the bin spectral properties

  // Bin the photon's position - Only add to bins in the specified range
  const Vec3 r_cam = point - camera_center;  // Position relative to camera center
  const double x_cam = inverse_pixel_width * (r_cam.dot(camera_xaxes[camera]) + image_radius);
  const double y_cam = inverse_pixel_width * (r_cam.dot(camera_yaxes[camera]) + image_radius);
  const bool in_image_range = (x_cam >= 0. && x_cam < double(n_pixels) &&
                               y_cam >= 0. && y_cam < double(n_pixels));
  const int ix = floor(x_cam);
  const int iy = floor(y_cam);

  // Add intrinsic luminosity contributions
  if (output_emission && in_image_range) {
    for (int bin = 0; bin < n_bins; ++bin) {
      const double weight = L_int[bin];
      if (output_images)
        #pragma omp atomic
        images_int[camera](ix, iy) += weight; // Intrinsic SB images
      if (output_bin_images)
        #pragma omp atomic
        bin_images_int[camera](ix, iy, bin) += weight; // Bin SB images
    }
  }

  while (true) {                             // Ray trace until escape
    // Compute the maximum distance the photon can travel in the cell
    tie(l, next_cell) = face_distance(point, k_cam, cell);

    // Set the absorption coefficients of each bin
    n_HI = x_HI[cell] * n_H[cell];
    n_HeI = x_HeI[cell] * n_H[cell];
    n_HeII = x_HeII[cell] * n_H[cell];
    for (int bin = 0; bin < n_bins; ++bin)
      k_abs[bin] = n_HI * sigma_HI[bin]      // HI ionization contribution
                 + n_HeI * sigma_HeI[bin]    // HeI ionization contribution
                 + n_HeII * sigma_HeII[bin]; // HeII ionization contribution

    // Check for spherical escape
    if (spherical_escape) {
      if (l_exit <= l) {                     // Exit sphere before cell
        for (int bin = 0; bin < n_bins; ++bin)
          tau_escs[bin] += k_abs[bin] * l_exit; // Cumulative LOS optical depth
        rho_dl += rho[cell] * l_exit;        // Gas column density [g/cm^2]
        break;                               // Valid tau_esc completion
      }
      l_exit -= l;                           // Remaining distance to exit
    }

    // Cumulative LOS optical depth
    for (int bin = 0; bin < n_bins; ++bin)
      tau_escs[bin] += k_abs[bin] * l;       // Cumulative LOS optical depth
    rho_dl += rho[cell] * l;                 // Gas column density [g/cm^2]

    // Calculate the new position of the photon
    point += k_cam * l;

#ifdef SPHERICAL
    if (next_cell == INSIDE)                 // Check if the photon is trapped
      break;
#endif
    if (next_cell == OUTSIDE)                // Check if the photon escapes
      break;
    cell = next_cell;                        // Update the cell index
  }

  // Add escaped luminosity contributions
  double L_weight_int = 0.;                  // Total luminosity (intrinsic)
  double L_weight_esc = 0.;                  // Total luminosity (escaped)
  for (int bin = 0; bin < n_bins; ++bin) {
    // Luminosity is reduced by exp(-tau)
    const double weight = L_int[bin] * exp(-tau_escs[bin]);
    L_weight_int += L_int[bin];              // Sum of all bins (intrinsic)
    L_weight_esc += weight;                  // Sum of all bins (escaped)

    if (output_bin_escape_fractions)
      #pragma omp atomic
      bin_f_escs[camera][bin] += weight;     // Escaped bin luminosities

    if (output_bin_images && in_image_range)
      #pragma omp atomic
      bin_images[camera](ix, iy, bin) += weight; // Escaped bin SB images
  }
  if (output_escape_fractions)
    #pragma omp atomic
    f_escs[camera] += L_weight_esc;          // Escaped luminosity
  if (output_images && in_image_range)
    #pragma omp atomic
    images[camera](ix, iy) += L_weight_esc;  // Escaped SB images
  if (output_gas_columns_int)
    #pragma omp atomic
    gas_columns_int[camera] += L_weight_int * rho_dl; // Gas column density [g/cm^2]
  if (output_gas_columns_esc)
    #pragma omp atomic
    gas_columns_esc[camera] += L_weight_esc * rho_dl; // Gas column density [g/cm^2]
}

/* Ray trace for escape calculations. */
void Escape::calculate_escape() {
  // Allocate local arrays (n_threads, n_bins)
  all_L_int.resize(n_threads);               // Each thread has its own L_int
  all_sigma_HI.resize(n_threads);            // Each thread has its own sigma_HI
  all_sigma_HeI.resize(n_threads);           // Each thread has its own sigma_HeI
  all_sigma_HeII.resize(n_threads);          // Each thread has its own sigma_HeII
  all_k_abs.resize(n_threads);               // Each thread has its own k_abs
  all_tau_escs.resize(n_threads);            // Each thread has its own tau_escs
  #pragma omp parallel
  {
    all_L_int[thread].resize(n_bins);        // Intrinsic bin luminosities
    all_sigma_HI[thread].resize(n_bins);     // Bin cross-sections for HI
    all_sigma_HeI[thread].resize(n_bins);    // Bin cross-sections for HeI
    all_sigma_HeII[thread].resize(n_bins);   // Bin cross-sections for HeII
    all_k_abs[thread].resize(n_bins);        // Bin absorption coefficients
    all_tau_escs[thread].resize(n_bins);     // Optical depth to escape
  }

  // Assign luminosity and opacity functions
  if (source_model == "GMC") {               // RT is based on the GMC model
    setup_spectra = setup_spectra_GMC;
  } else if (source_model == "MRT") {        // RT is based on the MRT model
    error("MRT not implemented.");
  } else if (source_model == "BC03-IMF-SOLAR") {
    setup_spectra = setup_spectra_imf_solar;
  }

  // Models with global cross-sections can be initialized once
  if (source_model == "GMC" || source_model == "MRT") {
    #pragma omp parallel
    {
      auto& sigma_HI = all_sigma_HI[thread]; // Reference to the local sigma_HI
      auto& sigma_HeI = all_sigma_HeI[thread]; // Reference to the local sigma_HeI
      auto& sigma_HeII = all_sigma_HeII[thread]; // Reference to the local sigma_HeII
      for (int bin = 0; bin < n_bins; ++bin) {
        sigma_HI[bin] = global_sigma_HI[bin];
        sigma_HeI[bin] = global_sigma_HeI[bin];
        sigma_HeII[bin] = global_sigma_HeII[bin];
      }
    }
  } else if (source_model == "BC03-IMF-SOLAR") {
    #pragma omp parallel
    {
      all_sigma_HeI[thread][0] = 0.;         // These indices are always zero
      all_sigma_HeII[thread][0] = 0.;
      all_sigma_HeII[thread][1] = 0.;
    }
  }

  const size_t n_rays = n_stars * n_cameras; // Number of ray calculations
  const size_t interval = (n_rays < 200) ? 1 : n_rays / 100; // Interval between updates
  size_t n_finished = 0;                     // Progress for printing
  if (root)
    cout << "\n RT progress:   0%\b\b\b\b" << std::flush;
  #pragma omp parallel for schedule(dynamic)
  for (size_t i = 0; i < n_rays; ++i) {
    // Ray tracing for each star and camera
    const int star = i / n_cameras;          // (star, camera) indices
    const int camera = i % n_cameras;
    ray_trace(star, camera);                 // Ray tracing calculations

    // Print completed progress
    if (root) {
      size_t i_finished;                     // Progress counter
      #pragma omp atomic capture
      i_finished = ++n_finished;             // Update finished counter
      if (i_finished % interval == 0)
        cout << std::setw(3) << (100 * i_finished) / n_rays << "%\b\b\b\b" << std::flush;
    }
  }
  if (root)
    cout << "100%" << endl;
}

/* Convert observables to the correct units. */
void Escape::correct_units() {
  cout << "\n RT: Converting to physical units ..." << endl;

  // Escape fractions [fraction]
  if (output_escape_fractions)
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera) {
      f_escs[camera] /= L_tot;               // Normalize to total luminosity
      if (output_bin_escape_fractions)
        for (int bin = 0; bin < n_bins; ++bin)
          bin_f_escs[camera][bin] /= bin_L_tot[bin]; // Normalize each bin
    }

  // Gas column density [g/cm^2]
  if (output_gas_columns_int)
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera)
      gas_columns_int[camera] /= L_tot;      // Normalize to total luminosity (intrinsic)
  if (output_gas_columns_esc)
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera)
      if (f_escs[camera] > 0.)
        gas_columns_esc[camera] /= L_tot * f_escs[camera]; // Normalize to total luminosity (escaped)

  // Surface brightness images [erg/s/cm^2]
  const double to_image_units = 1. / pixel_area; // L / dA
  if (output_images)
    rescale(images, to_image_units);         // Apply image correction

  // Bin SB images [erg/s/cm^2]
  if (output_bin_images)
    rescale(bin_images, to_image_units);     // Apply bin image correction

  // Intrinsic cameras
  if (output_emission) {
    // Surface brightness images [erg/s/cm^2]
    if (output_images)
      rescale(images_int, to_image_units);   // Apply image correction

    // Bin SB images [erg/s/cm^2]
    if (output_bin_images)
      rescale(bin_images_int, to_image_units); // Apply bin image correction
  }
}

/* Driver for escape calculations. */
void Escape::run() {
  escape_timer.start();

  // Perform ray tracing escape calculations
  calculate_escape();

  // Collect the results from different processors
  // if (n_ranks > 1)
  //   reduce_escape();

  if (root) {
    // Convert observables to the correct units
    correct_units();
    print("\n f_esc (LOS)", f_escs);
    if (output_gas_columns_int)
      print("\n N_gas (int)", gas_columns_int, "g/cm^2");
    if (output_gas_columns_esc)
      print("\n N_gas (esc)", gas_columns_esc, "g/cm^2");
  }
  escape_timer.stop();
}
