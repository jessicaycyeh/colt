/*******************
 * escape/Escape.h *
 *******************

 * Module declarations.

*/

#ifndef ESCAPE_INCLUDED
#define ESCAPE_INCLUDED

#include "../Simulation.h" // Base simulation class

/* Ray-based escape module. */
class Escape : public Simulation {
public:
  void run() override;                       // Module calculations

protected:
  void module_config(YAML::Node& file, YAML::Node& node) override;
  void setup() override;                     // Setup simulation data
  void print_info() override;                // Print simulation information
  void write_module(const H5::H5File& f) override; // Module output

private:
  // Ionizing photon models
  string source_model = "GMC";               // Escape source model
  vector<double> global_sigma_HI;            // HI ionization cross-section [cm^2]
  vector<double> global_sigma_HeI;           // HeI ionization cross-section [cm^2]
  vector<double> global_sigma_HeII;          // HeII ionization cross-section [cm^2]
  vector<double> global_mean_energy;         // Mean energy of each frequency bin [erg]
  double L_tot = 0.;                         // Total luminosity [erg/s]
  double Ndot_tot = 0.;                      // Total emission rate [photons/s]
  vector<double> bin_L_tot;                  // Total bin luminosities [erg/s]
  vector<double> bin_Ndot_tot;               // Total bin rates [photons/s]
  vector<int> cell_of_star;                  // Cell index of each active star

  // Information about secondary cameras
  bool output_emission = false;              // Output intrinsic emission without transport
  bool output_bin_escape_fractions = true;   // Output bin escape fractions
  vector<vector<double>> bin_f_escs;         // Bin escape fractions [fraction]
  vector<Image<double>> images_int;          // Intrinsic surface brightness images [erg/s/cm^2]
  bool output_bin_images = false;            // Output bin surface brightness images
  vector<Cube<double>> bin_images;           // Escaped bin SB images [erg/s/cm^2]
  vector<Cube<double>> bin_images_int;       // Intrinsic bin SB images [erg/s/cm^2]
  bool output_gas_columns_int = true;        // Output gas column densities (intrinsic)
  vector<double> gas_columns_int;            // Gas column densities [g/cm^2]
  bool output_gas_columns_esc = true;        // Output gas column densities (escaped)
  vector<double> gas_columns_esc;            // Gas column densities [g/cm^2]

  void ray_trace(const int star, const int camera); // Single ray
  void calculate_escape();                   // All stars and camera directions
  void correct_units();                      // Convert to observed units
};

#endif // ESCAPE_INCLUDED
