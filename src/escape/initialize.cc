/************************
 * escape/initialize.cc *
 ************************

 * Initialization: Simulation setup, allocation, opening messages, etc.

*/

#include "../proto.h"
#include "Escape.h"

bool avoid_cell(const int cell); // Avoid calculations for certain cells
int find_cell(const Vec3 point, int cell); // Cell index of a point

/* Setup everything for the escape module. */
void Escape::setup() {
  // Setup spherical escape
  if (spherical_escape)
    setup_spherical_escape();                // General spherical escape setup

  // Setup cameras
  if (have_cameras) {
    setup_cameras();                         // General camera setup

    if (root && output_bin_images)
      cout << "\nSpectral Bin Images: "
           << double(n_cameras) * double(n_pixels) * double(n_pixels) * double(n_bins) * 8. / 1024. / 1024. / 1024.
           << " GB  (" << n_cameras << ", " << n_pixels << ", " << n_pixels << ", " << n_bins << ")" << endl;

    // Reset camera data and initialize with zeros
    if (output_escape_fractions)
      f_escs = vector<double>(n_cameras);    // Escape fractions
    if (output_bin_escape_fractions)
      bin_f_escs = vector<vector<double>>(n_cameras, vector<double>(n_bins));
    if (output_images)
      images = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
    if (output_bin_images)
      bin_images = vector<Cube<double>>(n_cameras, Cube<double>(n_pixels, n_pixels, n_bins));
    if (output_gas_columns_int)
      gas_columns_int = vector<double>(n_cameras); // Gas column densities [g/cm^2]
    if (output_gas_columns_esc)
      gas_columns_esc = vector<double>(n_cameras); // Gas column densities [g/cm^2]

    // Initialize intrinsic cameras
    if (output_emission) {
      if (output_images)
        images_int = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
      if (output_bin_images)
        bin_images_int = vector<Cube<double>>(n_cameras, Cube<double>(n_pixels, n_pixels, n_bins));
    }
  }

  // Setup star-based emission sources
  if (star_based_emission) {
    vector<int> active_flags(n_stars);       // Set active star flags
    cell_of_star.resize(n_stars);            // Cell index of each active star
    if (source_model == "GMC") {
      // Calculate the star luminosities
      double L_HI = 0.;                      // Total ionizing luminosity [erg/s]
      double L_HeI = 0.;
      double L_HeII = 0.;
      #pragma omp parallel for reduction(+:L_HI) reduction(+:L_HeI) reduction(+:L_HeII)
      for (int i = 0; i < n_stars; ++i) {
        // Avoid stars that are outside the mass range
        if (m_massive_star[i] <= 0.)
          continue;
        if (m_massive_star[i] < 12. || m_massive_star[i] > 100.)
          error("GMC model expects stars within the range 12-100 Msun. ["
                + to_string(m_massive_star[i]) + " Msun]");
        // Avoid stars that are outside the emission radius
        if (spherical_escape &&
           (r_star[i] - escape_center).dot() >= emission_radius2)
          continue;
        cell_of_star[i] = find_cell(r_star[i], 0); // Search for the cell
        if (avoid_cell(cell_of_star[i]))
          continue;                          // Ignore certain cells
        active_flags[i] = 1;                 // Keep track of active stars
        const double logM = log10(m_massive_star[i]);
        L_HI += pow(10., 29.1169841759 + 8.09061*logM - 1.82165*logM*logM); // [erg/s]
        L_HeI += pow(10., 28.3419526386 + 9.45*logM - 2.14563*logM*logM); // [erg/s]
        L_HeII += pow(10., 24.9894768034 + 13.0864*logM - 3.00213*logM*logM); // [erg/s]
      }
      bin_L_tot = {L_HI, L_HeI, L_HeII};     // Total bin luminosities [erg/s]
      bin_Ndot_tot = {bin_L_tot[0] / global_mean_energy[0], // Total rate [photons/s]
                      bin_L_tot[1] / global_mean_energy[1],
                      bin_L_tot[2] / global_mean_energy[2]};
      L_tot = bin_L_tot[0] + bin_L_tot[1] + bin_L_tot[2]; // Total luminosity [erg/s]
      Ndot_tot = bin_Ndot_tot[0] + bin_Ndot_tot[1] + bin_Ndot_tot[2]; // Total rate [photons/s]
    } else if (source_model == "MRT") {
      error("MRT source model is not implemented.");
    } else if (source_model == "BC03-IMF-SOLAR") {
      // Calculate the star luminosities
      double L_HI = 0.;                      // Total ionizing luminosity [erg/s]
      double L_HeI = 0.;
      double L_HeII = 0.;
      double Ndot_HI = 0.;                   // Total ionizing rate [photons/s]
      double Ndot_HeI = 0.;
      double Ndot_HeII = 0.;
      double sigma_Ndot_HI_1 = 0.;           // Cross-section rates [cm^2 photons/s]
      double sigma_Ndot_HI_2 = 0.;
      double sigma_Ndot_HI_3 = 0.;
      double sigma_Ndot_HeI_2 = 0.;
      double sigma_Ndot_HeI_3 = 0.;
      double sigma_Ndot_HeII_3 = 0.;
      #pragma omp parallel for reduction(+:L_HI) reduction(+:L_HeI) reduction(+:L_HeII) \
                               reduction(+:Ndot_HI) reduction(+:Ndot_HeI) reduction(+:Ndot_HeII) \
                               reduction(+:sigma_Ndot_HI_1) reduction(+:sigma_Ndot_HI_2) \
                               reduction(+:sigma_Ndot_HI_3) reduction(+:sigma_Ndot_HeI_2) \
                               reduction(+:sigma_Ndot_HeI_3) reduction(+:sigma_Ndot_HeII_3)
      for (int i = 0; i < n_stars; ++i) {
        // Avoid stars that are outside the emission radius
        if (spherical_escape &&
           (r_star[i] - escape_center).dot() >= emission_radius2)
          continue;
        cell_of_star[i] = find_cell(r_star[i], 0); // Search for the cell
        if (avoid_cell(cell_of_star[i]))
          continue;                          // Ignore certain cells
        active_flags[i] = 1;                 // Keep track of active stars
        const double mass = m_init_star[i];  // Initial mass of the star [Msun]
        const double age = age_star[i];      // Age of the star [Gyr]
        int edge = -1;
        if (age <= 1.00000001e-3)            // Age minimum = 1 Myr
          edge = 0;
        else if (age >= 99.999999)           // Age maximum = 100 Gyr
          edge = 50;
        else {
          const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
          const double f_age = floor(d_age); // Floor coordinate
          const double frac_R = d_age - f_age; // Interpolation fraction (right)
          const double frac_L = 1. - frac_R; // Interpolation fraction (left)
          const int i_L = f_age;             // Table age index (left)
          const int i_R = i_L + 1;           // Table age index (right)
          L_HI += mass * (ion_age.L_HI[i_L]*frac_L + ion_age.L_HI[i_R]*frac_R); // HI [erg/s]
          L_HeI += mass * (ion_age.L_HeI[i_L]*frac_L + ion_age.L_HeI[i_R]*frac_R); // HeI [erg/s]
          L_HeII += mass * (ion_age.L_HeII[i_L]*frac_L + ion_age.L_HeII[i_R]*frac_R); // HeII [erg/s]
          Ndot_HI += mass * (ion_age.Ndot_HI[i_L]*frac_L + ion_age.Ndot_HI[i_R]*frac_R); // Rates [photons/s]
          Ndot_HeI += mass * (ion_age.Ndot_HeI[i_L]*frac_L + ion_age.Ndot_HeI[i_R]*frac_R);
          Ndot_HeII += mass * (ion_age.Ndot_HeII[i_L]*frac_L + ion_age.Ndot_HeII[i_R]*frac_R);
          // Cross-section rates [cm^2 photons/s]
          sigma_Ndot_HI_1 += mass * (ion_age.sigma_HI_1[i_L]*ion_age.Ndot_HI[i_L]*frac_L
                                   + ion_age.sigma_HI_1[i_R]*ion_age.Ndot_HI[i_R]*frac_R);
          sigma_Ndot_HI_2 += mass * (ion_age.sigma_HI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L
                                   + ion_age.sigma_HI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
          sigma_Ndot_HI_3 += mass * (ion_age.sigma_HI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                   + ion_age.sigma_HI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
          sigma_Ndot_HeI_2 += mass * (ion_age.sigma_HeI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L
                                    + ion_age.sigma_HeI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
          sigma_Ndot_HeI_3 += mass * (ion_age.sigma_HeI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                    + ion_age.sigma_HeI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
          sigma_Ndot_HeII_3 += mass * (ion_age.sigma_HeII_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                     + ion_age.sigma_HeII_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
        }

        // Age was out of range so use the bounding value
        if (edge != -1) {
          L_HI += mass * ion_age.L_HI[edge]; // HI [erg/s]
          L_HeI += mass * ion_age.L_HeI[edge]; // HeI [erg/s]
          L_HeII += mass * ion_age.L_HeII[edge]; // HeII [erg/s]
          const double local_Ndot_HI = mass * ion_age.Ndot_HI[edge];
          const double local_Ndot_HeI = mass * ion_age.Ndot_HeI[edge];
          const double local_Ndot_HeII = mass * ion_age.Ndot_HeII[edge];
          Ndot_HI += local_Ndot_HI;          // Rates [photons/s]
          Ndot_HeI += local_Ndot_HeI;
          Ndot_HeII += local_Ndot_HeII;
          sigma_Ndot_HI_1 += local_Ndot_HI * ion_age.sigma_HI_1[edge]; // Cross-section rates [cm^2 photons/s]
          sigma_Ndot_HI_2 += local_Ndot_HeI * ion_age.sigma_HI_2[edge];
          sigma_Ndot_HI_3 += local_Ndot_HeII * ion_age.sigma_HI_3[edge];
          sigma_Ndot_HeI_2 += local_Ndot_HeI * ion_age.sigma_HeI_2[edge];
          sigma_Ndot_HeI_3 += local_Ndot_HeII * ion_age.sigma_HeI_3[edge];
          sigma_Ndot_HeII_3 += local_Ndot_HeII * ion_age.sigma_HeII_3[edge];
        }
      }

      bin_L_tot = {L_HI, L_HeI, L_HeII};     // Total bin luminosities [erg/s]
      bin_Ndot_tot = {Ndot_HI, Ndot_HeI, Ndot_HeII}; // Total bin rates [photons/s]
      // Global weighted cross-sections [cm^2] and mean energies [erg]
      global_sigma_HI = {sigma_Ndot_HI_1 / Ndot_HI, sigma_Ndot_HI_2 / Ndot_HeI, sigma_Ndot_HI_3 / Ndot_HeII};
      global_sigma_HeI = {0., sigma_Ndot_HeI_2 / Ndot_HeI, sigma_Ndot_HeI_3 / Ndot_HeII};
      global_sigma_HeII = {0., 0., sigma_Ndot_HeII_3 / Ndot_HeII};
      global_mean_energy = {L_HI / Ndot_HI, L_HeI / Ndot_HeI, L_HeII / Ndot_HeII};
      L_tot = bin_L_tot[0] + bin_L_tot[1] + bin_L_tot[2]; // Total luminosity [erg/s]
      Ndot_tot = bin_Ndot_tot[0] + bin_Ndot_tot[1] + bin_Ndot_tot[2]; // Total rate [photons/s]
    }

    // Filter star data
    int n_active = 0;                        // Number of actively emitting stars
    for (int i = 0; i < n_stars; ++i)
      if (active_flags[i] && i > n_active) {
        r_star[n_active] = r_star[i];        // Move active stars to the front
        if (read_m_massive_star)             // Massive stellar masses
          m_massive_star[n_active] = m_massive_star[i];
        if (read_m_init_star)                // Initial stellar masses
          m_init_star[n_active] = m_init_star[i];
        if (read_Z_star)                     // Stellar metallicities
          Z_star[n_active] = Z_star[i];
        if (read_age_star)                   // Stellar ages
          age_star[n_active] = age_star[i];
        cell_of_star[n_active] = cell_of_star[i]; // Cell index of each star
        n_active++;
      }
    if (n_active <= 0)
      error("There are no active stars in this simulation");
    n_stars = n_active;                      // Restrict access to active stars
    r_star.resize(n_active);
    if (read_m_massive_star)
      m_massive_star.resize(n_active);
    if (read_m_init_star)
      m_init_star.resize(n_active);
    if (read_Z_star)
      Z_star.resize(n_active);
    if (read_age_star)
      age_star.resize(n_active);
    cell_of_star.resize(n_active);           // Cell index of each active star
  }
}
