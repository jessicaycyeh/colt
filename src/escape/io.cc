/****************
 * escape/io.cc *
 ****************

 * All I/O operations related to the escape module.

*/

#include "../proto.h"
#include "Escape.h"
#include "../io_hdf5.h" // HDF5 read/write functions

/* Function to print additional escape information. */
void Escape::print_info() {
  // Print field list
  cout << "\nEscaping RT information: [" << source_model << "]"
       << "\n  n_bins     = " << n_bins;
  if (spherical_escape)
    cout << "\n  r_escape   = " << escape_radius / kpc << " kpc"
         << "\n  r_emission = " << emission_radius / kpc << " kpc"
         << "\n  esc center = " << escape_center / kpc << " kpc";
  cout << endl;

  // Ionizing photon models
  if (source_model == "GMC" || source_model == "MRT" || source_model == "BC03-IMF-SOLAR") {
    print("  sigma_HI  ", global_sigma_HI, "cm^2");
    print("  sigma_HeI ", global_sigma_HeI, "cm^2");
    print("  sigma_HeII", global_sigma_HeII, "cm^2");
    print("  avg energy", global_mean_energy, "eV", eV);
  }

  // Source information
  cout << "\nStrength of the source: [n_active_stars = " << n_stars << "]"
       << "\n  Luminosity = " << L_tot << " erg/s"
       << "\n             = " << L_tot / Lsun << " Lsun"
       << "\n  Prod. Rate = " << Ndot_tot << " photons/s" << endl;
  print("  Lum (bins)", bin_L_tot, "erg/s");
  print("            ", bin_L_tot, "Lsun", Lsun);
  print("  Prod rates", bin_Ndot_tot, "photons/s");

  print_cameras();                           // General camera parameters
}

/* Writes escape data and info to the specified hdf5 file. */
void Escape::write_module(const H5File& f) {
  // Simulation information
  write(f, "Ndot_tot", Ndot_tot);            // Total emission rate [photons/s]
  write(f, "L_tot", L_tot);                  // Total luminosity [erg/s]
  write(f, "n_bins", n_bins);                // Number of frequency bins
  write(f, "bin_Ndot_tot", bin_Ndot_tot, "photons/s"); // Bin rates [photons/s]
  write(f, "bin_L_tot", bin_L_tot, "erg/s"); // Bin luminosities [erg/s]
  if (source_model == "GMC" || source_model == "MRT" || source_model == "BC03-IMF-SOLAR") {
    write(f, "sigma_HI", global_sigma_HI, "cm^2"); // Ionizing cross-sections
    write(f, "sigma_HeI", global_sigma_HeI, "cm^2");
    write(f, "sigma_HeII", global_sigma_HeII, "cm^2");
    write(f, "mean_energy", global_mean_energy, "erg");
  }

  // Information about emission sources
  {
    Group g = f.createGroup("/sources");
    if (star_based_emission)
      write(g, "n_stars", n_stars);          // Number of stars

    write(g, "spherical_escape", spherical_escape); // Photons escape from a sphere
    if (spherical_escape) {
      write(g, "escape_radius", escape_radius); // Radius for spherical escape [cm]
      write(g, "emission_radius", emission_radius); // Radius for spherical emission [cm]
      write(g, "escape_center", escape_center, "cm"); // Center of the escape region [cm]
    }
  }

  // Additional camera info
  if (have_cameras) {
    if (output_escape_fractions)
      write(f, "f_escs", f_escs);            // Escape fractions [fraction]
    if (output_bin_escape_fractions)
      write(f, "bin_f_escs", bin_f_escs);    // Bin escape fractions [fraction]
    if (output_images)
      write(f, "images", images, "erg/s/cm^2"); // Surface brightness images [erg/s/cm^2]
    if (output_bin_images)
      write(f, "bin_images", bin_images, "erg/s/cm^2"); // Bin SB images [erg/s/cm^2]
    if (output_gas_columns_int)
      write(f, "gas_columns_int", gas_columns_int, "g/cm^2"); // Gas column densities [g/cm^2]
    if (output_gas_columns_esc)
      write(f, "gas_columns_esc", gas_columns_esc, "g/cm^2"); // Gas column densities [g/cm^2]

    // Intrinsic emission
    if (output_emission) {
      if (output_images)
        write(f, "images_int", images_int, "erg/s/cm^2"); // Surface brightness images [erg/s/cm^2]
      if (output_bin_images)
        write(f, "bin_images_int", bin_images_int, "erg/s/cm^2"); // Bin SB images [erg/s/cm^2]
    }
  }
}
