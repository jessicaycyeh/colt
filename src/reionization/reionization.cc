/********************************
 * reionization/reionization.cc *
 ********************************

 * Driver: Assign rays for projections, etc.

*/

#include "../proto.h"
#include "ReionizationBox.h"
#include "../timing.h" // Timing functionality

extern Timer EoR_timer;

/* Calculate rank offsets. */
vector<int> ReionizationBox::calculate_rank_offsets() {
  vector<int> rank_offsets(n_ranks + 1);     // Number of columns per rank
  const int n_files_rank = n_files_tot / n_ranks; // Equal assignment
  const int remainder = n_files_tot - n_files_rank * n_ranks;
  #pragma omp parallel for
  for (int i = 1; i < n_ranks; ++i) {
    int first_file_rank = i * n_files_rank;  // File start range
    if (n_ranks - i <= remainder)
      first_file_rank += remainder - n_ranks + i; // Correct file range
    rank_offsets[i] = offsets[first_file_rank]; // Match to first file on rank
  }
  rank_offsets[n_ranks] = offsets[n_files_tot]; // Final cumulative count
  return rank_offsets;
}

/* Calculate all projections. */
void ReionizationBox::calculate_projections() {
  for (int i = 0; i < n_projections; ++i) {
    if (root)
      cout << "\nProjection progress: " << i + 1 << " / " << n_projections;
    auto& field = fields[proj_list[i].field];
    const int weight = proj_list[i].weight;
    Image<float>& proj = projections[i];     // Current projection
    for (int file = 0; file < n_files; ++file) {
      const int n_cols = n_columns[file];    // File columns
      const int offset = offsets[file];      // File column offsets
      Image<float>& ds = field.data[file];   // Dataset
      switch (weight) {                      // Calculation determined by weight
        case SUM:                            // Conserved integral projection
          #pragma omp parallel for
          for (int j = 0; j < n_cols; ++j)
            proj[offset + j] = ds.sum(j);
          break;
        case AVG:                            // Volume-weighted average
          #pragma omp parallel for
          for (int j = 0; j < n_cols; ++j)
            proj[offset + j] = ds.mean(j);
          break;
        default:                             // BoxField-weighted average
          auto& wds = fields[weight].data[file]; // Weight dataset
          #pragma omp parallel for
          for (int j = 0; j < n_cols; ++j)
            proj[offset + j] = ds.weighted_mean(j, wds);
      }
    }
    if (root)
      cout << endl;
  }

  // Reduce projections to root
  if (n_ranks > 1) {
    if (root) {                              // Receive from all tasks
      cout << "\nReducing projections..." << endl;

      // Calculate the rank offsets
      vector<int> rank_offsets = calculate_rank_offsets();
      vector<MPI_Request> requests(n_ranks * n_projections, MPI_REQUEST_NULL);
      for (int j = 1; j < n_ranks; ++j) {
        int rank_count = rank_offsets[j+1] - rank_offsets[j];
        for (int i = 0; i < n_projections; ++i)
          MPI_Irecv(projections[i].data() + rank_offsets[j], rank_count,
            MPI_FLOAT, j, i, MPI_COMM_WORLD, &requests[j*n_projections + i]);
      }
      MPI_Waitall(n_ranks * n_projections, requests.data(), MPI_STATUSES_IGNORE);
    } else {                                 // Send to root task
      for (int i = 0; i < n_projections; ++i)
        MPI_Send(projections[i].data(), offsets[n_files], MPI_FLOAT, ROOT, i, MPI_COMM_WORLD);
    }
  }
}

/* Driver for reionization module. */
void ReionizationBox::run() {
  EoR_timer.start();

  if (n_projections > 0)
    calculate_projections();                 // Calculate all projections

  EoR_timer.stop();
}
