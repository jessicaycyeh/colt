/**************************
 * reionization/config.cc *
 **************************

 * Reionization module configuration.

*/

#include "../proto.h"
#include "ReionizationBox.h"
#include "../config.h" // Configuration functions

static inline int key_of_known_field(const string& field) {
  // 3D grid
  if (field == "Density")
    return ReionizationBox::Density;
  else if (field == "DensityStars")
    return ReionizationBox::DensityStars;
  else if (field == "DensityDust")
    return ReionizationBox::DensityDust;
  else if (field == "DensityMetals")
    return ReionizationBox::DensityMetals;
  else if (field == "HII_Fraction")
    return ReionizationBox::HII_Fraction;
  else if (field == "HeIII_Fraction")
    return ReionizationBox::HeIII_Fraction;
  else if (field == "Temperature")
    return ReionizationBox::Temperature;
  else if (field == "IonEnergy")
    return ReionizationBox::IonEnergy;
  // Spectral grid
  else if (field == "DensityHI")
    return ReionizationBox::DensityHI;
  else if (field == "DensityEOS")
    return ReionizationBox::DensityEOS;
  else if (field == "LyaLuminosityRec")
    return ReionizationBox::LyaLuminosityRec;
  else if (field == "LyaLuminosityCol")
    return ReionizationBox::LyaLuminosityCol;
  else if (field == "IonLuminosityStars")
    return ReionizationBox::IonLuminosityStars;
  else if (field == "IonLuminosityAGN")
    return ReionizationBox::IonLuminosityAGN;
  // Likely to be depricated
  else if (field == "NumParticles")
    return ReionizationBox::NumParticles;
  else if (field == "Volume")
    return ReionizationBox::Volume;
  else if (field == "DensityAGN")
    return ReionizationBox::DensityAGN;
  else if (field == "Metallicity")
    return ReionizationBox::Metallicity;
  else if (field == "DustMetallicity")
    return ReionizationBox::DustMetallicity;
  else if (field == "VelocitySquared")
    return ReionizationBox::VelocitySquared;
  error("Unrecognized field name: " + field);
  return 0;
}

static inline int key_of_known_weight(const string& weight) {
  if (weight == "Sum" || weight == "sum")
    return SUM;
  else if (weight == "Average" || weight == "avg" || weight == "mean")
    return AVG;
  else if (weight == "Mass" || weight == "mass")
    return ReionizationBox::Density;
  return key_of_known_field(weight);
}

/* Reionization module configuration. */
void ReionizationBox::module_config(YAML::Node& file, YAML::Node& node) {
  if (!cosmological)
    cosmological = true;                     // Require cosmological flag

  // List of fields
  std::set<FieldWeightPair> proj_set;
  if (file["field_weight_pairs"]) {
    vector<std::pair<string,string>> field_weight_pairs;
    load("field_weight_pairs", field_weight_pairs); // Projection fields and weights
    for (const auto& fw : field_weight_pairs)
      proj_set.emplace(key_of_known_field(fw.first), key_of_known_weight(fw.second));
  } else {
    strings projection_fields;                 // Projection field names
    if (file["fields"]) {
      load("fields", projection_fields);       // Projection fields
    } else {
      projection_fields = { "Density" };       // Default denisty projection
    }

    strings projection_weights;                // Projection weight names
    if (file["weights"])
      load("weights", projection_weights);     // Projection weights

    const size_t n_proj = projection_fields.size();
    while (projection_weights.size() > n_proj)
      projection_weights.pop_back();           // Remove extra weights
    while (projection_weights.size() < n_proj)
      projection_weights.push_back("avg");     // Default to volume averages

    // Remove duplicate fields
    for (size_t i = 0; i < n_proj; ++i)
      proj_set.emplace(key_of_known_field(projection_fields[i]),
                       key_of_known_weight(projection_weights[i]));
  }

  // Save unique projections list
  n_projections = proj_set.size();           // Number of projections
  proj_list.reserve(n_projections);          // Reserve list size
  vector<int> active_flags(n_box_fields);    // Set active field flags
  for (auto& fw : proj_set) {
    active_flags[fw.field] = 1;              // Ensure field is active
    if (fw.weight >= 0)                      // Avoid weight alias range
      active_flags[fw.weight] = 1;           // Ensure weight is active
    proj_list.push_back(fw);                 // Copy to projections list
  }
  for (int field = 0; field < n_box_fields; ++field)
    if (active_flags[field])
      active_fields.push_back(field);        // Save list of active fields
  n_active_fields = active_fields.size();    // Save number of active fields
}
