/**********************
 * reionization/io.cc *
 **********************

 * All I/O operations related to the reionization module.

*/

#include "../proto.h"
#include "ReionizationBox.h"
#include "../io_hdf5.h" // Module HDF5 read/write functions
#include "../timing.h" // Timing functionality

extern Timer read_timer;
extern Timer write_timer;

double calculate_d_L(const double z); // Luminosity distance [cm]
void write_sim_info(const H5File& f); // Write general simulation information

/* Function to print additional reionization information. */
void ReionizationBox::print_info() {
  // Print field list
  if (n_projections > 0) {
    cout << "\nProjection fields:";
    string field, weight;
    for (auto& fw : proj_list) {
      field = fields[fw.field].name;
      if (fw.weight == SUM)
        weight = "sum";
      else if (fw.weight == AVG)
        weight = "avg";
      else if (fw.weight == Density)
        weight = "mass";
      else
        weight = fields[fw.weight].name;
      cout << "\n  " << field << " [" << weight << "]";
    }
    cout << endl;
  }
}

/* Read top level information. */
void ReionizationBox::read_info() {
  // Setup file naming scheme, e.g. renderings/render_001/render_001.000.hdf5
  pre_filename = (init_dir.empty() ? init_base : init_dir + "/" + init_base) +
                 snap_str + "/" + init_base + snap_str + ".";
  const string filename = sub_file(0);       // Read from the first file

  // Open file and read info
  H5File f(filename, H5F_ACC_RDONLY);

  // Initialize cosmology information
  Group g = f.openGroup("Header");           // Info is in the Header group
  read(g, "Redshift", z);                    // Current simulation redshift
  read(g, "HubbleParam", h100);              // Hubble constant [100 km/s/Mpc]
  read(g, "Omega0", Omega0);                 // Matter density [rho_crit_0]
  read(g, "OmegaBaryon", OmegaB);            // Baryon density [rho_crit_0]
  d_L = calculate_d_L(z);                    // Luminosity distance [cm]

  // Check length, mass, and velocity units
  read(g, "UnitLength_in_cm", unit_length);  // Unit length [cm]
  read(g, "UnitMass_in_g", unit_mass);       // Unit mass [g]
  read(g, "UnitVelocity_in_cm_per_s", unit_velocity); // Unit velocity [cm/s]
  if (fabs(1. - unit_length / kpc) > 0.01)
    root_error("UnitLength_in_cm is not kpc: " + to_string(unit_length));
  if (fabs(1. - unit_mass / (1e10*Msun)) > 0.01)
    root_error("UnitMass_in_g is not 10^10 Msun: " + to_string(unit_mass));
  if (fabs(1. - unit_velocity / km) > 0.01)
    root_error("UnitVelocity_in_cm_per_s is not km/s: " + to_string(unit_velocity));

  // Initialize image information
  read(g, "NumFiles", n_files_tot);          // Number of files per snapshot
  read(g, "NumPixels", n_pixels);            // Number of cells on a side
  n_pixels2 = n_pixels * n_pixels;           // Data is a cube
  n_cells = size_t(n_pixels2) * size_t(n_pixels); // Total number of cells
  read(g, "BoxSize", box_size);              // Simulation box size [a*length/h]
  box_area = box_size * box_size;            // Box area [a*length/h]^2
  box_volume = box_area * box_size;          // Box volume [a*length/h]^3
  bbox[0] = {0., 0., 0.};                    // Bounding box min [a*length/h]
  bbox[1] = {box_size, box_size, box_size};  // Bounding box max [a*length/h]
  max_bbox_width = box_size;                 // Max bbox width [a*length/h]
  cell_width = box_size / double(n_pixels);  // Cell widths [a*length/h]
  cell_area = cell_width * cell_width;       // Cell area [a*length/h]^2
  cell_volume = cell_area * cell_width;      // Cell volume [a*length/h]^3

  // File assignments
  n_files = n_files_tot / n_ranks;           // Equal assignment
  first_file = rank * n_files;               // File start range
  const int remainder = n_files_tot - n_files * n_ranks;
  if (n_ranks - rank <= remainder) {
    ++n_files;                               // Assign remaining work
    first_file += remainder - n_ranks + rank; // Correct file range
  }
}

/* Read subfile dataset sizes. */
static inline int get_file_sizes(const string filename) {
  H5File f(filename, H5F_ACC_RDONLY);        // Open file
  return get_size(f, "Density");             // Infer dataset size
}

/* Allocate space for each file. */
void ReionizationBox::setup_dataspace() {
  const int n_files_local = root ? n_files_tot : n_files;
  file_sizes.resize(n_files_local);          // Allocate space for each file
  n_columns.resize(n_files_local);
  offsets.resize(n_files_local + 1);
  for (int i = 0; i < n_files_local; ++i) {
    file_sizes[i] = get_file_sizes(sub_file(i)); // Infer the dataset sizes
    n_columns[i] = file_sizes[i] / n_pixels; // Number of (x,y) columns
    if (file_sizes[i] % n_pixels != 0)
      error(sub_file(i) + " has dataset sizes [" + to_string(file_sizes[i]) +
            "] that are not divisible by n_pixels [" + to_string(n_pixels) + "]");
  }
  offsets[0] = 0;                            // No offset for the first file
  for (int i = 0; i < n_files_local; ++i)
    offsets[i+1] = offsets[i] + n_columns[i]; // Cumulative column offsets
}

/* Initialize active fields. Units(a, h, length, mass, velocity, to_cgs) */
void ReionizationBox::setup_fields() {
  for (const auto field : active_fields)
    fields[field].data.resize(n_files);      // Allocate space for each file

  const double dimensionless = 0.;
  const double already_cgs = 1.;
  const double unit_volume = unit_length * unit_length * unit_length; // L^3
  const double unit_density = unit_mass / unit_volume; // M / L^3
  const double unit_velocity2 = unit_velocity * unit_velocity; // V^2
  const double unit_energy = unit_mass * unit_velocity2; // M V^2
  const double unit_luminosity = unit_energy * unit_velocity / unit_length; // M V^3 / L

  // 3D grid
  fields[Density].name = "Density";          // Gas density [h^2/a^3 * mass/length^3]
  fields[Density].units = Units(-3., 2., -3., 1., 0., unit_density);

  fields[DensityStars].name = "DensityStars"; // Stellar density [h^2/a^3 * mass/length^3]
  fields[DensityStars].units = Units(-3., 2., -3., 1., 0., unit_density);

  fields[DensityDust].name = "DensityDust";  // Dust density [h^2/a^3 * mass/length^3]
  fields[DensityDust].units = Units(-3., 2., -3., 1., 0., unit_density);

  fields[DensityMetals].name = "DensityMetals"; // Metal density [h^2/a^3 * mass/length^3]
  fields[DensityMetals].units = Units(-3., 2., -3., 1., 0., unit_density);

  fields[HII_Fraction].name = "HII_Fraction"; // HII fraction
  fields[HII_Fraction].units = Units(0., 0., 0., 0., 0., dimensionless);

  fields[HeIII_Fraction].name = "HeIII_Fraction"; // HeIII fraction
  fields[HeIII_Fraction].units = Units(0., 0., 0., 0., 0., dimensionless);

  fields[Temperature].name = "Temperature";  // Gas temperature [K]
  fields[Temperature].units = Units(0., 0., 0., 0., 0., already_cgs);

  fields[IonEnergy].name = "IonEnergy";      // Ionizing radiation energy [energy]
  fields[IonEnergy].units = Units(0., -1., 0., 1., 2., unit_energy);

  // Spectral grid
  fields[DensityHI].name = "DensityHI";      // HI density [h^2/a^3 * mass/length^3]
  fields[DensityHI].units = Units(-3., 2., -3., 1., 0., unit_density);

  fields[DensityEOS].name = "DensityEOS";    // EOS density [h^2/a^3 * mass/length^3]
  fields[DensityEOS].units = Units(-3., 2., -3., 1., 0., unit_density);

  fields[LyaLuminosityRec].name = "LyaLuminosityRec";  // Lyman-alpha recombinations [energy/time]
  fields[LyaLuminosityRec].units = Units(0., 0., -1., 1., 3., unit_luminosity);

  fields[LyaLuminosityCol].name = "LyaLuminosityCol";  // Lyman-alpha collisions [energy/time]
  fields[LyaLuminosityCol].units = Units(0., 0., -1., 1., 3., unit_luminosity);

  fields[IonLuminosityStars].name = "IonLuminosityStars"; // Stellar ionization [energy/time]
  fields[IonLuminosityStars].units = Units(0., 0., -1., 1., 3., unit_luminosity);

  fields[IonLuminosityAGN].name = "IonLuminosityAGN";  // Black hole ionization [energy/time]
  fields[IonLuminosityAGN].units = Units(0., 0., -1., 1., 3., unit_luminosity);

  // Likely to be depricated
  fields[NumParticles].name = "NumParticles"; // Number of gas particles
  fields[NumParticles].units = Units(0., 0., 0., 0., 0., dimensionless);

  fields[Volume].name = "Volume";            // Gas volume [a*length/h]^3
  fields[Volume].units = Units(3., -3., 3., 0., 0., unit_volume);

  fields[DensityAGN].name = "DensityAGN";    // Black hole density [h^2/a^3 * mass/length^3]
  fields[DensityAGN].units = Units(-3., 2., -3., 1., 0., unit_density);

  fields[Metallicity].name = "Metallicity";  // Gas metallicity
  fields[Metallicity].units = Units(0., 0., 0., 0., 0., dimensionless);

  fields[DustMetallicity].name = "DustMetallicity"; // Dust-to-gas-ratio
  fields[DustMetallicity].units = Units(0., 0., 0., 0., 0., dimensionless);

  fields[VelocitySquared].name = "VelocitySquared"; // Gas velocity^2 [a*velocity^2]
  fields[VelocitySquared].units = Units(1., 0., 0., 0., 2., unit_velocity2);

  // NumPhotons,                                // Number of photons index
  // Velocities,                                // Gas velocity index
}

static inline void verify_units(DataSet& d, const string& name, const double value) {
  if (H5Aexists(d.getId(), name.c_str()) > 0) {
    double attr;                             // Attribute value
    Attribute a = d.openAttribute(name);     // Access the attribute
    a.read(PredType::NATIVE_DOUBLE, &attr);  // Read attribute from file
    if (attr != value)
      error("Unexpected units for " + name);
  }
}

/* Verify the units of a specified dataset. */
static inline void verify_units(DataSet& d, Units& units) {
  verify_units(d, "a_scaling", units.a_scaling);
  verify_units(d, "h_scaling", units.h_scaling);
  verify_units(d, "length_scaling", units.length_scaling);
  verify_units(d, "mass_scaling", units.mass_scaling);
  verify_units(d, "velocity_scaling", units.velocity_scaling);
}

/* Read a file dataset. */
inline void read(const string& name, Image<float>& im, Units& units,
                 const hsize_t size, const string& filename) {
  H5File f(filename, H5F_ACC_RDONLY);        // Open file
  verify_field(f, name);                     // Ensure the dataset exists
  DataSet d = f.openDataSet(name);           // Access the dataset
  verify_shape(f, name, d, {size});          // Ensure it has the expected shape
  d.read(im.data(), PredType::NATIVE_FLOAT); // Read dataset from file
  verify_units(d, units);                    // Verify it has the expected units
}

/* Read the data for a specified field and file. */
void ReionizationBox::read_dataset(const int field, const int file) {
  BoxField& fld = fields[field];             // BoxField struct
  read(fld.name, fld.data[file], fld.units, file_sizes[file], sub_file(file));
}

/* Read full data after initial setup. */
void ReionizationBox::read_data() {
  const int n_datasets = n_files * n_active_fields; // Possible number of datasets
  #pragma omp parallel for schedule(dynamic)
  for (int i = 0; i < n_datasets; ++i) {
    const int file = i / n_active_fields;    // File index
    const int afi = i - file * n_active_fields; // Active field index
    const int field = active_fields[afi];    // BoxField index
    fields[field].data[file] = Image<float>(n_columns[file], n_pixels);
  }
  for (const auto field : active_fields)
    for (int file = 0; file < n_files; ++file)
      read_dataset(field, file);             // Read all active datasets
}

/* Write dataset unit scalings. */
static inline void write(DataSet& d, Units& units) {
  write(d, "a_scaling", units.a_scaling);
  write(d, "h_scaling", units.h_scaling);
  write(d, "length_scaling", units.length_scaling);
  write(d, "mass_scaling", units.mass_scaling);
  write(d, "velocity_scaling", units.velocity_scaling);
  write(d, "to_cgs", units.to_cgs);
}

/* Write Image dataset. */
template <typename File_or_Group>
static void write(const File_or_Group& f, const string& name, Image<float>& im, Units& units) {
  check_dataset(f, name);                    // Ensure dataset uniqueness
  const hsize_t dims[] = {im.nx(), im.ny()}; // Dimension sizes
  DataSpace s(2, dims);                      // Specify data space
  PredType PT = PredType::NATIVE_FLOAT;
  DataSet d = f.createDataSet(name, PT, s);  // Access dataset
  d.write(im.data(), PT);                    // Write dataset to file
  write(d, units);                           // Write dataset unit scalings
}

/* Reads reionization data and info from the hdf5 files. */
void ReionizationBox::read_hdf5() {
  read_timer.start();

  read_info();                               // Read top level info
  setup_dataspace();                         // Allocate space for each file
  setup_fields();                            // Initialize active fields
  read_data();                               // Read full datasets

  // Print the initial conditions data
  if (root) {
    cout << "\nSimulation info: [Reionization Box]"
         << "\n  redshift   = " << z;
    if (verbose)
      cout << "\n  Omega0     = " << Omega0 << " rho_crit_0"
           << "\n  OmegaB     = " << OmegaB << " rho_crit_0"
           << "\n  H_0        = " << 100. * h100 << " km/s/Mpc"
           << "\n  d_L        = " << d_L / Gpc << " Gpc"
           << "\n  n_files    = " << n_files_tot;
    cout << "\n  n_pixels   = " << n_pixels
         << "\n  box_size   = " << 1e-3 * box_size << " cMpc/h = "
                                << box_size / (1e3 * h100 * (1. + z)) << " Mpc"
         << "\n  cell_width = " << cell_width << " ckpc/h = "
                                << cell_width / (h100 * (1. + z)) << " kpc" << endl;

    if (verbose) {
      cout << "\nGrid data: [" << n_active_fields << " active fields]" << endl;
      print("  file_sizes", file_sizes);
      print("  n_columns ", n_columns);
      print("  offsets   ", offsets);
    }
  }
  read_timer.stop();
}

/* Writes data and info to the specified hdf5 filename. */
void ReionizationBox::write_hdf5() {
  write_timer.start();
  // Open file and write all data
  H5File f(output_dir + "/" + output_base + snap_str + "." + output_ext, H5F_ACC_TRUNC);

  // Write general simulation information
  write_sim_info(f);

  // Write module specific data
  write(f, "unit_length", unit_length);      // Unit length [cm]
  write(f, "unit_mass", unit_mass);          // Unit mass [g]
  write(f, "unit_velocity", unit_velocity);  // Unit velocity [cm/s]

  write(f, "image_width", box_size);         // Image width [a*length/h]
  write(f, "pixel_width", cell_width);       // Pixel width [a*length/h]
  write(f, "pixel_area", cell_area);         // Pixel area [a*length/h]^2
  write(f, "n_pixels", n_pixels);            // Number of pixels

  if (n_projections > 0) {
    write(f, "n_projections", n_projections); // Number of projections

    for (int i = 0; i < n_projections; ++i) {
      const int field = proj_list[i].field;
      const int weight = proj_list[i].weight;
      string name = "proj_" + fields[field].name + "_";
      if (weight == SUM)                     // Conserved integral projection
        name += "sum";
      else if (weight == AVG)                // Volume-weighted average
        name += "avg";
      else if (weight == Density)            // Density-weighted is also mass-weighted
        name += "mass";
      else                                   // BoxField-weighted average
        name += fields[weight].name;
      write(f, name, projections[i], fields[field].units);
    }
  }

  write_timer.stop();
}
