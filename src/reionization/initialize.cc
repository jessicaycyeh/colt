/******************************
 * reionization/initialize.cc *
 ******************************

 * Initialization: Simulation setup, allocation, opening messages, etc.

*/

#include "../proto.h"
#include "ReionizationBox.h"

/* Setup everything for the reionization module. */
void ReionizationBox::setup() {
  // Allocate projection data
  if (n_projections > 0) {
    projections.resize(n_projections);       // Projection output data
    #pragma omp parallel for
    for (int i = 0; i < n_projections; ++i)
      projections[i] = Image<float>(n_pixels, n_pixels);
  }
}
