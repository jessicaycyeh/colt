/*!
 * \file    modules.cc
 *
 * \brief   Simulation initialize and finalize functions.
 *
 * \details This file contains the initialize and finalize functions that are
 *          common to multiple modules, calling virtual functions as needed.
 */

#include "proto.h"
#include "Simulation.h"
#include "timing.h" // Timing functionality

extern Timer main_timer; // Clock timing
extern Timer init_timer;
extern Timer final_timer;

void print_header(); // Print a header for the code
void print_footer(); // Print a footer for code setup
void print_success(); // Print a success banner

//! \brief Initialization: Simulation setup, allocation, opening messages, etc.
void Simulation::initialize() {
  init_timer.start();
  // First things first
  if (root)
    print_header();

  // Read data from an initial conditions file
  read_hdf5();

  // Make sure everything is ready for print statements, etc.
  setup();

  if (root) {
    // Print messages about the initialization status
    cout << "\nSuccessfully set up initial conditions." << endl;

    // Print additional header information
    print_info();
    print_footer();
  }
  init_timer.stop();
}

void reduce_clock_timings(); // MPI reduction (timing.cc)
void print_clock_timings();  // Print timings (timing.cc)

//! \brief Finalization: Write output, summary messages, etc.
void Simulation::finalize() {
  final_timer.start();
  if (root) {
    // Write output
    write_hdf5();

    // Success banner
    print_success();
  }

  final_timer.stop();
  main_timer.stop();

  // Reduce clock timings if MPI is used
  if (n_ranks > 1)
    reduce_clock_timings();

  // Print clock timings
  if (root)
    print_clock_timings();
}
