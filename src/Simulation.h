/****************
 * Simulation.h *
 ****************

 * Base class for running simulations.

*/

#ifndef SIMULATION_INCLUDED
#define SIMULATION_INCLUDED

#include "proto.h"
#include "yaml-cpp/yaml.h"
#include "H5Cpp.h"

// Pair of (field, weight) specifiers
struct FieldWeightPair {
  int field;                                 // Field index
  int weight;                                // Weight index (or alias)

  FieldWeightPair() = default;
  FieldWeightPair(int field, int weight) : field(field), weight(weight) {};

  bool operator==(const FieldWeightPair& other) const {
    return (field == other.field) && (weight == other.weight);
  }

  bool operator<(const FieldWeightPair& other) const {
    if (field < other.field)
      return true;
    if (weight < other.weight)
      return true;
    return false;
  }
};

class Simulation {
protected:
  Simulation() {};                           // Do not allow object creation

  // Cosmology variables
  bool cosmological = false;                 // Flag for cosmological simulations
  double Omega0 = 0.3111;                    // Matter density [rho_crit_0]
  double OmegaB = 0.04897;                   // Baryon density [rho_crit_0]
  double h100 = 0.6766;                      // Hubble constant [100 km/s/Mpc]
  double z = 0.;                             // Simulation redshift
  double d_L = 1.;                           // Luminosity distance [cm]
  double sim_time = -1.;                     // Simulation time [s]

  double calculate_d_L(const double z);      // Luminosity distance [cm]
  void camera_config(YAML::Node& file, YAML::Node& node); // General camera info
  void projection_config(YAML::Node& file, YAML::Node& node); // General projections info
  void spherical_escape_config(YAML::Node& file, YAML::Node& node); // Escape info
  void setup_cameras();                      // General setup for cameras
  void setup_projections();                  // General setup for projections
  void setup_spherical_escape();             // Setup for spherical escape
  void print_cameras();                      // Print camera parameters
  void write_camera_info(const H5::H5File& f); // Write camera parameters
  void write_sim_info(const H5::H5File& f);  // General simulation info

private:
  // Ensure implementation of required virtual functions
  void module_error(const string function) {
    error(function + "() is not implemented for the " + module + " module.");
  };
  virtual void module_config(YAML::Node& file, YAML::Node& node) { module_error("module_config"); }
  virtual void read_hdf5();                  // Read initial conditions
  virtual void setup() { module_error("setup"); } // Module specific setup
  virtual void print_info() { module_error("print_info"); } // Print module info
  virtual void write_hdf5();                 // Write output file
  virtual void write_module(const H5::H5File& f) { module_error("write_module"); } // Module output

public:
  void setup_config();                       // General configuration
  void initialize();                         // General initialization
  virtual void run() { module_error("run"); }
  void finalize();                           // General finalization
};

#endif // SIMULATION_INCLUDED
