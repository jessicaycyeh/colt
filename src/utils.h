/***********
 * utils.h *
 ***********

 * General utility function definitions.

*/

#ifndef UTILS_INCLUDED
#define UTILS_INCLUDED

#include "proto.h"

/* Sum the elements of a vector using OpenMP parallelization. */
template <typename T>
T omp_sum(const vector<T>& v) {
  T result = static_cast<T>(0);
  const int size = v.size();
  #pragma omp parallel for reduction(+:result)
  for (int i = 0; i < size; ++i)
    result += v[i];
  return result;
}

/* Sum the elements of an array using OpenMP parallelization. */
template <typename T, std::size_t N>
T omp_sum(const array<T, N>& v) {
  T result = static_cast<T>(0);
  const int size = v.size();
  #pragma omp parallel for reduction(+:result)
  for (int i = 0; i < size; ++i)
    result += v[i];
  return result;
}

/* Find the maximum value of a vector using OpenMP parallelization. */
template <typename T>
T omp_max(const vector<T>& v) {
  T result = v[0];
  const int size = v.size();
  #pragma omp parallel for reduction(max:result)
  for (int i = 1; i < size; ++i)
    if (v[i] > result)
      result = v[i];
  return result;
}

/* Similar behavior as numpy.linspace. */
inline vector<double> linspace(const double xmin, const double xmax, const int num=50, const bool endpoint=true) {
  // Basic error checking
  if (num < 2) {
    if (num == 1)
      return {xmin};
    error("linspace called with num < 1.");
  }

  // Step size based on whether to include the last point or not
  const double dx = (xmax - xmin) / (double)(endpoint ? num - 1 : num);
  vector<double> result;
  result.resize(num);
  #pragma omp parallel for
  for (int i = 0; i < num; ++i)
    result[i] = xmin + dx * double(i);
  return result;
}

/* Helper function to rescale a vector of vector-like objects. */
template <class C>
static inline void rescale(vector<C>& vec, const double scaling) {
  for (auto& val : vec) {
    #pragma omp parallel for
    for (size_t i = 0; i < val.size(); ++i)
      val[i] *= scaling;
  }
}

static inline void rescale(vector<double>& vec, const double scaling) {
  for (auto& val : vec)
    val *= scaling;
}

static inline void rescale_omp(vector<double>& vec, const double scaling) {
  const int vec_size = vec.size();
  #pragma omp parallel for
  for (int i = 0; i < vec_size; ++i)
    vec[i] *= scaling;
}

/* Helper function to divide vector-like objects. */
template <class C1, class C2>
static inline void divide(vector<C1>& vecs, const vector<C2>& divs) {
  if (vecs.size() != divs.size()) // Minimal error checking
    error("Attempting to divide two vectors with different sizes.");
  for (size_t i = 0; i < divs.size(); ++i) {
    auto& vec = vecs[i];
    auto& div = divs[i];
    #pragma omp parallel for
    for (size_t j = 0; j < div.size(); ++j)
      if (div[j] > 0.)
        vec[j] /= div[j];
  }
}

static inline void divide(vector<double>& vec, const vector<double>& div) {
  #pragma omp parallel for
  for (size_t i = 0; i < div.size(); ++i)
    if (div[i] > 0.)
      vec[i] /= div[i];
}

/* Dummy functions to return zero. */
template <typename T>
inline T return_zero(T) {
  return 0;
}
template <typename T>
inline T return_zero(T, T) {
  return 0;
}

/* Print vectors. */
template <typename T>
inline void print(vector<T>& v, double conv = 1.) {
  if (v.size() < 12) {
    for (size_t i = 0; i < v.size()-1; ++i)
      cout << (conv == 1. ? v[i] : v[i] / conv) << ", ";
    cout << (conv == 1. ? v[v.size()-1] : v[v.size()-1] / conv);
  } else {
    cout << (conv == 1. ? v[0] : v[0] / conv) << ", "
         << (conv == 1. ? v[1] : v[1] / conv) << ", "
         << (conv == 1. ? v[2] : v[2] / conv) << ", ... , "
         << (conv == 1. ? v[v.size()-3] : v[v.size()-3] / conv) << ", "
         << (conv == 1. ? v[v.size()-2] : v[v.size()-2] / conv) << ", "
         << (conv == 1. ? v[v.size()-1] : v[v.size()-1] / conv);
  }
}

template <typename T>
inline void print(string name, vector<T>& v, string units = "", double conv = 1.) {
  cout << name << " = (";
  print(v, conv);
  cout << ") " << units << endl;
}

/* Print arrays. */
template <typename T, std::size_t N>
inline void print(array<T, N>& v, double conv = 1.) {
  if (N < 12) {
    for (size_t i = 0; i < N-1; ++i)
      cout << (conv == 1. ? v[i] : v[i] / conv) << ", ";
    cout << (conv == 1. ? v[N-1] : v[N-1] / conv);
  } else {
    cout << (conv == 1. ? v[0] : v[0] / conv) << ", "
         << (conv == 1. ? v[1] : v[1] / conv) << ", "
         << (conv == 1. ? v[2] : v[2] / conv) << ", ... , "
         << (conv == 1. ? v[N-3] : v[N-3] / conv) << ", "
         << (conv == 1. ? v[N-2] : v[N-2] / conv) << ", "
         << (conv == 1. ? v[N-1] : v[N-1] / conv);
  }
}

template <typename T, std::size_t N>
inline void print(string name, array<T, N>& v, string units = "", double conv = 1.) {
  cout << name << " = (";
  print(v, conv);
  cout << ") " << units << endl;
}

/* Allow conversion to string format. */
inline string to_string(const string& str) {
  return string(str);
}

template <typename T>
inline string to_string(vector<T>& v, double conv = 1.) {
  string result = "(";
  for (size_t i = 0; i < v.size()-1; ++i)
    result += to_string(conv == 1. ? v[i] : v[i] / conv) + ", ";
  return result + (to_string(conv == 1. ? v[v.size()-1] : v[v.size()-1] / conv) + ")");
}

inline string to_string(strings& v) {
  string result = "(";
  for (size_t i = 0; i < v.size()-1; ++i)
    result += v[i] + ", ";
  return result + (v[v.size()-1] + ")");
}

template <typename T, typename S>
std::ostream& operator<<(std::ostream& os, const std::pair<T, S>& v) {
  os << "[" << v.first << ", " << v.second << "]";
  return os;
}

#endif // UTILS_INCLUDED
