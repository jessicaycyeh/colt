/***********
 * proto.h *
 ***********

 * General level function declarations.

*/

#ifndef PROTO_INCLUDED
#define PROTO_INCLUDED

// Standard headers
#include <iomanip>   // int -> string
#include <iostream>  // input / output
#include <string>    // Standard strings
#include <vector>    // Standard vectors
#include <array>     // Standard arrays
#include <tuple>     // Standard tuples
#include <limits>    // Numeric limits

#include "mpi.h"
#include <omp.h>

// Standard library usage
using std::cout;
using std::endl;
using std::string;
using std::to_string;
using std::vector;
using std::array;
using std::tuple;
using std::make_tuple;
using std::tie;
using strings = vector<string>;

// Simple function to print an error message and exit
#define error(message) { error_info(message, __FILE__, __LINE__, __func__); }
#define root_error(message) { if (root) error(message); MPI_Barrier(MPI_COMM_WORLD); }
template <typename T>
void error_info(const T message, const string file, const int line, const string func) {
  std::cerr << "\nError: " << message
            << "\nCheck: " << file << ":" << line << " in " << func << "()" << endl;
  MPI_Abort(MPI_COMM_WORLD, 1);
  exit(EXIT_FAILURE);
}

// Include general data structures
#include "compile_time.h" // Compile time options
#include "Vec3.h" // 3D vectors in (x,y,z) order
#include "image.h"  // 2D/3D matrix arrays in (x,y) order

// Include general template funtions
#include "utils.h"

// Universal constants
constexpr double c = 2.99792458e10;          // Speed of light [cm/s]
constexpr double kB = 1.380648813e-16;       // Boltzmann's constant [g cm^2/s^2/K]
constexpr double h = 6.626069573e-27;        // Planck's constant [erg/s]
constexpr double mH = 1.6735327e-24;         // Mass of hydrogen atom [g]
constexpr double me = 9.109382917e-28;       // Electron mass [g]
constexpr double ee = 4.80320451e-10;        // Electron charge [g^(1/2) cm^(3/2) / s]

// Emperical unit definitions
constexpr double Msun = 1.988435e33;         // Solar mass [g]
constexpr double Lsun = 3.839e33;            // Solar luminosity [erg/s]
constexpr double Zsun = 0.0134;              // Solar metallicity [mass fraction]
constexpr double eV = 1.60217725e-12;        // Electron volt: 1 eV = 1.6e-12 erg
constexpr double arcsec = 648000. / M_PI;    // arseconds per radian
constexpr double pc = 3.085677581467192e18;  // Units: 1 pc  = 3e18 cm
constexpr double kpc = 1e3 * pc;             // Units: 1 kpc = 3e21 cm
constexpr double Mpc = 1e6 * pc;             // Units: 1 Mpc = 3e24 cm
constexpr double Gpc = 1e9 * pc;             // Units: 1 Gpc = 3e27 cm
constexpr double km = 1e5;                   // Units: 1 km  = 1e5  cm
constexpr double angstrom = 1e-8;            // Units: 1 angstrom = 1e-8 cm
constexpr double day = 86400.;               // Units: 1 day = 24 * 3600 seconds
constexpr double yr = 365.24 * day;          // Units: 1 year = 365.24 days
constexpr double kyr = 1e3 * yr;             // Units: 1 kyr = 10^3 yr
constexpr double Myr = 1e6 * yr;             // Units: 1 Myr = 10^6 yr
constexpr double Gyr = 1e9 * yr;             // Units: 1 Gyr = 10^9 yr

// Utility constants
constexpr double negative_infinity = std::numeric_limits<double>::lowest(); // Lowest double
constexpr double positive_infinity = std::numeric_limits<double>::max(); // Highest double
constexpr int OUTSIDE = -1;                  // Flag for outside points
constexpr int INSIDE = -2;                   // Flag for inside points
constexpr int SUM = -3;                      // Alias for sum weight index
constexpr int AVG = -4;                      // Alias for volume average index
constexpr int MASS = -5;                     // Alias for mass average index

// Main control variables
extern string executable;                    // Executable name (command line)
extern string config_file;                   // Config file name (command line)
extern string snap_str;                      // Snapshot number (option command line)
extern int snap_num;                         // Snapshot number (converted from string)
extern string halo_str;                      // Group or subhalo number (option command line)
extern int halo_num;                         // Group or subhalo number (converted from string)
extern string init_dir;                      // Initial conditions directory (optional)
extern string init_base;                     // Initial conditions file base (optional)
extern string init_ext;                      // Initial conditions file extension
extern string init_file;                     // Initial conditions file (required)
extern string output_dir;                    // Output directory name ("output")
extern string output_base;                   // Output file base name ("colt")
extern string output_ext;                    // Output file extension ("hdf5")
extern string module;                        // Module type: mcrt, projections
extern bool verbose;                         // Verbose output for tests and debugging (false)
extern bool select_subhalo;                  // Specified a subhalo (true) 
extern unsigned long long seed;              // Seed for random number generator
#pragma omp threadprivate(seed)              // Unique to each thread

// Supplementary files
extern bool save_connections;                // Save voronoi connections to a file
extern bool save_circulators;                // Flag to save circulators
extern string cgal_dir;                      // CGAL connectivity directory (optional)
extern string cgal_base;                     // CGAL connectivity file base (optional)
extern string cgal_ext;                      // CGAL connectivity file extension
extern string cgal_file;                     // CGAL connectivity file (optional)

// MPI and OpenMP variables
extern int rank;                             // MPI local rank
extern int n_ranks;                          // Number of MPI processes
extern int thread;                           // OpenMP local thread
#pragma omp threadprivate(thread)            // Unique to each thread
extern int n_threads;                        // Number of OpenMP threads
constexpr int ROOT = 0;                      // Root process
extern bool root;                            // True if rank is root
extern bool load_balancing;                  // Use MPI load balancing algorithms

// Keep track of unit scalings
struct Units {
  double a_scaling;                          // Exponent of the cosmological factor
  double h_scaling;                          // Exponent of the hubble parameter
  double length_scaling;                     // Length unit scaling
  double mass_scaling;                       // Mass unit scaling
  double velocity_scaling;                   // Velocity unit scaling
  double to_cgs;                             // Conversion factor to cgs units

  Units() = default;
  Units(double a_scaling, double h_scaling, double length_scaling,
        double mass_scaling, double velocity_scaling, double to_cgs) :
        a_scaling(a_scaling), h_scaling(h_scaling), length_scaling(length_scaling),
        mass_scaling(mass_scaling), velocity_scaling(velocity_scaling), to_cgs(to_cgs) {};
};

// Group data and units
template<typename T>
struct GenericField {
  string name;                               // Field name
  Units units;                               // Field units
  vector<T> data;                            // Field data
};

using Field = GenericField<double>;

// List of possible fields
enum FieldIndex {
  Density = 0,                               // Density index
  Volume,                                    // Volume index
  Temperature,                               // Temperature index
  HydrogenDensity,                           // Hydrogen number density index
  LowerDensity,                              // Number density of lower transition index
  UpperDensity,                              // Number density of upper transition index
  HI_Fraction,                               // HI fraction index
  HII_Fraction,                              // HII fraction index
  H2_Fraction,                               // H2 fraction index
  HeI_Fraction,                              // HeI fraction index
  HeII_Fraction,                             // HeII fraction index
  ElectronAbundance,                         // Electron fraction index
  PhotoheatingRate,                          // Photoheating rate index
  StarFormationRate,                         // Star formation rate index
  DustTemperature,                           // Dust temperature index
  VelocitySquared,                           // Velocity^2 index
  VelocitySquaredLOS,                        // Velocity_LOS^2 index
  VelocityLOS,                               // Velocity_LOS index
  VelocityX,                                 // Velocity_X index
  VelocityY,                                 // Velocity_Y index
  MagneticFieldSquared,                      // Magnetic field^2 index
  MagneticFieldLOS,                          // Magnetic field LOS index
  MagneticFieldX,                            // Magnetic field X index
  MagneticFieldY,                            // Magnetic field Y index
  HydrogenFraction,                          // Mass fraction of hydrogen index
  Metallicity,                               // Gas metallicity index
  DustMetallicity,                           // Dust-to-gas ratio index
  DustCoefficient,                           // Absorption coefficient of dust index
  DustDensity,                               // Dust density index
  ElectronCoefficient,                       // Absorption coefficient of electrons index
  LineCoefficient,                           // Absorption coefficient of line index
  DampingParameter,                          // Line damping parameter index
  NonLocalATau0,                             // Nonlocal estimate of a * tau0 index
  LineEmissivity,                            // Line emissivity [erg/s/cm^3]
  DensitySquared,                            // Density^2 index
  IonizationFront,                           // Ionization front index
  n_fields                                   // Number of fields
};

extern int n_active_fields;                  // Number of active fields
extern vector<int> active_fields;            // List of active field indices
extern array<Field, n_fields> fields;        // File dataset fields

// Basic grid data
extern int n_cells;                          // Number of cells
extern int n_stars;                          // Number of stars
extern vector<Vec3> r;                       // Cell position [cm]
extern vector<double>& V;                    // Cell volume [cm^3]
extern array<Vec3, 2> bbox;                  // Bounding box [cm]
extern double max_bbox_width;                // Max bbox width [cm]

// Cartesian grid data
extern int nx;                               // Number of x cells
extern int ny;                               // Number of y cells
extern int nz;                               // Number of z cells
extern int nyz;                              // Save ny * nz
extern double wx;                            // Cell x width [cm]
extern double wy;                            // Cell y width [cm]
extern double wz;                            // Cell z width [cm]
extern double dV;                            // Cell volume [cm^3]

// Octree grid data (size = n_cells)
extern int n_leafs;                          // Number of leaf cells
extern vector<int> parent;                   // Cell parent
extern vector<array<int, 8>> child;          // Child cell indices
extern vector<bool> child_check;             // True if cell has children
extern vector<double> w;                     // Cell width [cm]

// Voronoi tessellation data
struct Face {
  int neighbor;                              // Neighbor index
  double area;                               // Face area [cm^2]
  vector<int> circulator;                    // Circulator indices
};
extern int n_edges;                          // Number of edge Voronoi cells
extern int n_neighbors_tot;                  // Total number of neighbors for all cells
extern int n_circulators_tot;                // Total number of circulators for all faces
extern vector<bool> edge_flag;               // Flag for edge cells
extern vector<vector<Face>> faces;           // Face connection data

// Additional data (size = n_cells)
extern vector<Vec3> v;                       // Bulk velocity [vth]
extern vector<Vec3> B;                       // Magnetic field [G]
extern vector<double>& rho;                  // Density [g/cm^3]
extern vector<double>& T;                    // Temperature [K]
extern vector<double>& n_H;                  // Hydrogen number density [cm^-3]
extern vector<double>& n_lower;              // Number density of lower transition [cm^-3]
extern vector<double>& n_upper;              // Number density of upper transition [cm^-3]
extern vector<double>& x_HI;                 // x_HI = n_HI / n_H
extern vector<double>& x_HII;                // x_HII = n_HII / n_H
extern vector<double>& x_H2;                 // x_H2 = n_H2 / n_H
extern vector<double>& x_HeI;                // x_HeI = n_HeI / n_H
extern vector<double>& x_HeII;               // x_HeII = n_HeII / n_H
extern vector<double>& x_e;                  // Electron fraction: x_e = n_e / n_H
extern vector<double>& G_ion;                // Photoheating rate [erg/s]
extern vector<double>& SFR;                  // Star formation rate [Msun/yr]
extern vector<double>& T_dust;               // Dust temperature [K]
extern vector<double>& v2;                   // Velocity^2 [cm^2/s^2]
extern vector<double>& v2_LOS;               // Velocity_LOS^2 [cm^2/s^2]
extern vector<double>& v_LOS;                // Velocity_LOS [cm/s]
extern vector<double>& v_x;                  // Velocity_X [cm/s]
extern vector<double>& v_y;                  // Velocity_Y [cm/s]
extern vector<double>& B2;                   // Magnetic field^2 [G^2]
extern vector<double>& B_LOS;                // Magnetic field LOS [G]
extern vector<double>& B_x;                  // Magnetic field X [G]
extern vector<double>& B_y;                  // Magnetic field Y [G]
extern vector<double>& X;                    // Mass fraction of hydrogen
extern vector<double>& Z;                    // Gas metallicity [mass fraction]
extern vector<double>& D;                    // Dust-to-gas ratio [mass fraction]
extern vector<double>& k_e;                  // Absorption coefficient of electrons [1/cm]
extern vector<double>& k_0;                  // Absorption coefficient of line [1/cm]
extern vector<double>& k_dust;               // Absorption coefficient of dust [1/cm]
extern vector<double>& rho_dust;             // Dust density [g/cm^3]
extern vector<double>& a;                    // "Damping parameter"
extern vector<double>& atau;                 // Nonlocal estimate of a * tau for each cell
extern vector<double>& j_line;               // Line emissivity [erg/s/cm^3]
extern vector<double>& rho2;                 // Density^2 [g^2/cm^6]
extern vector<double>& ion_front;            // Ionization front
extern double metallicity;                   // Use a constant metallicity
extern double dust_to_metal;                 // Use a constant dust-to-metal ratio
extern double dust_to_gas;                   // Use a constant dust-to-gas ratio
extern double dust_boost;                    // Optional dust boost factor
extern double constant_temperature;          // Constant temperature: T [K]
extern double T_floor;                       // Apply a temperature floor: T [K]
extern double neutral_fraction;              // Constant neutral fraction: x_HI
extern bool set_density_from_mass;           // Calculate the density as mass / volume
extern bool read_density_as_mass;            // Read the density as a mass field
extern bool read_hydrogen_fraction;          // Read mass fraction of hydrogen
extern bool read_ionized_fraction;           // Read x_HII from the input file
extern bool read_molecular_fraction;         // Read x_H2 from the input file
extern bool read_helium;                     // Read x_HeI and x_HeII from the input file
extern bool read_electron_fraction;          // Read x_e from the input file
extern bool read_photoheating;               // Read photoheating rate
extern bool read_SFR;                        // Read star formation rate
extern bool read_T_dust;                     // Read dust temperature
extern bool read_v2;                         // Read velocity^2
extern bool read_v2_LOS;                     // Read velocity_LOS^2
extern bool read_v_LOS;                      // Read velocity_LOS
extern bool read_v_x;                        // Read velocity_X
extern bool read_v_y;                        // Read velocity_Y
extern bool read_B;                          // Read magnetic field
extern bool read_B2;                         // Read magnetic field^2
extern bool read_B_LOS;                      // Read magnetic field LOS
extern bool read_B_x;                        // Read magnetic field X
extern bool read_B_y;                        // Read magnetic field Y
extern bool read_rho2;                       // Read density^2
extern bool read_ion_front;                  // Read ionization front
extern bool save_line_emissivity;            // Save the line emissivity

// Star data for continuum radiative transfer  (size = n_stars)
extern vector<Vec3> r_star;                  // Star positions [cm]
extern vector<double> L_cont_star;           // Star cont band [erg/s/angstrom]
extern vector<double> m_massive_star;        // Massive star masses [Msun]
extern vector<double> m_init_star;           // Initial star masses [Msun]
extern vector<double> Z_star;                // Star metallicities [mass fraction]
extern vector<double> age_star;              // Star ages [Gyr]
extern bool star_based_emission;             // Include star-based sources
extern bool background_emission;             // Include background sources
extern bool read_m_massive_star;             // Read massive star masses
extern bool read_m_init_star;                // Read initial star masses
extern bool read_Z_star;                     // Read star metallicities
extern bool read_age_star;                   // Read star ages

// Information about escape regions
extern bool spherical_escape;                // Photons escape from a sphere
extern Vec3 escape_center;                   // Center of the escape region [cm]
extern double escape_radius;                 // Radius for spherical escape [cm]
extern double escape_radius_bbox;            // Radius for spherical escape [min distance to bbox edge]
extern double escape_epsilon;                // Escape uncertainty [cm]
extern double escape_radius2;                // Escape radius^2 [cm^2]
extern double emission_radius;               // Radius for spherical emission [cm]
extern double emission_radius_bbox;          // Radius for spherical emission [min distance to bbox edge]
extern double emission_radius2;              // Emission radius^2 [cm^2]

// Information about the cameras
extern bool have_cameras;                    // Camera-based output
extern int n_cameras;                        // Number of cameras
extern int n_bins;                           // Number of frequency bins
extern int n_pixels;                         // Number of x,y image pixels
extern int n_exp;                            // Healpix exponent for cameras
extern int n_rot;                            // Number of rotation directions
extern int n_exp_atau;                       // Healpix exponent for a*tau0
extern double freq_min;                      // Generic frequency extrema [freq units]
extern double freq_max;                      // Calculated when freq is initialized
extern double freq_bin_width;                // Frequency bin width [freq units]
extern double observed_bin_width;            // Observed wavelength resolution [angstrom]
extern double inverse_freq_bin_width;        // Frequency bin width^-1 [freq units]
extern double rgb_freq_min;                  // RGB frequency lower limit [freq units]
extern double rgb_freq_max;                  // RGB frequency upper limit [freq units]
extern double inverse_rgb_freq_bin_width;    // RGB frequency bin width^-1 [freq units]
extern double Dv_cont_min;                   // Continuum velocity offset lower limit [km/s]
extern double Dv_cont_max;                   // Continuum velocity offset upper limit [km/s]
extern double Dv_cont_res;                   // Continuum velocity offset resolution [km/s]
extern double l_cont_res;                    // Continuum wavelength resolution [angstrom]
extern double R_cont_res;                    // Spectral resolution: R = lambda / Delta_lambda
extern vector<Vec3> camera_directions;       // Normalized camera directions
extern vector<Vec3> camera_xaxes;            // Camera "x-axis" direction vectors (x,y,z)
extern vector<Vec3> camera_yaxes;            // Camera "y-axis" direction vectors (x,y,z)
extern Vec3 camera_center;                   // Camera target position [cm]
extern vector<double> image_edges;           // Image relative edge positions [cm]
extern double image_width;                   // Image width [cm] (defaults to entire domain)
extern double image_width_cMpc;              // Image width [cMpc]
extern double image_radius;                  // Image radius [cm] (image_width / 2)
extern double image_radius_bbox;             // Image radius [min distance to bbox edge]
extern double pixel_width;                   // Pixel width [cm]
extern double pixel_area;                    // Pixel area [cm^2]
extern double inverse_pixel_width;           // Inverse pixel width [1/cm]
extern double pixel_arcsec;                  // Pixel width [arcsec] (optional)
extern double pixel_arcsec2;                 // Pixel area [arcsec^2] (optional)
extern double proj_depth;                    // Projection depth [cm] (defaults to image_width)
extern double proj_depth_cMpc;               // Projection depth [cMpc]
extern double proj_radius;                   // Projection radius [cm] (proj_depth / 2)
extern double proj_radius_bbox;              // Projection radius [min distance to bbox edge]
extern bool adaptive;                        // Adaptive convergence projections
extern double pixel_rtol;                    // Relative tolerance per pixel
extern bool perspective;                     // Perspective camera projections
extern bool output_escape_fractions;         // Output camera escape fractions
extern vector<double> f_escs;                // Escape fractions [fraction]
extern bool output_freq_avgs;                // Output frequency averages
extern vector<double> freq_avgs;             // Frequency averages [freq units]
extern bool output_freq_stds;                // Output frequency standard deviations
extern vector<double> freq_stds;             // Standard deviations [freq units]
extern bool output_freq_skews;               // Output frequency skewnesses
extern vector<double> freq_skews;            // Frequency skewnesses [normalized]
extern bool output_freq_kurts;               // Output frequency kurtoses
extern vector<double> freq_kurts;            // Frequency kurtoses [normalized]
extern bool output_fluxes;                   // Output spectral fluxes
extern vector<vector<double>> fluxes;        // Spectral fluxes [erg/s/cm^2/angstrom]
extern bool output_images;                   // Output surface brightness images
extern vector<Image<double>> images;         // Surface brightness images [erg/s/cm^2/arcsec^2]
extern bool output_images2;                  // Output statistical moment images
extern vector<Image<double>> images2;        // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
extern bool output_freq_images;              // Output average frequency images
extern vector<Image<double>> freq_images;    // Average frequency images [freq units]
extern bool output_freq2_images;             // Output frequency^2 images
extern vector<Image<double>> freq2_images;   // Frequency^2 images [freq^2 units]
extern bool output_freq3_images;             // Output frequency^3 images
extern vector<Image<double>> freq3_images;   // Frequency^3 images [freq^3 units]
extern bool output_freq4_images;             // Output frequency^4 images
extern vector<Image<double>> freq4_images;   // Frequency^4 images [freq^4 units]
extern bool output_rgb_images;               // Output flux-weighted frequency RGB images
extern vector<Image<Vec3>> rgb_images;       // Flux-weighted frequency RGB images
extern bool output_cubes;                    // Output spectral data cubes
extern vector<Cube<double>> cubes;           // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

// Tabulated ionizing spectral energy distributions
struct IonAgeTables {
  vector<double> L_HI;                       // Luminosities [erg/s/Msun]
  vector<double> L_HeI;
  vector<double> L_HeII;
  vector<double> Ndot_HI;                    // Photon rates [photons/s/Msun]
  vector<double> Ndot_HeI;
  vector<double> Ndot_HeII;
  vector<double> sigma_HI_1;                 // Cross-sections [cm^2]
  vector<double> sigma_HI_2;
  vector<double> sigma_HI_3;
  vector<double> sigma_HeI_2;
  vector<double> sigma_HeI_3;
  vector<double> sigma_HeII_3;
  vector<double> epsilon_HI_1;               // Photoheating [erg]
  vector<double> epsilon_HI_2;
  vector<double> epsilon_HI_3;
  vector<double> epsilon_HeI_2;
  vector<double> epsilon_HeI_3;
  vector<double> epsilon_HeII_3;
  vector<double> albedo_HI;                  // Dust scattering albedos
  vector<double> albedo_HeI;
  vector<double> albedo_HeII;
  vector<double> cosine_HI;                  // Dust scattering anisotropy parameters
  vector<double> cosine_HeI;
  vector<double> cosine_HeII;
  vector<double> kappa_HI;                   // Dust opacities [cm^2/g dust]
  vector<double> kappa_HeI;
  vector<double> kappa_HeII;
};
extern IonAgeTables ion_age;                 // Tabulated ionizing SEDs

// Tabulated ionizing spectral energy distributions
struct IonMetallicityAgeTables {
  vector<vector<double>> log_L_HI;           // Luminosities [erg/s/Msun]
  vector<vector<double>> log_L_HeI;
  vector<vector<double>> log_L_HeII;
  vector<vector<double>> log_Ndot_HI;        // Photon rates [photons/s/Msun]
  vector<vector<double>> log_Ndot_HeI;
  vector<vector<double>> log_Ndot_HeII;
  vector<vector<double>> log_sigma_Ndot_HI_1; // Cross-sections [cm^2]
  vector<vector<double>> log_sigma_Ndot_HI_2;
  vector<vector<double>> log_sigma_Ndot_HI_3;
  vector<vector<double>> log_sigma_Ndot_HeI_2;
  vector<vector<double>> log_sigma_Ndot_HeI_3;
  vector<vector<double>> log_sigma_Ndot_HeII_3;
  vector<vector<double>> log_sigma_L_HI_1;   // Photoheating [erg]
  vector<vector<double>> log_sigma_L_HI_2;
  vector<vector<double>> log_sigma_L_HI_3;
  vector<vector<double>> log_sigma_L_HeI_2;
  vector<vector<double>> log_sigma_L_HeI_3;
  vector<vector<double>> log_sigma_L_HeII_3;
  vector<vector<double>> log_albedo_Ndot_HI; // Dust scattering albedos
  vector<vector<double>> log_albedo_Ndot_HeI;
  vector<vector<double>> log_albedo_Ndot_HeII;
  vector<vector<double>> log_cosine_Ndot_HI; // Dust scattering anisotropy parameters
  vector<vector<double>> log_cosine_Ndot_HeI;
  vector<vector<double>> log_cosine_Ndot_HeII;
  vector<vector<double>> log_kappa_Ndot_HI;  // Dust opacities [cm^2/g dust]
  vector<vector<double>> log_kappa_Ndot_HeI;
  vector<vector<double>> log_kappa_Ndot_HeII;
};
extern IonMetallicityAgeTables ion_Z_age;    // Tabulated ionizing SEDs

extern double albedo_HI;                     // Dust scattering albedos
extern double albedo_HeI;
extern double albedo_HeII;
extern double cosine_HI;                     // Dust scattering anisotropy parameters
extern double cosine_HeI;
extern double cosine_HeII;
extern double kappa_HI;                      // Dust opacities [cm^2/g dust]
extern double kappa_HeI;
extern double kappa_HeII;

#endif // PROTO_INCLUDED
