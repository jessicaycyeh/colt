/************
 * timing.h *
 ************

 * Timing: Wall clock and real time functions.

*/

#ifndef TIMING_INCLUDED
#define TIMING_INCLUDED

#include <string> // Standard strings

class Timer {
private:
  std::string name;
  timeval tv_start, tv_stop;
  clock_t clock_start, clock_stop;
  double elapsed_time, elapsed_wall;
  bool timer_is_active;

public:
  void start();
  void stop();
  std::string get_name();
  double get_time();
  double get_wall();
  void set_time(double time);
  void set_wall(double wall);
  std::string str();
  std::string date();
  std::string efficiency();
  Timer(const std::string _name, bool _start = false) {
    name = _name;
    timer_is_active = false;
    elapsed_time = 0.;
    elapsed_wall = 0.;
    if (_start) // Start the timer
      start();
  }; // constructor
};

std::string info(Timer& t1, Timer& t2); // String with efficiencies

#endif // TIMING_INCLUDED
