/***************************
 * ionization/Ionization.h *
 ***************************

 * Module declarations.

*/

#ifndef IONIZATION_INCLUDED
#define IONIZATION_INCLUDED

#include "../Simulation.h" // Base simulation class

/* Ionization Monte Carlo radiative transfer module. */
class Ionization : public Simulation {
public:
  void run() override;                       // Module calculations

protected:
  void module_config(YAML::Node& file, YAML::Node& node) override;
  void setup() override;                     // Setup simulation data
  void print_info() override;                // Print simulation information
  void write_module(const H5::H5File& f) override; // Module output

private:
  int max_iter = 10;                         // Maximum number of iterations
  double max_error = 0.01;                   // Relative error for convergence

  string UVB_model = "";                     // UV background model (FG11)
  double UVB_rate_HI = 0.;                   // UV background ionization for HI [1/s]
  double UVB_rate_HeI = 0.;                  // UV background ionization for HeI [1/s]
  double UVB_rate_HeII = 0.;                 // UV background ionization for HeII [1/s]
  double UVB_heat_HI = 0.;                   // UV background photoheating for HI [erg/s]
  double UVB_heat_HeI = 0.;                  // UV background photoheating for HeI [erg/s]
  double UVB_heat_HeII = 0.;                 // UV background photoheating for HeII [erg/s]
  bool self_shielding = true;                // Include self-shielding
  double SS_alpha1 = 0.;                     // Self-shielding parameters
  double SS_alpha2 = 0.;                     // See Rahmati (2012)
  double SS_beta = 0.;
  double SS_xi = 0.;
  double SS_n0 = 0.;
  double SS_f = 0.;

  // Convergence history
  vector<double> x_V_HI;                     // Volume weighted fractions
  vector<double> x_V_HII;
  vector<double> x_V_HeI;
  vector<double> x_V_HeII;
  vector<double> x_V_HeIII;
  vector<double> x_m_HI;                     // Mass weighted fractions
  vector<double> x_m_HII;
  vector<double> x_m_HeI;
  vector<double> x_m_HeII;
  vector<double> x_m_HeIII;
  vector<double> rec_HII;                    // Recombination events per second
  vector<double> rec_HeII;
  vector<double> rec_HeIII;
  vector<double> col_HI;                     // Collisional ionizations per second
  vector<double> col_HeI;
  vector<double> col_HeII;

  void setup_UVB_FG11();                     // UV background initialization
  void setup_self_shielding();               // Self-shielding parameters
  double self_shielding_factor(const double nH); // Calculate the self-shielding factor
  void correct_units();                      // Convert to observed units
  void equilibrium_update(const bool active); // Photo-equilibrium abundances
  void print_convergence();                  // Print convergence statistics
  void write_ionization_info(const H5::H5File& f); // Write simulation information
};

#endif // IONIZATION_INCLUDED
