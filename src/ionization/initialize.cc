/****************************
 * ionization/initialize.cc *
 ****************************

 * Initialization: Simulation setup, allocation, opening messages, etc.

*/

#include "proto.h"
#include "Ionization.h"

bool avoid_cell(const int cell); // Avoid calculations for certain cells
int find_cell(const Vec3 point, int cell); // Cell index of a point
void assign_spectra(); // Assign luminosity and opacity functions

// BPASS tables use 13 metallicities and 51 ages
static const double logZ_BP[13] = {-5., -4., -3., log10(2e-3), log10(3e-3), log10(4e-3), log10(6e-3), log10(8e-3), -2., log10(1.4e-2), log10(2e-2), log10(3e-2), log10(4e-2)};

/* Setup everything for ionization radiative transfer. */
void Ionization::setup() {
  // Setup spherical escape
  if (spherical_escape)
    setup_spherical_escape();                // General spherical escape setup

  bin_f_esc = vector<double>(n_bins);        // Global bin escape fraction
  bin_f_abs = vector<double>(n_bins);        // Global bin dust absorption fraction

  // Setup cameras
  if (have_cameras) {
    setup_cameras();                         // General camera setup

    // Reset camera data and initialize with zeros
    if (output_escape_fractions)
      f_escs = vector<double>(n_cameras);    // Escape fractions
    if (output_bin_escape_fractions)
      bin_f_escs = vector<vector<double>>(n_cameras, vector<double>(n_bins)); // Bin escape fractions
    if (output_images)
      images = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
    if (output_bin_images)
      bin_images = vector<Cube<double>>(n_cameras, Cube<double>(n_pixels, n_pixels, n_bins));

    // Initialize intrinsic cameras (mcrt)
    if (output_mcrt_emission) {
      if (output_images)
        images_int = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
      if (output_bin_images)
        bin_images_int = vector<Cube<double>>(n_cameras, Cube<double>(n_pixels, n_pixels, n_bins));
    }

    // Initialize attenuation cameras (mcrt)
    if (output_mcrt_attenuation) {
      if (output_escape_fractions)
        f_escs_ext = vector<double>(n_cameras); // Escape fractions
      if (output_images)
        images_ext = vector<Image<double>>(n_cameras, Image<double>(n_pixels, n_pixels));
      if (output_bin_images)
        bin_images_ext = vector<Cube<double>>(n_cameras, Cube<double>(n_pixels, n_pixels, n_bins));
    }
  }

  // Convergence history
  x_V_HI.reserve(max_iter);                  // Volume weighted fractions
  x_V_HII.reserve(max_iter);
  x_V_HeI.reserve(max_iter);
  x_V_HeII.reserve(max_iter);
  x_V_HeIII.reserve(max_iter);
  x_m_HI.reserve(max_iter);                  // Mass weighted fractions
  x_m_HII.reserve(max_iter);
  x_m_HeI.reserve(max_iter);
  x_m_HeII.reserve(max_iter);
  x_m_HeIII.reserve(max_iter);
  rec_HII.reserve(max_iter);                 // Recombination events per second
  rec_HeII.reserve(max_iter);
  rec_HeIII.reserve(max_iter);
  col_HI.reserve(max_iter);                  // Collisional ionizations per second
  col_HeI.reserve(max_iter);
  col_HeII.reserve(max_iter);

  // Assign luminosity and opacity functions
  assign_spectra();

  // UV background field
  if (UVB_model == "FG11")
    setup_UVB_FG11();                        // Initialize UVB parameters
  else if (UVB_model != "")
    error("Requested UV background model is not implemented: " + UVB_model);
  if (self_shielding)
    setup_self_shielding();                  // Initialize SS parameters

  // Setup star-based emission sources
  if (star_based_emission) {
    // Calculate the pdf based on star luminosities
    j_cdf_stars.resize(n_stars);             // Cumulative distribution function
    j_weights_stars.resize(n_stars);         // Star luminosity boosting weights
    if (source_model == "GMC") {
      root_error("GMC source model is not implemented.");
    } else if (source_model == "MRT") {
      root_error("MRT source model is not implemented.");
    }
    if (source_model == "BC03-IMF-SOLAR" || source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100") {
      // Calculate the star luminosities
      double L_HI = 0.;                      // Total ionizing luminosity [erg/s]
      double L_HeI = 0.;
      double L_HeII = 0.;
      double Ndot_HI = 0.;                   // Total ionizing rate [photons/s]
      double Ndot_HeI = 0.;
      double Ndot_HeII = 0.;
      double sigma_Ndot_HI_1 = 0.;           // Cross-section rates [cm^2 photons/s]
      double sigma_Ndot_HI_2 = 0.;
      double sigma_Ndot_HI_3 = 0.;
      double sigma_Ndot_HeI_2 = 0.;
      double sigma_Ndot_HeI_3 = 0.;
      double sigma_Ndot_HeII_3 = 0.;
      double sigma_L_HI_1 = 0.;              // Photoheating rates [cm^2 erg/s]
      double sigma_L_HI_2 = 0.;
      double sigma_L_HI_3 = 0.;
      double sigma_L_HeI_2 = 0.;
      double sigma_L_HeI_3 = 0.;
      double sigma_L_HeII_3 = 0.;
      double kappa_Ndot_HI = 0.;             // Dust opacity rates [photons/s cm^2/g dust]
      double kappa_Ndot_HeI = 0.;
      double kappa_Ndot_HeII = 0.;
      double albedo_Ndot_HI = 0.;            // Dust scattering albedo rates [photons/s]
      double albedo_Ndot_HeI = 0.;
      double albedo_Ndot_HeII = 0.;
      double cosine_Ndot_HI = 0.;            // Dust scattering anisotropy rates [photons/s]
      double cosine_Ndot_HeI = 0.;
      double cosine_Ndot_HeII = 0.;
      if (source_model == "BC03-IMF-SOLAR") {
        #pragma omp parallel for reduction(+:L_HI) reduction(+:L_HeI) reduction(+:L_HeII) \
                                 reduction(+:Ndot_HI) reduction(+:Ndot_HeI) reduction(+:Ndot_HeII) \
                                 reduction(+:sigma_Ndot_HI_1) reduction(+:sigma_Ndot_HI_2) \
                                 reduction(+:sigma_Ndot_HI_3) reduction(+:sigma_Ndot_HeI_2) \
                                 reduction(+:sigma_Ndot_HeI_3) reduction(+:sigma_Ndot_HeII_3) \
                                 reduction(+:sigma_L_HI_1) reduction(+:sigma_L_HI_2) \
                                 reduction(+:sigma_L_HI_3) reduction(+:sigma_L_HeI_2) \
                                 reduction(+:sigma_L_HeI_3) reduction(+:sigma_L_HeII_3) \
                                 reduction(+:kappa_Ndot_HI) reduction(+:kappa_Ndot_HeI) reduction(+:kappa_Ndot_HeII) \
                                 reduction(+:albedo_Ndot_HI) reduction(+:albedo_Ndot_HeI) reduction(+:albedo_Ndot_HeII) \
                                 reduction(+:cosine_Ndot_HI) reduction(+:cosine_Ndot_HeI) reduction(+:cosine_Ndot_HeII)
        for (int i = 0; i < n_stars; ++i) {
          // Avoid stars that are in special cells or outside the emission radius
          if (avoid_cell(find_cell(r_star[i], 0)) || (spherical_escape &&
             (r_star[i] - escape_center).dot() >= emission_radius2)) {
            j_cdf_stars[i] = 0.;             // No emission
            continue;
          }
          const double mass = m_init_star[i]; // Initial mass of the star [Msun]
          const double age = age_star[i];    // Age of the star [Gyr]
          int edge = -1;
          if (age <= 1.00000001e-3)          // Age minimum = 1 Myr
            edge = 0;
          else if (age >= 99.999999)         // Age maximum = 100 Gyr
            edge = 50;
          else {
            const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
            const double f_age = floor(d_age); // Floor coordinate
            const double frac_R = d_age - f_age; // Interpolation fraction (right)
            const double frac_L = 1. - frac_R; // Interpolation fraction (left)
            const int i_L = f_age;           // Table age index (left)
            const int i_R = i_L + 1;         // Table age index (right)
            const double local_Ndot_HI = mass * (ion_age.Ndot_HI[i_L]*frac_L + ion_age.Ndot_HI[i_R]*frac_R);
            const double local_Ndot_HeI = mass * (ion_age.Ndot_HeI[i_L]*frac_L + ion_age.Ndot_HeI[i_R]*frac_R);
            const double local_Ndot_HeII = mass * (ion_age.Ndot_HeII[i_L]*frac_L + ion_age.Ndot_HeII[i_R]*frac_R);
            j_cdf_stars[i] = local_Ndot_HI + local_Ndot_HeI + local_Ndot_HeII; // Photon rate pdf
            Ndot_HI += local_Ndot_HI;        // Rates [photons/s]
            Ndot_HeI += local_Ndot_HeI;
            Ndot_HeII += local_Ndot_HeII;
            L_HI += mass * (ion_age.L_HI[i_L]*frac_L + ion_age.L_HI[i_R]*frac_R); // Luminosities [erg/s]
            L_HeI += mass * (ion_age.L_HeI[i_L]*frac_L + ion_age.L_HeI[i_R]*frac_R);
            L_HeII += mass * (ion_age.L_HeII[i_L]*frac_L + ion_age.L_HeII[i_R]*frac_R);
            // Cross-section rates [cm^2 photons/s]
            sigma_Ndot_HI_1 += mass * (ion_age.sigma_HI_1[i_L]*ion_age.Ndot_HI[i_L]*frac_L
                                     + ion_age.sigma_HI_1[i_R]*ion_age.Ndot_HI[i_R]*frac_R);
            sigma_Ndot_HI_2 += mass * (ion_age.sigma_HI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L
                                     + ion_age.sigma_HI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
            sigma_Ndot_HI_3 += mass * (ion_age.sigma_HI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                     + ion_age.sigma_HI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
            sigma_Ndot_HeI_2 += mass * (ion_age.sigma_HeI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L
                                      + ion_age.sigma_HeI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
            sigma_Ndot_HeI_3 += mass * (ion_age.sigma_HeI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                      + ion_age.sigma_HeI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
            sigma_Ndot_HeII_3 += mass * (ion_age.sigma_HeII_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                       + ion_age.sigma_HeII_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
            // Photoheating rates [cm^2 erg/s]
            sigma_L_HI_1 += mass * (ion_age.epsilon_HI_1[i_L]*ion_age.sigma_HI_1[i_L]*ion_age.Ndot_HI[i_L]*frac_L
                                  + ion_age.epsilon_HI_1[i_R]*ion_age.sigma_HI_1[i_R]*ion_age.Ndot_HI[i_R]*frac_R);
            sigma_L_HI_2 += mass * (ion_age.epsilon_HI_2[i_L]*ion_age.sigma_HI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L
                                  + ion_age.epsilon_HI_2[i_R]*ion_age.sigma_HI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
            sigma_L_HI_3 += mass * (ion_age.epsilon_HI_3[i_L]*ion_age.sigma_HI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                  + ion_age.epsilon_HI_3[i_R]*ion_age.sigma_HI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
            sigma_L_HeI_2 += mass * (ion_age.epsilon_HeI_2[i_L]*ion_age.sigma_HeI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L
                                   + ion_age.epsilon_HeI_2[i_R]*ion_age.sigma_HeI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
            sigma_L_HeI_3 += mass * (ion_age.epsilon_HeI_3[i_L]*ion_age.sigma_HeI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                   + ion_age.epsilon_HeI_3[i_R]*ion_age.sigma_HeI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
            sigma_L_HeII_3 += mass * (ion_age.epsilon_HeII_3[i_L]*ion_age.sigma_HeII_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                    + ion_age.epsilon_HeII_3[i_R]*ion_age.sigma_HeII_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
            kappa_Ndot_HI += mass * (ion_age.kappa_HI[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.kappa_HI[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Opacity rates [photons/s cm^2/g dust]
            kappa_Ndot_HeI += mass * (ion_age.kappa_HeI[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.kappa_HeI[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
            kappa_Ndot_HeII += mass * (ion_age.kappa_HeII[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.kappa_HeII[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
            albedo_Ndot_HI += mass * (ion_age.albedo_HI[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.albedo_HI[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Scattering albedo rates [photons/s]
            albedo_Ndot_HeI += mass * (ion_age.albedo_HeI[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.albedo_HeI[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
            albedo_Ndot_HeII += mass * (ion_age.albedo_HeII[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.albedo_HeII[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
            cosine_Ndot_HI += mass * (ion_age.cosine_HI[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.cosine_HI[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Scattering anisotropy rates [photons/s]
            cosine_Ndot_HeI += mass * (ion_age.cosine_HeI[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.cosine_HeI[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
            cosine_Ndot_HeII += mass * (ion_age.cosine_HeII[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.cosine_HeII[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
          }

          // Age was out of range so use the bounding value
          if (edge != -1) {
            const double local_Ndot_HI = mass * ion_age.Ndot_HI[edge];
            const double local_Ndot_HeI = mass * ion_age.Ndot_HeI[edge];
            const double local_Ndot_HeII = mass * ion_age.Ndot_HeII[edge];
            j_cdf_stars[i] = local_Ndot_HI + local_Ndot_HeI + local_Ndot_HeII; // Photon rate pdf
            Ndot_HI += local_Ndot_HI;        // Rates [photons/s]
            Ndot_HeI += local_Ndot_HeI;
            Ndot_HeII += local_Ndot_HeII;
            L_HI += mass * ion_age.L_HI[edge]; // Luminosities [erg/s]
            L_HeI += mass * ion_age.L_HeI[edge];
            L_HeII += mass * ion_age.L_HeII[edge];
            sigma_Ndot_HI_1 += local_Ndot_HI * ion_age.sigma_HI_1[edge]; // Cross-section rates [cm^2 photons/s]
            sigma_Ndot_HI_2 += local_Ndot_HeI * ion_age.sigma_HI_2[edge];
            sigma_Ndot_HI_3 += local_Ndot_HeII * ion_age.sigma_HI_3[edge];
            sigma_Ndot_HeI_2 += local_Ndot_HeI * ion_age.sigma_HeI_2[edge];
            sigma_Ndot_HeI_3 += local_Ndot_HeII * ion_age.sigma_HeI_3[edge];
            sigma_Ndot_HeII_3 += local_Ndot_HeII * ion_age.sigma_HeII_3[edge];
            sigma_L_HI_1 += local_Ndot_HI * ion_age.sigma_HI_1[edge] * ion_age.epsilon_HI_1[edge]; // Photoheating rates [cm^2 erg/s]
            sigma_L_HI_2 += local_Ndot_HeI * ion_age.sigma_HI_2[edge] * ion_age.epsilon_HI_2[edge];
            sigma_L_HI_3 += local_Ndot_HeII * ion_age.sigma_HI_3[edge] * ion_age.epsilon_HI_3[edge];
            sigma_L_HeI_2 += local_Ndot_HeI * ion_age.sigma_HeI_2[edge] * ion_age.epsilon_HeI_2[edge];
            sigma_L_HeI_3 += local_Ndot_HeII * ion_age.sigma_HeI_3[edge] * ion_age.epsilon_HeI_3[edge];
            sigma_L_HeII_3 += local_Ndot_HeII * ion_age.sigma_HeII_3[edge] * ion_age.epsilon_HeII_3[edge];
            kappa_Ndot_HI += local_Ndot_HI * ion_age.kappa_HI[edge]; // Opacity rates [photons/s cm^2/g dust]
            kappa_Ndot_HeI += local_Ndot_HeI * ion_age.kappa_HeI[edge];
            kappa_Ndot_HeII += local_Ndot_HeII * ion_age.kappa_HeII[edge];
            albedo_Ndot_HI += local_Ndot_HI * ion_age.albedo_HI[edge]; // Scattering albedo rates [photons/s]
            albedo_Ndot_HeI += local_Ndot_HeI * ion_age.albedo_HeI[edge];
            albedo_Ndot_HeII += local_Ndot_HeII * ion_age.albedo_HeII[edge];
            cosine_Ndot_HI += local_Ndot_HI * ion_age.cosine_HI[edge]; // Scattering anisotropy rates [photons/s]
            cosine_Ndot_HeI += local_Ndot_HeI * ion_age.cosine_HeI[edge];
            cosine_Ndot_HeII += local_Ndot_HeII * ion_age.cosine_HeII[edge];
          }
        }
      } else if (source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100") {
        #pragma omp parallel for reduction(+:L_HI) reduction(+:L_HeI) reduction(+:L_HeII) \
                                 reduction(+:Ndot_HI) reduction(+:Ndot_HeI) reduction(+:Ndot_HeII) \
                                 reduction(+:sigma_Ndot_HI_1) reduction(+:sigma_Ndot_HI_2) \
                                 reduction(+:sigma_Ndot_HI_3) reduction(+:sigma_Ndot_HeI_2) \
                                 reduction(+:sigma_Ndot_HeI_3) reduction(+:sigma_Ndot_HeII_3) \
                                 reduction(+:sigma_L_HI_1) reduction(+:sigma_L_HI_2) \
                                 reduction(+:sigma_L_HI_3) reduction(+:sigma_L_HeI_2) \
                                 reduction(+:sigma_L_HeI_3) reduction(+:sigma_L_HeII_3) \
                                 reduction(+:kappa_Ndot_HI) reduction(+:kappa_Ndot_HeI) reduction(+:kappa_Ndot_HeII) \
                                 reduction(+:albedo_Ndot_HI) reduction(+:albedo_Ndot_HeI) reduction(+:albedo_Ndot_HeII) \
                                 reduction(+:cosine_Ndot_HI) reduction(+:cosine_Ndot_HeI) reduction(+:cosine_Ndot_HeII)
        for (int i = 0; i < n_stars; ++i) {
          // Avoid stars that are in special cells or outside the emission radius
          if (avoid_cell(find_cell(r_star[i], 0)) || (spherical_escape &&
             (r_star[i] - escape_center).dot() >= emission_radius2)) {
            j_cdf_stars[i] = 0.;             // No emission
            continue;
          }
          const double mass = m_init_star[i]; // Initial mass of the star [Msun]
          const double Z = Z_star[i];        // Metallicity of the star
          const double age = age_star[i];    // Age of the star [Gyr]

          int i_L_Z = 0, i_L_age = 0;        // Lower interpolation index
          double frac_R_Z = 0., frac_R_age = 0.; // Upper interpolation fraction
          if (Z >= 4e-2) {                   // Metallicity maximum = 0.04
            i_L_Z = 11;                      // 13 metallicity bins
            frac_R_Z = 1.;
          } else if (Z > 1e-5) {             // Metallicity minimum = 10^-5
            const double logZ = log10(Z);    // Interpolate in log space
            while (logZ < logZ_BP[i_L_Z])
              i_L_Z--;                       // Search metallicity indices
            frac_R_Z = (logZ - logZ_BP[i_L_Z]) / (logZ_BP[i_L_Z+1] - logZ_BP[i_L_Z]);
          }
          if (age >= 100.) {                 // Age maximum = 100 Gyr
            i_L_age = 49;                    // 51 age bins
            frac_R_age = 1.;
          } else if (age > 1e-3) {           // Age minimum = 1 Myr
            const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
            const double f_age = floor(d_age); // Floor coordinate
            frac_R_age = d_age - f_age;      // Interpolation fraction (right)
            i_L_age = int(f_age);            // Table age index (left)
          }

          // Bilinear interpolation (based on left and right fractions)
          const int i_R_Z = i_L_Z + 1, i_R_age = i_L_age + 1;
          const double frac_L_Z = 1. - frac_R_Z, frac_L_age = 1. - frac_R_age;
          // Photon rates [photons/s]
          const double local_Ndot_HI = mass * pow(10., (ion_Z_age.log_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                     + (ion_Z_age.log_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          const double local_Ndot_HeI = mass * pow(10., (ion_Z_age.log_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                      + (ion_Z_age.log_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          const double local_Ndot_HeII = mass * pow(10., (ion_Z_age.log_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                       + (ion_Z_age.log_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          j_cdf_stars[i] = local_Ndot_HI + local_Ndot_HeI + local_Ndot_HeII; // Photon rate pdf
          Ndot_HI += local_Ndot_HI;
          Ndot_HeI += local_Ndot_HeI;
          Ndot_HeII += local_Ndot_HeII;
          // Luminosities [erg/s]
          L_HI += mass * pow(10., (ion_Z_age.log_L_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_L_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                + (ion_Z_age.log_L_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_L_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          L_HeI += mass * pow(10., (ion_Z_age.log_L_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_L_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                 + (ion_Z_age.log_L_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_L_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          L_HeII += mass * pow(10., (ion_Z_age.log_L_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_L_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                  + (ion_Z_age.log_L_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_L_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          // Cross-section rates [cm^2 photons/s]
          sigma_Ndot_HI_1 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HI_1[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_1[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                           + (ion_Z_age.log_sigma_Ndot_HI_1[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_1[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          sigma_Ndot_HI_2 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                           + (ion_Z_age.log_sigma_Ndot_HI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          sigma_Ndot_HI_3 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                           + (ion_Z_age.log_sigma_Ndot_HI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          sigma_Ndot_HeI_2 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HeI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                            + (ion_Z_age.log_sigma_Ndot_HeI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          sigma_Ndot_HeI_3 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HeI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                            + (ion_Z_age.log_sigma_Ndot_HeI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          sigma_Ndot_HeII_3 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HeII_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeII_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                             + (ion_Z_age.log_sigma_Ndot_HeII_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeII_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          // Photoheating rates [cm^2 erg/s]
          sigma_L_HI_1 += mass * pow(10., (ion_Z_age.log_sigma_L_HI_1[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_1[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                           + (ion_Z_age.log_sigma_L_HI_1[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_1[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          sigma_L_HI_2 += mass * pow(10., (ion_Z_age.log_sigma_L_HI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                           + (ion_Z_age.log_sigma_L_HI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          sigma_L_HI_3 += mass * pow(10., (ion_Z_age.log_sigma_L_HI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                           + (ion_Z_age.log_sigma_L_HI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          sigma_L_HeI_2 += mass * pow(10., (ion_Z_age.log_sigma_L_HeI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                            + (ion_Z_age.log_sigma_L_HeI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          sigma_L_HeI_3 += mass * pow(10., (ion_Z_age.log_sigma_L_HeI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                            + (ion_Z_age.log_sigma_L_HeI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          sigma_L_HeII_3 += mass * pow(10., (ion_Z_age.log_sigma_L_HeII_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeII_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                             + (ion_Z_age.log_sigma_L_HeII_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeII_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          // Opacity rates [photons/s cm^2/g dust]
          kappa_Ndot_HI += mass * pow(10., (ion_Z_age.log_kappa_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                         + (ion_Z_age.log_kappa_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          kappa_Ndot_HeI += mass * pow(10., (ion_Z_age.log_kappa_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                          + (ion_Z_age.log_kappa_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          kappa_Ndot_HeII += mass * pow(10., (ion_Z_age.log_kappa_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                           + (ion_Z_age.log_kappa_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          // Scattering albedo rates [photons/s]
          albedo_Ndot_HI += mass * pow(10., (ion_Z_age.log_albedo_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                         + (ion_Z_age.log_albedo_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          albedo_Ndot_HeI += mass * pow(10., (ion_Z_age.log_albedo_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                          + (ion_Z_age.log_albedo_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          albedo_Ndot_HeII += mass * pow(10., (ion_Z_age.log_albedo_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                           + (ion_Z_age.log_albedo_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          // Scattering anisotropy rates [photons/s]
          cosine_Ndot_HI += mass * pow(10., (ion_Z_age.log_cosine_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                         + (ion_Z_age.log_cosine_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          cosine_Ndot_HeI += mass * pow(10., (ion_Z_age.log_cosine_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                          + (ion_Z_age.log_cosine_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          cosine_Ndot_HeII += mass * pow(10., (ion_Z_age.log_cosine_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                           + (ion_Z_age.log_cosine_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        }
      }

      bin_L_tot = {L_HI, L_HeI, L_HeII};     // Total bin luminosities [erg/s]
      bin_Ndot_tot = {Ndot_HI, Ndot_HeI, Ndot_HeII}; // Total bin rates [photons/s]
      // Global weighted cross-sections [cm^2], photoheating [erg], and mean energies [erg]
      global_sigma_HI = {sigma_Ndot_HI_1 / Ndot_HI, sigma_Ndot_HI_2 / Ndot_HeI, sigma_Ndot_HI_3 / Ndot_HeII};
      global_sigma_HeI = {0., sigma_Ndot_HeI_2 / Ndot_HeI, sigma_Ndot_HeI_3 / Ndot_HeII};
      global_sigma_HeII = {0., 0., sigma_Ndot_HeII_3 / Ndot_HeII};
      global_epsilon_HI = {sigma_L_HI_1 / sigma_Ndot_HI_1, sigma_L_HI_2 / sigma_Ndot_HI_2, sigma_L_HI_3 / sigma_Ndot_HI_3};
      global_epsilon_HeI = {0., sigma_L_HeI_2 / sigma_Ndot_HeI_2, sigma_L_HeI_3 / sigma_Ndot_HeI_3};
      global_epsilon_HeII = {0., 0., sigma_L_HeII_3 / sigma_Ndot_HeII_3};
      global_mean_energy = {L_HI / Ndot_HI, L_HeI / Ndot_HeI, L_HeII / Ndot_HeII};
      global_kappa = {kappa_Ndot_HI / Ndot_HI, kappa_Ndot_HeI / Ndot_HeI, kappa_Ndot_HeII / Ndot_HeII};
      global_albedo = {albedo_Ndot_HI / Ndot_HI, albedo_Ndot_HeI / Ndot_HeI, albedo_Ndot_HeII / Ndot_HeII};
      global_cosine = {cosine_Ndot_HI / Ndot_HI, cosine_Ndot_HeI / Ndot_HeI, cosine_Ndot_HeII / Ndot_HeII};
      L_stars = bin_L_tot[0] + bin_L_tot[1] + bin_L_tot[2]; // Total luminosity [erg/s]
      Ndot_stars = bin_Ndot_tot[0] + bin_Ndot_tot[1] + bin_Ndot_tot[2]; // Total rate [photons/s]
    }

    L_tot += L_stars;                        // Add to the total luminosity
    Ndot_tot += Ndot_stars;                  // Add to the total photon rate

    const double _1_Ndot_tot = 1. / Ndot_stars;
    const double _1_n_photons = 1. / double(n_photons);
    if (j_exp == 1) {                        // No luminosity boosting
      for (int i = 1; i < n_stars; ++i)
        j_cdf_stars[i] += j_cdf_stars[i - 1]; // Turn the pdf into a cdf
      #pragma omp parallel for
      for (int i = 0; i < n_stars; ++i) {
        j_cdf_stars[i] *= _1_Ndot_tot;       // Normalize cdf
        j_weights_stars[i] = _1_n_photons;   // Equal weights
      }
    } else {                                 // Apply luminosity boosting
      #pragma omp parallel for
      for (int i = 0; i < n_stars; ++i) {
        j_weights_stars[i] = j_cdf_stars[i] * _1_Ndot_tot; // Copy the normalized pdf
        if (j_weights_stars[i] > 0.)         // Apply power law boost
          j_cdf_stars[i] = pow(j_weights_stars[i], j_exp); // pdf^j_exp (unnormalized)
        else
          j_cdf_stars[i] = 0.;               // No probability
      }
      const double _1_j_exp_sum = 1. / omp_sum(j_cdf_stars);
      const double dn_photons = double(n_photons);
      #pragma omp parallel for
      for (int i = 0; i < n_stars; ++i) {
        j_cdf_stars[i] *= _1_j_exp_sum;      // pdf^j_exp (normalized)
        if (j_cdf_stars[i] > 0.)
          j_weights_stars[i] /= j_cdf_stars[i] * dn_photons; // weight = pdf / (pdf^j_exp * n_photons)
      }
      for (int i = 1; i < n_stars; ++i)
        j_cdf_stars[i] += j_cdf_stars[i - 1]; // Turn the pdf into a cdf
    }

    // Populate a lookup table to quickly find cdf indices (stars)
    const double half_cdf = 0.5 / double(n_cdf_stars); // Half interval width
    for (int i = 0, star = 0; i < n_cdf_stars; ++i) {
      const double frac_cdf = double(2*i + 1) * half_cdf; // Probability interval midpoint
      while (j_cdf_stars[star] <= frac_cdf && star < n_stars - 1)
        star++;                              // Move to the right bin ( --> )
      j_map_stars[i] = star;                 // Lookup table for insertion
    }
  }

  // Calculate the mixed source pdf based on luminosities
  {
    // Setup mixed source properties (names, flags, and probabilities)
    const array<string, MAX_ION_SOURCE_TYPES> source_names = {"stars", "background"};
    const array<bool, MAX_ION_SOURCE_TYPES> source_flags = {star_based_emission, background_emission};
    mixed_ion_cdf = mixed_ion_pdf = {L_stars, L_background}; // Unnormalized pdf
    n_ion_source_types = 0;
    for (int i = 0; i < MAX_ION_SOURCE_TYPES; ++i)
      if (source_flags[i])
        n_ion_source_types++;
    if (n_ion_source_types == 0)
      error("No source types have been specified.");

    // Same procedure as above
    const double _1_Ndot_tot = 1. / omp_sum(mixed_ion_pdf);
    if (j_exp == 1) {                        // No luminosity boosting
      for (int i = 1; i < MAX_ION_SOURCE_TYPES; ++i)
        mixed_ion_cdf[i] += mixed_ion_cdf[i - 1]; // Turn the pdf into a cdf
      for (int i = 0; i < MAX_ION_SOURCE_TYPES; ++i) {
        mixed_ion_pdf[i] *= _1_Ndot_tot;     // Normalize pdf
        mixed_ion_cdf[i] *= _1_Ndot_tot;     // Normalize cdf
        mixed_ion_weights[i] = 1.;           // Equal weights
      }
    } else {                                 // Apply luminosity boosting
      for (int i = 0; i < MAX_ION_SOURCE_TYPES; ++i) {
        mixed_ion_weights[i] = mixed_ion_pdf[i] *= _1_Ndot_tot; // Copy the normalized pdf
        if (mixed_ion_pdf[i] > 0.)               // Apply power law boost
          mixed_ion_cdf[i] = pow(mixed_ion_pdf[i], j_exp); // pdf^j_exp (unnormalized)
        else
          mixed_ion_cdf[i] = 0.;             // No probability
      }
      const double _1_j_exp_sum = 1. / omp_sum(mixed_ion_cdf);
      for (int i = 0; i < MAX_ION_SOURCE_TYPES; ++i) {
        mixed_ion_cdf[i] *= _1_j_exp_sum;    // pdf^j_exp (normalized)
        if (mixed_ion_cdf[i] > 0.)
          mixed_ion_weights[i] /= mixed_ion_cdf[i]; // weight = pdf / pdf^j_exp
      }
      for (int i = 1; i < MAX_ION_SOURCE_TYPES; ++i)
        mixed_ion_cdf[i] += mixed_ion_cdf[i - 1]; // Turn the pdf into a cdf
    }

    // Setup masked versions
    for (int i = 0; i < MAX_ION_SOURCE_TYPES; ++i)
      if (source_flags[i]) {
        ion_source_types_mask.push_back(i);
        ion_source_names_mask.push_back(source_names[i]);
        mixed_ion_pdf_mask.push_back(mixed_ion_pdf[i]);
        mixed_ion_cdf_mask.push_back(mixed_ion_cdf[i]);
        mixed_ion_weights_mask.push_back(mixed_ion_weights[i]);
      }
  }

  // Reserve photon output buffers
  if (output_photons) {
    source_ids.resize(n_photons);            // Emission cell or star index
    if (n_ion_source_types > 1)
      source_types.resize(n_photons);        // Emission type specifier
    source_weights.resize(n_photons);        // Photon weight at emission
    freq_bins.resize(n_photons);             // Frequency at escape [bin]
    weights.resize(n_photons);               // Photon weight at escape
    dust_weights.resize(n_photons);          // Weight removed by dust absorption
    positions.resize(n_photons);             // Position at escape [cm]
    directions.resize(n_photons);            // Direction at escape
  }

  // Local ionization coupling
  rate_HI = vector<double>(n_cells);         // Photoionization integral for HI [1/s]
  rate_HeI = vector<double>(n_cells);        // Photoionization integral for HeI [1/s]
  rate_HeII = vector<double>(n_cells);       // Photoionization integral for HeII [1/s]
  if (output_photoheating)
    G_ion = vector<double>(n_cells);         // Photoheating rate [erg/s]
}

/* Setup a spatially uniform UV background based on the Faucher-Giguere model (2011 version). */
void Ionization::setup_UVB_FG11() {
  const double logz = log10(z + 1.);         // Log of current redshift
  const int n_logz = 214;                    // Size of logspace table
  const array<double,n_logz> gHIs = {3.76244e-14, 3.83213e-14, 3.90303e-14, 4.01290e-14, // Rate for HI [1/s]
    4.16007e-14, 4.31222e-14, 4.47007e-14, 4.63453e-14, 4.80462e-14, 4.98113e-14, 5.16490e-14,
    5.35503e-14, 5.55239e-14, 5.75769e-14, 5.97016e-14, 6.19076e-14, 6.42009e-14, 6.65751e-14,
    6.90410e-14, 7.16025e-14, 7.42548e-14, 7.70100e-14, 7.98695e-14, 8.28307e-14, 8.59068e-14,
    8.90972e-14, 9.24012e-14, 9.58331e-14, 9.94039e-14, 1.03087e-13, 1.06911e-13, 1.10885e-13,
    1.14984e-13, 1.19226e-13, 1.23641e-13, 1.28194e-13, 1.32903e-13, 1.37801e-13, 1.42848e-13,
    1.48065e-13, 1.53480e-13, 1.59057e-13, 1.64817e-13, 1.70784e-13, 1.76923e-13, 1.83257e-13,
    1.89805e-13, 1.96533e-13, 2.03467e-13, 2.10616e-13, 2.17951e-13, 2.25498e-13, 2.33258e-13,
    2.41203e-13, 2.49362e-13, 2.57724e-13, 2.66267e-13, 2.75018e-13, 2.83999e-13, 2.93102e-13,
    3.02401e-13, 3.11903e-13, 3.21502e-13, 3.31240e-13, 3.41165e-13, 3.51160e-13, 3.61251e-13,
    3.71488e-13, 3.81738e-13, 3.92027e-13, 4.02401e-13, 4.12718e-13, 4.22998e-13, 4.33286e-13,
    4.43424e-13, 4.53438e-13, 4.63352e-13, 4.73011e-13, 4.82441e-13, 4.91671e-13, 5.00591e-13,
    5.09267e-13, 5.17733e-13, 5.25902e-13, 5.33842e-13, 5.41633e-13, 5.48998e-13, 5.56077e-13,
    5.62935e-13, 5.69306e-13, 5.75296e-13, 5.80996e-13, 5.86202e-13, 5.90973e-13, 5.95411e-13,
    5.99308e-13, 6.02729e-13, 6.05783e-13, 6.08265e-13, 6.10246e-13, 6.11843e-13, 6.12855e-13,
    6.13357e-13, 6.13477e-13, 6.13017e-13, 6.12060e-13, 6.10742e-13, 6.08870e-13, 6.06530e-13,
    6.03867e-13, 6.00695e-13, 5.97115e-13, 5.93252e-13, 5.88951e-13, 5.84315e-13, 5.79553e-13,
    5.74327e-13, 5.68850e-13, 5.63301e-13, 5.57385e-13, 5.51270e-13, 5.45110e-13, 5.38750e-13,
    5.32273e-13, 5.25828e-13, 5.19261e-13, 5.12652e-13, 5.06142e-13, 4.99581e-13, 4.93041e-13,
    4.86655e-13, 4.80274e-13, 4.73961e-13, 4.67838e-13, 4.61761e-13, 4.55782e-13, 4.50010e-13,
    4.44306e-13, 4.38712e-13, 4.33323e-13, 4.28006e-13, 4.22791e-13, 4.17762e-13, 4.12791e-13,
    4.07901e-13, 4.03235e-13, 3.98514e-13, 3.93836e-13, 3.89305e-13, 3.84682e-13, 3.80033e-13,
    3.75380e-13, 3.70629e-13, 3.65741e-13, 3.60711e-13, 3.55487e-13, 3.50126e-13, 3.44716e-13,
    3.39250e-13, 3.33815e-13, 3.28484e-13, 3.23176e-13, 3.17893e-13, 3.12648e-13, 3.07359e-13,
    3.02040e-13, 2.96708e-13, 2.91291e-13, 2.85810e-13, 2.80289e-13, 2.74664e-13, 2.68965e-13,
    2.63257e-13, 2.57409e-13, 2.51476e-13, 2.45535e-13, 2.39468e-13, 2.33338e-13, 2.27174e-13,
    2.20960e-13, 2.14712e-13, 2.08469e-13, 2.02191e-13, 1.95911e-13, 1.89667e-13, 1.83417e-13,
    1.77197e-13, 1.71041e-13, 1.64910e-13, 1.58837e-13, 1.52855e-13, 1.46927e-13, 1.41081e-13,
    1.35349e-13, 1.29695e-13, 1.24146e-13, 1.18727e-13, 1.13406e-13, 1.07857e-13, 1.00976e-13,
    9.28557e-14, 8.37879e-14, 7.40917e-14, 6.40348e-14, 5.39463e-14, 4.41478e-14, 3.48885e-14,
    2.64282e-14, 1.89767e-14, 1.26822e-14, 7.64449e-15, 3.90686e-15, 1.45165e-15, 2.10205e-16};
  const array<double,n_logz> gHeIs = {2.08210e-14, 2.12572e-14, 2.17025e-14, 2.23918e-14, // Rate for HeI [1/s]
    2.33198e-14, 2.42879e-14, 2.52975e-14, 2.63568e-14, 2.74619e-14, 2.86143e-14, 2.98232e-14,
    3.10841e-14, 3.23989e-14, 3.37776e-14, 3.52155e-14, 3.67145e-14, 3.82859e-14, 3.99246e-14,
    4.16331e-14, 4.34246e-14, 4.52916e-14, 4.72369e-14, 4.92760e-14, 5.13987e-14, 5.36075e-14,
    5.59217e-14, 5.83284e-14, 6.08297e-14, 6.34491e-14, 6.61700e-14, 6.89941e-14, 7.19495e-14,
    7.50163e-14, 7.81946e-14, 8.15181e-14, 8.49624e-14, 8.85268e-14, 9.22523e-14, 9.61096e-14,
    1.00098e-13, 1.04261e-13, 1.08567e-13, 1.13014e-13, 1.17645e-13, 1.22429e-13, 1.27358e-13,
    1.32482e-13, 1.37766e-13, 1.43200e-13, 1.48839e-13, 1.54642e-13, 1.60595e-13, 1.66759e-13,
    1.73085e-13, 1.79553e-13, 1.86235e-13, 1.93073e-13, 2.00039e-13, 2.07216e-13, 2.14538e-13,
    2.21968e-13, 2.29599e-13, 2.37362e-13, 2.45206e-13, 2.53235e-13, 2.61371e-13, 2.69554e-13,
    2.77895e-13, 2.86311e-13, 2.94728e-13, 3.03264e-13, 3.11833e-13, 3.20347e-13, 3.28932e-13,
    3.37495e-13, 3.45939e-13, 3.54390e-13, 3.62758e-13, 3.70930e-13, 3.79047e-13, 3.87039e-13,
    3.94800e-13, 4.02487e-13, 4.10041e-13, 4.17334e-13, 4.24530e-13, 4.31557e-13, 4.38264e-13,
    4.44821e-13, 4.51181e-13, 4.57167e-13, 4.62960e-13, 4.68531e-13, 4.73684e-13, 4.78604e-13,
    4.83282e-13, 4.87503e-13, 4.91460e-13, 4.95162e-13, 4.98382e-13, 5.01314e-13, 5.03994e-13,
    5.06176e-13, 5.08061e-13, 5.09709e-13, 5.10859e-13, 5.11711e-13, 5.12361e-13, 5.12524e-13,
    5.12396e-13, 5.12115e-13, 5.11372e-13, 5.10365e-13, 5.09259e-13, 5.07720e-13, 5.05921e-13,
    5.04102e-13, 5.01885e-13, 4.99419e-13, 4.97035e-13, 4.94306e-13, 4.91369e-13, 4.88596e-13,
    4.85527e-13, 4.82286e-13, 4.79277e-13, 4.76017e-13, 4.72606e-13, 4.69496e-13, 4.66170e-13,
    4.62701e-13, 4.59597e-13, 4.56306e-13, 4.52855e-13, 4.49830e-13, 4.46633e-13, 4.43238e-13,
    4.40324e-13, 4.37247e-13, 4.33906e-13, 4.31098e-13, 4.28129e-13, 4.24803e-13, 4.22061e-13,
    4.19156e-13, 4.15595e-13, 4.12787e-13, 4.09782e-13, 4.05942e-13, 4.02955e-13, 3.99770e-13,
    3.95765e-13, 3.92516e-13, 3.89054e-13, 3.84702e-13, 3.80985e-13, 3.77102e-13, 3.72393e-13,
    3.68308e-13, 3.64184e-13, 3.59324e-13, 3.55056e-13, 3.50761e-13, 3.45703e-13, 3.41150e-13,
    3.36556e-13, 3.31186e-13, 3.26269e-13, 3.21311e-13, 3.15582e-13, 3.10280e-13, 3.04954e-13,
    2.98712e-13, 2.92929e-13, 2.87174e-13, 2.80426e-13, 2.74242e-13, 2.68097e-13, 2.61268e-13,
    2.54865e-13, 2.48500e-13, 2.41648e-13, 2.35119e-13, 2.28635e-13, 2.21755e-13, 2.15178e-13,
    2.08681e-13, 2.01855e-13, 1.95334e-13, 1.88923e-13, 1.82243e-13, 1.75871e-13, 1.69638e-13,
    1.63189e-13, 1.57052e-13, 1.51075e-13, 1.44932e-13, 1.39099e-13, 1.33017e-13, 1.24981e-13,
    1.15548e-13, 1.04913e-13, 9.31059e-14, 8.08920e-14, 6.85984e-14, 5.63508e-14, 4.47794e-14,
    3.41487e-14, 2.46605e-14, 1.65843e-14, 1.00704e-14, 5.17956e-15, 1.93818e-15, 2.82933e-16};
  const array<double,n_logz> gHeIIs = {1.12165e-16, 1.14812e-16, 1.17562e-16, 1.21829e-16, // Rate for HeII [1/s]
    1.27598e-16, 1.33706e-16, 1.40176e-16, 1.47032e-16, 1.54289e-16, 1.61972e-16, 1.70114e-16,
    1.78734e-16, 1.87857e-16, 1.97519e-16, 2.07747e-16, 2.18571e-16, 2.30028e-16, 2.42151e-16,
    2.54978e-16, 2.68549e-16, 2.82897e-16, 2.98071e-16, 3.14118e-16, 3.31072e-16, 3.48986e-16,
    3.67918e-16, 3.87904e-16, 4.09000e-16, 4.31274e-16, 4.54764e-16, 4.79529e-16, 5.05647e-16,
    5.33157e-16, 5.62124e-16, 5.92630e-16, 6.24716e-16, 6.58451e-16, 6.93928e-16, 7.31182e-16,
    7.70282e-16, 8.11311e-16, 8.54314e-16, 8.99354e-16, 9.46514e-16, 9.95835e-16, 1.04737e-15,
    1.10120e-15, 1.15735e-15, 1.21586e-15, 1.27681e-15, 1.34020e-15, 1.40606e-15, 1.47444e-15,
    1.54532e-15, 1.61871e-15, 1.69462e-15, 1.77301e-15, 1.85382e-15, 1.93707e-15, 2.02262e-15,
    2.11041e-15, 2.20036e-15, 2.29232e-15, 2.38612e-15, 2.48163e-15, 2.57862e-15, 2.67684e-15,
    2.77606e-15, 2.87595e-15, 2.97615e-15, 3.07631e-15, 3.17597e-15, 3.27461e-15, 3.37175e-15,
    3.46672e-15, 3.55884e-15, 3.64744e-15, 3.73165e-15, 3.81065e-15, 3.88427e-15, 3.95257e-15,
    4.01573e-15, 4.07414e-15, 4.12820e-15, 4.17807e-15, 4.22358e-15, 4.26440e-15, 4.30025e-15,
    4.33092e-15, 4.35615e-15, 4.37567e-15, 4.38935e-15, 4.39696e-15, 4.39830e-15, 4.39330e-15,
    4.38180e-15, 4.36368e-15, 4.33895e-15, 4.30754e-15, 4.26941e-15, 4.22468e-15, 4.17338e-15,
    4.11558e-15, 4.05149e-15, 3.98125e-15, 3.90504e-15, 3.82314e-15, 3.73584e-15, 3.64337e-15,
    3.54613e-15, 3.44448e-15, 3.33879e-15, 3.22950e-15, 3.11706e-15, 3.00188e-15, 2.88443e-15,
    2.76522e-15, 2.64469e-15, 2.52330e-15, 2.40159e-15, 2.27998e-15, 2.15895e-15, 2.03896e-15,
    1.92042e-15, 1.80373e-15, 1.68929e-15, 1.57745e-15, 1.46852e-15, 1.36282e-15, 1.26063e-15,
    1.16216e-15, 1.06765e-15, 9.77272e-16, 8.91151e-16, 8.09431e-16, 7.32188e-16, 6.59421e-16,
    5.91164e-16, 5.27374e-16, 4.67931e-16, 4.12768e-16, 3.61758e-16, 3.14704e-16, 2.71484e-16,
    2.31925e-16, 1.95774e-16, 1.62846e-16, 1.32991e-16, 1.06079e-16, 8.22042e-17, 6.15944e-17,
    4.44715e-17, 3.08870e-17, 2.06961e-17, 1.34960e-17, 8.69481e-18, 5.63078e-18, 3.70294e-18,
    2.48255e-18, 1.70973e-18, 1.21531e-18, 8.84193e-19, 6.53144e-19, 4.87505e-19, 3.66698e-19,
    2.77156e-19, 2.10272e-19, 1.60006e-19, 1.22011e-19, 9.31725e-20, 7.12932e-20, 5.46017e-20,
    4.18682e-20, 3.21503e-20, 2.47075e-20, 1.90054e-20, 1.46331e-20, 1.12719e-20, 8.68398e-21,
    6.69136e-21, 5.15238e-21, 3.96403e-21, 3.04862e-21, 2.33985e-21, 1.79111e-21, 1.36828e-21,
    1.04232e-21, 7.91355e-22, 5.99161e-22, 4.52039e-22, 3.39658e-22, 2.54299e-22, 1.89565e-22,
    1.40628e-22, 1.03871e-22, 7.63165e-23, 5.57465e-23, 4.05127e-23, 2.91731e-23, 2.05667e-23,
    1.41855e-23, 9.55468e-24, 6.26749e-24, 4.00081e-24, 2.47791e-24, 1.48254e-24, 8.53030e-25,
    4.68670e-25, 2.42781e-25, 1.16647e-25, 5.03609e-26, 1.83440e-26, 4.84395e-27, 4.96681e-28};
  const array<double,n_logz> eHIs = {2.47740e-25, 2.52581e-25, 2.57518e-25, 2.65152e-25, // Heating for HI [erg/s]
    2.75388e-25, 2.86025e-25, 2.97072e-25, 3.08600e-25, 3.20577e-25, 3.33015e-25, 3.45991e-25,
    3.59472e-25, 3.73468e-25, 3.88063e-25, 4.03223e-25, 4.18957e-25, 4.35363e-25, 4.52402e-25,
    4.70085e-25, 4.88515e-25, 5.07649e-25, 5.27498e-25, 5.48177e-25, 5.69631e-25, 5.91873e-25,
    6.15044e-25, 6.39069e-25, 6.63960e-25, 6.89882e-25, 7.16738e-25, 7.44539e-25, 7.73474e-25,
    8.03432e-25, 8.34412e-25, 8.66638e-25, 8.99975e-25, 9.34417e-25, 9.70214e-25, 1.00721e-24,
    1.04539e-24, 1.08502e-24, 1.12592e-24, 1.16809e-24, 1.21178e-24, 1.25682e-24, 1.30318e-24,
    1.35113e-24, 1.40050e-24, 1.45124e-24, 1.50361e-24, 1.55741e-24, 1.61262e-24, 1.66946e-24,
    1.72774e-24, 1.78739e-24, 1.84866e-24, 1.91132e-24, 1.97526e-24, 2.04084e-24, 2.10762e-24,
    2.17554e-24, 2.24497e-24, 2.31542e-24, 2.38675e-24, 2.45942e-24, 2.53289e-24, 2.60693e-24,
    2.68202e-24, 2.75755e-24, 2.83325e-24, 2.90959e-24, 2.98591e-24, 3.06186e-24, 3.13794e-24,
    3.21339e-24, 3.28784e-24, 3.36172e-24, 3.43427e-24, 3.50505e-24, 3.57461e-24, 3.64244e-24,
    3.70831e-24, 3.77290e-24, 3.83579e-24, 3.89669e-24, 3.95639e-24, 4.01381e-24, 4.06878e-24,
    4.12207e-24, 4.17272e-24, 4.22035e-24, 4.26588e-24, 4.30856e-24, 4.34784e-24, 4.38467e-24,
    4.41838e-24, 4.44836e-24, 4.47566e-24, 4.49965e-24, 4.51970e-24, 4.53692e-24, 4.55078e-24,
    4.56060e-24, 4.56758e-24, 4.57128e-24, 4.57099e-24, 4.56795e-24, 4.56188e-24, 4.55199e-24,
    4.53954e-24, 4.52444e-24, 4.50585e-24, 4.48500e-24, 4.46201e-24, 4.43592e-24, 4.40814e-24,
    4.37853e-24, 4.34630e-24, 4.31269e-24, 4.27802e-24, 4.24119e-24, 4.20332e-24, 4.16532e-24,
    4.12568e-24, 4.08546e-24, 4.04572e-24, 4.00481e-24, 3.96369e-24, 3.92362e-24, 3.88277e-24,
    3.84196e-24, 3.80270e-24, 3.76296e-24, 3.72335e-24, 3.68571e-24, 3.64775e-24, 3.60985e-24,
    3.57423e-24, 3.53837e-24, 3.50229e-24, 3.46874e-24, 3.43493e-24, 3.40042e-24, 3.36861e-24,
    3.33644e-24, 3.30245e-24, 3.27146e-24, 3.23981e-24, 3.20528e-24, 3.17402e-24, 3.14184e-24,
    3.10623e-24, 3.07347e-24, 3.03929e-24, 3.00081e-24, 2.96431e-24, 2.92659e-24, 2.88519e-24,
    2.84629e-24, 2.80737e-24, 2.76578e-24, 2.72686e-24, 2.68798e-24, 2.64603e-24, 2.60615e-24,
    2.56602e-24, 2.52258e-24, 2.48081e-24, 2.43868e-24, 2.39313e-24, 2.34908e-24, 2.30469e-24,
    2.25635e-24, 2.20953e-24, 2.16256e-24, 2.11129e-24, 2.06204e-24, 2.01276e-24, 1.96042e-24,
    1.90978e-24, 1.85920e-24, 1.80656e-24, 1.75522e-24, 1.70408e-24, 1.65138e-24, 1.60001e-24,
    1.54910e-24, 1.49701e-24, 1.44637e-24, 1.39643e-24, 1.34567e-24, 1.29645e-24, 1.24815e-24,
    1.19934e-24, 1.15218e-24, 1.10610e-24, 1.05977e-24, 1.01517e-24, 9.68663e-25, 9.08836e-25,
    8.38466e-25, 7.59438e-25, 6.73026e-25, 5.83548e-25, 4.93603e-25, 4.04893e-25, 3.21073e-25,
    2.44228e-25, 1.75991e-25, 1.18077e-25, 7.15023e-26, 3.66893e-26, 1.36933e-26, 1.99298e-27};
  const array<double,n_logz> eHeIs = {2.21352e-25, 2.26035e-25, 2.30815e-25, 2.38216e-25, // Heating for HeI [erg/s]
    2.48203e-25, 2.58638e-25, 2.69539e-25, 2.81009e-25, 2.92999e-25, 3.05526e-25, 3.18705e-25,
    3.32485e-25, 3.46884e-25, 3.62030e-25, 3.77865e-25, 3.94413e-25, 4.11817e-25, 4.30010e-25,
    4.49020e-25, 4.69012e-25, 4.89903e-25, 5.11723e-25, 5.34672e-25, 5.58637e-25, 5.83653e-25,
    6.09961e-25, 6.37406e-25, 6.66022e-25, 6.96109e-25, 7.27457e-25, 7.60104e-25, 7.94415e-25,
    8.30139e-25, 8.67296e-25, 9.06333e-25, 9.46935e-25, 9.89109e-25, 1.03339e-24, 1.07938e-24,
    1.12709e-24, 1.17710e-24, 1.22898e-24, 1.28273e-24, 1.33896e-24, 1.39721e-24, 1.45746e-24,
    1.52037e-24, 1.58543e-24, 1.65258e-24, 1.72257e-24, 1.79481e-24, 1.86918e-24, 1.94657e-24,
    2.02624e-24, 2.10802e-24, 2.19296e-24, 2.28012e-24, 2.36924e-24, 2.46155e-24, 2.55596e-24,
    2.65210e-24, 2.75142e-24, 2.85271e-24, 2.95544e-24, 3.06128e-24, 3.16882e-24, 3.27734e-24,
    3.38876e-24, 3.50146e-24, 3.61454e-24, 3.73013e-24, 3.84645e-24, 3.96235e-24, 4.08026e-24,
    4.19816e-24, 4.31476e-24, 4.43261e-24, 4.54961e-24, 4.66422e-24, 4.77926e-24, 4.89280e-24,
    5.00329e-24, 5.11399e-24, 5.22289e-24, 5.32802e-24, 5.43307e-24, 5.53562e-24, 5.63337e-24,
    5.73032e-24, 5.82448e-24, 5.91289e-24, 6.00002e-24, 6.08407e-24, 6.16146e-24, 6.23711e-24,
    6.30940e-24, 6.37419e-24, 6.43689e-24, 6.49611e-24, 6.54716e-24, 6.59584e-24, 6.64119e-24,
    6.67784e-24, 6.71202e-24, 6.74328e-24, 6.76553e-24, 6.78531e-24, 6.80292e-24, 6.81138e-24,
    6.81745e-24, 6.82238e-24, 6.81826e-24, 6.81201e-24, 6.80577e-24, 6.79043e-24, 6.77240e-24,
    6.75586e-24, 6.73036e-24, 6.70185e-24, 6.67718e-24, 6.64424e-24, 6.60877e-24, 6.57919e-24,
    6.54173e-24, 6.50194e-24, 6.46977e-24, 6.43004e-24, 6.38786e-24, 6.35514e-24, 6.31507e-24,
    6.27207e-24, 6.24047e-24, 6.20163e-24, 6.15882e-24, 6.12947e-24, 6.09287e-24, 6.05060e-24,
    6.02402e-24, 5.99020e-24, 5.94822e-24, 5.92445e-24, 5.89353e-24, 5.85107e-24, 5.82969e-24,
    5.80152e-24, 5.75127e-24, 5.72877e-24, 5.69886e-24, 5.64006e-24, 5.61438e-24, 5.58225e-24,
    5.52171e-24, 5.49367e-24, 5.46039e-24, 5.39707e-24, 5.36356e-24, 5.32674e-24, 5.26032e-24,
    5.22131e-24, 5.18159e-24, 5.11338e-24, 5.07029e-24, 5.02741e-24, 4.95580e-24, 4.90710e-24,
    4.85904e-24, 4.78224e-24, 4.72701e-24, 4.67297e-24, 4.59040e-24, 4.52881e-24, 4.46921e-24,
    4.37461e-24, 4.30192e-24, 4.23305e-24, 4.12512e-24, 4.04292e-24, 3.96438e-24, 3.85884e-24,
    3.77282e-24, 3.68957e-24, 3.58664e-24, 3.49803e-24, 3.41158e-24, 3.30828e-24, 3.21758e-24,
    3.12959e-24, 3.02662e-24, 2.93544e-24, 2.84745e-24, 2.74615e-24, 2.65600e-24, 2.56947e-24,
    2.47116e-24, 2.38346e-24, 2.29973e-24, 2.20567e-24, 2.12169e-24, 2.03551e-24, 1.91232e-24,
    1.77249e-24, 1.61556e-24, 1.43274e-24, 1.24677e-24, 1.06112e-24, 8.70519e-25, 6.92448e-25,
    5.29443e-25, 3.82589e-25, 2.57593e-25, 1.56831e-25, 8.07264e-26, 3.02490e-26, 4.42766e-27};
  const array<double,n_logz> eHeIIs = {5.00840e-27, 5.12205e-27, 5.23959e-27, 5.42173e-27, // Heating for HeII [erg/s]
    5.66713e-27, 5.92543e-27, 6.19738e-27, 6.48397e-27, 6.78559e-27, 7.10308e-27, 7.43765e-27,
    7.78983e-27, 8.16047e-27, 8.55090e-27, 8.96189e-27, 9.39441e-27, 9.84984e-27, 1.03291e-26,
    1.08334e-26, 1.13642e-26, 1.19225e-26, 1.25097e-26, 1.31276e-26, 1.37771e-26, 1.44598e-26,
    1.51778e-26, 1.59320e-26, 1.67241e-26, 1.75564e-26, 1.84300e-26, 1.93466e-26, 2.03088e-26,
    2.13177e-26, 2.23750e-26, 2.34836e-26, 2.46445e-26, 2.58595e-26, 2.71319e-26, 2.84623e-26,
    2.98526e-26, 3.13056e-26, 3.28223e-26, 3.44044e-26, 3.60544e-26, 3.77733e-26, 3.95624e-26,
    4.14241e-26, 4.33589e-26, 4.53676e-26, 4.74524e-26, 4.96129e-26, 5.18497e-26, 5.41640e-26,
    5.65548e-26, 5.90216e-26, 6.15648e-26, 6.41825e-26, 6.68724e-26, 6.96343e-26, 7.24643e-26,
    7.53590e-26, 7.83164e-26, 8.13307e-26, 8.43965e-26, 8.75097e-26, 9.06625e-26, 9.38466e-26,
    9.70556e-26, 1.00278e-25, 1.03504e-25, 1.06722e-25, 1.09919e-25, 1.13079e-25, 1.16188e-25,
    1.19227e-25, 1.22175e-25, 1.25015e-25, 1.27721e-25, 1.30270e-25, 1.32656e-25, 1.34881e-25,
    1.36945e-25, 1.38860e-25, 1.40634e-25, 1.42269e-25, 1.43760e-25, 1.45096e-25, 1.46267e-25,
    1.47268e-25, 1.48090e-25, 1.48724e-25, 1.49166e-25, 1.49408e-25, 1.49444e-25, 1.49273e-25,
    1.48888e-25, 1.48285e-25, 1.47466e-25, 1.46428e-25, 1.45169e-25, 1.43694e-25, 1.42004e-25,
    1.40100e-25, 1.37989e-25, 1.35676e-25, 1.33166e-25, 1.30469e-25, 1.27593e-25, 1.24547e-25,
    1.21341e-25, 1.17989e-25, 1.14501e-25, 1.10893e-25, 1.07177e-25, 1.03366e-25, 9.94779e-26,
    9.55269e-26, 9.15271e-26, 8.74950e-26, 8.34469e-26, 7.93969e-26, 7.53611e-26, 7.13549e-26,
    6.73916e-26, 6.34853e-26, 5.96499e-26, 5.58963e-26, 5.22360e-26, 4.86801e-26, 4.52365e-26,
    4.19132e-26, 3.87178e-26, 3.56551e-26, 3.27294e-26, 2.99450e-26, 2.73037e-26, 2.48065e-26,
    2.24545e-26, 2.02466e-26, 1.81801e-26, 1.62531e-26, 1.44613e-26, 1.27991e-26, 1.12618e-26,
    9.84290e-27, 8.53438e-27, 7.32975e-27, 6.22278e-27, 5.20850e-27, 4.28865e-27, 3.46929e-27,
    2.75794e-27, 2.15897e-27, 1.67078e-27, 1.28373e-27, 9.84237e-28, 7.55904e-28, 5.81517e-28,
    4.47613e-28, 3.45843e-28, 2.69491e-28, 2.11318e-28, 1.66280e-28, 1.31098e-28, 1.03491e-28,
    8.17011e-29, 6.44917e-29, 5.08899e-29, 4.01304e-29, 3.16163e-29, 2.48990e-29, 1.95887e-29,
    1.53997e-29, 1.21002e-29, 9.49923e-30, 7.45182e-30, 5.84138e-30, 4.57425e-30, 3.57772e-30,
    2.79488e-30, 2.17935e-30, 1.69630e-30, 1.31831e-30, 1.02167e-30, 7.89231e-31, 6.07990e-31,
    4.66800e-31, 3.57068e-31, 2.72229e-31, 2.06735e-31, 1.56322e-31, 1.17727e-31, 8.82520e-32,
    6.58262e-32, 4.88671e-32, 3.60782e-32, 2.64796e-32, 1.93293e-32, 1.39791e-32, 9.89734e-33,
    6.85374e-33, 4.63391e-33, 3.05136e-33, 1.95486e-33, 1.21498e-33, 7.29463e-34, 4.21100e-34,
    2.32071e-34, 1.20595e-34, 5.81084e-35, 2.51545e-35, 9.18775e-36, 2.43198e-36, 2.49922e-37};

  if (logz <= 1e-10) {                       // Set rates to z = 0 value
    UVB_rate_HI = gHIs[0];
    UVB_rate_HeI = gHeIs[0];
    UVB_rate_HeII = gHeIIs[0];
    UVB_heat_HI = eHIs[0];
    UVB_heat_HeI = eHeIs[0];
    UVB_heat_HeII = eHeIIs[0];
  } else if (logz >= 1.064999999) {          // Set rates to zero for z > 11.6
    UVB_rate_HI = 0.;
    UVB_rate_HeI = 0.;
    UVB_rate_HeII = 0.;
    UVB_heat_HI = 0.;
    UVB_heat_HeI = 0.;
    UVB_heat_HeII = 0.;
  } else {
    const double d_logz = 200. * logz;       // Table coordinate
    const double f_logz = floor(d_logz);     // Floor coordinate
    const double frac_R = d_logz - f_logz;   // Interpolation fraction (right)
    const double frac_L = 1. - frac_R;       // Interpolation fraction (left)
    const int i_L = f_logz;                  // Table age index (left)
    const int i_R = i_L + 1;                 // Table age index (right)
    UVB_rate_HI = pow(10., log10(gHIs[i_L])*frac_L + log10(gHIs[i_R])*frac_R); // Rate for HI [1/s]
    UVB_rate_HeI = pow(10., log10(gHeIs[i_L])*frac_L + log10(gHeIs[i_R])*frac_R); // Rate for HeI [1/s]
    UVB_rate_HeII = pow(10., log10(gHeIIs[i_L])*frac_L + log10(gHeIIs[i_R])*frac_R); // Rate for HeII [1/s]
    UVB_heat_HI = pow(10., log10(eHIs[i_L])*frac_L + log10(eHIs[i_R])*frac_R); // Heating for HI [erg/s]
    UVB_heat_HeI = pow(10., log10(eHeIs[i_L])*frac_L + log10(eHeIs[i_R])*frac_R); // Heating for HeI [erg/s]
    UVB_heat_HeII = pow(10., log10(eHeIIs[i_L])*frac_L + log10(eHeIIs[i_R])*frac_R); // Heating for HeII [erg/s]
  }
}

/* Setup self-shielding parameters for a spatially uniform UV background based on the Rahmati model (2012). */
void Ionization::setup_self_shielding() {
  const array<double,7> SS_alpha1s = {-3.98, -2.94, -2.22, -1.99, -2.05, -2.63, -2.63};
  const array<double,7> SS_alpha2s = {-1.09, -0.90, -1.09, -0.88, -0.75, -0.57, -0.57};
  const array<double,7> SS_betas = {1.29, 1.21, 1.75, 1.72, 1.93, 1.77, 1.77};
  const array<double,7> SS_xis = {1., 1., 1., 1., 1., 1., 0.};
  const array<double,7> SS_n0s = {-2.94, -2.29, -2.06, -2.13, -2.23, -2.35, -2.35};
  const array<double,7> SS_fs = {0.01, 0.03, 0.03, 0.04, 0.02, 0.01, 0.01};

  if (z <= 1e-10) {                          // Set to z = 0 values
    SS_alpha1 = SS_alpha1s[0];
    SS_alpha2 = SS_alpha2s[0];
    SS_beta = SS_betas[0];
    SS_xi = SS_xis[0];
    SS_n0 = SS_n0s[0];
    SS_f = SS_fs[0];
  } else if (z >= 5.99999999) {              // Set to z = 6 values
    SS_alpha1 = SS_alpha1s[6];
    SS_alpha2 = SS_alpha2s[6];
    SS_beta = SS_betas[6];
    SS_xi = SS_xis[6];
    SS_n0 = SS_n0s[6];
    SS_f = SS_fs[6];
    self_shielding = false;                  // Turn off self-shielding
  } else {
    const double f_z = floor(z);             // Floor coordinate
    const double frac_R = z - f_z;           // Interpolation fraction (right)
    const double frac_L = 1. - frac_R;       // Interpolation fraction (left)
    const int i_L = f_z;                     // Table age index (left)
    const int i_R = i_L + 1;                 // Table age index (right)
    SS_alpha1 = SS_alpha1s[i_L]*frac_L + SS_alpha1s[i_R]*frac_R;
    SS_alpha2 = SS_alpha2s[i_L]*frac_L + SS_alpha2s[i_R]*frac_R;
    SS_beta = SS_betas[i_L]*frac_L + SS_betas[i_R]*frac_R;
    SS_xi = SS_xis[i_L]*frac_L + SS_xis[i_R]*frac_R;
    SS_n0 = SS_n0s[i_L]*frac_L + SS_n0s[i_R]*frac_R;
    SS_f = SS_fs[i_L]*frac_L + SS_fs[i_R]*frac_R;
  }
}
