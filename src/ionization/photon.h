/************
 * photon.h *
 ************

 * Ionization photon packets.

*/

#ifndef ION_PHOTON_INCLUDED
#define ION_PHOTON_INCLUDED

#include "../Vec3.h"                         // 3D vectors in (x,y,z) order

struct IonPhoton {
  IonPhoton();                               // Initialization constructor
  void print();                              // Print photon data
  void ray_trace();                          // Perform ionization calculations

  // Photon properties
  int source_id;                             // Index of emission source
  int source_type;                           // Type of emission source
  double source_weight;                      // Photon weight at emission
  int current_cell;                          // Index of the current cell
  Vec3 position;                             // Photon position [cm]
  Vec3 direction;                            // Photon direction (normalized)
  int frequency_bin;                         // Photon frequency bin
  double weight;                             // Photon weight (normalized)
  double dust_weight;                        // Weight removed by dust absorption

  // Spectral properties
  double luminosity;                         // Source luminosity [erg/s]
  double Ndot;                               // Source rate [photons/s]
  double sigma_HI;                           // HI cross-section [cm^2]
  double sigma_HeI;                          // HeI cross-section [cm^2]
  double sigma_HeII;                         // HeII cross-section [cm^2]
  double sigma_epsilon_HI;                   // HI photoheating [erg cm^2]
  double sigma_epsilon_HeI;                  // HeI photoheating [erg cm^2]
  double sigma_epsilon_HeII;                 // HeII photoheating [erg cm^2]
  double kappa;                              // Dust opacity [cm^2/g dust]
  double albedo;                             // Scattering albedo
  double cosine;                             // Scattering anisotropy

  // Radiative transfer properties
  double k_abs;                              // Absorption coefficient [1/cm]
  double k_dust_abs;                         // Dust absorption coefficient [1/cm]

private:
  void move_photon(const double distance);   // Updates the position and weight

  // void ionization_scatter();
  void dust_scatter();

  void calculate_LOS_int();
  void calculate_LOS_int(const int camera);

  void calculate_LOS_ext();
  void calculate_LOS_ext(const int camera);

  void calculate_LOS(const int scatter_type);
  void calculate_LOS(const int scatter_type, const int camera);
};

#endif // ION_PHOTON_INCLUDED
