/***************************
 * ionization/ray_trace.cc *
 ***************************

 * Ray tracer: Follow individual photon packets.

*/

#include "proto.h"
#include "photon.h" // Photon packets

double ran(); // Random number (from global seed)
Vec3 isotropic_direction(); // Generate direction from an isotropic distribution
double spherical_escape_distance(Vec3 point, Vec3 direction); // Distance to escape the bounding sphere
bool avoid_cell(const int cell); // Avoid calculations for certain cells
int find_cell(const Vec3 point, int cell); // Cell index of a point
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell
void set_derived_dust_parameters(const double cosine); // Set the derived dust parameters

static const int ISOTROPIC = 1;
static const double _2pi = 2. * M_PI;
static const double WEIGHT_DISCARD = 1e-14;  // exp(-23) ~ 1e-10; exp(-32) ~ 1e-14

static void (*setup_spectra)(IonPhoton *, const int); // Set the bin and spectral properties

/* Set the spectral properties of each bin based on the BC03-IMF-SOLAR model. */
static void setup_spectra_imf_solar(IonPhoton *p, const int star) {
  const double mass = m_init_star[star];     // Initial mass of the star [Msun]
  const double age = age_star[star];         // Age of the star [Gyr]
  int edge = -1;
  if (age <= 1.00000001e-3)                  // Age minimum = 1 Myr
    edge = 0;
  else if (age >= 99.999999)                 // Age maximum = 100 Gyr
    edge = 50;
  else {
    const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
    const double f_age = floor(d_age);       // Floor coordinate
    const double frac_R = d_age - f_age;     // Interpolation fraction (right)
    const double frac_L = 1. - frac_R;       // Interpolation fraction (left)
    const int i_L = f_age;                   // Table age index (left)
    const int i_R = i_L + 1;                 // Table age index (right)
    const double Ndot_int[3] = {
      mass * (ion_age.Ndot_HI[i_L]*frac_L + ion_age.Ndot_HI[i_R]*frac_R), // HI [photons/s]
      mass * (ion_age.Ndot_HeI[i_L]*frac_L + ion_age.Ndot_HeI[i_R]*frac_R), // HeI [photons/s]
      mass * (ion_age.Ndot_HeII[i_L]*frac_L + ion_age.Ndot_HeII[i_R]*frac_R)}; // HeII [photons/s]
    double Ndot_cdf_0 = Ndot_int[0];
    double Ndot_cdf_1 = Ndot_int[1] + Ndot_cdf_0;
    double Ndot_cdf_tot = Ndot_int[2] + Ndot_cdf_1;
    Ndot_cdf_0 /= Ndot_cdf_tot;
    Ndot_cdf_1 /= Ndot_cdf_tot;
    const double bin_comp = ran();
    if (bin_comp < Ndot_cdf_0) {
      p->frequency_bin = 0;                  // Photon frequency bin
      p->Ndot = Ndot_int[0];                 // Source rate [photons/s]
      p->luminosity = mass * (ion_age.L_HI[i_L]*frac_L + ion_age.L_HI[i_R]*frac_R); // Source luminosity [erg/s]
      const double mass_Ndot = mass / p->Ndot; // Interpolation normalization
      p->sigma_HI = mass_Ndot * (ion_age.sigma_HI_1[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.sigma_HI_1[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Cross-sections [cm^2]
      p->sigma_HeI = 0.;                     // Always zero
      p->sigma_HeII = 0.;                    // Always zero
      if (output_photoheating) {             // Photoheating [erg cm^2]
        p->sigma_epsilon_HI = mass_Ndot * (ion_age.epsilon_HI_1[i_L]*ion_age.sigma_HI_1[i_L]*ion_age.Ndot_HI[i_L]*frac_L
                                         + ion_age.epsilon_HI_1[i_R]*ion_age.sigma_HI_1[i_R]*ion_age.Ndot_HI[i_R]*frac_R);
        p->sigma_epsilon_HeI = 0.;           // Always zero
        p->sigma_epsilon_HeII = 0.;          // Always zero
      }
      p->kappa = mass_Ndot * (ion_age.kappa_HI[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.kappa_HI[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Dust opacity [cm^2/g dust]
      p->albedo = mass_Ndot * (ion_age.albedo_HI[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.albedo_HI[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Scattering albedo
      p->cosine = mass_Ndot * (ion_age.cosine_HI[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.cosine_HI[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Scattering anisotropy
    } else if (bin_comp < Ndot_cdf_1) {
      p->frequency_bin = 1;                  // Photon frequency bin
      p->Ndot = Ndot_int[1];                 // Source rate [photons/s]
      p->luminosity = mass * (ion_age.L_HeI[i_L]*frac_L + ion_age.L_HeI[i_R]*frac_R); // Source luminosity [erg/s]
      const double mass_Ndot = mass / p->Ndot; // Interpolation normalization
      p->sigma_HI = mass_Ndot * (ion_age.sigma_HI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.sigma_HI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R); // Cross-sections [cm^2]
      p->sigma_HeI = mass_Ndot * (ion_age.sigma_HeI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.sigma_HeI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
      p->sigma_HeII = 0.;                    // Always zero
      if (output_photoheating) {             // Photoheating [erg cm^2]
        p->sigma_epsilon_HI = mass_Ndot * (ion_age.epsilon_HI_2[i_L]*ion_age.sigma_HI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L
                                         + ion_age.epsilon_HI_2[i_R]*ion_age.sigma_HI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
        p->sigma_epsilon_HeI = mass_Ndot * (ion_age.epsilon_HeI_2[i_L]*ion_age.sigma_HeI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L
                                          + ion_age.epsilon_HeI_2[i_R]*ion_age.sigma_HeI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
        p->sigma_epsilon_HeII = 0.;          // Always zero
      }
      p->kappa = mass_Ndot * (ion_age.kappa_HeI[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.kappa_HeI[i_R]*ion_age.Ndot_HeI[i_R]*frac_R); // Dust opacity [cm^2/g dust]
      p->albedo = mass_Ndot * (ion_age.albedo_HeI[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.albedo_HeI[i_R]*ion_age.Ndot_HeI[i_R]*frac_R); // Scattering albedo
      p->cosine = mass_Ndot * (ion_age.cosine_HeI[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.cosine_HeI[i_R]*ion_age.Ndot_HeI[i_R]*frac_R); // Scattering anisotropy
    } else {
      p->frequency_bin = 2;                  // Photon frequency bin
      p->Ndot = Ndot_int[2];                 // Source rate [photons/s]
      p->luminosity = mass * (ion_age.L_HeII[i_L]*frac_L + ion_age.L_HeII[i_R]*frac_R); // Source luminosity [erg/s]
      const double mass_Ndot = mass / p->Ndot; // Interpolation normalization
      p->sigma_HI = mass_Ndot * (ion_age.sigma_HI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.sigma_HI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R); // Cross-sections [cm^2]
      p->sigma_HeI = mass_Ndot * (ion_age.sigma_HeI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.sigma_HeI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
      p->sigma_HeII = mass_Ndot * (ion_age.sigma_HeII_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.sigma_HeII_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
      if (output_photoheating) {             // Photoheating [erg cm^2]
        p->sigma_epsilon_HI = mass_Ndot * (ion_age.epsilon_HI_3[i_L]*ion_age.sigma_HI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                         + ion_age.epsilon_HI_3[i_R]*ion_age.sigma_HI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
        p->sigma_epsilon_HeI = mass_Ndot * (ion_age.epsilon_HeI_3[i_L]*ion_age.sigma_HeI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                          + ion_age.epsilon_HeI_3[i_R]*ion_age.sigma_HeI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
        p->sigma_epsilon_HeII = mass_Ndot * (ion_age.epsilon_HeII_3[i_L]*ion_age.sigma_HeII_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                           + ion_age.epsilon_HeII_3[i_R]*ion_age.sigma_HeII_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
      }
      p->kappa = mass_Ndot * (ion_age.kappa_HeII[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.kappa_HeII[i_R]*ion_age.Ndot_HeII[i_R]*frac_R); // Dust opacity [cm^2/g dust]
      p->albedo = mass_Ndot * (ion_age.albedo_HeII[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.albedo_HeII[i_R]*ion_age.Ndot_HeII[i_R]*frac_R); // Scattering albedo
      p->cosine = mass_Ndot * (ion_age.cosine_HeII[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.cosine_HeII[i_R]*ion_age.Ndot_HeII[i_R]*frac_R); // Scattering anisotropy
    }
    return;                                  // Finished setup
  }

  // Age was out of range so use the bounding value
  const double Ndot_int[3] = {
    mass * ion_age.Ndot_HI[edge],            // HI [photons/s]
    mass * ion_age.Ndot_HeI[edge],           // HeI [photons/s]
    mass * ion_age.Ndot_HeII[edge]};         // HeII [photons/s]
  double Ndot_cdf_0 = Ndot_int[0];
  double Ndot_cdf_1 = Ndot_int[1] + Ndot_cdf_0;
  double Ndot_cdf_tot = Ndot_int[2] + Ndot_cdf_1;
  Ndot_cdf_0 /= Ndot_cdf_tot;
  Ndot_cdf_1 /= Ndot_cdf_tot;
  const double bin_comp = ran();
  if (bin_comp < Ndot_cdf_0) {
    p->frequency_bin = 0;
    p->Ndot = Ndot_int[0];                   // Source rate [photons/s]
    p->luminosity = mass * ion_age.L_HI[edge]; // Source luminosity [erg/s]
    p->sigma_HI = ion_age.sigma_HI_1[edge];  // Cross-sections [cm^2]
    p->sigma_HeI = 0.;                       // Always zero
    p->sigma_HeII = 0.;                      // Always zero
    if (output_photoheating) {               // Photoheating [erg cm^2]
      p->sigma_epsilon_HI = ion_age.epsilon_HI_1[edge] * ion_age.sigma_HI_1[edge];
      p->sigma_epsilon_HeI = 0.;             // Always zero
      p->sigma_epsilon_HeII = 0.;            // Always zero
    }
    p->kappa = ion_age.kappa_HI[edge];       // Dust opacity [cm^2/g dust]
    p->albedo = ion_age.albedo_HI[edge];     // Scattering albedo
    p->cosine = ion_age.cosine_HI[edge];     // Scattering anisotropy
  } else if (bin_comp < Ndot_cdf_1) {
    p->frequency_bin = 1;
    p->Ndot = Ndot_int[1];                   // Source rate [photons/s]
    p->luminosity = mass * ion_age.L_HeI[edge]; // Source luminosity [erg/s]
    p->sigma_HI = ion_age.sigma_HI_2[edge];  // Cross-sections [cm^2]
    p->sigma_HeI = ion_age.sigma_HeI_2[edge];
    p->sigma_HeII = 0.;                      // Always zero
    if (output_photoheating) {               // Photoheating [erg cm^2]
      p->sigma_epsilon_HI = ion_age.epsilon_HI_2[edge] * ion_age.sigma_HI_2[edge];
      p->sigma_epsilon_HeI = ion_age.epsilon_HeI_2[edge] * ion_age.sigma_HeI_2[edge];
      p->sigma_epsilon_HeII = 0.;            // Always zero
    }
    p->kappa = ion_age.kappa_HeI[edge];      // Dust opacity [cm^2/g dust]
    p->albedo = ion_age.albedo_HeI[edge];    // Scattering albedo
    p->cosine = ion_age.cosine_HeI[edge];    // Scattering anisotropy
  } else {
    p->frequency_bin = 2;
    p->Ndot = Ndot_int[2];                   // Source rate [photons/s]
    p->luminosity = mass * ion_age.L_HeII[edge]; // Source luminosity [erg/s]
    p->sigma_HI = ion_age.sigma_HI_3[edge];  // Cross-sections [cm^2]
    p->sigma_HeI = ion_age.sigma_HeI_3[edge];
    p->sigma_HeII = ion_age.sigma_HeII_3[edge];
    if (output_photoheating) {               // Photoheating [erg cm^2]
      p->sigma_epsilon_HI = ion_age.epsilon_HI_3[edge] * ion_age.sigma_HI_3[edge];
      p->sigma_epsilon_HeI = ion_age.epsilon_HeI_3[edge] * ion_age.sigma_HeI_3[edge];
      p->sigma_epsilon_HeII = ion_age.epsilon_HeII_3[edge] * ion_age.sigma_HeII_3[edge];
    }
    p->kappa = ion_age.kappa_HeII[edge];     // Dust opacity [cm^2/g dust]
    p->albedo = ion_age.albedo_HeII[edge];   // Scattering albedo
    p->cosine = ion_age.cosine_HeII[edge];   // Scattering anisotropy
  }
}

// BPASS tables use 13 metallicities and 51 ages
static const double logZ_BP[13] = {-5., -4., -3., log10(2e-3), log10(3e-3), log10(4e-3), log10(6e-3), log10(8e-3), -2., log10(1.4e-2), log10(2e-2), log10(3e-2), log10(4e-2)};

/* Set the spectral properties of each bin based on the BPASS-IMF-135-100 or BPASS-CHAB-100 models. */
static void setup_spectra_imf(IonPhoton *p, const int star) {
  const double mass = m_init_star[star];     // Initial mass of the star [Msun]
  const double Z = Z_star[star];             // Metallicity of the star
  const double age = age_star[star];         // Age of the star [Gyr]

  int i_L_Z = 0, i_L_age = 0;                // Lower interpolation index
  double frac_R_Z = 0., frac_R_age = 0.;     // Upper interpolation fraction
  if (Z >= 4e-2) {                           // Metallicity maximum = 0.04
    i_L_Z = 11;                              // 13 metallicity bins
    frac_R_Z = 1.;
  } else if (Z > 1e-5) {                     // Metallicity minimum = 10^-5
    const double logZ = log10(Z);            // Interpolate in log space
    while (logZ < logZ_BP[i_L_Z])
      i_L_Z--;                               // Search metallicity indices
    frac_R_Z = (logZ - logZ_BP[i_L_Z]) / (logZ_BP[i_L_Z+1] - logZ_BP[i_L_Z]);
  }
  if (age >= 100.) {                         // Age maximum = 100 Gyr
    i_L_age = 49;                            // 51 age bins
    frac_R_age = 1.;
  } else if (age > 1e-3) {                   // Age minimum = 1 Myr
    const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
    const double f_age = floor(d_age);       // Floor coordinate
    frac_R_age = d_age - f_age;              // Interpolation fraction (right)
    i_L_age = int(f_age);                    // Table age index (left)
  }

  // Bilinear interpolation (based on left and right fractions)
  const int i_R_Z = i_L_Z + 1, i_R_age = i_L_age + 1;
  const double frac_L_Z = 1. - frac_R_Z, frac_L_age = 1. - frac_R_age;
  // Photon rates [photons/s]
  const double Ndot_int[3] = {
    mass * pow(10., (ion_Z_age.log_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                  + (ion_Z_age.log_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age),
    mass * pow(10., (ion_Z_age.log_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                  + (ion_Z_age.log_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age),
    mass * pow(10., (ion_Z_age.log_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                  + (ion_Z_age.log_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age)};
  double Ndot_cdf_0 = Ndot_int[0];
  double Ndot_cdf_1 = Ndot_int[1] + Ndot_cdf_0;
  double Ndot_cdf_tot = Ndot_int[2] + Ndot_cdf_1;
  Ndot_cdf_0 /= Ndot_cdf_tot;
  Ndot_cdf_1 /= Ndot_cdf_tot;
  const double bin_comp = ran();
  if (bin_comp < Ndot_cdf_0) {
    p->frequency_bin = 0;                    // Photon frequency bin
    p->Ndot = Ndot_int[0];                   // Source rate [photons/s]
    p->luminosity = mass * pow(10., (ion_Z_age.log_L_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_L_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                  + (ion_Z_age.log_L_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_L_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Source luminosity [erg/s]
    const double mass_Ndot = mass / p->Ndot; // Interpolation normalization
    p->sigma_HI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_Ndot_HI_1[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_1[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                     + (ion_Z_age.log_sigma_Ndot_HI_1[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_1[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Cross-sections [cm^2]
    p->sigma_HeI = 0.;                       // Always zero
    p->sigma_HeII = 0.;                      // Always zero
    if (output_photoheating) {               // Photoheating [erg cm^2]
      p->sigma_epsilon_HI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_L_HI_1[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_1[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                               + (ion_Z_age.log_sigma_L_HI_1[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_1[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
      p->sigma_epsilon_HeI = 0.;             // Always zero
      p->sigma_epsilon_HeII = 0.;            // Always zero
    }
    p->kappa = mass_Ndot * pow(10., (ion_Z_age.log_kappa_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                  + (ion_Z_age.log_kappa_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Dust opacity [cm^2/g dust]
    p->albedo = mass_Ndot * pow(10., (ion_Z_age.log_albedo_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                   + (ion_Z_age.log_albedo_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Scattering albedo
    p->cosine = mass_Ndot * pow(10., (ion_Z_age.log_cosine_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                   + (ion_Z_age.log_cosine_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Scattering anisotropy
  } else if (bin_comp < Ndot_cdf_1) {
    p->frequency_bin = 1;                    // Photon frequency bin
    p->Ndot = Ndot_int[1];                   // Source rate [photons/s]
    p->luminosity = mass * pow(10., (ion_Z_age.log_L_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_L_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                  + (ion_Z_age.log_L_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_L_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Source luminosity [erg/s]
    const double mass_Ndot = mass / p->Ndot; // Interpolation normalization
    p->sigma_HI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_Ndot_HI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                     + (ion_Z_age.log_sigma_Ndot_HI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Cross-sections [cm^2]
    p->sigma_HeI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_Ndot_HeI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                      + (ion_Z_age.log_sigma_Ndot_HeI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
    p->sigma_HeII = 0.;                      // Always zero
    if (output_photoheating) {               // Photoheating [erg cm^2]
      p->sigma_epsilon_HI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_L_HI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                               + (ion_Z_age.log_sigma_L_HI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
      p->sigma_epsilon_HeI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_L_HeI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                + (ion_Z_age.log_sigma_L_HeI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
      p->sigma_epsilon_HeII = 0.;            // Always zero
    }
    p->kappa = mass_Ndot * pow(10., (ion_Z_age.log_kappa_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                  + (ion_Z_age.log_kappa_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Dust opacity [cm^2/g dust]
    p->albedo = mass_Ndot * pow(10., (ion_Z_age.log_albedo_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                   + (ion_Z_age.log_albedo_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Scattering albedo
    p->cosine = mass_Ndot * pow(10., (ion_Z_age.log_cosine_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                   + (ion_Z_age.log_cosine_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Scattering anisotropy
  } else {
    p->frequency_bin = 2;                    // Photon frequency bin
    p->Ndot = Ndot_int[2];                   // Source rate [photons/s]
    p->luminosity = mass * pow(10., (ion_Z_age.log_L_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_L_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                  + (ion_Z_age.log_L_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_L_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Source luminosity [erg/s]
    const double mass_Ndot = mass / p->Ndot; // Interpolation normalization
    p->sigma_HI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_Ndot_HI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                     + (ion_Z_age.log_sigma_Ndot_HI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Cross-sections [cm^2]
    p->sigma_HeI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_Ndot_HeI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                      + (ion_Z_age.log_sigma_Ndot_HeI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
    p->sigma_HeII = mass_Ndot * pow(10., (ion_Z_age.log_sigma_Ndot_HeII_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeII_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                       + (ion_Z_age.log_sigma_Ndot_HeII_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeII_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
    if (output_photoheating) {               // Photoheating [erg cm^2]
      p->sigma_epsilon_HI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_L_HI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                               + (ion_Z_age.log_sigma_L_HI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
      p->sigma_epsilon_HeI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_L_HeI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                + (ion_Z_age.log_sigma_L_HeI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
      p->sigma_epsilon_HeII = mass_Ndot * pow(10., (ion_Z_age.log_sigma_L_HeII_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeII_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                 + (ion_Z_age.log_sigma_L_HeII_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeII_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
    }
    p->kappa = mass_Ndot * pow(10., (ion_Z_age.log_kappa_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                  + (ion_Z_age.log_kappa_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Dust opacity [cm^2/g dust]
    p->albedo = mass_Ndot * pow(10., (ion_Z_age.log_albedo_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                   + (ion_Z_age.log_albedo_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Scattering albedo
    p->cosine = mass_Ndot * pow(10., (ion_Z_age.log_cosine_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                   + (ion_Z_age.log_cosine_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Scattering anisotropy
  }
}

void assign_spectra() {
  // Assign luminosity and opacity functions
  if (source_model == "GMC") {               // RT is based on the GMC model
    error("GMC not implemented.");
  } else if (source_model == "MRT") {        // RT is based on the MRT model
    error("MRT not implemented.");
  } else if (source_model == "BC03-IMF-SOLAR") {
    setup_spectra = setup_spectra_imf_solar;
  } else if (source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100") {
    setup_spectra = setup_spectra_imf;
  }
}

IonPhoton::IonPhoton() {
  // Draw the source that the photon comes from (stars, background)
  const double mixed_comp = ran();
  if (mixed_comp < mixed_ion_cdf[STARS]) {   // Star-based emission
    const double ran_comp = ran();           // Draw the star that the photon comes from
    int i = j_map_stars[int(ran_comp * double(n_cdf_stars))]; // Start search from lookup table
    while (j_cdf_stars[i] > ran_comp && i > 0)
      --i;                                   // Move "inward"  ( <-- )
    while (j_cdf_stars[i] <= ran_comp && i < n_stars - 1)
      ++i;                                   // Move "outward" ( --> )
    source_id = i;                           // Star index
    source_type = STARS;                     // Star source
    source_weight = mixed_ion_weights[STARS] * j_weights_stars[i]; // Star-based luminosity boosting
    current_cell = find_cell(r_star[i], 0);  // Cell index hosting the star (cell = 0)
    position = r_star[i];                    // Set the photon position to the star position
    setup_spectra(this, i);                  // Set the bin and spectral properties
  } else if (mixed_comp < mixed_ion_cdf[BACKGROUND]) { // Background source emission
    error("Background sources are not implemented!");
    // position = isotropic_direction() * r_background; // Known radius
    // current_cell = find_cell(position, 0);   // Cell index hosting the sampled photon
    // source_id = current_cell;                // Cell index
    source_type = BACKGROUND;                // Background source
    source_weight = mixed_ion_weights[BACKGROUND] / double(n_photons); // Equal weight photons
  } else
    error("Unexpected source probability behavior");

  if (avoid_cell(current_cell))
    error("Attempting to insert photon in an invalid cell");
  if (kappa < 0.)
    error("kappa must be >= 0");             // Validate kappa_dust range
  if (albedo < 0. || albedo > 1.)
    error("albedo must be in the range [0,1]"); // Validate albedo range
  if (cosine < -1. || cosine > 1.)
    error("cosine must be in the range [-1,1]"); // Validate cosine range
  if (fabs(cosine) < 1e-6)
    cosine = (cosine < 0.) ? -1e-6 : 1e-6;   // Avoid division by zero

  direction = isotropic_direction();         // Initial direction (isotropic)
  weight = source_weight;                    // Set the initial photon weight
  dust_weight = 0.;                          // Set the initial dust weight
  set_derived_dust_parameters(cosine);       // Set the derived dust parameters

  // Note: calculate_LOS requires an initialized photon so should be done last
  calculate_LOS(ISOTROPIC);                  // Flux for optically thin cases
  if (output_mcrt_emission)
    calculate_LOS_int();                     // Intrinsic emission cameras
  if (output_mcrt_attenuation)
    calculate_LOS_ext();                     // Attenuated emission cameras
}

/* Print photon data. */
void IonPhoton::print() {
  cout << "IonPhoton {"
       << "\n  source_id     = " << source_id
       << "\n  source_type   = " << source_type
       << "\n  source_weight = " << source_weight
       << "\n  current_cell  = " << current_cell
       << "\n  position      = " << position / kpc << " kpc"
       << "\n  direction     = " << direction
       << "\n  frequency_bin = " << frequency_bin
       << "\n  weight        = " << weight
       << "\n  dust_weight   = " << dust_weight
       << "\n  luminosity    = " << luminosity << " erg/s"
       << "\n  Ndot          = " << Ndot << " photons/s"
       << "\n  sigma_HI      = " << sigma_HI << " cm^2"
       << "\n  sigma_HeI     = " << sigma_HeI << " cm^2"
       << "\n  sigma_HeII    = " << sigma_HeII << " cm^2"
       << "\n  sigma_epsilon_HI = " << sigma_epsilon_HI << " erg cm^2"
       << "\n  sigma_epsilon_HeI = " << sigma_epsilon_HeI << " erg cm^2"
       << "\n  sigma_epsilon_HeII = " << sigma_epsilon_HeII << " erg cm^2"
       << "\n  kappa         = " << kappa << " cm^2/g dust"
       << "\n  albedo        = " << albedo
       << "\n  cosine        = " << cosine
       << "\n}" << endl;
}

/* Updates the position and weight of the photon */
inline void IonPhoton::move_photon(const double distance) {
  const double dtau_abs = k_abs * distance;  // Absorption optical depth
  const double exp_dtau_abs = exp(-dtau_abs);
  const double rate_weight = weight * ((dtau_abs < 1e-5) ? // Numerical stability
    (1. - 0.5 * dtau_abs) * distance :       // Series approx (dtau_abs << 1)
    (1. - exp_dtau_abs) / k_abs);            // Radiative transfer solution

  // Update ionization rates (Note: x_HI etc. are in the equilibrium solver)
  #pragma omp atomic
  rate_HI[current_cell] += rate_weight * sigma_HI;
  #pragma omp atomic
  rate_HeI[current_cell] += rate_weight * sigma_HeI;
  #pragma omp atomic
  rate_HeII[current_cell] += rate_weight * sigma_HeII;
  if (output_photoheating) {                 // Combined photoheating [erg]
    const double sigma_epsilon_eff = x_HI[current_cell] * sigma_epsilon_HI
                                   + x_HeI[current_cell] * sigma_epsilon_HeI
                                   + x_HeII[current_cell] * sigma_epsilon_HeII;
    #pragma omp atomic
    G_ion[current_cell] += rate_weight * sigma_epsilon_eff;
  }

  // Update the position and weight
  position += direction * distance;          // Update photon position
  weight *= exp_dtau_abs;                    // Reduce weight by exp(-dtau_abs)
  dust_weight += k_dust_abs * rate_weight;   // Weight removed by dust absorption
}

/* Ray tracer (ionization) */
void IonPhoton::ray_trace() {
  int neib_cell;                             // Neighbor cell index
  double dl_neib, dl_exit = 0.;              // Neighbor and exit distances
  double dtau_scat = -log(ran());            // Optical depth to next scatter
  if (spherical_escape)                      // Initialize escape distance
    dl_exit = spherical_escape_distance(position, direction);

  while (true) {
    // Compute the maximum distance the photon can travel in the cell
    tie(dl_neib, neib_cell) = face_distance(position, direction, current_cell);

    // Check for spherical escape
    if (spherical_escape && dl_exit <= dl_neib) { // Exit sphere before cell
      dl_neib = dl_exit;                     // Set neighbor distance to sphere
      neib_cell = OUTSIDE;                   // Flag for escape condition
    }

    // Check whether the photon is scattered
    const double k_dust = kappa * rho_dust[current_cell];
    const double k_scat = albedo * k_dust;
    k_dust_abs = k_dust - k_scat;            // Save dust extinction coefficient
    const double sigma_eff = x_HI[current_cell] * sigma_HI
                           + x_HeI[current_cell] * sigma_HeI
                           + x_HeII[current_cell] * sigma_HeII;
    k_abs = n_H[current_cell] * sigma_eff + k_dust_abs;
    const double dtau_neib = k_scat * dl_neib; // Optical depth to neighboring cell

    if (dtau_scat <= dtau_neib) {            // A scattering event takes place
      // Calculate the new position from the remaining optical depth
      const double dl_scat = dtau_scat / k_scat; // Actual length traveled
      move_photon(dl_scat);                  // Update position and weight
      if (weight < WEIGHT_DISCARD) {         // Consider the photon packet absorbed
        if (k_dust_abs > 0.)
          dust_weight += weight * k_dust_abs / k_abs; // Assign remaining weight
        weight = 0.;                         // Flag the photon as absorbed
        break;                               // Stop ray-tracing procedure
      }

      // Scatter: Update direction, cameras, etc.
      dust_scatter();                        // Interaction is with dust

      // Reset the escape distance (new direction)
      if (spherical_escape)
        dl_exit = spherical_escape_distance(position, direction);

      // Generate a new random optical depth
      dtau_scat = -log(ran());
      continue;
    }

    // No scattering event
    dtau_scat -= dtau_neib;                  // Subtract traversed optical depth
    if (spherical_escape)
      dl_exit -= dl_neib;                    // Subtract traversed distance

    // Calculate the new position of the photon
    move_photon(dl_neib);                    // Update position and weight
    if (weight < WEIGHT_DISCARD) {           // Consider the photon packet absorbed
      if (k_dust_abs > 0.)
        dust_weight += weight * k_dust_abs / k_abs; // Assign remaining weight
      weight = 0.;                           // Flag the photon as absorbed
      break;                                 // Stop ray-tracing procedure
    }
    current_cell = neib_cell;                // Update the cell index

#ifdef SPHERICAL
    if (neib_cell == INSIDE)                 // Check if the photon is trapped
      break;                                 // Stop ray-tracing procedure
#endif

    if (neib_cell == OUTSIDE)                // Check if the photon escapes
      break;                                 // Stop ray-tracing procedure
  }

  // Calculate global statistics
  if (weight > 0.) {
    #pragma omp atomic
    bin_f_esc[frequency_bin] += weight;      // Global escape fraction
  }
  if (dust_weight > 0.) {
    #pragma omp atomic
    bin_f_abs[frequency_bin] += dust_weight; // Global dust absorption fraction
  }
}
