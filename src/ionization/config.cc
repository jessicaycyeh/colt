/************************
 * ionization/config.cc *
 ************************

 * Module configuration for the simulation.

*/

#include "proto.h"
#include "Ionization.h"
#include "../config.h" // Configuration functions
#include "../io_hdf5.h" // HDF5 read/write functions

void setup_bc03_imf_solar_table(const string& dust_model); // Tabulated BC03 spectral properties
void setup_bpass_imf135_100_table(const string& dust_model); // Tabulated BPASS (135_100) spectral properties
void setup_bpass_chab_100_table(const string& dust_model); // Tabulated BPASS (chab_100) spectral properties

/* Module configuration for the simulation. */
void Ionization::module_config(YAML::Node& file, YAML::Node& node) {
  // Set the dust properties: Opacity, albedo, scattering anisotropy
  if (file["dust_model"])
    load("dust_model", dust_model);          // Dust model: SMC, MW, etc.
  if (dust_model == "") {
    load("albedo_HI", albedo_HI);            // Dust scattering albedos
    load("albedo_HeI", albedo_HeI);
    load("albedo_HeII", albedo_HeII);
    load("cosine_HI", cosine_HI);            // Dust scattering anisotropy parameters
    load("cosine_HeI", cosine_HeI);
    load("cosine_HeII", cosine_HeII);
    load("kappa_HI", kappa_HI);              // Dust opacities [cm^2/g dust]
    load("kappa_HeI", kappa_HeI);
    load("kappa_HeII", kappa_HeII);
  }
  load("f_ion", f_ion);                      // H_II region survival fraction
  load("T_sputter", T_sputter);              // Thermal sputtering cutoff [K]
  if (f_ion < 0. || f_ion > 1.)
    error("f_ion must be in the range [0,1]"); // Validate f_ion range
  if (file["metallicity"]) {
    load("metallicity", metallicity);        // Constant metallicity [mass fraction]
    if (metallicity < 0. || metallicity > 1.)
      error("metallicity must be in the range [0,1]"); // Validate metallicity range
  }
  if (file["dust_to_metal"]) {
    if (file["dust_to_gas"])
      error("Cannot specify both dust_to_metal and dust_to_gas");
    load("dust_to_metal", dust_to_metal);    // Constant dust-to-metal ratio
    if (dust_to_metal < 0. || dust_to_metal > 1.)
      error("dust_to_metal must be in the range [0,1]"); // Validate dust_to_metal range
  }
  if (file["dust_to_gas"]) {
    load("dust_to_gas", dust_to_gas);        // Constant dust-to-gas ratio
    if (dust_to_gas < 0. || dust_to_gas > 1.)
      error("dust_to_gas must be in the range [0,1]"); // Validate dust_to_gas range
  }
  if (file["dust_boost"]) {
    load("dust_boost", dust_boost);          // Optional dust boost factor
    if (dust_boost <= 0.)
      error("dust_boost must be positive");  // Validate dust_boost range
  }

  // Ionization parameters
  if (file["constant_temperature"]) {
    load("constant_temperature", constant_temperature); // Constant temperature: T [K]
    if (constant_temperature < 0.)
      error("constant_temperature must be positive"); // Validate constant_temperature range
  } else if (file["T_floor"]) {
    load("T_floor", T_floor);                // Apply a temperature floor: T [K]
    if (T_floor < 0.)
      error("T_floor must be positive");     // Validate T_floor range
  }
  if (file["neutral_fraction"]) {
    load("neutral_fraction", neutral_fraction); // Constant neutral fraction: x_HI
    if (neutral_fraction < 0. || neutral_fraction > 1.)
      error("neutral_fraction must be in the range [0,1]"); // Validate neutral_fraction range
  }
  load("set_density_from_mass", set_density_from_mass); // Calculate density as mass / volume
  load("read_density_as_mass", read_density_as_mass); // Read density as mass
  if (file["read_hydrogen_fraction"])
    load("read_hydrogen_fraction", read_hydrogen_fraction); // Read mass fraction of hydrogen
  load("read_ionized_fraction", read_ionized_fraction); // Read x_HII from the input file
  load("read_molecular_fraction", read_molecular_fraction); // Read x_H2 from the input file
  load("read_electron_fraction", read_electron_fraction); // Read x_e from the input file

  // Internal simulation flags
  load("source_model", source_model);        // Model for the sources
  if (source_model == "GMC") {
    star_based_emission = true;              // Star-based emission
    read_m_massive_star = true;              // Requires (massive) stellar masses
    read_helium = true;                      // Requires helium states too
    n_bins = 3;                              // Ionizing photons [HI,HeI,HeII]
    global_sigma_HI = {2.99466e-18, 5.68179e-19, 7.97294e-20}; // HI ionizing cross-sections
    global_sigma_HeI = {0., 3.71495e-18, 5.70978e-19}; // HeI ionizing cross-sections
    global_sigma_HeII = {0., 0., 1.06525e-18}; // HeII ionizing cross-sections
    global_epsilon_HI = {0., 0., 0.};        // HI photoheating
    global_epsilon_HeI = {0., 0., 0.};       // HeI photoheating
    global_epsilon_HeII = {0., 0., 0.};      // HeII photoheating
    global_mean_energy = {18.8567*eV, 35.0815*eV, 64.9768*eV}; // Mean energy of each bin [erg]
  } else if (source_model == "MRT") {
    star_based_emission = true;              // Star-based emission
    read_m_init_star = true;                 // Requires initial stellar masses
    read_Z_star = true;                      // Requires stellar metallicities
    read_age_star = true;                    // Requires stellar ages
    read_helium = true;                      // Requires helium states too
    n_bins = 3;                              // Ionizing photons [HI,HeI,HeII]
    global_sigma_HI = {3.3718e-18, 7.89957e-19, 1.09584e-19}; // HI ionizing cross-sections
    global_sigma_HeI = {0., 5.10279e-18, 7.76916e-19}; // HeI ionizing cross-sections
    global_sigma_HeII = {0., 0., 1.42413e-18}; // HeII ionizing cross-sections
    global_epsilon_HI = {0., 0., 0.};        // HI photoheating
    global_epsilon_HeI = {0., 0., 0.};       // HeI photoheating
    global_epsilon_HeII = {0., 0., 0.};      // HeII photoheating
    global_mean_energy = {18.0058*eV, 29.8868*eV, 56.8456*eV}; // Mean energy of each bin [erg]
  } else if (source_model == "BC03-IMF-SOLAR" || source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100") {
    star_based_emission = true;              // Star-based emission
    read_m_init_star = true;                 // Requires initial stellar masses
    if (source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100")
      read_Z_star = true;                    // Requires stellar metallicities
    read_age_star = true;                    // Requires stellar ages
    read_helium = true;                      // Requires helium states too
    n_bins = 3;                              // Ionizing photons [HI,HeI,HeII]
    if (source_model == "BC03-IMF-SOLAR")
      setup_bc03_imf_solar_table(dust_model); // Tabulated BC03 spectral properties
    else if (source_model == "BPASS-IMF-135-100")
      setup_bpass_imf135_100_table(dust_model); // Tabulated BPASS (135_100) spectral properties
    else // (source_model == "BPASS-CHAB-100")
      setup_bpass_chab_100_table(dust_model); // Tabulated BPASS (chab_100) spectral properties
  } else if (source_model == "custom") {
    load("n_bins", n_bins);                  // Number of frequency bins
    if (n_bins <= 0)
      error("n_bins must be > 0");           // Validate n_bins range
    error("The custom source_model is not implemented yet!");
    // load("kappa_dust", kappa_dust);            // Dust opacity [cm^2/g dust]
    // load("albedo", albedo);                    // Dust albedo for scattering vs. absorption
    // load("cosine", cosine);                    // Anisotropy parameter: <μ> for dust scattering
    // if (kappa_dust < 0.)
    //   error("kappa_dust must be >= 0");        // Validate kappa_dust range
    // if (albedo < 0. || albedo > 1.)
    //   error("albedo must be in the range [0,1]"); // Validate albedo range
    // if (cosine < -1. || cosine > 1.)
    //   error("cosine must be in the range [-1,1]"); // Validate cosine range
    // if (fabs(cosine) < 1e-6)
    //   cosine = (cosine < 0.) ? -1e-6 : 1e-6;   // Avoid division by zero
  } else
    error("Unrecognized value for source_model: " + source_model);

  if (star_based_emission) {
    load("j_exp", j_exp);                    // Luminosity boosting exponent
    if (j_exp <= 0. || j_exp > 1.)
      error("j_exp must be in the range (0,1]"); // Validate j_exp range
  }

  // UV background
  if (file["UVB_model"])
    load("UVB_model", UVB_model);            // UV background model: FG11, etc.
  if (file["self_shielding"])
    load("self_shielding", self_shielding);  // Include UVB self-shielding

  // Escape variables
  spherical_escape_config(file, node);       // General spherical escape setup
  load("output_photons", output_photons);    // Output escaped photon packets
  if (output_photons)
    load("photon_file", photon_file);        // Output a separate photon file
  load("output_abundances", output_abundances); // Output abundances
  if (output_abundances)
    load("abundances_file", abundances_file); // Output a separate abundances file
  load("output_photoheating", output_photoheating); // Output photoheating rates
  if (output_photoheating)
    load("photoheating_file", photoheating_file); // Output a separate photoheating file

  // Information about the simulation
  load("n_photons", n_photons);              // Number of photon packets
  load("max_iter", max_iter);                // Maximum number of iterations
  load("max_error", max_error);              // Relative error for convergence
  if (n_photons <= 0)
    error("n_photons must be > 0");          // Validate n_photons range
  if (max_iter <= 0 || max_iter > 100)
    error("max_iter must be in the range [1,100]"); // Validate max_iter range
  if (max_error < 0. || max_error > 1.)
    error("max_error must be in the range [0,1]"); // Validate max_error range

  // Information about the cameras
  camera_config(file, node);                 // General camera setup
  if (have_cameras) {
    load("output_escape_fractions", output_escape_fractions); // Output camera escape fractions
    load("output_bin_escape_fractions", output_bin_escape_fractions); // Output bin escape fractions
    load("output_images", output_images);    // Output surface brightness images
    load("output_bin_images", output_bin_images); // Output bin SB images
    load("output_mcrt_emission", output_mcrt_emission); // Output intrinsic emission without transport (mcrt)
    load("output_mcrt_attenuation", output_mcrt_attenuation); // Output attenuated emission without scattering (mcrt)
    // Check camera output compatibility
    if (!(output_escape_fractions || output_bin_escape_fractions || output_images || output_bin_images))
      root_error("Cameras were requested without any camera output types.");
  } else {
    output_escape_fractions = output_fluxes = output_images = false; // Reset defaults
  }
}
