/*************************
 * ionization/scatter.cc *
 *************************

 * Scattering Event: Scattering angle, optical depth, etc.

*/

#include "proto.h"
#include "photon.h" // Photon packets

double ran(); // Random number (from global seed)
Vec3 isotropic_direction(); // Generate direction from an isotropic distribution
double spherical_escape_distance(Vec3 point, Vec3 direction); // Distance to escape the bounding sphere
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell

static const int ISOTROPIC = 1, DUST_SCAT = 4;
static const double _2pi = 2. * M_PI;
static const double TAU_DISCARD = 32.;       // exp(-23) ~ 1e-10; exp(-32) ~ 1e-14

static double _1_2g, _1pg2_2g, _1mg2_2D, _1mg2_2g, _1mg_2g; // Constants for Henyey-Greenstein dust scattering
#pragma omp threadprivate(_1_2g, _1pg2_2g, _1mg2_2D, _1mg2_2g, _1mg_2g) // Unique to each thread/photon

/* Set the derived dust parameters. */
void set_derived_dust_parameters(const double cosine) {
  // Constants for the Henyey-Greenstein dust scattering phase function
  _1_2g    = 0.5 / cosine;                   // = 1 / (2g)
  _1pg2_2g = _1_2g + 0.5 * cosine;           // = (1+g^2) / (2g) = 1/(2g) + g/2
  _1mg2_2g = _1_2g - 0.5 * cosine;           // = (1-g^2) / (2g) = 1/(2g) - g/2
  _1mg_2g  = _1_2g - 0.5;                    // = (1-g) / (2g) = 1/(2g) - 1/2
  _1mg2_2D = _1mg2_2g * 0.5 * sqrt(_1_2g);   // = (1-g^2) / [2 (2g)^(3/2)]
}

/* Line-of-sight emission calculation. */
void IonPhoton::calculate_LOS_int() {
  for (int camera = 0; camera < n_cameras; ++camera)
    calculate_LOS_int(camera);
}

void IonPhoton::calculate_LOS_int(int camera) {
  // Surface brightness LOS weight: sum( W(mu) * exp(-tau_esc) )
  const double weight_int = 0.5 * source_weight;

  // Bin the photon's position - Only add to bins in the specified range
  const Vec3 r_cam = position - camera_center; // Position relative to camera center
  const double x_cam = inverse_pixel_width * (r_cam.dot(camera_xaxes[camera]) + image_radius);
  const double y_cam = inverse_pixel_width * (r_cam.dot(camera_yaxes[camera]) + image_radius);
  const bool in_image_range = (x_cam >= 0. && x_cam < double(n_pixels) &&
                               y_cam >= 0. && y_cam < double(n_pixels));

  if (in_image_range) {
    const int ix = floor(x_cam);
    const int iy = floor(y_cam);
    if (output_images)
      #pragma omp atomic
      images_int[camera](ix, iy) += weight_int;

    if (output_bin_images) {
      #pragma omp atomic
      bin_images_int[camera](ix, iy, frequency_bin) += weight_int;
    }
  }
}

/* Line-of-sight attenuation calculation. */
void IonPhoton::calculate_LOS_ext() {
  for (int camera = 0; camera < n_cameras; ++camera)
    calculate_LOS_ext(camera);
}

void IonPhoton::calculate_LOS_ext(int camera) {
  int cell = current_cell;                   // Current cell index
  int next_cell;                             // Next cell index
  double tau_esc = -log(weight);             // Optical depth to escape
  double l, l_exit = 0.;                     // Path lengths
  Vec3 point = position;                     // Photon position
  Vec3 k_cam = camera_directions[camera];    // Camera direction
  if (spherical_escape)                      // Initialize escape distance
    l_exit = spherical_escape_distance(point, k_cam);

  while (true) {                             // Ray trace until escape
    // Compute the maximum distance the photon can travel in the cell
    tie(l, next_cell) = face_distance(point, k_cam, cell);

#ifdef SPHERICAL
    if (next_cell == INSIDE)                 // Check if the ray is trapped
      return;                                // No point to continue
#endif

    // Check for spherical escape
    if (spherical_escape) {
      if (l_exit <= l) {                     // Exit sphere before cell
        tau_esc += kappa * rho_dust[cell] * l_exit; // Cumulative LOS optical depth
        if (tau_esc > TAU_DISCARD)           // Early exit condition
          return;                            // No point to continue
        break;                               // Valid tau_esc completion
      }
      l_exit -= l;                           // Remaining distance to exit
    }

    // Cumulative LOS optical depth (dust)
    tau_esc += kappa * rho_dust[cell] * l;

    // Discard photons from the calculation if tau_esc > TAU_DISCARD
    if (tau_esc > TAU_DISCARD)
      return;                                // No point to continue

    // Calculate the new position of the photon
    point += k_cam * l;

    if (next_cell == OUTSIDE)                // Check if the photon escapes
      break;
    cell = next_cell;                        // Update the cell index
  }

  // Surface brightness LOS weight: sum( W(mu) * exp(-tau_esc) )
  const double weight_ext = 0.5 * exp(-tau_esc);

  // Calculate the escape fraction for each camera
  if (output_escape_fractions)
    #pragma omp atomic
    f_escs_ext[camera] += weight_ext;

  // Bin the photon's position - Only add to bins in the specified range
  const Vec3 r_cam = position - camera_center; // Position relative to camera center
  const double x_cam = inverse_pixel_width * (r_cam.dot(camera_xaxes[camera]) + image_radius);
  const double y_cam = inverse_pixel_width * (r_cam.dot(camera_yaxes[camera]) + image_radius);
  const bool in_image_range = (x_cam >= 0. && x_cam < double(n_pixels) &&
                               y_cam >= 0. && y_cam < double(n_pixels));

  if (output_bin_escape_fractions) {
    #pragma omp atomic
    bin_f_escs_ext[camera][frequency_bin] += weight_ext;
  }
  if (in_image_range) {
    const int ix = floor(x_cam);
    const int iy = floor(y_cam);
    if (output_images)
      #pragma omp atomic
      images_ext[camera](ix, iy) += weight_ext;

    if (output_bin_images) {
      #pragma omp atomic
      bin_images_ext[camera](ix, iy, frequency_bin) += weight_ext;
    }
  }
}

/* Line-of-sight calculation via next event estimation. */
void IonPhoton::calculate_LOS(int scatter_type) {
  for (int camera = 0; camera < n_cameras; ++camera)
    calculate_LOS(scatter_type, camera);
}

void IonPhoton::calculate_LOS(int scatter_type, int camera) {
  int cell = current_cell;                   // Current cell index
  int next_cell;                             // Next cell index
  double tau_esc = -log(weight);             // Optical depth to escape
  double sigma_eff;                          // Ionization cross-section
  double l, l_exit = 0.;                     // Path lengths
  Vec3 point = position;                     // Photon position
  Vec3 k_cam = camera_directions[camera];    // Camera direction
  if (spherical_escape)                      // Initialize escape distance
    l_exit = spherical_escape_distance(point, k_cam);

  while (true) {                             // Ray trace until escape
    // Compute the maximum distance the photon can travel in the cell
    tie(l, next_cell) = face_distance(point, k_cam, cell);

#ifdef SPHERICAL
    if (next_cell == INSIDE)                 // Check if the ray is trapped
      return;                                // No point to continue
#endif

    // Check for spherical escape
    if (spherical_escape) {
      if (l_exit <= l) {                     // Exit sphere before cell
        l = l_exit;                          // Set neighbor distance to sphere
        next_cell = OUTSIDE;                 // Flag for escape condition
      }
      l_exit -= l;                           // Remaining distance to exit
    }

    // Cumulative LOS optical depth (ionization + dust)
    sigma_eff = x_HI[cell] * sigma_HI + x_HeI[cell] * sigma_HeI + x_HeII[cell] * sigma_HeII;
    tau_esc += (n_H[cell] * sigma_eff + kappa * rho_dust[cell]) * l;

    // Discard photons from the calculation if tau_esc > TAU_DISCARD
    if (tau_esc > TAU_DISCARD)
      return;                                // No point to continue

    // Calculate the new position of the photon
    point += k_cam * l;                      // Update photon position

    if (next_cell == OUTSIDE)                // Check if the photon escapes
      break;
    cell = next_cell;                        // Update the cell index
  }

  // Surface brightness LOS weight: sum( W(mu) * exp(-tau_esc) )
  double mu, weight_cam;
  switch (scatter_type) {                    // Determine the scattering weight
    case ISOTROPIC:                          // Isotropic scattering
      weight_cam = 0.5;
      break;
    case DUST_SCAT:                          // Here W = 1/2 (1 - g^2) / (1 + g^2 - 2gμ)^(3/2)
      mu = direction.dot(k_cam);
      weight_cam = _1mg2_2D * pow(_1pg2_2g - mu, -1.5);
      break;
    default:
      weight_cam = 0.;
      error("Unrecognized scatter type in camera calculation.");
  }

  // Combine the phase and optical depth weights
  weight_cam *= exp(-tau_esc);

  // Calculate the escape fraction for each camera
  if (output_escape_fractions)
    #pragma omp atomic
    f_escs[camera] += weight_cam;

  // Note: Convert the position to image coordinates *before* casting
  // to an int to avoid overflow and entering UB territory

  // Bin the photon's position - Only add to bins in the specified range
  const Vec3 r_cam = position - camera_center; // Position relative to camera center
  const double x_cam = inverse_pixel_width * (r_cam.dot(camera_xaxes[camera]) + image_radius);
  const double y_cam = inverse_pixel_width * (r_cam.dot(camera_yaxes[camera]) + image_radius);
  const bool in_image_range = (x_cam >= 0. && x_cam < double(n_pixels) &&
                               y_cam >= 0. && y_cam < double(n_pixels));

  if (output_bin_escape_fractions) {
    #pragma omp atomic
    bin_f_escs[camera][frequency_bin] += weight_cam;
  }
  if (in_image_range) {
    const int ix = floor(x_cam);
    const int iy = floor(y_cam);
    if (output_images)
      #pragma omp atomic
      images[camera](ix, iy) += weight_cam;

    if (output_bin_images) {
      #pragma omp atomic
      bin_images[camera](ix, iy, frequency_bin) += weight_cam;
    }
  }
}

/* Generate random angles from an anisotropic distribution (dust). */
static Vec3 anisotropic_direction_HG(Vec3 k) {
  // Henyey-Greenstein phase function for <μ>: ζ = random number in [0,1]
  const double xiS = _1mg2_2g / (_1mg_2g + ran()); // ξ = (1-g^2) / (1-g+2gζ)
  const double muS = _1pg2_2g - _1_2g * xiS * xiS; // μ = (1+g^2-ξ^2) / (2g)

  // Precompute sin and cos multiplied by sqrt(1-mu^2)
  const double phi = _2pi * ran();           // Arbitrary azimuthal angle
  const double sqrt_1muS2 = sqrt(1. - muS * muS);
  const double sin_phi = sqrt_1muS2 * sin(phi);
  const double cos_phi = sqrt_1muS2 * cos(phi);
  const double zS = sqrt(1. - k.z * k.z);    // sqrt(kx^2+ky^2) = sqrt(1-kz^2)

  // Outgoing direction in terms of the angles mu and phi
  Vec3 k_out {
    muS * k.x + (k.y * cos_phi + k.x * k.z * sin_phi) / zS,
    muS * k.y - (k.x * cos_phi - k.y * k.z * sin_phi) / zS,
    muS * k.z - sin_phi * zS,
  };
  k_out.normalize();                         // Normalize to avoid error
  return k_out;
}

// /* Isolate everything that happens during the ionization scattering event. */
// void IonPhoton::ionization_scatter() {
//   // Camera calculations using the Isotropic phase function
//   calculate_LOS(ISOTROPIC);

//   // Outgoing direction from the anisotropic Henyey-Greenstein phase function
//   direction = isotropic_direction();
// }

/* Isolate everything that happens during the dust scattering event. */
void IonPhoton::dust_scatter() {
  // Camera calculations using the dust phase function
  calculate_LOS(DUST_SCAT);

  // Outgoing direction from the anisotropic Henyey-Greenstein phase function
  direction = anisotropic_direction_HG(direction);
}
