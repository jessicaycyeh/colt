/****************************
 * ionization/ionization.cc *
 ****************************

 * Driver: Assign photon packets for radiative transfer, etc.

*/

#include "proto.h"
#include "Ionization.h"
#include "../timing.h" // Timing functionality
#include "photon.h" // Photon packets
#include <chrono>

extern Timer mcrt_timer; // Clock timing
extern Timer reduce_timer; // MPI timing

void print_ionization_observables(); // Post-calculation printing
bool avoid_cell(const int cell); // Avoid calculations for certain cells
Vec3 cell_center(const int cell); // Center of the cell

/* Recombination coefficient (HII Case B) - Units: cm^3/s */
static inline double alpha_B_HII(const double T) {
  // Case B recombination coefficient - Units: cm^3/s
  // α_B(T) is from Hui & Gnedin (1997) good to 0.7% accuracy from 1 K < T < 10^9 K
  const double lambda = 315614. / T;         // 2 T_i/T where T_i = 157,807 K
  return 2.753e-14 * pow(lambda, 1.5) / pow(1. + pow(lambda / 2.74, 0.407), 2.242);
}

/* Recombination coefficient (HeII Case B) - Units: cm^3/s */
static inline double alpha_B_HeII(const double T) {
  const double lambda = 570670. / T;         // 2 T_i/T where T_i = 285,335 K
  return 1.26e-14 * pow(lambda, 0.75);
}

/* Diaelectric recombination coefficient (HeII Case B) - Units: cm^3/s */
static inline double alpha_D_HeII(const double T) {
  return 1.9e-3 * pow(T, -1.5) * exp(-470000./T) * (1. + 0.3 * exp(-94000./T));
}

/* Recombination coefficient (HeIII Case B) - Units: cm^3/s */
static inline double alpha_B_HeIII(const double T) {
  const double lambda = 1263030. / T;        // 2 T_i/T where T_i = 631,515 K
  return 5.506e-14 * pow(lambda, 1.5) / pow(1. + pow(lambda / 2.74, 0.407), 2.242);
}

static inline void print_top() {
  cout << "\n+----------+------------+------------+------------+"
       << "\n| Progress |  Elapsed   |  Estimate  |  Remaining |"
       << "\n+----------+------------+------------+------------+";
}

static inline void print_bottom() {
  cout << "\n+----------+------------+------------+------------+" << endl;
}

/* Print the simulation progress and remaining time estimate. */
static inline void print_progress(const std::chrono::system_clock::time_point start, const int finished, const int total) {
  auto time_elapsed = std::chrono::system_clock::now() - start;
  auto time_remaining = time_elapsed * double(total - finished) / double(finished);
  long long seconds_elapsed = std::chrono::duration_cast<std::chrono::seconds>(time_elapsed).count();
  long long seconds_remaining = std::chrono::duration_cast<std::chrono::seconds>(time_remaining).count();
  long long seconds_estimate = seconds_elapsed + seconds_remaining;
  printf("\n|  %5.1f%%  |  %02lld:%02lld:%02lld  |  %02lld:%02lld:%02lld  |  %02lld:%02lld:%02lld  |",
         100. * double(finished) / double(total),
         seconds_elapsed / 3600, (seconds_elapsed / 60) % 60, seconds_elapsed % 60,
         seconds_estimate / 3600, (seconds_estimate / 60) % 60, seconds_estimate % 60,
         seconds_remaining / 3600, (seconds_remaining / 60) % 60, seconds_remaining % 60);
}

/* Move vector elements within a vector over disjoint ranges. */
template <typename T>
static inline void disjoint_move(vector<T>& vec, const int last, const int move) {
  #pragma omp parallel for
  for (int i = n_escaped; i < last; ++i)
    vec[i+move] = vec[i];                    // Move non-overlapping elements
}

/* Move absorbed photons in the info vectors. */
static inline void move_absorbed_photons(const int dest) {
  const int offset = dest - n_escaped;       // Memory offset for absorbed photons
  if (offset == 0 || n_absorbed == 0)
    return;                                  // No need to move absorbed photons
  if (offset < 0 || n_absorbed < 0 || dest + n_absorbed > n_photons)
    error("Attemping to move absorbed photons out of expected bounds: n_absorbed = " +
          to_string(n_absorbed) + ", dest = " + to_string(dest) + ", n_escaped = " +
          to_string(n_escaped) + ", n_photons = " + to_string(n_photons));
  // Move range: [n_escaped, n_escaped + n_absorbed) -> [dest, dest + n_absorbed)
  // These overlap if n_absorbed > offset, so only move non-overlapping elements
  const int last = (n_absorbed > offset) ? dest : n_escaped + n_absorbed;
  const int move = (n_absorbed > offset) ? n_absorbed : offset;
  disjoint_move(source_ids, last, move);
  if (n_ion_source_types > 1)
    disjoint_move(source_types, last, move);
  disjoint_move(source_weights, last, move);
  disjoint_move(freq_bins, last, move);
  disjoint_move(weights, last, move);
  disjoint_move(dust_weights, last, move);
  disjoint_move(positions, last, move);
  disjoint_move(directions, last, move);
}

/* Swap photons in the info vectors. */
static inline void swap_photons(const int i, const int j) {
  std::swap(source_ids[i], source_ids[j]);
  if (n_ion_source_types > 1)
    std::swap(source_types[i], source_types[j]);
  std::swap(source_weights[i], source_weights[j]);
  std::swap(freq_bins[i], freq_bins[j]);
  std::swap(weights[i], weights[j]);
  std::swap(dust_weights[i], dust_weights[j]);
  std::swap(positions[i], positions[j]);
  std::swap(directions[i], directions[j]);
}

/* Add photon information to the output buffers. */
static inline void add_to_output_buffers(const struct IonPhoton& p, const int index) {
  source_ids[index] = p.source_id;
  if (n_ion_source_types > 1)
    source_types[index] = p.source_type;
  source_weights[index] = p.source_weight;
  freq_bins[index] = p.frequency_bin;
  weights[index] = p.weight;
  dust_weights[index] = p.dust_weight;
  positions[index] = p.position;
  directions[index] = p.direction;
}

/* Sort escaped photons to the beginning and absorbed photons to the end. */
static void sort_photons() {
  // Count the number of escaped and absorbed photons
  n_escaped = n_absorbed = 0;
  #pragma omp parallel for reduction(+:n_escaped) reduction(+:n_absorbed)
  for (int i = 0; i < n_finished; ++i) {
    if (weights[i] > 0.)
      ++n_escaped;                           // Escaped photon
    else
      ++n_absorbed;                          // Absorbed photon
  }

  // Sort the escaped and absorbed photons
  for (int i_esc = 0, i_abs = 0; i_esc < n_escaped; ++i_esc) {
    if (weights[i_esc] > 0.)
      continue;                              // Valid escaped photon
    do {
      ++i_abs;                               // Search from end of list
    } while (weights[n_finished-i_abs] <= 0.); // Find next escaped photon
    swap_photons(i_esc, n_finished-i_abs);   // Swap esc/abs photons
  }
}

/* All tasks have a static assignment of equal work. */
static void equal_workers() {
  if (root)
    print_top();                             // Progress top
  const auto start = std::chrono::system_clock::now(); // Reference time
  int work = n_photons / n_ranks;            // Work assignment
  if (rank < n_photons - work * n_ranks)
    ++work;                                  // Assign remaining work
  const int checks = pow(double(work+1), 0.2); // Number of time checks
  const int interval = work / checks;        // Interval between updates

  #pragma omp parallel for schedule(dynamic)
  for (int i = 0; i < work; ++i) {
    // Create photon packet and perform ray tracing
    auto p = IonPhoton();
    p.ray_trace();

    // Print completed progress and remaining time estimate
    int index;                               // Photon array index
    #pragma omp atomic capture
    index = n_finished++;                    // Update finished counter
    if (output_photons)
      add_to_output_buffers(p, index);       // Populate output buffers
    if (root && ++index % interval == 0)     // Index -> print counter
      print_progress(start, index, work);
  }
  if (root)
    print_bottom();                          // Progress bottom
}

/* Convenience function to MPI_Reduce vectors to root. */
template <class T>
static inline void mpi_reduce(vector<T>& vs, MPI_Op op = MPI_SUM) {
  for (auto& v : vs) {
    if (root)
      MPI_Reduce(MPI_IN_PLACE, v.data(), v.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
    else
      MPI_Reduce(v.data(), v.data(), v.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
  }
}

/* Convenience function to MPI_Reduce vector to root. */
static inline void mpi_reduce(vector<double>& v, MPI_Op op = MPI_SUM) {
  if (root)
    MPI_Reduce(MPI_IN_PLACE, v.data(), v.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
  else
    MPI_Reduce(v.data(), v.data(), v.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
}

/* Convenience function to MPI_Bcast vector from root. */
static inline void mpi_bcast(vector<double>& v) {
  MPI_Bcast(v.data(), v.size(), MPI_DOUBLE, ROOT, MPI_COMM_WORLD);
}

/* Convenience function to MPI_Bcast scalar from root. */
static inline void mpi_bcast(double& v) {
  MPI_Bcast(&v, 1, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);
}

// MPI collection helper struct
struct RecvInfo {
  void *buffer;                              // Address of receive buffer
  int count;                                 // Number of elements in buffer
  MPI_Datatype datatype;                     // Datatype of buffer elements
  int source;                                // Rank of the source
  int tag;                                   // Message tag
};

/* Convenience function to MPI_Recv data to root. */
static inline void mpi_recv(struct RecvInfo& recv_info) {
  MPI_Recv(recv_info.buffer, recv_info.count, recv_info.datatype,
           recv_info.source, recv_info.tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

/* Set a vector to zeros. */
static inline void clear(vector<double>& v) {
  #pragma omp parallel for
  for (size_t i = 0; i < v.size(); ++i)
    v[i] = 0.;                               // Clear values
}

/* Set a vector to zeros. */
template <class T>
static inline void clear(vector<T>& vs) {
  for (auto& v : vs) {
    #pragma omp parallel for
    for (size_t i = 0; i < v.size(); ++i)
      v[i] = 0.;                             // Clear values
  }
}

/* Ensure unique MPI message tags for safety. */
enum {
  TAG_ESI, TAG_ASI,                          // Emission cell or star index
  TAG_EST, TAG_AST,                          // Emission type specifier
  TAG_ESW, TAG_ASW,                          // Photon weight at emission
  TAG_EB,  TAG_AB,                           // Frequency at escape [code_freq]
  TAG_EW,  TAG_AW,                           // Photon weight at escape
  TAG_EDW, TAG_ADW,                          // Weight removed by dust absorption
  TAG_EP,  TAG_AP,                           // Position at escape [cm]
  TAG_ED,  TAG_AD                            // Direction at escape
};

/* Convenience function to MPI_Send photon info to root. */
static void mpi_collect_photons() {
  // Gather the number of escaped and absorbed photons to root
  vector<int> rank_esc, rank_abs;            // Number of esc/abs photons
  if (root) {
    rank_esc.resize(n_ranks);                // Allocate space for esc counts
    rank_abs.resize(n_ranks);                // Allocate space for abs counts
  }
  MPI_Gather(&n_escaped, 1, MPI_INT, rank_esc.data(), 1, MPI_INT, ROOT, MPI_COMM_WORLD);
  MPI_Gather(&n_absorbed, 1, MPI_INT, rank_abs.data(), 1, MPI_INT, ROOT, MPI_COMM_WORLD);

  if (root) {
    vector<int> ind_esc, ind_abs;            // Index ranges for rank collection
    ind_esc.resize(n_ranks+1);               // Allocate space for esc indices
    ind_abs.resize(n_ranks+1);               // Allocate space for abs indices
    ind_esc[0] = 0;                          // Start root index at zero
    for (int i = 0; i < n_ranks; ++i)
      ind_esc[i+1] = ind_esc[i] + rank_esc[i];
    ind_abs[0] = ind_esc[n_ranks];           // Start root index at end of esc
    for (int i = 0; i < n_ranks; ++i)
      ind_abs[i+1] = ind_abs[i] + rank_abs[i];

    // Move root absorbed photons to correct global position
    move_absorbed_photons(ind_abs[0]);

    // Build a master list of buffers and receive counts
    vector<struct RecvInfo> buffers;
    for (int i = 1; i < n_ranks; ++i) {
      // Receive escaped photon info
      const int i_esc = ind_esc[i];
      const int n_esc = ind_esc[i+1] - ind_esc[i];
      if (n_esc > 0) {
        buffers.push_back({&source_ids[i_esc], n_esc, MPI_INT, i, TAG_ESI});
        if (n_ion_source_types > 1)
          buffers.push_back({&source_types[i_esc], n_esc, MPI_INT, i, TAG_EST});
        buffers.push_back({&source_weights[i_esc], n_esc, MPI_DOUBLE, i, TAG_ESW});
        buffers.push_back({&freq_bins[i_esc], n_esc, MPI_INT, i, TAG_EB});
        buffers.push_back({&weights[i_esc], n_esc, MPI_DOUBLE, i, TAG_EW});
        buffers.push_back({&dust_weights[i_esc], n_esc, MPI_DOUBLE, i, TAG_EDW});
        buffers.push_back({&positions[i_esc], 3*n_esc, MPI_DOUBLE, i, TAG_EP});
        buffers.push_back({&directions[i_esc], 3*n_esc, MPI_DOUBLE, i, TAG_ED});
      }

      // Receive absorbed photon info
      const int i_abs = ind_abs[i];
      const int n_abs = ind_abs[i+1] - ind_abs[i];
      if (n_abs > 0) {
        buffers.push_back({&source_ids[i_abs], n_abs, MPI_INT, i, TAG_ASI});
        if (n_ion_source_types > 1)
          buffers.push_back({&source_types[i_abs], n_abs, MPI_INT, i, TAG_AST});
        buffers.push_back({&source_weights[i_abs], n_abs, MPI_DOUBLE, i, TAG_ASW});
        buffers.push_back({&freq_bins[i_abs], n_abs, MPI_INT, i, TAG_AB});
        buffers.push_back({&weights[i_abs], n_abs, MPI_DOUBLE, i, TAG_AW});
        buffers.push_back({&dust_weights[i_abs], n_abs, MPI_DOUBLE, i, TAG_ADW});
        buffers.push_back({&positions[i_abs], 3*n_abs, MPI_DOUBLE, i, TAG_AP});
        buffers.push_back({&directions[i_abs], 3*n_abs, MPI_DOUBLE, i, TAG_AD});
      }
    }

    // Receive data from all buffers
    const int n_buffers = buffers.size();    // Number of buffers
    #pragma omp parallel for
    for (int i = 0; i < n_buffers; ++i)
      mpi_recv(buffers[i]);                  // Helper to receive data

    // Update global photon counts
    n_escaped = ind_esc[n_ranks] - ind_esc[0];
    n_absorbed = ind_abs[n_ranks] - ind_abs[0];
  } else {
    // Send escaped photon info
    if (n_escaped > 0) {
      MPI_Send(source_ids.data(), n_escaped, MPI_INT, ROOT, TAG_ESI, MPI_COMM_WORLD);
      if (n_ion_source_types > 1)
        MPI_Send(source_types.data(), n_escaped, MPI_INT, ROOT, TAG_EST, MPI_COMM_WORLD);
      MPI_Send(source_weights.data(), n_escaped, MPI_DOUBLE, ROOT, TAG_ESW, MPI_COMM_WORLD);
      MPI_Send(freq_bins.data(), n_escaped, MPI_INT, ROOT, TAG_EB, MPI_COMM_WORLD);
      MPI_Send(weights.data(), n_escaped, MPI_DOUBLE, ROOT, TAG_EW, MPI_COMM_WORLD);
      MPI_Send(dust_weights.data(), n_escaped, MPI_DOUBLE, ROOT, TAG_EDW, MPI_COMM_WORLD);
      MPI_Send(positions.data(), 3*n_escaped, MPI_DOUBLE, ROOT, TAG_EP, MPI_COMM_WORLD);
      MPI_Send(directions.data(), 3*n_escaped, MPI_DOUBLE, ROOT, TAG_ED, MPI_COMM_WORLD);
    }

    // Send absorbed photon info
    if (n_absorbed > 0) {
      MPI_Send(&source_ids[n_escaped], n_absorbed, MPI_INT, ROOT, TAG_ASI, MPI_COMM_WORLD);
      if (n_ion_source_types > 1)
        MPI_Send(&source_types[n_escaped], n_absorbed, MPI_INT, ROOT, TAG_AST, MPI_COMM_WORLD);
      MPI_Send(&source_weights[n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_ASW, MPI_COMM_WORLD);
      MPI_Send(&freq_bins[n_escaped], n_absorbed, MPI_INT, ROOT, TAG_AB, MPI_COMM_WORLD);
      MPI_Send(&weights[n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_AW, MPI_COMM_WORLD);
      MPI_Send(&dust_weights[n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_ADW, MPI_COMM_WORLD);
      MPI_Send(&positions[n_escaped], 3*n_absorbed, MPI_DOUBLE, ROOT, TAG_AP, MPI_COMM_WORLD);
      MPI_Send(&directions[n_escaped], 3*n_absorbed, MPI_DOUBLE, ROOT, TAG_AD, MPI_COMM_WORLD);
    }
  }
}

/* MPI reductions of all output data. */
static void reduce_ionization() {
  if (root)
    cout << "\nIonization: MPI Reductions ..." << endl;

  // If we do not synchronize all ranks, the reductions may time out
  MPI_Barrier(MPI_COMM_WORLD);

  reduce_timer.start();

  // Global statistics
  mpi_reduce(bin_f_esc);                     // Global bin escape fraction
  mpi_reduce(bin_f_abs);                     // Global bin dust absorption fraction

  // Escape fractions [fraction]
  if (output_escape_fractions)
    mpi_reduce(f_escs);                      // Apply f_esc reductions

  // Bin escape fractions [fraction]
  if (output_bin_escape_fractions)
    mpi_reduce(bin_f_escs);                  // Apply bin_f_esc reductions

  // Surface brightness images [photons/s/cm^2]
  if (output_images)
    mpi_reduce(images);                      // Apply image reductions

  // Bin SB images [photons/s/cm^2]
  if (output_bin_images)
    mpi_reduce(bin_images);                  // Apply bin_image reductions

  // Intrinsic cameras (mcrt)
  if (output_mcrt_emission) {
    // Surface brightness images [photons/s/cm^2]
    if (output_images)
      mpi_reduce(images_int);                // Apply image reductions

    // Bin SB images [photons/s/cm^2]
    if (output_bin_images)
      mpi_reduce(bin_images_int);            // Apply bin_image reductions
  }

  // Attenuation cameras (mcrt)
  if (output_mcrt_attenuation) {
    // Escape fractions [fraction]
    if (output_escape_fractions)
      mpi_reduce(f_escs_ext);                // Apply f_esc reductions

    // Bin escape fractions [fraction]
    if (output_bin_escape_fractions)
      mpi_reduce(bin_f_escs_ext);            // Apply bin_f_esc reductions

    // Surface brightness images [photons/s/cm^2]
    if (output_images)
      mpi_reduce(images_ext);                // Apply image reductions

    // Bin SB images [photons/s/cm^2]
    if (output_bin_images)
      mpi_reduce(bin_images_ext);            // Apply bin_image reductions
  }

  // Absorbed and escaped photon info
  if (output_photons)
    mpi_collect_photons();                   // Collect photons to root

  // Photoionization integrals
  mpi_reduce(rate_HI);
  mpi_reduce(rate_HeI);
  mpi_reduce(rate_HeII);

  // Photoheating rates
  if (output_photoheating)
    mpi_reduce(G_ion);

  reduce_timer.stop();
}

/* Convert observables to the correct units. */
void Ionization::correct_units() {
  cout << "\nIonization: Converting to physical units ..." << endl;

  // Escape fractions [fraction]
  f_esc = omp_sum(bin_f_esc);                // Global escape fraction
  f_abs = omp_sum(bin_f_abs);                // Global dust absorption fraction
  #pragma omp parallel for
  for (int i = 0; i < n_bins; ++i) {
    bin_f_esc[i] *= Ndot_tot / bin_Ndot_tot[i]; // Global bin escape fraction
    bin_f_abs[i] *= Ndot_tot / bin_Ndot_tot[i]; // Global bin dust absorption fraction
  }
  if (output_escape_fractions)
    rescale(f_escs, 2.);                     // Apply phase function correction

  // Bin escape fractions [fraction]
  if (output_bin_escape_fractions) {
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera)
      for (int i = 0; i < n_bins; ++i)
        bin_f_escs[camera][i] *= 2. * Ndot_tot / bin_Ndot_tot[i];
  }

  // Surface brightness images [photons/s/cm^2]
  const double to_image_units = 2. * Ndot_tot / pixel_area; // 2 Ndot / dA
  if (output_images)
    rescale(images, to_image_units);         // Apply image correction

  // Bin SB images [photons/s/cm^2]
  if (output_bin_images)
    rescale(bin_images, to_image_units);     // Apply bin_image correction

  // Intrinsic cameras (mcrt)
  if (output_mcrt_emission) {
    // Surface brightness images [photons/s/cm^2]
    if (output_images)
      rescale(images_int, to_image_units);   // Apply image correction

    // Bin SB images [photons/s/cm^2]
    if (output_bin_images)
      rescale(bin_images_int, to_image_units); // Apply bin_image correction
  }

  // Attenuation cameras (mcrt)
  if (output_mcrt_attenuation) {
    // Escape fractions [fraction]
    if (output_escape_fractions)
      rescale(f_escs_ext, 2.);               // Apply phase function correction

    // Bin escape fractions [fraction]
    if (output_bin_escape_fractions) {
      #pragma omp parallel for
      for (int camera = 0; camera < n_cameras; ++camera)
        for (int i = 0; i < n_bins; ++i)
          bin_f_escs_ext[camera][i] *= 2. * Ndot_tot / bin_Ndot_tot[i];
    }

    // Surface brightness images [photons/s/cm^2]
    if (output_images)
      rescale(images_ext, to_image_units);   // Apply image correction

    // Bin SB images [photons/s/cm^2]
    if (output_bin_images)
      rescale(bin_images_ext, to_image_units); // Apply bin_image correction
  }

  // Photoionization integrals (convert to photons/s)
  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i) {
    if (avoid_cell(i))
      continue;
#ifdef CARTESIAN
    const double Ndot_V = Ndot_tot / dV;     // Normalizations (constant volume)
#else
    const double Ndot_V = Ndot_tot / V[i];   // Normalizations
#endif
    rate_HI[i] *= Ndot_V;                    // Photoionization (convert to photons/s/H)
    rate_HeI[i] *= Ndot_V;
    rate_HeII[i] *= Ndot_V;
    if (output_photoheating)
      G_ion[i] *= Ndot_tot * n_H[i];         // Photoheating (convert to erg/s)
  }
}

/* Calculate the self-shielding factor based on the hydrogen number density [cm^-3]. */
inline double Ionization::self_shielding_factor(const double nH) {
  if (!self_shielding)
    return 1.;                               // No self-shielding

  double SS_factor = SS_f * pow(1. + nH / pow(10., SS_n0), SS_alpha2)
    + (1. - SS_f) * pow(1. + pow(nH / pow(10., SS_n0), SS_beta), SS_alpha1);

  if (SS_factor > 1.)
    error("UV background self-shielding factor > 1!");

  return 1. - SS_xi * (1. - SS_factor);      // Allow smooth rescaling
}

/* Photoionization equilibrium calculation to update abundances. */
void Ionization::equilibrium_update(const bool active = true) {
  const double X_prim = 0.76;                // Primordial mass fraction of hydrogen
  const double yHe = (1. - X_prim) / (4. * X_prim); // Conversion to helium (Note: Ignores metals)
  double V_tot = 0.;                         // Total volume
  double V_HI = 0.;                          // Volume weighted values
  double V_HeI = 0.;
  double V_HeII = 0.;
  double m_tot = 0.;                         // Total mass
  double m_HI = 0.;                          // Mass weighted values
  double m_HeI = 0.;
  double m_HeII = 0.;
  double r_HII = 0.;                         // Recombination events per second
  double r_HeII = 0.;
  double r_HeIII = 0.;
  double c_HI = 0.;                          // Collisional ionizations per second
  double c_HeI = 0.;
  double c_HeII = 0.;
  #pragma omp parallel for reduction(+:V_tot) reduction(+:V_HI) reduction(+:V_HeI) reduction(+:V_HeII) \
                           reduction(+:m_tot) reduction(+:m_HI) reduction(+:m_HeI) reduction(+:m_HeII) \
                           reduction(+:r_HII) reduction(+:r_HeII) reduction(+:r_HeIII) \
                           reduction(+:c_HI) reduction(+:c_HeI) reduction(+:c_HeII)
  for (int i = 0; i < n_cells; ++i) {
    if (avoid_cell(i) || (spherical_escape &&
       (cell_center(i) - escape_center).dot() >= escape_radius2)) {
      x_HI[i] = 0.;                          // Set cell to be fully ionized
      x_HII[i] = 1.;
      x_HeI[i] = 0.;
      x_HeII[i] = 0.;
      // x_HeIII[i] = yHe;
      x_e[i] = 1. + 2. * yHe;
      continue;                              // Cell is not relevant
    }

#ifdef CARTESIAN
    const double V_cell = dV;                // Constant cell volume [cm^3]
#else
    const double V_cell = V[i];              // Cell volume [cm^3]
#endif
    const double m_cell = rho[i] * V_cell;   // Cell mass [g]
    const double nH = n_H[i];                // Hydrogen number density [cm^-3]
    double xHI, xHII, xHeI, xHeII, xHeIII, xe, ne; // x_X = n_X / n_H
    const double fac_xH2 = read_molecular_fraction ? 1. - 2. * x_H2[i] : 1.; // H2 correction factor
    if (fac_xH2 < 0. || fac_xH2 > 1.)
      error("Molecular fraction is out of range: 1 - 2 x_H2 = " + to_string(fac_xH2));
    double xe_prev = 1.;                     // Track previous value
    int count = 0;                           // Number of iterations

    // Recombination rate coefficients (Case B) [cm^3/s]
    const double aHII = alpha_B_HII(T[i]);   // H+   + e-  ->  H + γ
    const double aHeII = alpha_B_HeII(T[i]); // He+  + e-  ->  He + γ
    const double aHeIII = alpha_B_HeIII(T[i]); // He++ + e-  ->  He+ + γ
    const double aD = alpha_D_HeII(T[i]);    // Dielectric recombination (HeII)

    // Collisional ionization rate coefficients [cm^3/s] (Cen 1992)
    const double T_fac = sqrt(T[i]) / (1. + sqrt(T[i] * 1e-5)); // Common factor
    const double bHI = (T[i] > 5e3) ? 5.85e-11 * T_fac * exp(-157809.1/T[i]) : 0.;  // H    + e-  ->  H+   + 2e-
    const double bHeI = (T[i] > 8e3) ? 2.38e-11 * T_fac * exp(-285335.4/T[i]) : 0.; // He   + e-  ->  He+  + 2e-
    const double bHeII = (T[i] > 1e4) ? 5.68e-12 * T_fac * exp(-631515./T[i]) : 0.; // He+  + e-  ->  He++ + 2e-

    // Photoionization integrals
    const double SS_factor = self_shielding_factor(nH);
    const double gHI = rate_HI[i] + SS_factor * UVB_rate_HI;
    const double gHeI = rate_HeI[i] + SS_factor * UVB_rate_HeI;
    const double gHeII = rate_HeII[i] + SS_factor * UVB_rate_HeII;
    double gHIne = gHI / nH;                 // Divided by n_e
    double gHeIne = gHeI / nH;
    double gHeIIne = gHeII / nH;
    if (output_photoheating) {               // UVB photoheating
      const double UVB_heat_eff = x_HI[i] * UVB_heat_HI
                                + x_HeI[i] * UVB_heat_HeI
                                + x_HeII[i] * UVB_heat_HeII;
      G_ion[i] += SS_factor * V_cell * n_H[i] * UVB_heat_eff; // Total rate [erg/s]
    }

    // Evaluate number densities iteratively, see Katz, Weinberg, Hernquist (1996)
    if (active) {                            // Photoionization equilibrium
      while (true) {
        xHI = aHII * fac_xH2 / (aHII + bHI + gHIne); // Equation (33)
        xHII = fac_xH2 - xHI;                // Equation (34) : 1 - x_HI - 2 x_H2
        if (bHeI + gHeIne < 1e-60) {         // No ionization
          xHeI = yHe;
          xHeII = 0.;
          xHeIII = 0.;
        } else {
          xHeII = yHe / (1. + (aHeII + aD) / (bHeI + gHeIne) + (bHeII + gHeIIne) / aHeIII); // Equation (35)
          xHeI = xHeII * (aHeII + aD) / (bHeI + gHeIne); // Equation (36)
          xHeIII = xHeII * (bHeII + gHeIIne) / aHeIII; // Equation (37)
        }
        xe = xHII + xHeII + 2. * xHeIII;     // Equation (38)
        xe = 0.5 * (xe + xe_prev);           // Stability

        if(fabs(xe - xe_prev) < 1e-9)
          break;                             // Converged

        if(++count > 100)
          error("The photoionization equilibrium calculation did not converge within 100 iterations!");

        xe_prev = xe;                        // Track previous value
        ne = xe * nH;                        // Update electron number density
        gHIne = gHI / ne;                    // Update ionization integrals
        gHeIne = gHeI / ne;
        gHeIIne = gHeII / ne;
      };
    } else {                                 // Passive calculations
      xHI = x_HI[i];                         // Already initialized
      xHII = x_HII[i];
      xHeI = x_HeI[i];
      xHeII = x_HeII[i];
      xHeIII = yHe - xHeI - xHeII;
      if (xHeIII < 0.)
        xHeIII = 0.;
      xe = x_e[i];
      ne = xe * nH;                          // Update electron number density
    }

    // Global summary statistics
    V_tot += V_cell;                         // Total volume
    V_HI += xHI * V_cell;                    // Volume weighted values
    V_HeI += xHeI * V_cell;
    V_HeII += xHeII * V_cell;
    m_tot += m_cell;                         // Total mass
    m_HI += xHI * m_cell;                    // Mass weighted values
    m_HeI += xHeI * m_cell;
    m_HeII += xHeII * m_cell;
    const double nH_ne_V = nH * ne * V_cell;
    r_HII += xHII * aHII * nH_ne_V;          // Recombination events per second
    r_HeII += xHeII * aHeII * nH_ne_V;
    r_HeIII += xHeIII * aHeIII * nH_ne_V;
    c_HI += xHI * bHI * nH_ne_V;             // Collisional ionizations per second
    c_HeI += xHeI * bHeI * nH_ne_V;
    c_HeII += xHeII * bHeII * nH_ne_V;

    // Update cell abundances
    if (active) {
      x_HI[i] = xHI;
      x_HII[i] = xHII;
      x_HeI[i] = xHeI;
      x_HeII[i] = xHeII;
      // x_HeIII[i] = xHeIII;
      x_e[i] = xe;
    }
  }

  // Global summary statistics
  x_V_HI.push_back(V_HI / V_tot);            // Volume weighted fractions
  x_V_HII.push_back(1. - x_V_HI.back());
  x_V_HeI.push_back(V_HeI / V_tot);
  x_V_HeII.push_back(V_HeII / V_tot);
  x_V_HeIII.push_back(yHe - x_V_HeI.back() - x_V_HeII.back());
  x_m_HI.push_back(m_HI / m_tot);            // Mass weighted fractions
  x_m_HII.push_back(1. - x_m_HI.back());
  x_m_HeI.push_back(m_HeI / m_tot);
  x_m_HeII.push_back(m_HeII / m_tot);
  x_m_HeIII.push_back(yHe - x_m_HeI.back() - x_m_HeII.back());
  rec_HII.push_back(r_HII);                  // Recombination events per second
  rec_HeII.push_back(r_HeII);
  rec_HeIII.push_back(r_HeIII);
  col_HI.push_back(c_HI);                    // Collisional ionizations per second
  col_HeI.push_back(c_HeI);
  col_HeII.push_back(c_HeII);
}

/* Print latest convergence statistics. */
void Ionization::print_convergence() {
  // Print summary of updates
  const int i = x_V_HI.size() - 1;           // Last index
  const int im1 = i - 1;
  cout << "\nGlobal convergence statistics and relative differences:"
       << "\n Volume-weighted averages:"
       << "\n  x_HI    | " << x_V_HI[im1] << "  -->  " << x_V_HI[i] << "  (" << 100. * fabs(1. - x_V_HI[im1] / x_V_HI[i]) << "%)"
       << "\n  x_HII   | " << x_V_HII[im1] << "  -->  " << x_V_HII[i] << "  (" << 100. * fabs(1. - x_V_HII[im1] / x_V_HII[i]) << "%)"
       << "\n  x_HeI   | " << x_V_HeI[im1] << "  -->  " << x_V_HeI[i] << "  (" << 100. * fabs(1. - x_V_HeI[im1] / x_V_HeI[i]) << "%)"
       << "\n  x_HeII  | " << x_V_HeII[im1] << "  -->  " << x_V_HeII[i] << "  (" << 100. * fabs(1. - x_V_HeII[im1] / x_V_HeII[i]) << "%)"
       << "\n  x_HeIII | " << x_V_HeIII[im1] << "  -->  " << x_V_HeIII[i] << "  (" << 100. * fabs(1. - x_V_HeIII[im1] / x_V_HeIII[i]) << "%)"
       << "\n Mass-weighted averages:"
       << "\n  x_HI    | " << x_m_HI[im1] << "  -->  " << x_m_HI[i] << "  (" << 100. * fabs(1. - x_m_HI[im1] / x_m_HI[i]) << "%)"
       << "\n  x_HII   | " << x_m_HII[im1] << "  -->  " << x_m_HII[i] << "  (" << 100. * fabs(1. - x_m_HII[im1] / x_m_HII[i]) << "%)"
       << "\n  x_HeI   | " << x_m_HeI[im1] << "  -->  " << x_m_HeI[i] << "  (" << 100. * fabs(1. - x_m_HeI[im1] / x_m_HeI[i]) << "%)"
       << "\n  x_HeII  | " << x_m_HeII[im1] << "  -->  " << x_m_HeII[i] << "  (" << 100. * fabs(1. - x_m_HeII[im1] / x_m_HeII[i]) << "%)"
       << "\n  x_HeIII | " << x_m_HeIII[im1] << "  -->  " << x_m_HeIII[i] << "  (" << 100. * fabs(1. - x_m_HeIII[im1] / x_m_HeIII[i]) << "%)"
       << "\n Recombination events per second:"
       << "\n  R_HII   | " << rec_HII[im1] << "  -->  " << rec_HII[i] << "  (" << 100. * fabs(1. - rec_HII[im1] / rec_HII[i]) << "%)"
       << "\n  R_HeII  | " << rec_HeII[im1] << "  -->  " << rec_HeII[i] << "  (" << 100. * fabs(1. - rec_HeII[im1] / rec_HeII[i]) << "%)"
       << "\n  R_HeIII | " << rec_HeIII[im1] << "  -->  " << rec_HeIII[i] << "  (" << 100. * fabs(1. - rec_HeIII[im1] / rec_HeIII[i]) << "%)"
       << "\n Collisional ionizations per second:"
       << "\n  C_HI    | " << col_HI[im1] << "  -->  " << col_HI[i] << "  (" << 100. * fabs(1. - col_HI[im1] / col_HI[i]) << "%)"
       << "\n  C_HeI   | " << col_HeI[im1] << "  -->  " << col_HeI[i] << "  (" << 100. * fabs(1. - col_HeI[im1] / col_HeI[i]) << "%)"
       << "\n  C_HeII  | " << col_HeII[im1] << "  -->  " << col_HeII[i] << "  (" << 100. * fabs(1. - col_HeII[im1] / col_HeII[i]) << "%)" << endl;
}

/* Driver for ionization calculations. */
void Ionization::run() {
  mcrt_timer.start();

  // Perform MCRT iterations until convergence
  double ion_error = 1.;                     // Recombination relative error
  equilibrium_update(false);                 // Base convergence calculations
  if (output_photoheating)
    clear(G_ion);                            // UVB can corrupt photoheating
  for (int iter = 1; iter <= max_iter; ++iter) {
    // Perform the actual ionization calculations
    n_finished = 0;                          // Reset finished progress
    equal_workers();                         // Assign equal work across ranks

    // Sort escaped and absorbed photons
    if (output_photons)
      sort_photons();

    // Add all independent photons from different processors
    if (n_ranks > 1)
      reduce_ionization();

    if (root) {
      // Convert observables to the correct units
      correct_units();
      print_ionization_observables();
      equilibrium_update();                  // Update abundances
      print_convergence();                   // Print convergence statistics
      ion_error = fabs(1. - rec_HII[iter-1] / rec_HII[iter]); // Error estimate
      cout << "\nIonization: Finished iteration " << iter << endl;
    }

    // Early completion if convergence is reached
    mpi_bcast(ion_error);                    // Report convergence statistics
    if (ion_error < max_error)
      break;

    // Remainder of loop is not needed on the final iteration
    if (iter == max_iter) {
      if (root)
        cout << "\nWarning: Reached max number of iterations before convergence!" << endl;
      break;
    }

    // Write intermediate output files if requested
    // if (output_iter)
    //   write_hdf5();                          // Simulation info and data

    // Broadcast abundances to other ranks
    if (n_ranks > 1) {
      mpi_bcast(x_HI);
      mpi_bcast(x_HII);
      mpi_bcast(x_HeI);
      mpi_bcast(x_HeII);
      // mpi_bcast(x_HeIII);
      mpi_bcast(x_e);
    }

    // Update the dust density in case survival is affected
    if (f_ion != 1.) {
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        if (avoid_cell(i))
          continue;
        const double f_dust = (T[i] > T_sputter) ? 0. : 1. + (f_ion - 1.) * x_HII[i];
        rho_dust[i] = f_dust * rho[i] * D[i]; // Based on a dust-to-gas ratio
      }
    }

    // Clear cameras and path estimators
    clear(rate_HI);                          // Photoionization integrals
    clear(rate_HeI);
    clear(rate_HeII);
    if (output_photoheating)
      clear(G_ion);                          // Photoheating rates
    clear(bin_f_esc);                        // Global bin escape fraction
    clear(bin_f_abs);                        // Global bin dust absorption fraction
    if (have_cameras) {
      // Primary cameras (mcrt)
      if (output_escape_fractions)
        clear(f_escs);                       // Escape fractions
      if (output_bin_escape_fractions)
        clear(bin_f_escs);                   // Bin escape fractions
      if (output_images)
        clear(images);                       // Surface brightness images
      if (output_bin_images)
        clear(bin_images);                   // Bin surface brightness images

      // Intrinsic cameras (mcrt)
      if (output_mcrt_emission) {
        if (output_images)
          clear(images_int);                 // Surface brightness images
        if (output_bin_images)
          clear(bin_images_int);             // Bin surface brightness images
      }

      // Attenuation cameras (mcrt)
      if (output_mcrt_attenuation) {
        if (output_escape_fractions)
          clear(f_escs_ext);                 // Escape fractions
        if (output_images)
          clear(images_ext);                 // Surface brightness images
        if (output_bin_images)
          clear(bin_images_ext);             // Bin surface brightness images
      }
    }
  }

  mcrt_timer.stop();
}
