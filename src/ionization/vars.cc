/**********************
 * ionization/vars.cc *
 **********************

 * Module global variable declarations (defaults).

*/

#include "proto.h"

/* Information about the source */
// int n_photons = 1;                           // Actual number of photon packets used
// int n_absorbed;                              // Number of absorbed photons
// int n_escaped;                               // Number of escaped photons
// int n_finished;                              // Track finished progress
string source_model = "GMC";                 // Spectral source model
// double L_tot = 0.;                           // Total luminosity [erg/s]
// double Ndot_tot = 0.;                        // Total emission rate [photons/s]
vector<double> bin_L_tot;                    // Total bin luminosities [erg/s]
vector<double> bin_Ndot_tot;                 // Total bin rates [photons/s]
double L_stars = 0.;                         // Stellar luminosity [erg/s]
double Ndot_stars = 0.;                      // Stellar rate [photons/s]
double L_background = 0.;                    // Background luminosity [erg/s]
double Ndot_background = 0.;                 // Background rate [photons/s]

/* Spatial emission from stars */
// double j_exp = 1;                            // Luminosity boosting exponent (j_exp < 1 favors lower emissivity cells)
// array<int, n_cdf_stars> j_map_stars;         // Lookup table for photon insertion (stars)
// vector<double> j_cdf_stars;                  // Cumulative distribution function for emission
// vector<double> j_weights_stars;              // Initial photon weights for each star

/* Distinguish between source types */
int n_ion_source_types;                      // Number of source types used
array<double, MAX_ION_SOURCE_TYPES> mixed_ion_pdf; // Mixed source probability distribution function
array<double, MAX_ION_SOURCE_TYPES> mixed_ion_cdf; // Mixed source cumulative distribution function
array<double, MAX_ION_SOURCE_TYPES> mixed_ion_weights; // Mixed source weights (stars, background)
vector<int> ion_source_types_mask;           // Source type mask indices
strings ion_source_names_mask;               // Source type names (masked)
vector<double> mixed_ion_pdf_mask;           // Mixed source pdf (masked)
vector<double> mixed_ion_cdf_mask;           // Mixed source cdf (masked)
vector<double> mixed_ion_weights_mask;       // Mixed source weights (masked)

/* Ionizing photon models */
vector<double> global_sigma_HI;              // HI ionization cross-section [cm^2]
vector<double> global_sigma_HeI;             // HeI ionization cross-section [cm^2]
vector<double> global_sigma_HeII;            // HeII ionization cross-section [cm^2]
vector<double> global_epsilon_HI;            // HI ionization heating [erg]
vector<double> global_epsilon_HeI;           // HeI ionization heating [erg]
vector<double> global_epsilon_HeII;          // HeII ionization heating [erg]
vector<double> global_mean_energy;           // Mean energy of each frequency bin [erg]

/* Information about the dust model */
// string dust_model = "";                      // Dust model: SMC, MW, etc.
vector<double> global_kappa;                 // Dust opacity [cm^2/g dust]
vector<double> global_albedo;                // Dust albedo for scattering vs. absorption
vector<double> global_cosine;                // Anisotropy parameter: <μ> for dust scattering
// double f_ion = 1.;                           // H_II region survival fraction
// double T_sputter = 1e6;                      // Thermal sputtering cutoff [K]

/* Information about local ionization coupling */
vector<double> rate_HI;                      // Photoionization integral for HI [1/s]
vector<double> rate_HeI;                     // Photoionization integral for HeI [1/s]
vector<double> rate_HeII;                    // Photoionization integral for HeII [1/s]

/* Information about escaped photons */
// double f_esc = 0.;                           // Global escape fraction
double f_abs = 0.;                           // Global dust absorption fraction
vector<double> bin_f_esc;                    // Global bin escape fraction
vector<double> bin_f_abs;                    // Global bin dust absorption fraction
// bool output_photons = true;                  // Output photon packets (esc/abs)
// string photon_file = "";                     // Output a separate photon file
bool output_abundances = true;               // Output abundances (x_HI etc)
string abundances_file = "";                 // Output a separate abundances file
bool output_photoheating = false;            // Output photoheating rates
string photoheating_file = "";               // Output a separate photoheating file
// vector<int> source_ids;                      // Emission cell or star index
// vector<int> source_types;                    // Emission type specifier
// vector<double> source_weights;               // Photon weight at emission
vector<int> freq_bins;                       // Frequency bin at escape [bin]
// vector<double> weights;                      // Photon weight at escape
vector<double> dust_weights;                 // Weight removed by dust absorption
// vector<Vec3> positions;                      // Position at escape [cm]
// vector<Vec3> directions;                     // Direction at escape

/* Information about secondary cameras (mcrt) */
// bool output_mcrt_emission = false;           // Output intrinsic emission without transport
// bool output_mcrt_attenuation = false;        // Output attenuated emission without scattering
// vector<double> f_escs_ext;                   // Attenuated escape fractions [fraction]
bool output_bin_escape_fractions = true;     // Output bin escape fractions
vector<vector<double>> bin_f_escs;           // Bin escape fractions [fraction]
vector<vector<double>> bin_f_escs_ext;       // Attenuated bin escape fractions [fraction]
// vector<Image<double>> images_int;            // Intrinsic surface brightness images [photons/s/cm^2]
// vector<Image<double>> images_ext;            // Attenuated surface brightness images [photons/s/cm^2]
bool output_bin_images = false;              // Output bin surface brightness images
vector<Cube<double>> bin_images;             // Escaped bin SB images [photons/s/cm^2]
vector<Cube<double>> bin_images_int;         // Intrinsic bin SB images [photons/s/cm^2]
vector<Cube<double>> bin_images_ext;         // Attenuated bin SB images [photons/s/cm^2]
