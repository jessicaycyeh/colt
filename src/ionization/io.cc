/********************
 * ionization/io.cc *
 ********************

 * All I/O operations related to ionization radiative transfer.

*/

#include "proto.h"
#include "Ionization.h"
#include "../io_hdf5.h" // HDF5 read/write functions

void write_sim_info(const H5File& f); // Write general simulation information

/* Print additional ionization radiative transfer information. */
void Ionization::print_info() {
  // Print photon parameters
  cout << "\nPhoton parameters: [" << source_model << "]"
       << "\n  n_bins     = " << n_bins
       << "\n  n_photons  = " << n_photons;
  if (spherical_escape)
    cout << "\n  r_escape   = " << escape_radius / kpc << " kpc"
         << "\n  r_emission = " << emission_radius / kpc << " kpc"
         << "\n  esc center = " << escape_center / kpc << " kpc";
  cout << endl;

  // Ionizing photon models
  const bool have_global_averages = (source_model == "GMC" || source_model == "MRT" ||
    source_model == "BC03-IMF-SOLAR" || source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100");
  if (have_global_averages) {
    print("  sigma_HI  ", global_sigma_HI, "cm^2");
    print("  sigma_HeI ", global_sigma_HeI, "cm^2");
    print("  sigma_HeII", global_sigma_HeII, "cm^2");
    print("  epsilon_HI  ", global_epsilon_HI, "eV", eV);
    print("  epsilon_HeI ", global_epsilon_HeI, "eV", eV);
    print("  epsilon_HeII", global_epsilon_HeII, "eV", eV);
    print("  avg energy", global_mean_energy, "eV", eV);
  }

  // UV background
  if (UVB_model != "")
    cout << "\nUV background parameters: [" << UVB_model << "]"
         << "\n  Gamma_HI   = " << UVB_rate_HI << " 1/s"
         << "\n  Gamma_HeI  = " << UVB_rate_HeI << " 1/s"
         << "\n  Gamma_HeII = " << UVB_rate_HeII << " 1/s"
         << "\n  Heat_HI    = " << UVB_heat_HI << " erg/s"
         << "\n  Heat_HeI   = " << UVB_heat_HeI << " erg/s"
         << "\n  Heat_HeII  = " << UVB_heat_HeII << " erg/s" << endl;

  cout << "\nDust properties:";
  if (dust_model != "")
    cout << " " << dust_model << " dust model";
  cout << "\n  f_ion      = " << f_ion
       << "\n  T_sputter  = " << T_sputter << " K" << endl;
  if (have_global_averages) {
    print("  kappas    ", global_kappa, "cm^2/g dust");
    print("  albedos   ", global_albedo);
    print("  cosines   ", global_cosine);
  }

  if (verbose && star_based_emission) {
    cout << "\nStar emission data:";
    print("  j_cdf     ", j_cdf_stars);
    print("  j_weights ", j_weights_stars);
  }

  if (n_ion_source_types > 1)
    cout << "\nMixed source calculation uses: " << to_string(ion_source_names_mask)
         << "\n  mixed pdf  = " << to_string(mixed_ion_pdf_mask)
         << "\n  boost cdf  = " << to_string(mixed_ion_cdf_mask)
         << "\n   weights   = " << to_string(mixed_ion_weights_mask) << endl;

  cout << "\nStrength of the source:"
       << "\n  Luminosity = " << L_tot << " erg/s"
       << "\n             = " << L_tot / Lsun << " Lsun"
       << "\n  Prod. Rate = " << Ndot_tot << " photons/s" << endl;
  print("  Lum (bins)", bin_L_tot, "erg/s");
  print("            ", bin_L_tot, "Lsun", Lsun);
  print("  Prod rates", bin_Ndot_tot, "photons/s");

  // Print camera parameters
  if (have_cameras)
    print_cameras();                         // General camera parameters
}

/* Function to print additional data. */
void print_ionization_observables() {
  cout << "\nGlobal statistics:"
       << "\n Escape fraction = " << f_esc
       << "\n Dust absorption = " << f_abs << endl;
  print(" Bin f_esc", bin_f_esc);
  print(" Bin f_abs", bin_f_abs);

  if (output_escape_fractions) {
    cout << "\nCamera escape fractions:" << endl;
    print(" f_esc (LOS)", f_escs);
    if (output_mcrt_attenuation)
      print(" f_esc (ext)", f_escs_ext);
  }
}

/* Write general simulation information. */
void Ionization::write_ionization_info(const H5File& f) {
  // Simulation information
  write(f, "n_bins", n_bins);                // Number of frequency bins
  write(f, "n_photons", n_photons);          // Actual number of photon packets used
  const int n_iter = x_V_HI.size() - 1;
  write(f, "n_iter", n_iter);                // Number of iterations performed
  write(f, "max_iter", max_iter);            // Maximum number of iterations
  write(f, "max_error", max_error);          // Relative error for convergence
  write(f, "Ndot_tot", Ndot_tot);            // Total emission rate [photons/s]
  write(f, "L_tot", L_tot);                  // Total luminosity [erg/s]
  write(f, "f_esc", f_esc);                  // Global escape fraction
  write(f, "f_abs", f_abs);                  // Global dust absorption fraction
  write(f, "bin_f_esc", bin_f_esc);          // Global bin escape fraction
  write(f, "bin_f_abs", bin_f_abs);          // Global bin dust absorption fraction
  write(f, "bin_Ndot_tot", bin_Ndot_tot, "photons/s"); // Bin rates [photons/s]
  write(f, "bin_L_tot", bin_L_tot, "erg/s"); // Bin luminosities [erg/s]
  const bool have_global_averages = (source_model == "GMC" || source_model == "MRT" ||
    source_model == "BC03-IMF-SOLAR" || source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100");
  if (have_global_averages) {
    write(f, "sigma_HI", global_sigma_HI, "cm^2"); // Ionizing cross-sections
    write(f, "sigma_HeI", global_sigma_HeI, "cm^2");
    write(f, "sigma_HeII", global_sigma_HeII, "cm^2");
    write(f, "epsilon_HI", global_epsilon_HI, "erg"); // Photoheating
    write(f, "epsilon_HeI", global_epsilon_HeI, "erg");
    write(f, "epsilon_HeII", global_epsilon_HeII, "erg");
    write(f, "mean_energy", global_mean_energy, "erg");
  }

  // Information about convergence history
  {
    Group g = f.createGroup("/conv");
    write(g, "x_V_HI", x_V_HI);              // Volume weighted fractions
    write(g, "x_V_HII", x_V_HII);
    write(g, "x_V_HeI", x_V_HeI);
    write(g, "x_V_HeII", x_V_HeII);
    write(g, "x_V_HeIII", x_V_HeIII);
    write(g, "x_m_HI", x_m_HI);              // Mass weighted fractions
    write(g, "x_m_HII", x_m_HII);
    write(g, "x_m_HeI", x_m_HeI);
    write(g, "x_m_HeII", x_m_HeII);
    write(g, "x_m_HeIII", x_m_HeIII);
    write(g, "rec_HII", rec_HII, "1/s");     // Recombination events per second
    write(g, "rec_HeII", rec_HeII);
    write(g, "rec_HeIII", rec_HeIII);
    write(g, "col_HI", col_HI, "1/s");       // Collisional ionizations per second
    write(g, "col_HeI", col_HeI);
    write(g, "col_HeII", col_HeII);
  }

  // Information about emission sources
  {
    Group g = f.createGroup("/sources");
    if (star_based_emission)
      write(g, "n_stars", n_stars);          // Number of stars

    write(g, "n_source_types", n_ion_source_types); // Number of source types used
    write_attr(g, "source_types", ion_source_types_mask); // Source type mask indices
    write_attr(g, "source_names", ion_source_names_mask); // Source type names (masked)
    write_attr(g, "mixed_pdf", mixed_ion_pdf_mask); // Mixed source pdf (masked)
    write_attr(g, "mixed_cdf", mixed_ion_cdf_mask); // Mixed source cdf (masked)
    write_attr(g, "mixed_weights", mixed_ion_weights_mask); // Mixed source weights (masked)

    write(g, "spherical_escape", spherical_escape); // Photons escape from a sphere
    if (spherical_escape) {
      write(g, "escape_radius", escape_radius); // Radius for spherical escape [cm]
      write(g, "emission_radius", emission_radius); // Radius for spherical emission [cm]
      write(g, "escape_center", escape_center, "cm"); // Center of the escape region [cm]
    }
  }

  {
    Group g = f.createGroup("/dust");
    write(g, "dust_model", dust_model);      // Dust model: SMC, MW, etc.
    if (metallicity >= 0.)
      write(g, "metallicity", metallicity);  // Constant metallicity
    if (dust_to_metal >= 0.)
      write(g, "dust_to_metal", dust_to_metal); // Constant dust-to-metal ratio
    if (dust_to_gas >= 0.)
      write(g, "dust_to_gas", dust_to_gas);  // Constant dust-to-gas ratio
    if (dust_boost > 0.)
      write(g, "dust_boost", dust_boost);    // Optional dust boost factor
    if (have_global_averages) {
      write(g, "kappa", global_kappa, "cm^2/g dust"); // Dust opacity [cm^2/g dust]
      write(g, "albedo", global_albedo);     // Dust albedo for scattering vs. absorption
      write(g, "cosine", global_cosine);     // Anisotropy parameter: <μ> for dust scattering
    }
    write(g, "f_ion", f_ion);                // H_II region survival fraction
    write(g, "T_sputter", T_sputter);        // Thermal sputtering cutoff [K]
  }
}

/* Write photon data to a specified group or file. */
template <typename File_or_Group>
static void write_photons(const File_or_Group& f) {
  write(f, "n_escaped", n_escaped);          // Number of escaped photons
  write(f, "n_absorbed", n_absorbed);        // Number of photons absorbed
  write(f, "source_id", source_ids);         // Emission cell or star index
  if (n_ion_source_types > 1)
    write(f, "source_type", source_types);   // Emission type specifier
  write(f, "source_weight", source_weights); // Photon weight at emission
  write(f, "frequency_bin", freq_bins);      // Frequency at escape [bin]
  write(f, "weight", weights);               // Photon weight at escape
  write(f, "dust_weight", dust_weights);     // Weight removed by dust absorption
  write(f, "position", positions, "cm");     // Position at escape [cm]
  write(f, "direction", directions);         // Direction at escape
}

/* Delete a dataset link if it exists. */
template <typename File_or_Group>
static inline void delete_link(const File_or_Group& f, const string& name) {
  if (H5Lexists(f.getId(), name.c_str(), H5P_DEFAULT) > 0) {
    H5Ldelete(f.getId(), name.c_str(), H5P_DEFAULT);
    cout << "\nWarning: Replacing " << name << " dataset." << endl;
  }
}

/* Write abundances data to a specified group or file. */
template <typename File_or_Group>
static void write_abundances(const File_or_Group& f) {
  delete_link(f, "x_HI");                    // Allow overwriting datasets
  delete_link(f, "x_HII");
  delete_link(f, "x_HeI");
  delete_link(f, "x_HeII");
  delete_link(f, "x_e");
  write(f, "x_HI", x_HI);                    // x_HI = n_HI / n_H
  write(f, "x_HII", x_HII);                  // x_HII = n_HII / n_H
  write(f, "x_HeI", x_HeI);                  // x_HeI = n_HeI / n_H
  write(f, "x_HeII", x_HeII);                // x_HeII = n_HeII / n_H
  write(f, "x_e", x_e);                      // Electron fraction: x_e = n_e / n_H
}

/* Write photoheating data to a specified group or file. */
template <typename File_or_Group>
static void write_photoheating(const File_or_Group& f) {
  delete_link(f, "G_ion");                   // Allow overwriting datasets
  write(f, "G_ion", G_ion, "erg/s");         // Photoheating rate [erg/s]
}

/* Writes ionization data and info to the specified hdf5 filename */
void Ionization::write_module(const H5File& f) {
  // Write general ionization info
  write_ionization_info(f);

  // Additional camera info
  if (have_cameras) {
    if (output_escape_fractions)
      write(f, "f_escs", f_escs);            // Escape fractions [fraction]
    if (output_bin_escape_fractions)
      write(f, "bin_f_escs", bin_f_escs);    // Bin escape fractions [fraction]
    if (output_images)
      write(f, "images", images, "photons/s/cm^2"); // Surface brightness images [photons/s/cm^2]
    if (output_bin_images)
      write(f, "bin_images", bin_images, "photons/s/cm^2"); // Bin SB images [photons/s/cm^2]

    // Intrinsic emission (mcrt)
    if (output_mcrt_emission) {
      if (output_images)
        write(f, "images_int", images_int, "photons/s/cm^2"); // Surface brightness images [photons/s/cm^2]
      if (output_bin_images)
        write(f, "bin_images_int", bin_images_int, "photons/s/cm^2"); // Bin SB images [photons/s/cm^2]
    }

    // Attenuated emission  (mcrt)
    if (output_mcrt_attenuation) {
      if (output_escape_fractions)
        write(f, "f_escs_ext", f_escs_ext);  // Escape fractions [fraction]
      if (output_bin_escape_fractions)
        write(f, "bin_f_escs_ext", bin_f_escs_ext); // Bin escape fractions [fraction]
      if (output_images)
        write(f, "images_ext", images_ext, "photons/s/cm^2"); // Surface brightness images [photons/s/cm^2]
      if (output_bin_images)
        write(f, "bin_images_ext", bin_images_ext, "photons/s/cm^2"); // Bin SB images [photons/s/cm^2]
    }
  }

  if (output_photons) {
    if (photon_file.empty()) {               // Output photons to the same file
      Group g = f.createGroup("/photons");
      write_photons(g);                      // Write to the photons group
    } else {                                 // Output a separate photon file
      H5File f_photons(output_dir + "/" + output_base + "_" + photon_file +
                       snap_str + halo_str + "." + output_ext, H5F_ACC_TRUNC);
      write_sim_info(f_photons);             // Write general simulation info
      write_ionization_info(f_photons);      // Write general ionization info
      write_photons(f_photons);              // Write to the main group
    }
  }

  if (output_abundances) {
    if (abundances_file.empty()) {           // Output abundances to init_file
      H5File f_init(init_file, H5F_ACC_RDWR);
      write_abundances(f_init);              // Write to the main group
    } else {                                 // Output a separate abundances file
      H5File f_ab(output_dir + "/" + output_base + "_" + abundances_file +
                  snap_str + halo_str + "." + output_ext, H5F_ACC_TRUNC);
      write_sim_info(f_ab);                  // Write general simulation info
      write_ionization_info(f_ab);           // Write general ionization info
      write_abundances(f_ab);                // Write to the main group
    }
  }

  if (output_photoheating) {
    if (photoheating_file.empty()) {         // Output photoheating to init_file
      H5File f_init(init_file, H5F_ACC_RDWR);
      write_photoheating(f_init);            // Write to the main group
    } else {                                 // Output a separate photoheating file
      H5File f_ph(output_dir + "/" + output_base + "_" + photoheating_file +
                  snap_str + halo_str + "." + output_ext, H5F_ACC_TRUNC);
      write_sim_info(f_ph);                  // Write general simulation info
      write_ionization_info(f_ph);           // Write general ionization info
      write_photoheating(f_ph);              // Write to the main group
    }
  }
}
