/**********************
 * ionization/proto.h *
 **********************

 * Module function declarations.

*/

#ifndef IONIZATION_PROTO_INCLUDED
#define IONIZATION_PROTO_INCLUDED

// Inherited headers
#include "../proto.h"

// Information about the source
extern int n_photons;                        // Actual number of photon packets used
extern int n_absorbed;                       // Number of absorbed photons
extern int n_escaped;                        // Number of escaped photons
extern int n_finished;                       // Track finished progress
extern string source_model;                  // Spectral source model
extern double L_tot;                         // Total luminosity [erg/s]
extern double Ndot_tot;                      // Total emission rate [photons/s]
extern vector<double> bin_L_tot;             // Total bin luminosities [erg/s]
extern vector<double> bin_Ndot_tot;          // Total bin rates [photons/s]
extern double L_stars;                       // Stellar luminosity [erg/s]
extern double Ndot_stars;                    // Stellar rate [photons/s]
extern double L_background;                  // Background luminosity [erg/s]
extern double Ndot_background;               // Background rate [photons/s]

// Spatial emission from cells and stars
extern double j_exp;                         // Luminosity boosting exponent (j_exp < 1 favors lower emissivity cells)
constexpr int n_cdf_stars = 10000;           // Number of entrees in the cdf lookup table (stars)
extern array<int, n_cdf_stars> j_map_stars;  // Lookup table for photon insertion (stars)
extern vector<double> j_cdf_stars;           // Cumulative distribution function for emission
extern vector<double> j_weights_stars;       // Initial photon weights for each star

// Distinguish between source types
enum IonSourceTypes {
  STARS = 0,                                 // Star-based emission
  BACKGROUND = 1,                            // Background emission
  MAX_ION_SOURCE_TYPES = 2                   // Max number of source types
};
extern int n_ion_source_types;               // Number of source types used
extern array<double, MAX_ION_SOURCE_TYPES> mixed_ion_pdf; // Mixed source probability distribution function
extern array<double, MAX_ION_SOURCE_TYPES> mixed_ion_cdf; // Mixed source cumulative distribution function
extern array<double, MAX_ION_SOURCE_TYPES> mixed_ion_weights; // Mixed source weights (point, cells, stars)
extern vector<int> ion_source_types_mask;    // Source type mask indices
extern strings ion_source_names_mask;        // Source type names (masked)
extern vector<double> mixed_ion_pdf_mask;    // Mixed source pdf (masked)
extern vector<double> mixed_ion_cdf_mask;    // Mixed source cdf (masked)
extern vector<double> mixed_ion_weights_mask; // Mixed source weights (masked)

// Ionizing photon models
extern vector<double> global_sigma_HI;       // HI ionization cross-section [cm^2]
extern vector<double> global_sigma_HeI;      // HeI ionization cross-section [cm^2]
extern vector<double> global_sigma_HeII;     // HeII ionization cross-section [cm^2]
extern vector<double> global_epsilon_HI;     // HI ionization heating [erg]
extern vector<double> global_epsilon_HeI;    // HeI ionization heating [erg]
extern vector<double> global_epsilon_HeII;   // HeII ionization heating [erg]
extern vector<double> global_mean_energy;    // Mean energy of each frequency bin [erg]

// Information about the dust model
extern string dust_model;                    // Dust model: SMC, MW, etc.
extern vector<double> global_kappa;          // Dust opacity [cm^2/g dust]
extern vector<double> global_albedo;         // Dust albedo for scattering vs. absorption
extern vector<double> global_cosine;         // Anisotropy parameter: <μ> for dust scattering
extern double f_ion;                         // H_II region survival fraction
extern double T_sputter;                     // Thermal sputtering cutoff [K]

// Information about local ionization coupling
extern vector<double> rate_HI;               // Photoionization integral for HI [1/s]
extern vector<double> rate_HeI;              // Photoionization integral for HeI [1/s]
extern vector<double> rate_HeII;             // Photoionization integral for HeII [1/s]

// Information about escaped photons
extern double f_esc;                         // Global escape fraction
extern double f_abs;                         // Global dust absorption fraction
extern vector<double> bin_f_esc;             // Global bin escape fraction
extern vector<double> bin_f_abs;             // Global bin dust absorption fraction
extern bool output_photons;                  // Output photon packets (esc/abs)
extern string photon_file;                   // Output a separate photon file
extern bool output_abundances;               // Output abundances (x_HI etc)
extern string abundances_file;               // Output a separate abundances file
extern bool output_photoheating;             // Output photoheating rates
extern string photoheating_file;             // Output a separate photoheating file
extern vector<int> source_ids;               // Emission cell or star index
extern vector<int> source_types;             // Emission type specifier
extern vector<double> source_weights;        // Photon weight at emission
extern vector<int> freq_bins;                // Frequency bin at escape [bin]
extern vector<double> weights;               // Photon weight at escape
extern vector<double> dust_weights;          // Weight removed by dust absorption
extern vector<Vec3> positions;               // Position at escape [cm]
extern vector<Vec3> directions;              // Direction at escape

// Information about secondary cameras (mcrt)
extern bool output_mcrt_emission;            // Output intrinsic emission without transport
extern bool output_mcrt_attenuation;         // Output attenuated emission without scattering
extern vector<double> f_escs_ext;            // Attenuated escape fractions [fraction]
extern bool output_bin_escape_fractions;     // Output bin escape fractions
extern vector<vector<double>> bin_f_escs;    // Bin escape fractions [fraction]
extern vector<vector<double>> bin_f_escs_ext; // Attenuated bin escape fractions [fraction]
extern vector<Image<double>> images_int;     // Intrinsic surface brightness images [photons/s/cm^2]
extern vector<Image<double>> images_ext;     // Attenuated surface brightness images [photons/s/cm^2]
extern bool output_bin_images;               // Output bin surface brightness images
extern vector<Cube<double>> bin_images;      // Escaped bin SB images [photons/s/cm^2]
extern vector<Cube<double>> bin_images_int;  // Intrinsic bin SB images [photons/s/cm^2]
extern vector<Cube<double>> bin_images_ext;  // Attenuated bin SB images [photons/s/cm^2]

#endif // IONIZATION_PROTO_INCLUDED
