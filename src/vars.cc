/***********
 * vars.cc *
 ***********

 * General global variable declarations (defaults).

*/

#include "proto.h"

// Main control variables
string executable  = "";                     // Executable name (command line)
string config_file = "";                     // Config file name (command line)
string snap_str    = "";                     // Snapshot number (option command line)
int snap_num       = -1;                     // Snapshot number (converted from string)
string halo_str    = "";                     // Group or subhalo number (option command line)
int halo_num       = -1;                     // Group or subhalo number (converted from string)
string init_dir    = "";                     // Initial conditions directory (optional)
string init_base   = "";                     // Initial conditions file base (optional)
string init_ext    = "hdf5";                 // Initial conditions file extension
string init_file   = "";                     // Initial conditions file (required)
string output_dir  = "output";               // Output directory name
string output_base = "colt";                 // Output file base name
string output_ext  = "hdf5";                 // Output file extension
string module      = "mcrt";                 // Module type: mcrt, projections
bool verbose       = false;                  // Verbose output for tests and debugging
bool select_subhalo = true;                  // Specified a subhalo
unsigned long long seed;                     // Seed for random number generator

// Supplementary files
bool save_connections = true;                // Save voronoi connections to a file
bool save_circulators = true;                // Save voronoi circulators to a file
string cgal_dir    = "";                     // CGAL connectivity directory (optional)
string cgal_base   = "";                     // CGAL connectivity file base (optional)
string cgal_ext    = "hdf5";                 // CGAL connectivity file extension
string cgal_file   = "";                     // CGAL connectivity file (optional)

// MPI and OpenMP variables
int rank;                                    // MPI local rank
int n_ranks;                                 // Number of MPI processes
int thread;                                  // OpenMP local thread
int n_threads;                               // Number of OpenMP threads
bool root;                                   // True if rank is root
bool load_balancing = false;                 // Use MPI load balancing algorithms

// Lists of data fields
int n_active_fields = 0;                     // Number of active fields
vector<int> active_fields;                   // List of active field indices
array<Field, n_fields> fields;               // File dataset fields

static int setup_fields() {
  fields[Density].name = "rho";              // Gas density [g/cm^3]
  fields[Volume].name = "V";                 // Volume [cm^3]
  fields[Temperature].name = "T";            // Temperature [K]
  fields[HydrogenDensity].name = "n_H";      // Hydrogen number density [cm^-3]
  fields[LowerDensity].name = "n_lower";     // Number density of lower transition [cm^-3]
  fields[UpperDensity].name = "n_upper";     // Number density of upper transition [cm^-3]
  fields[HI_Fraction].name = "x_HI";         // x_HI = n_HI / n_H
  fields[HII_Fraction].name = "x_HII";       // x_HII = n_HII / n_H
  fields[H2_Fraction].name = "x_H2";         // x_H2 = n_H2 / n_H
  fields[HeI_Fraction].name = "x_HeI";       // x_HeI = n_HeI / n_H
  fields[HeII_Fraction].name = "x_HeII";     // x_HeII = n_HeII / n_H
  fields[ElectronAbundance].name = "x_e";    // Electron fraction: x_e = n_e / n_H
  fields[PhotoheatingRate].name = "G_ion";   // Photoheating rate [erg/s]
  fields[StarFormationRate].name = "SFR";    // Star formation rate [Msun/yr]
  fields[DustTemperature].name = "T_dust";   // Dust temperature [K]
  fields[VelocitySquared].name = "v2";       // Velocity^2 [cm^2/s^2]
  fields[VelocitySquaredLOS].name = "v2_LOS"; // Velocity_LOS^2 [cm^2/s^2]
  fields[VelocityLOS].name = "v_LOS";        // Velocity_LOS [cm/s]
  fields[VelocityX].name = "v_x";            // Velocity_X [cm/s]
  fields[VelocityY].name = "v_y";            // Velocity_Y [cm/s]
  fields[MagneticFieldSquared].name = "B2";  // Magnetic field^2 [G^2]
  fields[MagneticFieldLOS].name = "B_LOS";   // Magnetic field LOS [G]
  fields[MagneticFieldX].name = "B_x";       // Magnetic field X [G]
  fields[MagneticFieldY].name = "B_y";       // Magnetic field Y [G]
  fields[HydrogenFraction].name = "X";       // Mass fraction of hydrogen
  fields[Metallicity].name = "Z";            // Gas metallicity [mass fraction]
  fields[DustMetallicity].name = "D";        // Dust-to-gas ratio [mass fraction]
  fields[ElectronCoefficient].name = "k_e";  // Absorption coefficient of electrons [1/cm]
  fields[LineCoefficient].name = "k_0";      // Absorption coefficient of line [1/cm]
  fields[DustCoefficient].name = "k_dust";   // Absorption coefficient of dust [1/cm]
  fields[DustDensity].name = "rho_dust";     // Dust density [g/cm^3]
  fields[DampingParameter].name = "a";       // "Damping parameter"
  fields[NonLocalATau0].name = "atau";       // Nonlocal estimate of a * tau for each cell
  fields[DensitySquared].name = "rho2";      // Gas density^2 [g^2/cm^6]
  fields[IonizationFront].name = "ion_front"; // Gaussian centered on x_HI
  return 0;
}
static const int setup_fields_flag = setup_fields(); // Setup helper

// Basic grid data
int n_cells = 1;                             // Number of cells
int n_stars = 0;                             // Number of stars
vector<Vec3> r;                              // Cell position [cm]
vector<double>& V = fields[Volume].data;     // Cell volume [cm^3]
array<Vec3, 2> bbox;                         // Bounding box [cm]
double max_bbox_width;                       // Max bbox width [cm]

// Cartesian grid data
int nx = 0;                                  // Number of x cells
int ny = 0;                                  // Number of y cells
int nz = 0;                                  // Number of z cells
int nyz = 0;                                 // Save ny * nz
double wx = 0.;                              // Cell x width [cm]
double wy = 0.;                              // Cell y width [cm]
double wz = 0.;                              // Cell z width [cm]
double dV = 0.;                              // Cell volume [cm^3]

// Octree grid data (size = n_cells)
int n_leafs = 1;                             // Number of leaf cells
vector<int> parent;                          // Cell parent
vector<array<int, 8>> child;                 // Child cell indices
vector<bool> child_check;                    // True if cell has children
vector<double> w;                            // Cell width [cm]

// Voronoi tessellation data
int n_edges;                                 // Number of edge Voronoi cells
int n_neighbors_tot;                         // Total number of neighbors for all cells
int n_circulators_tot;                       // Total number of circulators for all faces
vector<bool> edge_flag;                      // Flag for edge cells
vector<vector<Face>> faces;                  // Face connection data

// Additional data (size = n_cells)
vector<Vec3> v;                              // Bulk velocity [vth]
vector<Vec3> B;                              // Magnetic field [G]
vector<double>& rho = fields[Density].data;  // Density [g/cm^3]
vector<double>& T = fields[Temperature].data; // Temperature [K]
vector<double>& n_H = fields[HydrogenDensity].data; // Hydrogen number density [cm^-3]
vector<double>& n_lower = fields[LowerDensity].data; // Number density of lower transition [cm^-3]
vector<double>& n_upper = fields[UpperDensity].data; // Number density of upper transition [cm^-3]
vector<double>& x_HI = fields[HI_Fraction].data; // x_HI = n_HI / n_H
vector<double>& x_HII = fields[HII_Fraction].data; // x_HII = n_HII / n_H
vector<double>& x_H2 = fields[H2_Fraction].data; // x_H2 = n_H2 / n_H
vector<double>& x_HeI = fields[HeI_Fraction].data; // x_HeI = n_HeI / n_H
vector<double>& x_HeII = fields[HeII_Fraction].data; // x_HeII = n_HeII / n_H
vector<double>& x_e = fields[ElectronAbundance].data; // Electron fraction: x_e = n_e / n_H
vector<double>& G_ion = fields[PhotoheatingRate].data; // Photoheating rate [erg/s]
vector<double>& SFR = fields[StarFormationRate].data; // Star formation rate [Msun/yr]
vector<double>& T_dust = fields[DustTemperature].data; // Dust temperature [K]
vector<double>& v2 = fields[VelocitySquared].data; // Velocity^2 [cm^2/s^2]
vector<double>& v2_LOS = fields[VelocitySquaredLOS].data; // Velocity_LOS^2 [cm^2/s^2]
vector<double>& v_LOS = fields[VelocityLOS].data; // Velocity_LOS [cm/s]
vector<double>& v_x = fields[VelocityX].data; // Velocity_X [cm/s]
vector<double>& v_y = fields[VelocityY].data; // Velocity_Y [cm/s]
vector<double>& B2 = fields[MagneticFieldSquared].data; // Magnetic field^2 [G^2]
vector<double>& B_LOS = fields[MagneticFieldLOS].data; // Magnetic field LOS [G]
vector<double>& B_x = fields[MagneticFieldX].data; // Magnetic field X [G]
vector<double>& B_y = fields[MagneticFieldY].data; // Magnetic field Y [G]
vector<double>& X = fields[HydrogenFraction].data; // Mass fraction of hydrogen
vector<double>& Z = fields[Metallicity].data; // Gas metallicity [mass fraction]
vector<double>& D = fields[DustMetallicity].data; // Dust-to-gas ratio [mass fraction]
vector<double>& k_e = fields[ElectronCoefficient].data; // Absorption coefficient of electrons [1/cm]
vector<double>& k_0 = fields[LineCoefficient].data; // Absorption coefficient of line [1/cm]
vector<double>& k_dust = fields[DustCoefficient].data; // Absorption coefficient of dust [1/cm]
vector<double>& rho_dust = fields[DustDensity].data; // Dust density [g/cm^3]
vector<double>& a = fields[DampingParameter].data; // "Damping parameter"
vector<double>& atau = fields[NonLocalATau0].data; // Nonlocal estimate of a * tau for each cell
vector<double>& j_line = fields[LineEmissivity].data; // Line emissivity [erg/s/cm^3]
vector<double>& rho2 = fields[DensitySquared].data; // Density^2 [g^2/cm^6]
vector<double>& ion_front = fields[IonizationFront].data; // Ionization front
double metallicity = -1.;                    // Use a constant metallicity
double dust_to_metal = -1.;                  // Use a constant dust-to-metal ratio
double dust_to_gas = -1.;                    // Use a constant dust-to-gas ratio
double dust_boost = 0.;                      // Optional dust boost factor
double constant_temperature = -1.;           // Constant temperature: T [K]
double T_floor = -1.;                        // Apply a temperature floor: T [K]
double neutral_fraction = -1.;               // Constant neutral fraction: x_HI
bool set_density_from_mass = false;          // Calculate the density as mass / volume
bool read_density_as_mass = false;           // Read the density as a mass field
bool read_hydrogen_fraction = false;         // Read mass fraction of hydrogen
bool read_ionized_fraction = false;          // Read x_HII from the input file
bool read_molecular_fraction = false;        // Read x_H2 from the input file
bool read_helium = false;                    // Read x_HeI and x_HeII from the input file
bool read_electron_fraction = false;         // Read x_e from the input file
bool read_photoheating = false;              // Read photoheating rate
bool read_SFR = false;                       // Read star formation rate
bool read_T_dust = false;                    // Read dust temperature
bool read_v2 = false;                        // Read velocity^2
bool read_v2_LOS = false;                    // Read velocity_LOS^2
bool read_v_LOS = false;                     // Read velocity_LOS
bool read_v_x = false;                       // Read velocity_X
bool read_v_y = false;                       // Read velocity_Y
bool read_B = false;                         // Read magnetic field
bool read_B2 = false;                        // Read magnetic field^2
bool read_B_LOS = false;                     // Read magnetic field LOS
bool read_B_x = false;                       // Read magnetic field X
bool read_B_y = false;                       // Read magnetic field Y
bool read_rho2 = false;                      // Read density^2
bool read_ion_front = false;                 // Read ionization front
bool save_line_emissivity = false;           // Save the line emissivity

// Star data for continuum radiative transfer  (size = n_stars)
vector<Vec3> r_star;                         // Star positions [cm]
vector<double> L_cont_star;                  // Stellar continuum [erg/s/angstrom]
vector<double> m_massive_star;               // Star masses [Msun]
vector<double> m_init_star;                  // Initial star masses [Msun]
vector<double> Z_star;                       // Star metallicities [mass fraction]
vector<double> age_star;                     // Star ages [Gyr]
bool star_based_emission = false;            // Include star-based sources
bool background_emission = false;            // Include background sources
bool read_m_massive_star = false;            // Read star masses
bool read_m_init_star = false;               // Read initial star masses
bool read_Z_star = false;                    // Read star metallicities
bool read_age_star = false;                  // Read star ages

// Information about escape regions
bool spherical_escape = false;               // Photons escape from a sphere
Vec3 escape_center = {0.,0.,0.};             // Center of the escape region [cm]
double escape_radius = 0.;                   // Radius for spherical escape [cm]
double escape_radius_bbox = 0.;              // Radius for spherical escape [min distance to bbox edge]
double escape_epsilon;                       // Escape uncertainty [cm]
double escape_radius2;                       // Escape radius^2 [cm^2]
double emission_radius = 0.;                 // Radius for spherical emission [cm]
double emission_radius_bbox = 0.;            // Radius for spherical emission [min distance to bbox edge]
double emission_radius2;                     // Emission radius^2 [cm^2]

// Information about the cameras
bool have_cameras = false;                   // Camera-based output
int n_cameras = 0;                           // Number of cameras
int n_bins = 100;                            // Number of frequency bins
int n_pixels = 100;                          // Number of x,y image pixels
int n_exp = -1;                              // Healpix exponent for cameras
int n_rot = 0;                               // Number of rotation directions
// Vec3 rotation_axis = {0.,0.,1.};             // Rotation axis, e.g. (0,0,1)
int n_exp_atau = 0;                          // Healpix exponent for a*tau0
double freq_min = -1000.;                    // Generic frequency extrema [freq units]
double freq_max = 1000.;                     // Calculated when freq is initialized
double freq_bin_width = -1.;                 // Frequency bin width [freq units]
double observed_bin_width;                   // Observed wavelength resolution [angstrom]
double inverse_freq_bin_width;               // Frequency bin width^-1 [freq units]
double rgb_freq_min = -1000.;                // RGB frequency lower limit [freq units]
double rgb_freq_max = 1000.;                 // RGB frequency upper limit [freq units]
double inverse_rgb_freq_bin_width;           // RGB frequency bin width^-1 [freq units]
double Dv_cont_min = -2000.;                 // Continuum velocity offset lower limit [km/s]
double Dv_cont_max = 2000.;                  // Continuum velocity offset upper limit [km/s]
double Dv_cont_res;                          // Continuum velocity offset resolution [km/s]
double l_cont_res;                           // Continuum wavelength resolution [angstrom]
double R_cont_res;                           // Spectral resolution: R = lambda / Delta_lambda
vector<Vec3> camera_directions;              // Normalized camera directions
vector<Vec3> camera_xaxes;                   // Camera "x-axis" direction vectors (x,y,z)
vector<Vec3> camera_yaxes;                   // Camera "y-axis" direction vectors (x,y,z)
Vec3 camera_center = {0.,0.,0.};             // Camera target position [cm]
vector<double> image_edges;                  // Image relative edge positions [cm]
double image_width = 0.;                     // Image width [cm] (defaults to entire domain)
double image_width_cMpc = 0.;                // Image width [cMpc]
double image_radius = -1.;                   // Image radius [cm] (image_width / 2)
double image_radius_bbox = 0.;               // Image radius [min distance to bbox edge]
double pixel_width = 0.;                     // Pixel width [cm]
double pixel_area;                           // Pixel area [cm^2]
double inverse_pixel_width;                  // Inverse pixel width [1/cm]
double pixel_arcsec = 0.;                    // Pixel width [arcsec] (optional)
double pixel_arcsec2;                        // Pixel area [arcsec^2] (optional)
double proj_depth = 0.;                      // Projection depth [cm] (defaults to image_width)
double proj_depth_cMpc = 0.;                 // Projection depth [cMpc]
double proj_radius = -1.;                    // Projection radius [cm] (proj_depth / 2)
double proj_radius_bbox = 0.;                // Projection radius [min distance to bbox edge]
bool adaptive = false;                       // Adaptive convergence projections
double pixel_rtol = 0.01;                    // Relative tolerance per pixel
bool perspective = false;                    // Perspective camera projections
bool output_escape_fractions = true;         // Output camera escape fractions
vector<double> f_escs;                       // Escape fractions [fraction]
bool output_freq_avgs = false;               // Output frequency averages
vector<double> freq_avgs;                    // Frequency averages [freq units]
bool output_freq_stds = false;               // Output frequency standard deviations
vector<double> freq_stds;                    // Standard deviations [freq units]
bool output_freq_skews = false;              // Output frequency skewnesses
vector<double> freq_skews;                   // Frequency skewnesses [normalized]
bool output_freq_kurts = false;              // Output frequency kurtoses
vector<double> freq_kurts;                   // Frequency kurtoses [normalized]
bool output_fluxes = true;                   // Output spectral fluxes
vector<vector<double>> fluxes;               // Spectral fluxes [erg/s/cm^2/angstrom]
bool output_images = true;                   // Output surface brightness images
vector<Image<double>> images;                // Surface brightness images [erg/s/cm^2/arcsec^2]
bool output_images2 = false;                 // Output statistical moment images
vector<Image<double>> images2;               // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
bool output_freq_images = false;             // Output average frequency images
vector<Image<double>> freq_images;           // Average frequency images [freq units]
bool output_freq2_images = false;            // Output frequency^2 images
vector<Image<double>> freq2_images;          // Frequency^2 images [freq^2 units]
bool output_freq3_images = false;            // Output frequency^3 images
vector<Image<double>> freq3_images;          // Frequency^3 images [freq^3 units]
bool output_freq4_images = false;            // Output frequency^4 images
vector<Image<double>> freq4_images;          // Frequency^4 images [freq^4 units]
bool output_rgb_images = false;              // Output flux-weighted frequency RGB images
vector<Image<Vec3>> rgb_images;              // Flux-weighted frequency RGB images
bool output_cubes = false;                   // Output spectral data cubes
vector<Cube<double>> cubes;                  // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

// Tabulated ionizing spectral energy distributions
IonAgeTables ion_age;                        // Tabulated ionizing SEDs
IonMetallicityAgeTables ion_Z_age;           // Tabulated ionizing SEDs
double albedo_HI = 0.;                       // Dust scattering albedos
double albedo_HeI = 0.;
double albedo_HeII = 0.;
double cosine_HI = 0.;                       // Dust scattering anisotropy parameters
double cosine_HeI = 0.;
double cosine_HeII = 0.;
double kappa_HI = 1e5;                       // Dust opacities [cm^2/g dust]
double kappa_HeI = 1e5;
double kappa_HeII = 1e5;
