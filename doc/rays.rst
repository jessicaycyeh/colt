.. _rays:

.. raw:: html

   <style> .dv {color:#839192; font-weight:bold; font-style: italic} </style>

.. role:: dv

Ray Data Extractions
====================

This page explains the options for the extraction of ray-like data from simulations.

.. _Ray General Parameters:

General Ray Parameters
----------------------

The following parameters are important ray extraction parameters:

Coming soon.

.. * ``n_photons`` -- Actual number of photon packets used.
..   :dv:`[Default value: 1]`
