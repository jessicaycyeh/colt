.. _ionization:

.. raw:: html

   <style> .dv {color:#839192; font-weight:bold; font-style: italic} </style>

.. role:: dv

MCRT Ionization Equilibrium
===========================

This page explains the options for ionization equilibrium Monte Carlo radiative transfer (MCRT) simulations.

.. _Ionization General Parameters:

General Parameters
------------------

The following parameters are important module parameters:

Coming soon.

.. * ``n_photons`` -- Actual number of photon packets used.
..   :dv:`[Default value: 1]`
