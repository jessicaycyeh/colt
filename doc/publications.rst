.. _publications:

Publications
============

This page maintains a current list of publications using various aspects of COLT.

Original Methods Paper:
-----------------------

 * Smith, A., Safranek-Shrader, C., Bromm, V., Milosavljević, M., 2015, `MNRAS, 449, 4336 <https://doi.org/10.1093/mnras/stv565>`_: 'The Lyman α signature of the first galaxies'

Science Papers:
---------------

 * Kimock, B., Narayanan, D., Smith, A., Ma, X., Feldmann, R., Anglés-Alcázar, D., Bromm, V., Dave, R., Geach, J., Hopkins, P., Kereš, D., 2020, *ApJ*, in press: 'The Origin and Evolution of Lyman-α Blobs in Cosmological Galaxy Formation Simulations' (`arXiv:2004.08397 <https://arxiv.org/abs/2004.08397>`_)

 * Lao, B.-X., Smith, A., 2020, `MNRAS, 497, 3925 <https://doi.org/10.1093/mnras/staa2198>`_: 'Resonant-line radiative transfer within power-law density profiles'

 * Yang, Y.-L., Evans, N. J., Smith, A., Lee, J.-E., Tobin, J. J., Terebey, S., Calcutt, H., Jørgensen, J. K., Green, J. D., Bourke, T. L., 2020, `*ApJ*, 891, 61 <https://doi.org/10.3847/1538-4357/ab7201>`_: 'Constraining the Infalling Envelope Models of Embedded Protostars: BHR 71 and Its Hot Corino'

 * Smith, A., Ma, X., Bromm, V., Finkelstein, S. L., Hopkins, P. F., Faucher-Giguère, C.-A., Kereš, D., 2019, `MNRAS, 484, 39 <https://doi.org/10.1093/mnras/sty3483>`_: 'The physics of Lyman α escape from high-redshift galaxies'

 * Smith, A., Tsang, B. T.-H., Bromm, V., Milosavljević, M., 2018, `MNRAS, 479, 2065 <https://doi.org/10.1093/mnras/sty1509>`_: 'Discrete diffusion Lyman α radiative transfer'

 * Smith, A., Becerra, F., Bromm, V., Hernquist, L., 2017, `MNRAS, 472, 205 <https://doi.org/10.1093/mnras/stx1993>`_: 'Radiative effects during the assembly of direct collapse black holes'

 * Smith, A., Bromm, V., Loeb, A., 2017, `MNRAS, 464, 2963 <https://doi.org/10.1093/mnras/stw2591>`_: 'Lyman α radiation hydrodynamics of galactic winds before cosmic reionization'

 * Smith, A., Bromm, V., Loeb, A., 2016, `MNRAS, 460, 3143 <https://doi.org/10.1093/mnras/stw1129>`_: 'Evidence for a direct collapse black hole in the Lyman α source CR7'
