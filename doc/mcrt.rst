.. _mcrt:

.. raw:: html

   <style> .dv {color:#839192; font-weight:bold; font-style: italic} </style>

.. role:: dv

Monte Carlo Radiative Transfer
==============================

This page explains the options for Monte Carlo radiative transfer (MCRT) simulations.

.. _MCRT General Parameters:

General MCRT Parameters
-----------------------

The following parameters are important MCRT parameters:

* ``n_photons`` -- Actual number of photon packets used.
  :dv:`[Default value: 1]`

* ``output_photons`` -- Flag to output escaped and absorbed photon packets.
  :dv:`[Default value: true]`

* ``photon_file`` -- Output a separate photon file with this base name, e.g. "photons".
  :dv:`[Optional]`

.. _MCRT Line Parameters:

Line Parameters
---------------

The following parameters are related to line specifications:

* ``line`` -- Name of the line to be modeled.
  :dv:`[Default value: Lyman-alpha | Other options: Balmer-alpha, Balmer-beta]`

* ``dust_model`` -- Dust model for the line radiative transfer.
  :dv:`[Options: SMC, MW, LAURSEN_SMC]`

* ``kappa_dust`` -- Dust opacity at the wavelength of the line.
  :dv:`[Default value: Based on dust_model and line | Units: cm^2/g of dust]`

* ``sigma_dust`` -- Dust cross section at the wavelength of the line.
  :dv:`[Default value: Based on dust_model and line | Units: cm^2/Z/hydrogen atom]`

* ``albedo`` -- Dust scattering albedo defined as :math:`A = k_\text{scat} / (k_\text{abs} + k_\text{scat})`.
  :dv:`[Default value: Based on dust_model and line | Valid range: 0 < albedo < 1]`

* ``g_dust`` -- Anisotropy parameter :math:`\langle \mu \rangle` for dust scattering.
  :dv:`[Default value: Based on dust_model and line | Valid range: -1 < g_dust < 1]`

* ``f_ion`` -- Survival fraction of dust in :math:`{\rm H\,{\small II}}` regions
  :dv:`[Default value: 1 | Valid range: 0 < f_ion < 1]`

* ``T_sputter`` -- Thermal sputtering temperature cutoff, such that no dust exists within gas hotter than this value.
  :dv:`[Default value: 1e6 | Units: K]`

.. _MCRT Resonance Line Parameters:

Resonance Line Parameters
-------------------------

The following parameters are related to resonance lines:

* ``recoil`` -- Include recoil induced Doppler shifting with line scattering.
  :dv:`[Default value: true]`

* ``x_crit`` -- Constant critical frequency for core-skipping.
  :dv:`[Default value: 0 | Valid range: x_crit > 0]`

* ``dynamical_core_skipping`` -- Non-local core-skipping for resonance lines.
  :dv:`[Default value: false]`

* ``n_exp_atau`` -- Healpix exponent for the non-local :math:`a \tau_0` core-skipping estimates, which results in :math:`12 \times 4^{n_\text{exp,atau}}` directions.
  :dv:`[Default value: 0 | Valid range: n_exp_atau >= 0]`

.. _MCRT Exit Parameters:

Exit Parameters
---------------

The following parameters are related to exit options:

* ``doppler_frequency`` -- Output frequency in units of Doppler widths, otherwise as a velocity offset from line center (in units of km/s).
  :dv:`[Default value: false]`

* ``T_exit`` -- Exit temperature for standardized Doppler frequency.
  :dv:`[Default value: 1e4 | Units: K]`

* ``v_exit`` -- Exit velocity for shifted observer frames.
  :dv:`[Default value: (0,0,0) | Units: cm/s]`

* ``exit_wrt_com`` -- Exit with respect to the center of mass velocity frame.
  :dv:`[Default value: false]`

.. note:: In what follows the units of "freq" refer to km/s unless using doppler_frequency.

.. _MCRT Radiation Source Parameters:

Radiation Source Parameters
---------------------------

The following parameters are related to line radiative transfer options:

* ``recombinations`` -- Include recombination emission with luminosity :math:`\mathcal{L}_\text{rec} = h \nu_0 \int \alpha_\text{B,eff}(T) n_e n_\text{HII} \text{d}V`.
  :dv:`[Default value: false]`

* ``collisions`` -- Include collisional excitation emission with luminosity for Lyα of :math:`\mathcal{L}_\text{col} = h \nu_0 \int q_{1s2p}(T) n_e n_\text{HI} \text{d}V`. Note: This flag also works for Hα (but not Hβ), which can also have some collisional excitation emission at the few percent level.
  :dv:`[Default value: false]`

* ``continuum`` -- Include stellar continuum emission around the line.
  :dv:`[Default value: false]`

* ``continuum_range`` -- Continuum velocity offset range for frequency sampling.
  :dv:`[Default value: (-2000,2000) | Units: km/s]`

* ``j_exp`` -- Luminosity boosting exponent for power-law emission biasing, i.e. probabilities are rescaled according to :math:`\propto \mathcal{L}_\text{cell}^{j_\text{exp}}` such that :math:`j_\text{exp} < 1` can better sample lower emissivity regions.
  :dv:`[Default value: 1 | Valid range: 0 < j_exp < 1]`

* ``emission_n_min`` -- Minimum number density of emitting cells.
  :dv:`[Default value: 0 | Units: cm^-3]`

* ``emission_n_max`` -- Maximum number density of emitting cells.
  :dv:`[Default value: infinity | Units: cm^-3]`

* ``point`` -- Point source position in `[0 0 0]` format.
  :dv:`[Optional | Units: cm]`

* ``x_point`` -- Point source :math:`x` position.
  :dv:`[Optional | Units: cm]`

* ``y_point`` -- Point source :math:`y` position.
  :dv:`[Optional | Units: cm]`

* ``z_point`` -- Point source :math:`z` position.
  :dv:`[Optional | Units: cm]`

* ``L_point`` -- Point source luminosity.
  :dv:`[Default value: 1 | Units: erg/s]`

* ``r_shell`` -- Shell source radius.
  :dv:`[Optional | Units: cm]`

* ``L_shell`` -- Shell source luminosity.
  :dv:`[Default value: 1 | Units: erg/s]`

.. _MCRT Camera Parameters:

Camera Parameters
-----------------

The following parameters are related to cameras options:

* ``n_bins`` -- Number of frequency bins.
  :dv:`[Default value: 100]`

* ``freq_range`` -- Frequency extrema in `[-1000 1000]` format.
  :dv:`[Default value: (-1000,1000) | Units: freq]`

* ``freq_min`` -- Frequency range minimum.
  :dv:`[Default value: -1000 | Units: freq]`

* ``freq_max`` -- Frequency range maximum.
  :dv:`[Default value: 1000 | Units: freq]`

* ``output_fluxes`` -- Output spectral fluxes.
  :dv:`[Default value: true | Dimensions: (n_cameras, n_bins) | Output units: erg/s/cm^2/angstrom]`

* ``output_images`` -- Output surface brightness images.
  :dv:`[Default value: true | Dimensions: (n_cameras, n_pixels, n_pixels) | Output units: erg/s/cm^2/arcsec^2]`

* ``output_cubes`` -- Output spectral data cubes.
  :dv:`[Default value: false | Dimensions: (n_cameras, n_pixels, n_pixels, n_bins) | Output units: erg/s/cm^2/arcsec^2/angstrom]`

* ``output_images2`` -- Output statistical moment images, e.g. to estimate convergence statistics as the relative error is approximately :math:`\sqrt{\text{images2}} / \text{images}`.
  :dv:`[Default value: false | Dimensions: (n_cameras, n_pixels, n_pixels) | Output units: erg^2/s^2/cm^4/arcsec^4]`

The following parameters produce intrinsic or attenuation only images:

* ``output_mcrt_emission`` -- Output intrinsic emission without transport based on MCRT sampling.
  :dv:`[Default value: false | Dimensions: (n_cameras, n_pixels, n_pixels) | Output units: erg/s/cm^2/arcsec^2]`

* ``output_mcrt_attenuation`` -- Output attenuated emission without scattering based on MCRT sampling.
  :dv:`[Default value: false | Dimensions: (n_cameras, n_pixels, n_pixels) | Output units: erg/s/cm^2/arcsec^2]`

* ``output_proj_emission`` -- Output intrinsic emission without transport with ray-based imaging.
  :dv:`[Default value: false | Dimensions: (n_cameras, n_pixels, n_pixels) | Output units: erg/s/cm^2/arcsec^2]`

* ``output_proj_attenuation`` -- Output attenuated emission without scattering with ray-based imaging.
  :dv:`[Default value: false | Dimensions: (n_cameras, n_pixels, n_pixels) | Output units: erg/s/cm^2/arcsec^2]`

.. note:: The intrinsic and attenuation only images do not require scattering. Therefore, the projection outputs are preferred as they employ a ray-based quadtree imaging method for a noiseless solution with adaptive spatial convergence.

The following parameters produce spatially integrated camera frequency moments :dv:`[Dimensions: n_cameras]`:

* ``output_escape_fractions`` -- Output camera escape fractions.
  :dv:`[Default value: true]`

* ``output_freq_avgs`` -- Output camera frequency averages.
  :dv:`[Default value: false | Output units: freq]`

* ``output_freq_stds`` -- Output frequency standard deviations.
  :dv:`[Default value: false | Output units: freq]`

* ``output_freq_skews`` -- Output frequency skewnesses.
  :dv:`[Default value: false]`

* ``output_freq_kurts`` -- Output frequency kurtoses.
  :dv:`[Default value: false]`

The following parameters produce camera frequency moments for each image pixel  :dv:`[Dimensions: (n_cameras, n_pixels, n_pixels)]`:

* ``output_freq_images`` -- Output average frequency images.
  :dv:`[Default value: false | Output units: freq]`

* ``output_freq2_images`` -- Output frequency^2 images.
  :dv:`[Default value: false | Output units: freq^2]`

* ``output_freq3_images`` -- Output frequency^3 images.
  :dv:`[Default value: false | Output units: freq^3]`

* ``output_freq4_images`` -- Output frequency^4 images.
  :dv:`[Default value: false | Output units: freq^4]`

The following parameters are useful for on-the-fly false color imaging:

* ``output_rgb_images`` -- Output flux-weighted frequency RGB images. Note: The normalized image log intensity can be used as the alpha opacity of the image to give a nice illustration of the spectral data cube.
  :dv:`[Default value: false | Dimensions: (n_cameras, n_pixels, n_pixels, 3) | Output units: RGB color]`

* ``rgb_freq_range`` -- Frequency extrema in `[-1000 1000]` format.
  :dv:`[Default value: (-1000,1000) | Units: freq]`

* ``rgb_freq_min`` -- Frequency range minimum.
  :dv:`[Default value: -1000 | Units: freq]`

* ``rgb_freq_max`` -- Frequency range maximum.
  :dv:`[Default value: 1000 | Units: freq]`

* ``adjust_camera_frequency`` -- Adjust frequencies for each camera based on offsets from a file. Note: This was implemented to center the RGB and other cameras for Lyα on the Hα line center. Thus, the file must have a dataset called "freq_avgs" of length n_cameras.
  :dv:`[Default value: false]`

* ``freq_offset_file`` -- Frequency offset file (optional).
  :dv:`[Default value:` ``init_file``:dv:`]`

* ``freq_offset_dir`` -- Frequency offset directory (optional).
  :dv:`[Default value:` ``init_dir``:dv:`]`

* ``freq_offset_base`` -- Frequency offset file base (optional).
  :dv:`[Default value:` ``init_base``:dv:`]`

* ``freq_offset_ext`` -- Frequency offset file extension (optional).
  :dv:`[Default value: hdf5]`

.. _MCRT Advanced Parameters:

Advanced Parameters
-------------------

The following parameters are related to advanced options:

* ``two_level_atom`` -- Line radiative transfer incorporates occupation number densities from both lower and upper transitions. If this is false then we assume that bound electrons are always in the ground state. Note: This currently also switches from a Voigt to a Gaussian line profile, and assumes complete redistribution for scattering.
  :dv:`[Default value: false]`

* ``line_scattering`` -- Turn line scattering on/off. Note: It is not recommended to override this option unless you know exactly what you are doing.
  :dv:`[Default value: Based on line]`

* ``load_balancing`` -- Use MPI load balancing algorithms. Note: Due to the OpenMP load balancing this is not recommended in general as the number of MPI tasks is usually much smaller than the number of photon packets.
  :dv:`[Default value: false]`
