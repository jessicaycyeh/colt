.. _simulations:

.. raw:: html

   <style> .dv {color:#839192; font-weight:bold; font-style: italic} </style>

.. role:: dv

Simulation Parameters
=====================

This page explains the options for configuring COLT simulations.

.. _Configuration:

Configuration Files
-------------------

COLT uses the **yaml-cpp** library to allow for flexible and convenient configuration of run-time parameters. We introduce the ``config.yml`` file with the following example:

.. code-block:: yaml

    # COLT config file
    --- !mcrt                     # Monte Carlo radiative transfer module

    init_dir: ics                 # Initial conditions directory (default: "ics")
    init_base: colt               # Initial conditions base name (optional)
    output_dir: output            # Output directory name (default: "output")
    output_base: Lya              # Output file base name (default: "colt")

    cameras:                      # Add camera directions manually
      - [1,0,0]
      - [0,1,0]
      - [0,0,1]

    ... Additional run-time parameters

The format is essentially a human readable python dictionary of ``key: value`` pairs that allows #-style comments and complex specifications such as the above list of camera directions.

The YAML header ``--- !mcrt`` specifies which module to run, a convention that sets the module apart from more typical run-time parameters. The value is interpreted in the main file prior to instantiating the simulation to allow high level class polymorphism. By default the MCRT module is assumed in cases where the header line is not included.

.. note:: COLT configuration is design to be minimally verbose, transparent, and catch common unintended errors. In practice this means that only expected parameters are allowed and an error is raised for unexpected or repeated parameters. If a parameter is not specified then the default value will be assigned. Note that the default may depend on other parameters but this is determined by the code logic, i.e. the order of parameters does not matter. Only a few parameters are actually required, such as the initial conditions file naming scheme. However, to ensure transparency a complete config file with all expected options is written out as ``config.yml-full`` for future reference.

.. _General Parameters:

General Parameters
------------------

The following parameters are shared by most modules:

* ``verbose`` -- Verbose output for tests and debugging.
  :dv:`[Default value: false]`

* ``init_file`` -- Initial conditions file name.
  :dv:`[Required if` ``init_base`` :dv:`is not specified]`

* ``init_dir`` -- Initial conditions directory.
  :dv:`[Optional]`

* ``init_base`` -- Initial conditions file base.
  :dv:`[Optional]`

* ``init_ext`` -- Initial conditions file extension.
  :dv:`[Default value: hdf5]`

* ``output_dir`` -- Output directory name.
  :dv:`[Default value: output]`

* ``output_base`` -- Output file base name.
  :dv:`[Default value: colt]`

* ``output_ext`` -- Output file extension.
  :dv:`[Default value: hdf5]`

* ``snap_padding`` -- Leading zeros for snapshot number formatting.
  :dv:`[Default value: 3]`

.. note:: In practice, an initial conditions file may be specified by the full name via ``init_file`` or the file pattern ``<init_dir>/<init_base>_<snapshot>.<init_ext>``, which is then mirrored in the convention for output files, e.g. ``output/colt_000.hdf5``.

.. _Cosmological Parameters:

Cosmological Parameters
-----------------------

The following parameters are related to the cosmology:

* ``cosmological`` -- Flag for cosmological simulations. Note: This is for cosmological vs. intrinsic observers, i.e. it changes the output units but not the physics.
  :dv:`[Default value: false]`

* ``Omega0`` -- Matter density in units of the current critical density :math:`\rho_\text{crit,0}`.
  :dv:`[Default value: 0.3111]`

* ``OmegaB`` -- Baryon density in units of the current critical density :math:`\rho_\text{crit,0}`.
  :dv:`[Default value: 0.04897]`

* ``h100`` -- Hubble constant in units of 100 km/s/Mpc.
  :dv:`[Default value: 0.6766]`

.. note:: Cosmological parameters are needed for calculating observed distances and other module specific quantities. The values are from the Planck mission (`2018 results <https://ui.adsabs.harvard.edu/abs/2020A%26A...641A...6P/abstract>`_). For consistency, these parameters may also be read from the initial conditions file.

.. _Gas Parameters:

Gas Parameters
--------------

The following parameters are related to the gas environment:

* ``constant_temperature`` -- Enforce a constant temperature.
  :dv:`[Optional | Units: K]`

* ``T_floor`` -- Apply a temperature floor to all gas cells.
  :dv:`[Optional | Units: K]`

* ``neutral_fraction`` -- Enforce a constant neutral fraction, :math:`x_\text{HI} = n_\text{HI} / n_\text{H}`.
  :dv:`[Optional]`

* ``metallicity`` -- Enforce a constant metallicity.
  :dv:`[Optional | Units: Mass fraction]`

* ``dust_to_metal`` -- Enforce a constant dust-to-metal ratio.
  :dv:`[Optional | Units: Mass fraction]`

* ``dust_to_gas`` -- Enforce a constant dust-to-gas ratio.
  :dv:`[Optional | Units: Mass fraction]`

* ``dust_boost`` -- Rescale the dust values by this boost factor.
  :dv:`[Optional]`

* ``v_turb`` -- Effective microturbulent velocity.
  :dv:`[Default value: 0 | Units: cm/s]`

* ``v_turb_kms`` -- Effective microturbulent velocity.
  :dv:`[Default value: 0 | Units: km/s]`

* ``T_turb`` -- Effective microturbulent temperature.
  :dv:`[Default value: 0 | Units: K]`

* ``scaled_microturb`` -- Scaled microturbulence model where high density gas is more turbulent. Specifically, :math:`T_\text{turb} = {\rm K\,km}^2 \rho / (\gamma k_\text{B})` with an adiabatic index of :math:`\gamma = 5/3`.
  :dv:`[Default value: false]`

.. note:: The microturbulence parameter follows the standard treatment of adding Gaussian line broadening in quadrature: :math:`b = \sqrt{v_\text{th}^2 + v_\text{turb}^2}`. This acts as an effective temperature floor for line radiative transfer with :math:`v_\text{turb}^2 = 2 k_\text{B} T_\text{turb} / m_\text{carrier}`.

.. _Camera Parameters:

Camera Parameters
-----------------

The following parameters are related to controlling cameras:

* ``n_exp`` -- Healpix exponent for camera directions, which results in :math:`12 \times 4^{n_\text{exp}}` directions in RING ordering.
  :dv:`[Optional | Valid range: n_exp >= 0]`

* ``n_rot`` -- Number of rotation camera directions, which are evenly spaced in angle around a common axis, e.g. to make movies.
  :dv:`[Optional | Valid range: n_rot > 0]`

* ``camera`` -- Add a single camera direction manually in `[1 0 0]` format.
  :dv:`[Optional]`

* ``cameras`` -- Add a list of camera directions manually in `[1 0 0]` format.
  :dv:`[Optional]`

* ``camera_center`` -- Camera target position in `[0 0 0]` format.
  :dv:`[Default value: (0,0,0) | Units: cm]`

* ``image_width`` -- Image width defining a square aperture.
  :dv:`[Units: cm]`

* ``image_radius`` -- Image radius or half of the image width.
  :dv:`[Units: cm]`

* ``image_radius_bbox`` -- Image radius or half of the image width.
  :dv:`[Units: bounding box]`

* ``n_pixels`` -- Number of :math:`(x,y)` image pixels.
  :dv:`[Default value: 100]`

* ``pixel_width`` -- Intrinsic width of each image pixel.
  :dv:`[Units: cm]`

* ``pixel_arcsec`` -- Observed angular width of each image pixel.
  :dv:`[Units: arcseconds]`

* ``pixel_arcsec2`` -- Observed angular area of each image pixel.
  :dv:`[Units: arcseconds^2]`

.. note:: Some combinations of camera parameters are redundant but the configuration detects inconsistencies and gives sensible error messages to address these. If left unspecified, the cameras are set up to capture the entire domain as defined by the simulation bounding box.

.. _Escape Parameters:

Escape Parameters
-----------------

The following parameters are related to controlling the conditions for escape:

* ``spherical_escape`` -- Flag to restrict ray-tracing a spherical region.
  :dv:`[Default value: false]`

* ``escape_center`` -- Center of the escape region in `[0 0 0]` format.
  :dv:`[Default value: (0,0,0) | Units: cm]`

* ``escape_radius`` -- Radius for the spherical escape region.
  :dv:`[Units: cm]`

* ``escape_radius_bbox`` -- Radius for the spherical escape region.
  :dv:`[Units: bounding box]`

* ``emission_radius`` -- Radius for the spherical emission region.
  :dv:`[Units: cm]`

* ``emission_radius_bbox`` -- Radius for the spherical emission region.
  :dv:`[Units: bounding box]`

.. _Geometry Parameters:

Geometry Parameters
-------------------

The following parameters are related to specific geometries:

* ``save_connections`` -- Save the Voronoi connections to a file.
  :dv:`[Default value: true]`

* ``cgal_file`` -- CGAL connectivity file (optional).
  :dv:`[Default value:` ``init_file``:dv:`]`

* ``cgal_dir`` -- CGAL connectivity directory (optional).
  :dv:`[Default value:` ``init_dir``:dv:`]`

* ``cgal_base`` -- CGAL connectivity file base (optional).
  :dv:`[Default value:` ``init_base``:dv:`]`

* ``cgal_ext`` -- CGAL connectivity file extension (optional).
  :dv:`[Default value: hdf5]`

.. note:: The CGAL connectivity is usually saved to the original initial conditions file for future use. Here we allow the option of either not saving the connections or writing them to a different file.

.. _Advanced Parameters:

Advanced Parameters
-------------------

The following parameters are related to advanced options:

* ``read_density_as_mass`` -- Read the density as a mass field. Note: This can be useful for applying density-like analyses methods, e.g. ray-based projections, to point-like data types such as SPH data, star particles, or dark matter.
  :dv:`[Default value: false]`

* ``read_hydrogen_fraction`` -- Read the hydrogen mass fraction :math:`X` from the input file, otherwise we assume the primordial value of :math:`X = 0.76`.
  :dv:`[Default value: false]`

* ``read_ionized_fraction`` -- Read :math:`x_\text{HII}` from the input file in addition to :math:`x_\text{HI}`, otherwise we assume :math:`x_\text{HII} = 1 - x_\text{HI}`.
  :dv:`[Default value: false]`

* ``read_electron_fraction`` -- Read :math:`x_e` from the input file, otherwise we assume :math:`x_e = x_\text{HII}`.
  :dv:`[Default value: false]`
