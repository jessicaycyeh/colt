.. _projections:

.. raw:: html

   <style> .dv {color:#839192; font-weight:bold; font-style: italic} </style>

.. role:: dv

Adaptive Projections
====================

This page explains the options for adaptive convergence volumetric projection simulations.

.. _Projection General Parameters:

General Parameters
------------------

The following parameters are important projection parameters:

Coming soon.

.. * ``n_photons`` -- Actual number of photon packets used.
..   :dv:`[Default value: 1]`
