.. _api:

:mod:`colt`
===========

..
 .. currentmodule:: pydro

..
 .. automodule:: pydro
..
   :members:


.. Usage
.. *****

.. All functionality may be found in the ``pydro`` package::

..     import pydro

.. The information below represents the complete specification of the classes.


.. Cosmology Class
.. ***************

..
 .. autoclass:: pydro.Cosmology
..
   :members:


.. Mesh Class
.. **********

..
 .. autoclass:: pydro.Mesh
..
   :members:


.. Simulation Class
.. ****************

..
 .. autoclass:: pydro.Simulation
..
   :members:

.. doxygenvariable:: km

.. doxygenclass:: Escape
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

