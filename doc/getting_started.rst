.. _getting_started:

Getting Started
===============

This page explains the basic features of COLT from installation to running simulations.

.. _Compilation:

Compilation
-----------

Obtain the COLT source code by cloning the repository: ::

    $ git clone git@bitbucket.org:aaron_smith/colt.git

This creates a new folder named ``colt`` containing the source code (``src``), documentation (``doc``), and compilation files (e.g. ``Makefile``). After installing the required libraries (see :ref:`Dependencies`) and setting up the build environment (see :ref:`Makefile`) the executable may be compiled with: ::

    $ cd colt
    $ make -j

.. _Dependencies:

Dependencies
------------

Note that there are required dependencies:

- **MPI:** Message Passing Interface library for standard code parallelization

- **HDF5:** Hierarchical Data Format library for reading and writing files (with `C++ wrappers <https://portal.hdfgroup.org/pages/viewpage.action?pageId=50073884>`_)

- **yaml-cpp:** Library for parsing YAML config files (`github repository <https://github.com/jbeder/yaml-cpp>`_)

There is also an additional dependency for Voronoi geometry:

- **CGAL:** `Computational Geometry Algorithms Library <https://www.cgal.org>`_ for constructing Delaunay triangulations

.. note:: On MacOS X all of these dependencies can be conveniently installed with the `Homebrew <https://brew.sh>`_ package manager with the following command: ::

    $ brew install openmpi libomp hdf5 yaml-cpp cgal

.. _Makefile:

Makefile
--------

The provided ``Makefile`` allows a flexible build based on the specific computing environment. Currently, COLT is designed to have minimal compile time options summarized in the provided ``template-defines.yml`` file:

.. code-block:: yaml

    MACHINE: gcc       # Makefile build system
    GEOMETRY: voronoi  # Geometry type: slab, spherical, cartesian, octree, voronoi
    HAVE_CGAL: true    # Include the CGAL library (requires voronoi geometry)

These options can either be provided in the ``Makefile`` itself or in a yaml (or bash) file with a default location of ``defines.yml`` (see note below). The main option is the ``MACHINE`` with known systems: `homebrew`, `gcc`, `pleiades`, `comet`, `odyssey`, `stampede`, and `supermuc`. Other systems may require slight modification based on the existing examples in the ``Makefile``. The other main option is the ``GEOMETRY`` with known types: `slab`, `spherical`, `cartesian`, `octree`, `voronoi`. More information is given in the initial conditions specifications.

.. note:: It is usually most convenient to perform a default in-place build, which generates the compiled object files and executable in the same directory. However, to allow the flexibility of out-of-place builds you may customize the ``DEFS``, ``BUILD``, and ``EXE`` paths, e.g. to run multiple geometries with the same source distribution.

    $ make -j -C /source/path DEFS={path}/defines.yml BUILD={path}/build EXE={path}/colt

.. _Running:

Running a Simulation
--------------------

After compilation, the executable ``colt`` can be run by specifying a YAML configuration file, usually called ``config.yml``. In case you are streamlining analysis over several snapshots with the same config file you can also specify the snapshot number. It is also importantant to specify the number of OpenMP threads before execution. A typical command to run COLT with 4 tasks and 16 threads looks like the following: ::

    $ export OMP_NUM_THREADS=16
    $ mpirun -n 4 ./colt config.yml [snapshot]

.. note:: Please consult the system documentation about running hybrid MPI + OpenMP jobs as some environments may require special commands, e.g. a linux cluster might need ``--bind-to none`` to release the core binding. Supercomputers typically supply job scripts and specific advice for hybrid execution within their user guide documentation.
