.. _files:

Input and Output Files
======================

This page explains the general structure and philosophy of initial conditions and simulation output files for COLT.

.. _ICs:

Initial Conditions Files
------------------------

COLT uses the Hierarchical Data Format (HDF5) library for reading and writing files (with `C++ wrappers <https://portal.hdfgroup.org/pages/viewpage.action?pageId=50073884>`_).

More details coming soon.

.. _Supported Geometries:

Supported Geometries
--------------------

 * Plane Parallel Slab
 * Spherical Shells
 * 3D Cartesian grid
 * Adaptive Resolution Octree
 * Unstructured Voronoi Mesh
 * Secondary Outputs

More details coming soon.

.. _Output Files:

Output Files
------------

Coming soon.
