#! /usr/bin/env python
import sys

trues = ['true', 'yes', 'on']

def read_defs():
    """ Read file content to a definitions dictionary. """
    defs = {}
    with open('defines.log') as f:          # Written by Makefile
        for line in f:                      # Read each line
            line = line.strip()             # Remove new line characters
            if line:                        # Check for empty lines
                key, val = line.split('=')  # File format: "key=val"
                defs[key] = val
    return defs

def write_geometry(defs):
    """ Write geometry section. """
    if 'GEOMETRY' not in defs:
        defs['GEOMETRY'] = 'slab'  # Allow future use
    geometry = defs['GEOMETRY']
    geometries = ['slab', 'spherical', 'cartesian', 'octree', 'voronoi']
    if geometry not in geometries:
        raise ValueError(f'GEOMETRY = {geometry} is not a valid option. {geometries}')
    if 'HAVE_CGAL' in defs:
        if geometry != 'voronoi':  # Only allowed for voronoi geometry
            raise ValueError(f'HAVE_CGAL requires GEOMETRY = voronoi. [{geometry}]')
        if defs['HAVE_CGAL'] not in trues:  # Only allow true values
            raise ValueError(f'HAVE_CGAL = {defs["HAVE_CGAL"]} is not a valid option. {trues}')
        defs['HAVE_CGAL'] = True  # Reached the only valid case
    else:
        defs['HAVE_CGAL'] = False  # Default to False
    if defs['HAVE_CGAL']:
        cgal_str = '\n#define HAVE_CGAL\n'
        if 'CGAL_LINKED_WITH_TBB' in defs:
            if defs['CGAL_LINKED_WITH_TBB'] not in trues:  # Only allow true values
                raise ValueError(f'CGAL_LINKED_WITH_TBB = {defs["CGAL_LINKED_WITH_TBB"]} is not a valid option. {trues}')
            defs['CGAL_LINKED_WITH_TBB'] = True  # Save as boolean
            cgal_str += '#define CGAL_LINKED_WITH_TBB\n'
        cgal_str += 'constexpr bool have_cgal = true;\n'
    else:
        cgal_str = 'constexpr bool have_cgal = false;\n'
    return (f'#define {geometry.upper()}\n'
      'enum Geometry { slab, spherical, cartesian, octree, voronoi };\n'
      f'constexpr Geometry geometry = {geometry};\n' + cgal_str)

def write_electron_scattering(defs):
    """ Write electron scattering section. """
    if 'ELECTRON_SCATTERING' in defs:
        if defs['ELECTRON_SCATTERING'] not in trues:  # Only allow true values
            raise ValueError(f'ELECTRON_SCATTERING = {defs["ELECTRON_SCATTERING"]} is not a valid option. {trues}')
        defs['ELECTRON_SCATTERING'] = True  # Reached the only valid case
        return '\n#define ELECTRON_SCATTERING\nconstexpr bool electron_scattering = true;\n'
    else:
        defs['ELECTRON_SCATTERING'] = False  # Default to False
        return '\nconstexpr bool electron_scattering = false;\n'

def write_special_relativity(defs):
    """ Write special relativity section. """
    if 'SPECIAL_RELATIVITY' in defs:
        if defs['SPECIAL_RELATIVITY'] not in trues:  # Only allow true values
            raise ValueError(f'SPECIAL_RELATIVITY = {defs["SPECIAL_RELATIVITY"]} is not a valid option. {trues}')
        defs['SPECIAL_RELATIVITY'] = True  # Reached the only valid case
        return '\n#define SPECIAL_RELATIVITY\nconstexpr bool special_relativity = true;\n'
    else:
        defs['SPECIAL_RELATIVITY'] = False  # Default to False
        return '\nconstexpr bool special_relativity = false;\n'

# def write_module(defs):
#     """ Write module section. """
#     if 'MODULE' not in defs:
#         defs['MODULE'] = 'mcrt'  # Allow future use
#     module = defs['MODULE']
#     modules = ['mcrt', 'projections', 'reionization']
#     if module not in modules:
#         raise ValueError(f'MODULE = {module} is not a valid option. {modules}')
#     return (f'#define {module.upper()}\n'
#       'enum Module { mcrt, projections, reionization };\n'
#       f'constexpr Module module = {module};\n')

def write_defs(filename):
    """ Write the compile time options file. """
    defs = read_defs()                      # Makefile aware definitions
    defs_h = ('#ifndef COMPILE_TIME_INCLUDED\n'
              '#define COMPILE_TIME_INCLUDED\n\n' + write_geometry(defs) +
              write_electron_scattering(defs) + write_special_relativity(defs) + '\n#endif\n')
    with open(filename, "w") as f:
        f.write(defs_h)

if __name__ == "__main__":
    if len(sys.argv) == 1:
        filename = 'src/compile_time.h'
    elif len(sys.argv) == 2:
        filename = sys.argv[1]
    else:
        raise ValueError('Expecting only one argument.')
    write_defs(filename)
